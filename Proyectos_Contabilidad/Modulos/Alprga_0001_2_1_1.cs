using System;
using System.Data.SqlClient;

public class Alprga_0001_2_1_1
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_2_1_1;                                    // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private int _ide_0001_2_1;                                      // 3    
    private short _eje_0001_2_1_1;                                  // 4    
    private string _fec_0001_2_1_1;                                     // 5    
    private byte _tim_0001_2_1_1;                                   // 6    
    private string _con_0001_2_1_1;                                 // 7    
    private decimal _imp_0001_2_1_1;                                // 8    
    private int _uar_0001_2_1_1;                                    // 10    
    private string _far_0001_2_1_1;                                 // 11    
    private string _dip_0001_2_1_1;                                 // 12    
    private int _uua_0001_2_1_1;                                    // 13    
    private string _fua_0001_2_1_1;                                 // 14    

    #endregion

    #region Propiedades

    public int ide_0001_2_1_1
    {
        get { return _ide_0001_2_1_1; }
        set { _ide_0001_2_1_1 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public int ide_0001_2_1
    {
        get { return _ide_0001_2_1; }
        set { _ide_0001_2_1 = value; }
    }
    public short eje_0001_2_1_1
    {
        get { return _eje_0001_2_1_1; }
        set { _eje_0001_2_1_1 = value; }
    }
    public string fec_0001_2_1_1
    {
        get { return _fec_0001_2_1_1; }
        set { _fec_0001_2_1_1 = value; }
    }
    public byte tim_0001_2_1_1
    {
        get { return _tim_0001_2_1_1; }
        set { _tim_0001_2_1_1 = value; }
    }
    public string con_0001_2_1_1
    {
        get { return _con_0001_2_1_1; }
        set { _con_0001_2_1_1 = value; }
    }
    public decimal imp_0001_2_1_1
    {
        get { return _imp_0001_2_1_1; }
        set { _imp_0001_2_1_1 = value; }
    }
    public int uar_0001_2_1_1
    {
        get { return _uar_0001_2_1_1; }
        set { _uar_0001_2_1_1 = value; }
    }
    public string far_0001_2_1_1
    {
        get { return _far_0001_2_1_1; }
        set { _far_0001_2_1_1 = value; }
    }
    public string dip_0001_2_1_1
    {
        get { return _dip_0001_2_1_1; }
        set { _dip_0001_2_1_1 = value; }
    }
    public int uua_0001_2_1_1
    {
        get { return _uua_0001_2_1_1; }
        set { _uua_0001_2_1_1 = value; }
    }
    public string fua_0001_2_1_1
    {
        get { return _fua_0001_2_1_1; }
        set { _fua_0001_2_1_1 = value; }
    }

    #endregion

    #region Constructores

    public Alprga_0001_2_1_1(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_2_1_1 = 0;
        _ide_0001 = 0;
        _ide_0001_2_1 = 0;
        _eje_0001_2_1_1 = 0;
        _fec_0001_2_1_1 = "";
        _tim_0001_2_1_1 = 0;
        _con_0001_2_1_1 = "";
        _imp_0001_2_1_1 = 0;
        _uar_0001_2_1_1 = 0;
        _far_0001_2_1_1 = "";
        _dip_0001_2_1_1 = "";
        _uua_0001_2_1_1 = 0;
        _fua_0001_2_1_1 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = $"INSERT INTO {_BaseDatos}.dbo.alprga_0001_2_1_1 (ide_0001, ide_0001_2_1, eje_0001_2_1_1, fec_0001_2_1_1, tim_0001_2_1_1, con_0001_2_1_1, imp_0001_2_1_1, uar_0001_2_1_1, far_0001_2_1_1, dip_0001_2_1_1, uua_0001_2_1_1, fua_0001_2_1_1)" +
            $" OUTPUT INSERTED.ide_0001_2_1_1 VALUES (@ide_0001, @ide_0001_2_1, @eje_0001_2_1_1, @fec_0001_2_1_1, @tim_0001_2_1_1, @con_0001_2_1_1, @imp_0001_2_1_1, @uar_0001_2_1_1, @far_0001_2_1_1, @dip_0001_2_1_1, @uua_0001_2_1_1, @fua_0001_2_1_1)";

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_0001_2_1", _ide_0001_2_1);
        Command.Parameters.AddWithValue("@eje_0001_2_1_1", _eje_0001_2_1_1);
        Command.Parameters.AddWithValue("@fec_0001_2_1_1", _fec_0001_2_1_1);
        Command.Parameters.AddWithValue("@tim_0001_2_1_1", _tim_0001_2_1_1);
        Command.Parameters.AddWithValue("@con_0001_2_1_1", _con_0001_2_1_1);
        Command.Parameters.AddWithValue("@imp_0001_2_1_1", _imp_0001_2_1_1);
        Command.Parameters.AddWithValue("@uar_0001_2_1_1", _uar_0001_2_1_1);
        Command.Parameters.AddWithValue("@far_0001_2_1_1", _far_0001_2_1_1);
        Command.Parameters.AddWithValue("@dip_0001_2_1_1", _dip_0001_2_1_1);
        Command.Parameters.AddWithValue("@uua_0001_2_1_1", _uua_0001_2_1_1);
        Command.Parameters.AddWithValue("@fua_0001_2_1_1", _fua_0001_2_1_1);

        Command.Connection = _sqlCon;
        _ide_0001_2_1_1 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_0001_2_1_1 WHERE ide_0001_2_1_1 = {_ide_0001_2_1_1}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_2_1_1 = (int)dr["ide_0001_2_1_1"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                ide_0001_2_1 = (int)dr["ide_0001_2_1"];
                eje_0001_2_1_1 = (short)dr["eje_0001_2_1_1"];
                fec_0001_2_1_1 = dr["fec_0001_2_1_1"].ToString();
                tim_0001_2_1_1 = Convert.ToByte(dr["tim_0001_2_1_1"]);
                con_0001_2_1_1 = (string)dr["con_0001_2_1_1"];
                imp_0001_2_1_1 = (decimal)dr["imp_0001_2_1_1"];
                uar_0001_2_1_1 = (int)dr["uar_0001_2_1_1"];
                far_0001_2_1_1 = dr["far_0001_2_1_1"].ToString();
                dip_0001_2_1_1 = (string)dr["dip_0001_2_1_1"];
                uua_0001_2_1_1 = (int)dr["uua_0001_2_1_1"];
                fua_0001_2_1_1 = dr["fua_0001_2_1_1"].ToString();
            }
        }
        else
            ide_0001_2_1_1 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alprga_0001_2_1_1 WHERE ide_0001_2_1_1 = {_ide_0001_2_1_1}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_2_1_1 SET");
        strSQL += string.Concat(" ide_0001 = @ide_0001");
        strSQL += string.Concat(", ide_0001_2_1 = @ide_0001_2_1");
        strSQL += string.Concat(", eje_0001_2_1_1 = @eje_0001_2_1_1");
        strSQL += string.Concat(", fec_0001_2_1_1 = @fec_0001_2_1_1");
        strSQL += string.Concat(", tim_0001_2_1_1 = @tim_0001_2_1_1");
        strSQL += string.Concat(", con_0001_2_1_1 = @con_0001_2_1_1");
        strSQL += string.Concat(", imp_0001_2_1_1 = @imp_0001_2_1_1");
        strSQL += string.Concat(", dip_0001_2_1_1 = @dip_0001_2_1_1");
        strSQL += string.Concat(", uua_0001_2_1_1 = @uua_0001_2_1_1");
        strSQL += string.Concat(", fua_0001_2_1_1 = @fua_0001_2_1_1");
        strSQL += string.Concat(" WHERE ide_0001_2_1_1 = ", _ide_0001_2_1_1);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_0001_2_1", _ide_0001_2_1);
        Command.Parameters.AddWithValue("@eje_0001_2_1_1", _eje_0001_2_1_1);
        Command.Parameters.AddWithValue("@fec_0001_2_1_1", _fec_0001_2_1_1);
        Command.Parameters.AddWithValue("@tim_0001_2_1_1", _tim_0001_2_1_1);
        Command.Parameters.AddWithValue("@con_0001_2_1_1", _con_0001_2_1_1);
        Command.Parameters.AddWithValue("@imp_0001_2_1_1", _imp_0001_2_1_1);
        Command.Parameters.AddWithValue("@dip_0001_2_1_1", _dip_0001_2_1_1);
        Command.Parameters.AddWithValue("@uua_0001_2_1_1", _uua_0001_2_1_1);
        Command.Parameters.AddWithValue("@fua_0001_2_1_1", _fua_0001_2_1_1);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}