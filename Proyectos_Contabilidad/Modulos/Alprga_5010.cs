using System;
using System.Data.SqlClient;

public class Alprga_5010
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_5010;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _cod_5010;                                          // 2   CLAVE_UNICA 
    private int _irt_5010;                                          // 3    
    private string _nid_5010;                                       // 4    
    private string _raz_5010;                                       // 5    
    private int _uar_5010;                                          // 6    
    private string _far_5010;                                       // 7    
    private string _dip_5010;                                       // 8    
    private int _uua_5010;                                          // 9    
    private string _fua_5010;                                       // 10    

    #endregion

    #region Propiedades

    public int ide_5010
    {
        get { return _ide_5010; }
        set { _ide_5010 = value; }
    }
    public int cod_5010
    {
        get { return _cod_5010; }
        set { _cod_5010 = value; }
    }
    public int irt_5010
    {
        get { return _irt_5010; }
        set { _irt_5010 = value; }
    }
    public string nid_5010
    {
        get { return _nid_5010; }
        set { _nid_5010 = value; }
    }
    public string raz_5010
    {
        get { return _raz_5010; }
        set { _raz_5010 = value; }
    }
    public int uar_5010
    {
        get { return _uar_5010; }
        set { _uar_5010 = value; }
    }
    public string far_5010
    {
        get { return _far_5010; }
        set { _far_5010 = value; }
    }
    public string dip_5010
    {
        get { return _dip_5010; }
        set { _dip_5010 = value; }
    }
    public int uua_5010
    {
        get { return _uua_5010; }
        set { _uua_5010 = value; }
    }
    public string fua_5010
    {
        get { return _fua_5010; }
        set { _fua_5010 = value; }
    }

    #endregion

    #region Constructores

    public Alprga_5010(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_5010 = 0;
        _cod_5010 = 0;
        _irt_5010 = 0;
        _nid_5010 = "";
        _raz_5010 = "";
        _uar_5010 = 0;
        _far_5010 = "";
        _dip_5010 = "";
        _uua_5010 = 0;
        _fua_5010 = "";
    }

    #endregion

    #region Métodos públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alprga_5010 (  cod_5010, irt_5010, nid_5010, raz_5010, uar_5010, far_5010, dip_5010, uua_5010, fua_5010)" +
            $" OUTPUT INSERTED.ide_5010 VALUES (  @cod_5010, @irt_5010, @nid_5010, @raz_5010, @uar_5010, @far_5010, @dip_5010, @uua_5010, @fua_5010)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_5010", _cod_5010);
        Command.Parameters.AddWithValue("@irt_5010", _irt_5010);
        Command.Parameters.AddWithValue("@nid_5010", _nid_5010);
        Command.Parameters.AddWithValue("@raz_5010", _raz_5010);
        Command.Parameters.AddWithValue("@uar_5010", _uar_5010);
        Command.Parameters.AddWithValue("@far_5010", _far_5010);
        Command.Parameters.AddWithValue("@dip_5010", _dip_5010);
        Command.Parameters.AddWithValue("@uua_5010", _uua_5010);
        Command.Parameters.AddWithValue("@fua_5010", _fua_5010);

        Command.Connection = _sqlCon;
        _ide_5010 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_5010 WHERE ide_5010 = {_ide_5010}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_5010 = (int)dr["ide_5010"];

            if (AsignarPropiedades)
            {
                cod_5010 = (int)dr["cod_5010"];
                irt_5010 = (int)dr["irt_5010"];
                nid_5010 = (string)dr["nid_5010"];
                raz_5010 = (string)dr["raz_5010"];
                uar_5010 = (int)dr["uar_5010"];
                far_5010 = dr["far_5010"].ToString();
                dip_5010 = (string)dr["dip_5010"];
                uua_5010 = (int)dr["uua_5010"];
                fua_5010 = dr["fua_5010"].ToString();
            }
        }
        else
            ide_5010 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alprga_5010 WHERE ide_5010 = {_ide_5010}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO  ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_5010 SET");
        strSQL += string.Concat(" cod_5010 = @cod_5010");
        strSQL += string.Concat(", irt_5010 = @irt_5010");
        strSQL += string.Concat(", nid_5010 = @nid_5010");
        strSQL += string.Concat(", raz_5010 = @raz_5010");
        strSQL += string.Concat(", dip_5010 = @dip_5010");
        strSQL += string.Concat(", uua_5010 = @uua_5010");
        strSQL += string.Concat(", fua_5010 = @fua_5010");
        strSQL += string.Concat(" WHERE ide_5010 = ", _ide_5010);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_5010", _cod_5010);
        Command.Parameters.AddWithValue("@irt_5010", _irt_5010);
        Command.Parameters.AddWithValue("@nid_5010", _nid_5010);
        Command.Parameters.AddWithValue("@raz_5010", _raz_5010);
        Command.Parameters.AddWithValue("@dip_5010", _dip_5010);
        Command.Parameters.AddWithValue("@uua_5010", _uua_5010);
        Command.Parameters.AddWithValue("@fua_5010", _fua_5010);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE ÚNICA: cod_5010
    public void Cargar_cod_5010(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_5010 WHERE cod_5010 = {_cod_5010}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_5010 = (int)dr["ide_5010"];

            if (AsignarPropiedades)
            {
                cod_5010 = (int)dr["cod_5010"];
                irt_5010 = (int)dr["irt_5010"];
                nid_5010 = (string)dr["nid_5010"];
                raz_5010 = (string)dr["raz_5010"];
                uar_5010 = (int)dr["uar_5010"];
                far_5010 = dr["far_5010"].ToString();
                dip_5010 = (string)dr["dip_5010"];
                uua_5010 = (int)dr["uua_5010"];
                fua_5010 = dr["fua_5010"].ToString();
            }
        }
        else
            ide_5010 = 0;

        dr.Close();
    }

    /// <summary>
    /// Actualiza los campos (No código [cod_5010])
    /// </summary>
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_5010 SET");
        strSQL += string.Concat(" irt_5010 = @irt_5010");
        strSQL += string.Concat(", nid_5010 = @nid_5010");
        strSQL += string.Concat(", raz_5010 = @raz_5010");
        strSQL += string.Concat(", dip_5010 = @dip_5010");
        strSQL += string.Concat(", uua_5010 = @uua_5010");
        strSQL += string.Concat(", fua_5010 = @fua_5010");
        strSQL += string.Concat(" WHERE ide_5010 = ", _ide_5010);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@irt_5010", _irt_5010);
        Command.Parameters.AddWithValue("@nid_5010", _nid_5010);
        Command.Parameters.AddWithValue("@raz_5010", _raz_5010);
        Command.Parameters.AddWithValue("@dip_5010", _dip_5010);
        Command.Parameters.AddWithValue("@uua_5010", _uua_5010);
        Command.Parameters.AddWithValue("@fua_5010", _fua_5010);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza el valor actual de la secuencia 'NUMERADOR DE CÓDIGOS DE AGENTES FINANCIADORES'
    /// </summary>
    /// <param name="Numero_Actual">Número actual de la secuencia</param>
    public void Actualizar_Secuencia(int Numero_Actual)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"ALTER SEQUENCE cod_5010 RESTART WITH {Numero_Actual}"
        };

        Command.ExecuteNonQuery();
    }
    #endregion

}