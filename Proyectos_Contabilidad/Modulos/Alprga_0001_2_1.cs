using System;
using System.Data;
using System.Data.SqlClient;

public class Alprga_0001_2_1
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_2_1;                                      // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private int _ide_0001_2;                                        // 3    
    private short? _eje_0001_2_1;                                   // 4    
    private int? _exp_0001_2_1;                                     // 5    
    private string _org_0001_2_1;                                   // 6    
    private string _eco_0001_2_1;                                   // 7    
    private string _den_0001_2_1;                                   // 8    
    private decimal _pri_0001_2_1;                                  // 10    
    private decimal _com_0001_2_1;                                  // 11    
    private decimal _rec_0001_2_1;                                  // 12    
    private decimal _iia_0001_2_1;                                  // 13    
    private int _uar_0001_2_1;                                      // 15    
    private string _far_0001_2_1;                                   // 16    
    private string _dip_0001_2_1;                                   // 17    
    private int _uua_0001_2_1;                                      // 18    
    private string _fua_0001_2_1;                                   // 19    

    #endregion

    #region Propiedades

    public int ide_0001_2_1
    {
        get { return _ide_0001_2_1; }
        set { _ide_0001_2_1 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public int ide_0001_2
    {
        get { return _ide_0001_2; }
        set { _ide_0001_2 = value; }
    }
    public short? eje_0001_2_1
    {
        get { return _eje_0001_2_1; }
        set { _eje_0001_2_1 = value; }
    }
    public int? exp_0001_2_1
    {
        get { return _exp_0001_2_1; }
        set { _exp_0001_2_1 = value; }
    }
    public string org_0001_2_1
    {
        get { return _org_0001_2_1; }
        set { _org_0001_2_1 = value; }
    }
    public string eco_0001_2_1
    {
        get { return _eco_0001_2_1; }
        set { _eco_0001_2_1 = value; }
    }
    public string den_0001_2_1
    {
        get { return _den_0001_2_1; }
        set { _den_0001_2_1 = value; }
    }
    public decimal pri_0001_2_1
    {
        get { return _pri_0001_2_1; }
        set { _pri_0001_2_1 = value; }
    }
    public decimal com_0001_2_1
    {
        get { return _com_0001_2_1; }
        set { _com_0001_2_1 = value; }
    }
    public decimal rec_0001_2_1
    {
        get { return _rec_0001_2_1; }
        set { _rec_0001_2_1 = value; }
    }
    public decimal iia_0001_2_1
    {
        get { return _iia_0001_2_1; }
        set { _iia_0001_2_1 = value; }
    }
    public int uar_0001_2_1
    {
        get { return _uar_0001_2_1; }
        set { _uar_0001_2_1 = value; }
    }
    public string far_0001_2_1
    {
        get { return _far_0001_2_1; }
        set { _far_0001_2_1 = value; }
    }
    public string dip_0001_2_1
    {
        get { return _dip_0001_2_1; }
        set { _dip_0001_2_1 = value; }
    }
    public int uua_0001_2_1
    {
        get { return _uua_0001_2_1; }
        set { _uua_0001_2_1 = value; }
    }
    public string fua_0001_2_1
    {
        get { return _fua_0001_2_1; }
        set { _fua_0001_2_1 = value; }
    }

    #endregion

    #region Constructores

    public Alprga_0001_2_1(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_2_1 = 0;
        _ide_0001 = 0;
        _ide_0001_2 = 0;
        _eje_0001_2_1 = null;
        _exp_0001_2_1 = null;
        _org_0001_2_1 = "";
        _eco_0001_2_1 = "";
        _den_0001_2_1 = "";
        _pri_0001_2_1 = 0;
        _com_0001_2_1 = 0;
        _rec_0001_2_1 = 0;
        _iia_0001_2_1 = 0;
        _uar_0001_2_1 = 0;
        _far_0001_2_1 = "";
        _dip_0001_2_1 = "";
        _uua_0001_2_1 = 0;
        _fua_0001_2_1 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alprga_0001_2_1 (ide_0001, ide_0001_2, eje_0001_2_1, exp_0001_2_1, org_0001_2_1, eco_0001_2_1, den_0001_2_1, pri_0001_2_1, com_0001_2_1, rec_0001_2_1, iia_0001_2_1, uar_0001_2_1, far_0001_2_1, dip_0001_2_1, uua_0001_2_1, fua_0001_2_1)" +
            $" OUTPUT INSERTED.ide_0001_2_1 VALUES (@ide_0001, @ide_0001_2, @eje_0001_2_1, @exp_0001_2_1, @org_0001_2_1, @eco_0001_2_1, @den_0001_2_1, @pri_0001_2_1, @com_0001_2_1, @rec_0001_2_1, @iia_0001_2_1, @uar_0001_2_1, @far_0001_2_1, @dip_0001_2_1, @uua_0001_2_1, @fua_0001_2_1)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_0001_2", _ide_0001_2);

        if (_eje_0001_2_1 == null)
            Command.Parameters.Add("@eje_0001_2_1", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@eje_0001_2_1", _eje_0001_2_1);

        if (_exp_0001_2_1 == null)
            Command.Parameters.Add("@exp_0001_2_1", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@exp_0001_2_1", _exp_0001_2_1);

        Command.Parameters.AddWithValue("@org_0001_2_1", _org_0001_2_1);
        Command.Parameters.AddWithValue("@eco_0001_2_1", _eco_0001_2_1);
        Command.Parameters.AddWithValue("@den_0001_2_1", _den_0001_2_1);
        Command.Parameters.AddWithValue("@pri_0001_2_1", _pri_0001_2_1);
        Command.Parameters.AddWithValue("@com_0001_2_1", _com_0001_2_1);
        Command.Parameters.AddWithValue("@rec_0001_2_1", _rec_0001_2_1);
        Command.Parameters.AddWithValue("@iia_0001_2_1", _iia_0001_2_1);
        Command.Parameters.AddWithValue("@uar_0001_2_1", _uar_0001_2_1);
        Command.Parameters.AddWithValue("@far_0001_2_1", _far_0001_2_1);
        Command.Parameters.AddWithValue("@dip_0001_2_1", _dip_0001_2_1);
        Command.Parameters.AddWithValue("@uua_0001_2_1", _uua_0001_2_1);
        Command.Parameters.AddWithValue("@fua_0001_2_1", _fua_0001_2_1);

        Command.Connection = _sqlCon;
        _ide_0001_2_1 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {

        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_0001_2_1 WHERE ide_0001_2_1 = {_ide_0001_2_1}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_2_1 = (int)dr["ide_0001_2_1"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                ide_0001_2 = (int)dr["ide_0001_2"];
                eje_0001_2_1 = DBNull.Value.Equals(dr["eje_0001_2_1"]) ? null : (short?)dr["eje_0001_2_1"];
                exp_0001_2_1 = DBNull.Value.Equals(dr["exp_0001_2_1"]) ? null : (int?)dr["exp_0001_2_1"];
                org_0001_2_1 = (string)dr["org_0001_2_1"];
                eco_0001_2_1 = (string)dr["eco_0001_2_1"];
                den_0001_2_1 = (string)dr["den_0001_2_1"];
                pri_0001_2_1 = (decimal)dr["pri_0001_2_1"];
                com_0001_2_1 = (decimal)dr["com_0001_2_1"];
                rec_0001_2_1 = (decimal)dr["rec_0001_2_1"];
                iia_0001_2_1 = (decimal)dr["iia_0001_2_1"];
                uar_0001_2_1 = (int)dr["uar_0001_2_1"];
                far_0001_2_1 = dr["far_0001_2_1"].ToString();
                dip_0001_2_1 = (string)dr["dip_0001_2_1"];
                uua_0001_2_1 = (int)dr["uua_0001_2_1"];
                fua_0001_2_1 = dr["fua_0001_2_1"].ToString();
            }
        }
        else
            ide_0001_2_1 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alprga_0001_2_1 WHERE ide_0001_2_1 = {_ide_0001_2_1}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_2_1 SET");
        strSQL += string.Concat(" ide_0001 = @ide_0001");
        strSQL += string.Concat(", ide_0001_2 = @ide_0001_2");
        strSQL += string.Concat(", eje_0001_2_1 = @eje_0001_2_1");
        strSQL += string.Concat(", exp_0001_2_1 = @exp_0001_2_1");
        strSQL += string.Concat(", org_0001_2_1 = @org_0001_2_1");
        strSQL += string.Concat(", eco_0001_2_1 = @eco_0001_2_1");
        strSQL += string.Concat(", den_0001_2_1 = @den_0001_2_1");
        strSQL += string.Concat(", pri_0001_2_1 = @pri_0001_2_1");
        strSQL += string.Concat(", com_0001_2_1 = @com_0001_2_1");
        strSQL += string.Concat(", rec_0001_2_1 = @rec_0001_2_1");
        strSQL += string.Concat(", iia_0001_2_1 = @iia_0001_2_1");
        strSQL += string.Concat(", dip_0001_2_1 = @dip_0001_2_1");
        strSQL += string.Concat(", uua_0001_2_1 = @uua_0001_2_1");
        strSQL += string.Concat(", fua_0001_2_1 = @fua_0001_2_1");
        strSQL += string.Concat(" WHERE ide_0001_2_1 = ", _ide_0001_2_1);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_0001_2", _ide_0001_2);
        
        if (_eje_0001_2_1 == null)
            Command.Parameters.Add("@eje_0001_2_1", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@eje_0001_2_1", _eje_0001_2_1);

        if (_exp_0001_2_1 == null)
            Command.Parameters.Add("@exp_0001_2_1", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@exp_0001_2_1", _exp_0001_2_1);

        Command.Parameters.AddWithValue("@org_0001_2_1", _org_0001_2_1);
        Command.Parameters.AddWithValue("@eco_0001_2_1", _eco_0001_2_1);
        Command.Parameters.AddWithValue("@den_0001_2_1", _den_0001_2_1);
        Command.Parameters.AddWithValue("@pri_0001_2_1", _pri_0001_2_1);
        Command.Parameters.AddWithValue("@com_0001_2_1", _com_0001_2_1);
        Command.Parameters.AddWithValue("@rec_0001_2_1", _rec_0001_2_1);
        Command.Parameters.AddWithValue("@iia_0001_2_1", _iia_0001_2_1);
        Command.Parameters.AddWithValue("@dip_0001_2_1", _dip_0001_2_1);
        Command.Parameters.AddWithValue("@uua_0001_2_1", _uua_0001_2_1);
        Command.Parameters.AddWithValue("@fua_0001_2_1", _fua_0001_2_1);

        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }


    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_2_1 SET");
        strSQL += string.Concat(" eje_0001_2_1 = @eje_0001_2_1");
        strSQL += string.Concat(", exp_0001_2_1 = @exp_0001_2_1");
        strSQL += string.Concat(", org_0001_2_1 = @org_0001_2_1");
        strSQL += string.Concat(", eco_0001_2_1 = @eco_0001_2_1");
        strSQL += string.Concat(", den_0001_2_1 = @den_0001_2_1");
        strSQL += string.Concat(", pri_0001_2_1 = @pri_0001_2_1");
        strSQL += string.Concat(", com_0001_2_1 = @com_0001_2_1");
        strSQL += string.Concat(", rec_0001_2_1 = @rec_0001_2_1");
        strSQL += string.Concat(", iia_0001_2_1 = @iia_0001_2_1");
        strSQL += string.Concat(", dip_0001_2_1 = @dip_0001_2_1");
        strSQL += string.Concat(", uua_0001_2_1 = @uua_0001_2_1");
        strSQL += string.Concat(", fua_0001_2_1 = @fua_0001_2_1");
        strSQL += string.Concat(" WHERE ide_0001_2_1 = ", _ide_0001_2_1);

        // ASIGNACIÓN DE PARÁMETROS
        if (_eje_0001_2_1 == null)
            Command.Parameters.Add("@eje_0001_2_1", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@eje_0001_2_1", _eje_0001_2_1);

        if (_exp_0001_2_1 == null)
            Command.Parameters.Add("@exp_0001_2_1", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@exp_0001_2_1", _exp_0001_2_1);

        Command.Parameters.AddWithValue("@org_0001_2_1", _org_0001_2_1);
        Command.Parameters.AddWithValue("@eco_0001_2_1", _eco_0001_2_1);
        Command.Parameters.AddWithValue("@den_0001_2_1", _den_0001_2_1);
        Command.Parameters.AddWithValue("@pri_0001_2_1", _pri_0001_2_1);
        Command.Parameters.AddWithValue("@com_0001_2_1", _com_0001_2_1);
        Command.Parameters.AddWithValue("@rec_0001_2_1", _rec_0001_2_1);
        Command.Parameters.AddWithValue("@iia_0001_2_1", _iia_0001_2_1);
        Command.Parameters.AddWithValue("@dip_0001_2_1", _dip_0001_2_1);
        Command.Parameters.AddWithValue("@uua_0001_2_1", _uua_0001_2_1);
        Command.Parameters.AddWithValue("@fua_0001_2_1", _fua_0001_2_1);

        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }


    #endregion

}