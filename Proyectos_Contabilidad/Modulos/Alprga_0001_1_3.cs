using System;
using System.Data;
using System.Data.SqlClient;

public class Alprga_0001_1_3
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private long _ide_0001_1_3;                                    // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private int _ide_0001_1;                                        // 3    
    private int? _ifa_0001_1_3;                                     // 4    
    private int? _ino_0001_1_3;                                     // 5    
    private short _eje_0001_1_3;                                    // 6    
    private int _exp_0001_1_3;                                      // 7    
    private int _doc_0001_1_3;                                      // 8    
    private string _fec_0001_1_3;                                           // 9    
    private string _tmo_0001_1_3;                                   // 13    
    private decimal _imp_0001_1_3;                                  // 14    
    private decimal? _por_0001_1_3;                                 // 15    
    private decimal _iim_0001_1_3;                                  // 16    
    private int _uar_0001_1_3;                                      // 17    
    private string _far_0001_1_3;                                   // 18    
    private string _dip_0001_1_3;                                   // 19    
    private int _uua_0001_1_3;                                      // 20    
    private string _fua_0001_1_3;                                   // 21    

    #endregion

    #region Propiedades

    public long ide_0001_1_3
    {
        get { return _ide_0001_1_3; }
        set { _ide_0001_1_3 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public int ide_0001_1
    {
        get { return _ide_0001_1; }
        set { _ide_0001_1 = value; }
    }
    public int? ifa_0001_1_3
    {
        get { return _ifa_0001_1_3; }
        set { _ifa_0001_1_3 = value; }
    }
    public int? ino_0001_1_3
    {
        get { return _ino_0001_1_3; }
        set { _ino_0001_1_3 = value; }
    }
    public short eje_0001_1_3
    {
        get { return _eje_0001_1_3; }
        set { _eje_0001_1_3 = value; }
    }
    public int exp_0001_1_3
    {
        get { return _exp_0001_1_3; }
        set { _exp_0001_1_3 = value; }
    }
    public int doc_0001_1_3
    {
        get { return _doc_0001_1_3; }
        set { _doc_0001_1_3 = value; }
    }
    public string fec_0001_1_3
    {
        get { return _fec_0001_1_3; }
        set { _fec_0001_1_3 = value; }
    }
    public string tmo_0001_1_3
    {
        get { return _tmo_0001_1_3; }
        set { _tmo_0001_1_3 = value; }
    }
    public decimal imp_0001_1_3
    {
        get { return _imp_0001_1_3; }
        set { _imp_0001_1_3 = value; }
    }
    public decimal? por_0001_1_3
    {
        get { return _por_0001_1_3; }
        set { _por_0001_1_3 = value; }
    }
    public decimal iim_0001_1_3
    {
        get { return _iim_0001_1_3; }
        set { _iim_0001_1_3 = value; }
    }
    public int uar_0001_1_3
    {
        get { return _uar_0001_1_3; }
        set { _uar_0001_1_3 = value; }
    }
    public string far_0001_1_3
    {
        get { return _far_0001_1_3; }
        set { _far_0001_1_3 = value; }
    }
    public string dip_0001_1_3
    {
        get { return _dip_0001_1_3; }
        set { _dip_0001_1_3 = value; }
    }
    public int uua_0001_1_3
    {
        get { return _uua_0001_1_3; }
        set { _uua_0001_1_3 = value; }
    }
    public string fua_0001_1_3
    {
        get { return _fua_0001_1_3; }
        set { _fua_0001_1_3 = value; }
    }

    #endregion

    #region Constructores

    public Alprga_0001_1_3(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_1_3 = 0;
        _ide_0001 = 0;
        _ide_0001_1 = 0;
        _ifa_0001_1_3 = null;
        _ino_0001_1_3 = null;
        _eje_0001_1_3 = 0;
        _exp_0001_1_3 = 0;
        _doc_0001_1_3 = 0;
        _fec_0001_1_3 = "";
        _tmo_0001_1_3 = "";
        _imp_0001_1_3 = 0;
        _por_0001_1_3 = null;
        _iim_0001_1_3 = 0;
        _uar_0001_1_3 = 0;
        _far_0001_1_3 = "";
        _dip_0001_1_3 = "";
        _uua_0001_1_3 = 0;
        _fua_0001_1_3 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alprga_0001_1_3 (ide_0001, ide_0001_1, ifa_0001_1_3, ino_0001_1_3, eje_0001_1_3, exp_0001_1_3, doc_0001_1_3, fec_0001_1_3, tmo_0001_1_3, imp_0001_1_3, por_0001_1_3, iim_0001_1_3, uar_0001_1_3, far_0001_1_3, dip_0001_1_3, uua_0001_1_3, fua_0001_1_3)" +
            $" OUTPUT INSERTED.ide_0001_1_3 VALUES (@ide_0001, @ide_0001_1, @ifa_0001_1_3, @ino_0001_1_3, @eje_0001_1_3, @exp_0001_1_3, @doc_0001_1_3, @fec_0001_1_3, @tmo_0001_1_3, @imp_0001_1_3, @por_0001_1_3, @iim_0001_1_3, @uar_0001_1_3, @far_0001_1_3, @dip_0001_1_3, @uua_0001_1_3, @fua_0001_1_3)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_0001_1", _ide_0001_1);

        if (_ifa_0001_1_3 == null)
            Command.Parameters.Add("@ifa_0001_1_3", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ifa_0001_1_3", _ifa_0001_1_3);

        if (_ino_0001_1_3 == null)
            Command.Parameters.Add("@ino_0001_1_3", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ino_0001_1_3", _ino_0001_1_3);

        Command.Parameters.AddWithValue("@eje_0001_1_3", _eje_0001_1_3);
        Command.Parameters.AddWithValue("@exp_0001_1_3", _exp_0001_1_3);
        Command.Parameters.AddWithValue("@doc_0001_1_3", _doc_0001_1_3);
        Command.Parameters.AddWithValue("@fec_0001_1_3", _fec_0001_1_3);
        Command.Parameters.AddWithValue("@tmo_0001_1_3", _tmo_0001_1_3);
        Command.Parameters.AddWithValue("@imp_0001_1_3", _imp_0001_1_3);

        if (_por_0001_1_3 == null)
            Command.Parameters.Add("@por_0001_1_3", SqlDbType.Decimal).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@por_0001_1_3", _por_0001_1_3);

        Command.Parameters.AddWithValue("@iim_0001_1_3", _iim_0001_1_3);
        Command.Parameters.AddWithValue("@uar_0001_1_3", _uar_0001_1_3);
        Command.Parameters.AddWithValue("@far_0001_1_3", _far_0001_1_3);
        Command.Parameters.AddWithValue("@dip_0001_1_3", _dip_0001_1_3);
        Command.Parameters.AddWithValue("@uua_0001_1_3", _uua_0001_1_3);
        Command.Parameters.AddWithValue("@fua_0001_1_3", _fua_0001_1_3);

        Command.Connection = _sqlCon;
        _ide_0001_1_3 = (long)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_0001_1_3 WHERE ide_0001_1_3 = {_ide_0001_1_3}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_1_3 = (long)dr["ide_0001_1_3"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                ide_0001_1 = (int)dr["ide_0001_1"];
                ifa_0001_1_3 = DBNull.Value.Equals(dr["ifa_0001_1_3"]) ? null : (int?)dr["ifa_0001_1_3"];
                ino_0001_1_3 = DBNull.Value.Equals(dr["ino_0001_1_3"]) ? null : (int?)dr["ino_0001_1_3"];
                eje_0001_1_3 = (short)dr["eje_0001_1_3"];
                exp_0001_1_3 = (int)dr["exp_0001_1_3"];
                doc_0001_1_3 = (int)dr["doc_0001_1_3"];
                fec_0001_1_3 = dr["fec_0001_1_3"].ToString();
                tmo_0001_1_3 = (string)dr["tmo_0001_1_3"];
                imp_0001_1_3 = (decimal)dr["imp_0001_1_3"];
                por_0001_1_3 = DBNull.Value.Equals(dr["por_0001_1_3"]) ? null : (decimal?)dr["por_0001_1_3"];
                iim_0001_1_3 = (decimal)dr["iim_0001_1_3"];
                uar_0001_1_3 = (int)dr["uar_0001_1_3"];
                far_0001_1_3 = dr["far_0001_1_3"].ToString();
                dip_0001_1_3 = (string)dr["dip_0001_1_3"];
                uua_0001_1_3 = (int)dr["uua_0001_1_3"];
                fua_0001_1_3 = dr["fua_0001_1_3"].ToString();
            }
        }
        else
            ide_0001_1_3 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alprga_0001_1_3 WHERE ide_0001_1_3 = {_ide_0001_1_3}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_1_3 SET");
        strSQL += string.Concat(" ide_0001 = @ide_0001");
        strSQL += string.Concat(", ide_0001_1 = @ide_0001_1");
        strSQL += string.Concat(", ifa_0001_1_3 = @ifa_0001_1_3");
        strSQL += string.Concat(", ino_0001_1_3 = @ino_0001_1_3");
        strSQL += string.Concat(", eje_0001_1_3 = @eje_0001_1_3");
        strSQL += string.Concat(", exp_0001_1_3 = @exp_0001_1_3");
        strSQL += string.Concat(", doc_0001_1_3 = @doc_0001_1_3");
        strSQL += string.Concat(", fec_0001_1_3 = @fec_0001_1_3");
        strSQL += string.Concat(", tmo_0001_1_3 = @tmo_0001_1_3");
        strSQL += string.Concat(", imp_0001_1_3 = @imp_0001_1_3");
        strSQL += string.Concat(", por_0001_1_3 = @por_0001_1_3");
        strSQL += string.Concat(", iim_0001_1_3 = @iim_0001_1_3");
        strSQL += string.Concat(", dip_0001_1_3 = @dip_0001_1_3");
        strSQL += string.Concat(", uua_0001_1_3 = @uua_0001_1_3");
        strSQL += string.Concat(", fua_0001_1_3 = @fua_0001_1_3");
        strSQL += string.Concat(" WHERE ide_0001_1_3 = ", _ide_0001_1_3);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_0001_1", _ide_0001_1);

        if (_ifa_0001_1_3 == null)
            Command.Parameters.Add("@ifa_0001_1_3", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ifa_0001_1_3", _ifa_0001_1_3);

        if (_ino_0001_1_3 == null)
            Command.Parameters.Add("@ino_0001_1_3", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ino_0001_1_3", _ino_0001_1_3);

        Command.Parameters.AddWithValue("@eje_0001_1_3", _eje_0001_1_3);
        Command.Parameters.AddWithValue("@exp_0001_1_3", _exp_0001_1_3);
        Command.Parameters.AddWithValue("@doc_0001_1_3", _doc_0001_1_3);
        Command.Parameters.AddWithValue("@fec_0001_1_3", _fec_0001_1_3);
        Command.Parameters.AddWithValue("@tmo_0001_1_3", _tmo_0001_1_3);
        Command.Parameters.AddWithValue("@imp_0001_1_3", _imp_0001_1_3);

        if (_por_0001_1_3 == null)
            Command.Parameters.Add("@por_0001_1_3", SqlDbType.Decimal).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@por_0001_1_3", _por_0001_1_3);

        Command.Parameters.AddWithValue("@iim_0001_1_3", _iim_0001_1_3);
        Command.Parameters.AddWithValue("@dip_0001_1_3", _dip_0001_1_3);
        Command.Parameters.AddWithValue("@uua_0001_1_3", _uua_0001_1_3);
        Command.Parameters.AddWithValue("@fua_0001_1_3", _fua_0001_1_3);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza registro
    /// </summary>
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_1_3 SET");
        strSQL += string.Concat(" ifa_0001_1_3 = @ifa_0001_1_3");
        strSQL += string.Concat(", ino_0001_1_3 = @ino_0001_1_3");
        strSQL += string.Concat(", eje_0001_1_3 = @eje_0001_1_3");
        strSQL += string.Concat(", exp_0001_1_3 = @exp_0001_1_3");
        strSQL += string.Concat(", doc_0001_1_3 = @doc_0001_1_3");
        strSQL += string.Concat(", fec_0001_1_3 = @fec_0001_1_3");
        strSQL += string.Concat(", tmo_0001_1_3 = @tmo_0001_1_3");
        strSQL += string.Concat(", imp_0001_1_3 = @imp_0001_1_3");
        strSQL += string.Concat(", por_0001_1_3 = @por_0001_1_3");
        strSQL += string.Concat(", iim_0001_1_3 = @iim_0001_1_3");
        strSQL += string.Concat(", dip_0001_1_3 = @dip_0001_1_3");
        strSQL += string.Concat(", uua_0001_1_3 = @uua_0001_1_3");
        strSQL += string.Concat(", fua_0001_1_3 = @fua_0001_1_3");
        strSQL += string.Concat(" WHERE ide_0001_1_3 = ", _ide_0001_1_3);

        // ASIGNACIÓN DE PARÁMETROS
        if (_ifa_0001_1_3 == null)
            Command.Parameters.Add("@ifa_0001_1_3", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ifa_0001_1_3", _ifa_0001_1_3);

        if (_ino_0001_1_3 == null)
            Command.Parameters.Add("@ino_0001_1_3", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ino_0001_1_3", _ino_0001_1_3);

        Command.Parameters.AddWithValue("@eje_0001_1_3", _eje_0001_1_3);
        Command.Parameters.AddWithValue("@exp_0001_1_3", _exp_0001_1_3);
        Command.Parameters.AddWithValue("@doc_0001_1_3", _doc_0001_1_3);
        Command.Parameters.AddWithValue("@fec_0001_1_3", _fec_0001_1_3);
        Command.Parameters.AddWithValue("@tmo_0001_1_3", _tmo_0001_1_3);
        Command.Parameters.AddWithValue("@imp_0001_1_3", _imp_0001_1_3);

        if (_por_0001_1_3 == null)
            Command.Parameters.Add("@por_0001_1_3", SqlDbType.Decimal).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@por_0001_1_3", _por_0001_1_3);

        Command.Parameters.AddWithValue("@iim_0001_1_3", _iim_0001_1_3);
        Command.Parameters.AddWithValue("@dip_0001_1_3", _dip_0001_1_3);
        Command.Parameters.AddWithValue("@uua_0001_1_3", _uua_0001_1_3);
        Command.Parameters.AddWithValue("@fua_0001_1_3", _fua_0001_1_3);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}