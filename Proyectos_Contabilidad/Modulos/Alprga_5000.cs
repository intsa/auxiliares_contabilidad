using System;
using System.Data.SqlClient;

public class Alprga_5000
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_5000;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _cod_5000;                                          // 2   CLAVE_UNICA 
    private string _den_5000;                                       // 3    
    private byte _scd_5000;                                         // 4    
    private int _uar_5000;                                          // 5    
    private string _far_5000;                                       // 6    
    private string _dip_5000;                                       // 7    
    private int _uua_5000;                                          // 8    
    private string _fua_5000;                                       // 9    

    #endregion

    #region Propiedades

    public int ide_5000
    {
        get { return _ide_5000; }
        set { _ide_5000 = value; }
    }
    public int cod_5000
    {
        get { return _cod_5000; }
        set { _cod_5000 = value; }
    }
    public string den_5000
    {
        get { return _den_5000; }
        set { _den_5000 = value; }
    }
    public byte scd_5000
    {
        get { return _scd_5000; }
        set { _scd_5000 = value; }
    }
    public int uar_5000
    {
        get { return _uar_5000; }
        set { _uar_5000 = value; }
    }
    public string far_5000
    {
        get { return _far_5000; }
        set { _far_5000 = value; }
    }
    public string dip_5000
    {
        get { return _dip_5000; }
        set { _dip_5000 = value; }
    }
    public int uua_5000
    {
        get { return _uua_5000; }
        set { _uua_5000 = value; }
    }
    public string fua_5000
    {
        get { return _fua_5000; }
        set { _fua_5000 = value; }
    }

    #endregion

    #region Constructores

    public Alprga_5000(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_5000 = 0;
        _cod_5000 = 0;
        _den_5000 = "";
        _scd_5000 = 0;
        _uar_5000 = 0;
        _far_5000 = "";
        _dip_5000 = "";
        _uua_5000 = 0;
        _fua_5000 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.Alprga_5000 (cod_5000, den_5000, scd_5000, uar_5000, far_5000, dip_5000, uua_5000, fua_5000)" +
            $" OUTPUT INSERTED.ide_5000 VALUES (@cod_5000, @den_5000, @scd_5000, @uar_5000, @far_5000, @dip_5000, @uua_5000, @fua_5000)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_5000", _cod_5000);
        Command.Parameters.AddWithValue("@den_5000", _den_5000);
        Command.Parameters.AddWithValue("@scd_5000", _scd_5000);
        Command.Parameters.AddWithValue("@uar_5000", _uar_5000);
        Command.Parameters.AddWithValue("@far_5000", _far_5000);
        Command.Parameters.AddWithValue("@dip_5000", _dip_5000);
        Command.Parameters.AddWithValue("@uua_5000", _uua_5000);
        Command.Parameters.AddWithValue("@fua_5000", _fua_5000);

        Command.Connection = _sqlCon;
        _ide_5000 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.Alprga_5000 WHERE ide_5000 = {_ide_5000}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_5000 = (int)dr["ide_5000"];

            if (AsignarPropiedades)
            {
                cod_5000 = (int)dr["cod_5000"];
                den_5000 = (string)dr["den_5000"];
                scd_5000 = Convert.ToByte(dr["scd_5000"]);
                uar_5000 = (int)dr["uar_5000"];
                far_5000 = dr["far_5000"].ToString();
                dip_5000 = (string)dr["dip_5000"];
                uua_5000 = (int)dr["uua_5000"];
                fua_5000 = dr["fua_5000"].ToString();
            }
        }
        else
            ide_5000 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.Alprga_5000 WHERE ide_5000 = {_ide_5000}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.Alprga_5000 SET");
        strSQL += string.Concat(" cod_5000 = @cod_5000");
        strSQL += string.Concat(", den_5000 = @den_5000");
        strSQL += string.Concat(", scd_5000 = @scd_5000");
        strSQL += string.Concat(", dip_5000 = @dip_5000");
        strSQL += string.Concat(", uua_5000 = @uua_5000");
        strSQL += string.Concat(", fua_5000 = @fua_5000");
        strSQL += string.Concat(" WHERE ide_5000 = ", _ide_5000);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_5000", _cod_5000);
        Command.Parameters.AddWithValue("@den_5000", _den_5000);
        Command.Parameters.AddWithValue("@scd_5000", _scd_5000);
        Command.Parameters.AddWithValue("@dip_5000", _dip_5000);
        Command.Parameters.AddWithValue("@uua_5000", _uua_5000);
        Command.Parameters.AddWithValue("@fua_5000", _fua_5000);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE ÚNICA: cod_5000
    public void Cargar_cod_5000(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.Alprga_5000 WHERE cod_5000 = {_cod_5000}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_5000 = (int)dr["ide_5000"];

            if (AsignarPropiedades)
            {
                cod_5000 = (int)dr["cod_5000"];
                den_5000 = (string)dr["den_5000"];
                scd_5000 = Convert.ToByte(dr["scd_5000"]);
                uar_5000 = (int)dr["uar_5000"];
                far_5000 = dr["far_5000"].ToString();
                dip_5000 = (string)dr["dip_5000"];
                uua_5000 = (int)dr["uua_5000"];
                fua_5000 = dr["fua_5000"].ToString();
            }
        }
        else
            ide_5000 = 0;

        dr.Close();
    }

    /// <summary>
    /// Actualiza los campos (No código [cod_5000])
    /// </summary>
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.Alprga_5000 SET");
        strSQL += string.Concat(" den_5000 = @den_5000");
        strSQL += string.Concat(", scd_5000 = @scd_5000");
        strSQL += string.Concat(", dip_5000 = @dip_5000");
        strSQL += string.Concat(", uua_5000 = @uua_5000");
        strSQL += string.Concat(", fua_5000 = @fua_5000");
        strSQL += string.Concat(" WHERE ide_5000 = ", _ide_5000);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_5000", _den_5000);
        Command.Parameters.AddWithValue("@scd_5000", _scd_5000);
        Command.Parameters.AddWithValue("@dip_5000", _dip_5000);
        Command.Parameters.AddWithValue("@uua_5000", _uua_5000);
        Command.Parameters.AddWithValue("@fua_5000", _fua_5000);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza el valor actual de la secuencia 'NUMERADOR  DE CÓDIGOS DE TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'
    /// </summary>
    /// <param name="Numero_Actual">Número actual de la secuencia</param>
    public void Actualizar_Secuencia(int Numero_Actual)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"USE {_BaseDatos}  ALTER SEQUENCE cod_5000 RESTART WITH {Numero_Actual}"
        };

        Command.ExecuteNonQuery();
    }
    #endregion
}