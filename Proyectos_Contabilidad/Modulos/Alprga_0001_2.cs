using System;
using System.Data.SqlClient;

public class Alprga_0001_2
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_2;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private int _ide_5010;                                          // 3    
    private decimal _cfi_0001_2;                                    // 4    
    private int _uar_0001_2;                                        // 5    
    private string _far_0001_2;                                     // 6    
    private string _dip_0001_2;                                     // 7    
    private int _uua_0001_2;                                        // 8    
    private string _fua_0001_2;                                     // 9

    #endregion

    #region Propiedades

    public int ide_0001_2
    {
        get { return _ide_0001_2; }
        set { _ide_0001_2 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public int ide_5010
    {
        get { return _ide_5010; }
        set { _ide_5010 = value; }
    }
    public decimal cfi_0001_2
    {
        get { return _cfi_0001_2; }
        set { _cfi_0001_2 = value; }
    }
    public int uar_0001_2
    {
        get { return _uar_0001_2; }
        set { _uar_0001_2 = value; }
    }
    public string far_0001_2
    {
        get { return _far_0001_2; }
        set { _far_0001_2 = value; }
    }
    public string dip_0001_2
    {
        get { return _dip_0001_2; }
        set { _dip_0001_2 = value; }
    }
    public int uua_0001_2
    {
        get { return _uua_0001_2; }
        set { _uua_0001_2 = value; }
    }
    public string fua_0001_2
    {
        get { return _fua_0001_2; }
        set { _fua_0001_2 = value; }
    }

    #endregion

    #region Constructores

    public Alprga_0001_2(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_2 = 0;
        _ide_0001 = 0;
        _ide_5010 = 0;
        _cfi_0001_2 = 0;
        _uar_0001_2 = 0;
        _far_0001_2 = "";
        _dip_0001_2 = "";
        _uua_0001_2 = 0;
        _fua_0001_2 = "";
    }

    #endregion

    #region Métodos públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alprga_0001_2 (ide_0001, ide_5010,cfi_0001_2, uar_0001_2, far_0001_2, dip_0001_2, uua_0001_2, fua_0001_2)" +
            $" OUTPUT INSERTED.ide_0001_2 VALUES (@ide_0001, @ide_5010, @cfi_0001_2, @uar_0001_2, @far_0001_2, @dip_0001_2, @uua_0001_2, @fua_0001_2)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_5010", _ide_5010);
        Command.Parameters.AddWithValue("@cfi_0001_2", _cfi_0001_2);
        Command.Parameters.AddWithValue("@uar_0001_2", _uar_0001_2);
        Command.Parameters.AddWithValue("@far_0001_2", _far_0001_2);
        Command.Parameters.AddWithValue("@dip_0001_2", _dip_0001_2);
        Command.Parameters.AddWithValue("@uua_0001_2", _uua_0001_2);
        Command.Parameters.AddWithValue("@fua_0001_2", _fua_0001_2);

        Command.Connection = _sqlCon;
        _ide_0001_2 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_0001_2 WHERE ide_0001_2 = {_ide_0001_2}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_2 = (int)dr["ide_0001_2"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                ide_5010 = (int)dr["ide_5010"];
                cfi_0001_2 = (decimal)dr["cfi_0001_2"];
                uar_0001_2 = (int)dr["uar_0001_2"];
                far_0001_2 = dr["far_0001_2"].ToString();
                dip_0001_2 = (string)dr["dip_0001_2"];
                uua_0001_2 = (int)dr["uua_0001_2"];
                fua_0001_2 = dr["fua_0001_2"].ToString();
            }
        }
        else
            ide_0001_2 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alprga_0001_2 WHERE ide_0001_2 = {_ide_0001_2}"
        };
 
        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_2 SET");
        strSQL += string.Concat(" ide_0001 = @ide_0001");
        strSQL += string.Concat(", ide_5010 = @ide_5010");
        strSQL += string.Concat(", cfi_0001_2 = @cfi_0001_2");
        strSQL += string.Concat(", dip_0001_2 = @dip_0001_2");
        strSQL += string.Concat(", uua_0001_2 = @uua_0001_2");
        strSQL += string.Concat(", fua_0001_2 = @fua_0001_2");
        strSQL += string.Concat(" WHERE ide_0001_2 = ", _ide_0001_2);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_5010", _ide_5010);
        Command.Parameters.AddWithValue("@cfi_0001_2", _cfi_0001_2);
        Command.Parameters.AddWithValue("@dip_0001_2", _dip_0001_2);
        Command.Parameters.AddWithValue("@uua_0001_2", _uua_0001_2);
        Command.Parameters.AddWithValue("@fua_0001_2", _fua_0001_2);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE ÚNICA: ide_0001_ide_5010
    public void Cargar_ide_0001_ide_5010(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_0001_2 WHERE ide_0001 = {_ide_0001} AND ide_5010 = {_ide_5010}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_2 = (int)dr["ide_0001_2"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                ide_5010 = (int)dr["ide_5010"];
                cfi_0001_2 = (decimal)dr["cfi_0001_2"];
                uar_0001_2 = (int)dr["uar_0001_2"];
                far_0001_2 = dr["far_0001_2"].ToString();
                dip_0001_2 = (string)dr["dip_0001_2"];
                uua_0001_2 = (int)dr["uua_0001_2"];
                fua_0001_2 = dr["fua_0001_2"].ToString();
            }
        }
        else
            ide_0001_2 = 0;

        dr.Close();
    }

    /// <summary>
    /// Actualiza registro
    /// </summary>
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_2 SET");
        strSQL += string.Concat(" ide_5010 = @ide_5010");
        strSQL += string.Concat(", cfi_0001_2 = @cfi_0001_2");
        strSQL += string.Concat(", dip_0001_2 = @dip_0001_2");
        strSQL += string.Concat(", uua_0001_2 = @uua_0001_2");
        strSQL += string.Concat(", fua_0001_2 = @fua_0001_2");
        strSQL += string.Concat(" WHERE ide_0001_2 = ", _ide_0001_2);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_5010", _ide_5010);
        Command.Parameters.AddWithValue("@cfi_0001_2", _cfi_0001_2);
        Command.Parameters.AddWithValue("@dip_0001_2", _dip_0001_2);
        Command.Parameters.AddWithValue("@uua_0001_2", _uua_0001_2);
        Command.Parameters.AddWithValue("@fua_0001_2", _fua_0001_2);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}