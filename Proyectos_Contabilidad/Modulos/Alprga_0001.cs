using System;
using System.Data;
using System.Data.SqlClient;

public class Alprga_0001
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _cod_0001;                                          // 2   CLAVE_UNICA 
    private string _den_0001;                                       // 3    
    private string _ref_0001;                                       // 4    
    private int _ide_5000;                                          // 5    
    private string _fei_0001;                                               // 6    
    private byte _dur_0001;                                         // 7    
    private decimal _cfi_0001;                                      // 9    
    private decimal _tot_0001;                                      // 10    
    private byte _scd_0001;                                         // 11    
    private string _ira_0001;                                       // 12    
    private int _uar_0001;                                          // 13    
    private string _far_0001;                                       // 14    
    private string _dip_0001;                                       // 15    
    private int _uua_0001;                                          // 16    
    private string _fua_0001;                                       // 17    

    #endregion

    #region Propiedades

    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public int cod_0001
    {
        get { return _cod_0001; }
        set { _cod_0001 = value; }
    }
    public string den_0001
    {
        get { return _den_0001; }
        set { _den_0001 = value; }
    }
    public string ref_0001
    {
        get { return _ref_0001; }
        set { _ref_0001 = value; }
    }
    public int ide_5000
    {
        get { return _ide_5000; }
        set { _ide_5000 = value; }
    }
    public string fei_0001
    {
        get { return _fei_0001; }
        set { _fei_0001 = value; }
    }
    public byte dur_0001
    {
        get { return _dur_0001; }
        set { _dur_0001 = value; }
    }
    public decimal cfi_0001
    {
        get { return _cfi_0001; }
        set { _cfi_0001 = value; }
    }
    public decimal tot_0001
    {
        get { return _tot_0001; }
        set { _tot_0001 = value; }
    }
    public byte scd_0001
    {
        get { return _scd_0001; }
        set { _scd_0001 = value; }
    }
    public string ira_0001
    {
        get { return _ira_0001; }
        set { _ira_0001 = value; }
    }
    public int uar_0001
    {
        get { return _uar_0001; }
        set { _uar_0001 = value; }
    }
    public string far_0001
    {
        get { return _far_0001; }
        set { _far_0001 = value; }
    }
    public string dip_0001
    {
        get { return _dip_0001; }
        set { _dip_0001 = value; }
    }
    public int uua_0001
    {
        get { return _uua_0001; }
        set { _uua_0001 = value; }
    }
    public string fua_0001
    {
        get { return _fua_0001; }
        set { _fua_0001 = value; }
    }

    #endregion

    #region Constructores

    public Alprga_0001(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001 = 0;
        _cod_0001 = 0;
        _den_0001 = "";
        _ref_0001 = "";
        _ide_5000 = 0;
        _fei_0001 = "";
        _dur_0001 = 0;
        _cfi_0001 = 0;
        _tot_0001 = 0;
        _scd_0001 = 0;
        _ira_0001 = null;
        _uar_0001 = 0;
        _far_0001 = "";
        _dip_0001 = "";
        _uua_0001 = 0;
        _fua_0001 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alprga_0001 (  cod_0001, den_0001, ref_0001, ide_5000, fei_0001, dur_0001, cfi_0001, tot_0001, scd_0001, ira_0001, uar_0001, far_0001, dip_0001, uua_0001, fua_0001)" +
            $" OUTPUT INSERTED.ide_0001 VALUES (  @cod_0001, @den_0001, @ref_0001, @ide_5000, @fei_0001, @dur_0001, @cfi_0001, @tot_0001, @scd_0001, @ira_0001, @uar_0001, @far_0001, @dip_0001, @uua_0001, @fua_0001)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_0001", _cod_0001);
        Command.Parameters.AddWithValue("@den_0001", _den_0001);
        Command.Parameters.AddWithValue("@ref_0001", _ref_0001);
        Command.Parameters.AddWithValue("@ide_5000", _ide_5000);
        Command.Parameters.AddWithValue("@fei_0001", _fei_0001);
        Command.Parameters.AddWithValue("@dur_0001", _dur_0001);
        Command.Parameters.AddWithValue("@cfi_0001", _cfi_0001);
        Command.Parameters.AddWithValue("@tot_0001", _tot_0001);
        Command.Parameters.AddWithValue("@scd_0001", _scd_0001);

        if (_ira_0001 == null)
            Command.Parameters.Add("@ira_0001", SqlDbType.VarChar).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ira_0001", _ira_0001);

        Command.Parameters.AddWithValue("@uar_0001", _uar_0001);
        Command.Parameters.AddWithValue("@far_0001", _far_0001);
        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.Connection = _sqlCon;
        _ide_0001 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_0001 WHERE ide_0001 = {_ide_0001}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001 = (int)dr["ide_0001"];

            if (AsignarPropiedades)
            {
                cod_0001 = (int)dr["cod_0001"];
                den_0001 = (string)dr["den_0001"];
                ref_0001 = (string)dr["ref_0001"];
                ide_5000 = (int)dr["ide_5000"];
                fei_0001 = (string)dr["fei_0001"];
                dur_0001 = Convert.ToByte(dr["dur_0001"]);
                cfi_0001 = (decimal)dr["cfi_0001"];
                tot_0001 = (decimal)dr["tot_0001"];
                scd_0001 = Convert.ToByte(dr["scd_0001"]);
                ira_0001 = DBNull.Value.Equals(dr["ira_0001"]) ? null : (string)dr["ira_0001"];
                uar_0001 = (int)dr["uar_0001"];
                far_0001 = dr["far_0001"].ToString();
                dip_0001 = (string)dr["dip_0001"];
                uua_0001 = (int)dr["uua_0001"];
                fua_0001 = dr["fua_0001"].ToString();
            }
        }
        else
            ide_0001 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alprga_0001 WHERE ide_0001 = {_ide_0001}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001 SET");
        strSQL += string.Concat(" cod_0001 = @cod_0001");
        strSQL += string.Concat(", den_0001 = @den_0001");
        strSQL += string.Concat(", ref_0001 = @ref_0001");
        strSQL += string.Concat(", ide_5000 = @ide_5000");
        strSQL += string.Concat(", fei_0001 = @fei_0001");
        strSQL += string.Concat(", dur_0001 = @dur_0001");
        strSQL += string.Concat(", cfi_0001 = @cfi_0001");
        strSQL += string.Concat(", tot_0001 = @tot_0001");
        strSQL += string.Concat(", scd_0001 = @scd_0001");
        strSQL += string.Concat(", ira_0001 = @ira_0001");
        strSQL += string.Concat(", dip_0001 = @dip_0001");
        strSQL += string.Concat(", uua_0001 = @uua_0001");
        strSQL += string.Concat(", fua_0001 = @fua_0001");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_0001", _cod_0001);
        Command.Parameters.AddWithValue("@den_0001", _den_0001);
        Command.Parameters.AddWithValue("@ref_0001", _ref_0001);
        Command.Parameters.AddWithValue("@ide_5000", _ide_5000);
        Command.Parameters.AddWithValue("@fei_0001", _fei_0001);
        Command.Parameters.AddWithValue("@dur_0001", _dur_0001);
        Command.Parameters.AddWithValue("@cfi_0001", _cfi_0001);
        Command.Parameters.AddWithValue("@tot_0001", _tot_0001);
        Command.Parameters.AddWithValue("@scd_0001", _scd_0001);

        if (_ira_0001 == null)
            Command.Parameters.Add("@ira_0001", SqlDbType.VarChar).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ira_0001", _ira_0001);

        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE ÚNICA: cod_0001
    public void Cargar_cod_0001(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_0001 WHERE cod_0001 = {_cod_0001}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001 = (int)dr["ide_0001"];

            if (AsignarPropiedades)
            {
                cod_0001 = (int)dr["cod_0001"];
                den_0001 = (string)dr["den_0001"];
                ref_0001 = (string)dr["ref_0001"];
                ide_5000 = (int)dr["ide_5000"];
                fei_0001 = (string)dr["fei_0001"];
                dur_0001 = Convert.ToByte(dr["dur_0001"]);
                cfi_0001 = (decimal)dr["cfi_0001"];
                tot_0001 = (decimal)dr["tot_0001"];
                scd_0001 = Convert.ToByte(dr["scd_0001"]);
                ira_0001 = DBNull.Value.Equals(dr["ira_0001"]) ? null : (string)dr["ira_0001"];
                uar_0001 = (int)dr["uar_0001"];
                far_0001 = dr["far_0001"].ToString();
                dip_0001 = (string)dr["dip_0001"];
                uua_0001 = (int)dr["uua_0001"];
                fua_0001 = dr["fua_0001"].ToString();
            }
        }
        else
            ide_0001 = 0;

        dr.Close();
    }

    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001 SET");
        strSQL += string.Concat(" den_0001 = @den_0001");
        strSQL += string.Concat(", ref_0001 = @ref_0001");
        strSQL += string.Concat(", ide_5000 = @ide_5000");
        strSQL += string.Concat(", fei_0001 = @fei_0001");
        strSQL += string.Concat(", dur_0001 = @dur_0001");
        strSQL += string.Concat(", cfi_0001 = @cfi_0001");
        strSQL += string.Concat(", tot_0001 = @tot_0001");
        strSQL += string.Concat(", scd_0001 = @scd_0001");
        strSQL += string.Concat(", ira_0001 = @ira_0001");
        strSQL += string.Concat(", dip_0001 = @dip_0001");
        strSQL += string.Concat(", uua_0001 = @uua_0001");
        strSQL += string.Concat(", fua_0001 = @fua_0001");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_0001", _den_0001);
        Command.Parameters.AddWithValue("@ref_0001", _ref_0001);
        Command.Parameters.AddWithValue("@ide_5000", _ide_5000);
        Command.Parameters.AddWithValue("@fei_0001", _fei_0001);
        Command.Parameters.AddWithValue("@dur_0001", _dur_0001);
        Command.Parameters.AddWithValue("@cfi_0001", _cfi_0001);
        Command.Parameters.AddWithValue("@tot_0001", _tot_0001);
        Command.Parameters.AddWithValue("@scd_0001", _scd_0001);

        if (_ira_0001 == null)
            Command.Parameters.Add("@ira_0001", SqlDbType.VarChar).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ira_0001", _ira_0001);

        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

     #endregion

}