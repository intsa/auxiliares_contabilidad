using System;
using System.Data;
using System.Data.SqlClient;

public class Alprga_0001_1_2
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_1_2;                                      // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private int _ide_0001_1;                                        // 3    
    private short _eje_0001_1_2;                                    // 4    
    private string _fec_0001_1_2;                                         // 5    
    private byte _tim_0001_1_2;                                       // 6    
    private string _con_0001_1_2;                                   // 7    
    private decimal _imp_0001_1_2;                                  // 8    
    private string _ira_0001_1_2;                                   // 9    
    private int _uar_0001_1_2;                                      // 10    
    private string _far_0001_1_2;                                   // 11    
    private string _dip_0001_1_2;                                   // 12    
    private int _uua_0001_1_2;                                      // 13    
    private string _fua_0001_1_2;                                   // 14    

    #endregion

    #region Propiedades

    public int ide_0001_1_2
    {
        get { return _ide_0001_1_2; }
        set { _ide_0001_1_2 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public int ide_0001_1
    {
        get { return _ide_0001_1; }
        set { _ide_0001_1 = value; }
    }
    public short eje_0001_1_2
    {
        get { return _eje_0001_1_2; }
        set { _eje_0001_1_2 = value; }
    }
    public string fec_0001_1_2
    {
        get { return _fec_0001_1_2; }
        set
        { _fec_0001_1_2 = value; }
    }
    public byte tim_0001_1_2
    {
        get { return _tim_0001_1_2; }
        set { _tim_0001_1_2 = value; }
    }
    public string con_0001_1_2
    {
        get { return _con_0001_1_2; }
        set { _con_0001_1_2 = value; }
    }
    public decimal imp_0001_1_2
    {
        get { return _imp_0001_1_2; }
        set { _imp_0001_1_2 = value; }
    }
    public string ira_0001_1_2
    {
        get { return _ira_0001_1_2; }
        set { _ira_0001_1_2 = value; }
    }
    public int uar_0001_1_2
    {
        get { return _uar_0001_1_2; }
        set { _uar_0001_1_2 = value; }
    }
    public string far_0001_1_2
    {
        get { return _far_0001_1_2; }
        set { _far_0001_1_2 = value; }
    }
    public string dip_0001_1_2
    {
        get { return _dip_0001_1_2; }
        set { _dip_0001_1_2 = value; }
    }
    public int uua_0001_1_2
    {
        get { return _uua_0001_1_2; }
        set { _uua_0001_1_2 = value; }
    }
    public string fua_0001_1_2
    {
        get { return _fua_0001_1_2; }
        set { _fua_0001_1_2 = value; }
    }

    #endregion

    #region Constructores

    public Alprga_0001_1_2(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_1_2 = 0;
        _ide_0001 = 0;
        _ide_0001_1 = 0;
        _eje_0001_1_2 = 0;
        _fec_0001_1_2 = "";
        _tim_0001_1_2 = 0;
        _con_0001_1_2 = "";
        _imp_0001_1_2 = 0;
        _ira_0001_1_2 = "";
        _uar_0001_1_2 = 0;
        _far_0001_1_2 = "";
        _dip_0001_1_2 = "";
        _uua_0001_1_2 = 0;
        _fua_0001_1_2 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alprga_0001_1_2 (ide_0001, ide_0001_1, eje_0001_1_2, fec_0001_1_2, tim_0001_1_2, con_0001_1_2, imp_0001_1_2, ira_0001_1_2, uar_0001_1_2, far_0001_1_2, dip_0001_1_2, uua_0001_1_2, fua_0001_1_2)" +
            $" OUTPUT INSERTED.ide_0001_1_2 VALUES (  @ide_0001, @ide_0001_1, @eje_0001_1_2, @fec_0001_1_2, @tim_0001_1_2, @con_0001_1_2, @imp_0001_1_2, @ira_0001_1_2, @uar_0001_1_2, @far_0001_1_2, @dip_0001_1_2, @uua_0001_1_2, @fua_0001_1_2)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_0001_1", _ide_0001_1);
        Command.Parameters.AddWithValue("@eje_0001_1_2", _eje_0001_1_2);
        Command.Parameters.AddWithValue("@fec_0001_1_2", _fec_0001_1_2);
        Command.Parameters.AddWithValue("@tim_0001_1_2", _tim_0001_1_2);
        Command.Parameters.AddWithValue("@con_0001_1_2", _con_0001_1_2);
        Command.Parameters.AddWithValue("@imp_0001_1_2", _imp_0001_1_2);

        if (_ira_0001_1_2 == null)
            Command.Parameters.Add("@ira_0001_1_2", SqlDbType.VarChar).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ira_0001_1_2", _ira_0001_1_2);

        Command.Parameters.AddWithValue("@uar_0001_1_2", _uar_0001_1_2);
        Command.Parameters.AddWithValue("@far_0001_1_2", _far_0001_1_2);
        Command.Parameters.AddWithValue("@dip_0001_1_2", _dip_0001_1_2);
        Command.Parameters.AddWithValue("@uua_0001_1_2", _uua_0001_1_2);
        Command.Parameters.AddWithValue("@fua_0001_1_2", _fua_0001_1_2);

        Command.Connection = _sqlCon;
        _ide_0001_1_2 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_0001_1_2 WHERE ide_0001_1_2 = {_ide_0001_1_2}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_1_2 = (int)dr["ide_0001_1_2"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                ide_0001_1 = (int)dr["ide_0001_1"];
                eje_0001_1_2 = (short)dr["eje_0001_1_2"];
                fec_0001_1_2 = dr["fec_0001_1_2"].ToString();
                tim_0001_1_2 = Convert.ToByte(dr["tim_0001_1_2"]);
                con_0001_1_2 = (string)dr["con_0001_1_2"];
                imp_0001_1_2 = (decimal)dr["imp_0001_1_2"];
                ira_0001_1_2 = DBNull.Value.Equals(dr["ira_0001_1_2"]) ? null : (string)dr["ira_0001_1_2"];
                uar_0001_1_2 = (int)dr["uar_0001_1_2"];
                far_0001_1_2 = dr["far_0001_1_2"].ToString();
                dip_0001_1_2 = (string)dr["dip_0001_1_2"];
                uua_0001_1_2 = (int)dr["uua_0001_1_2"];
                fua_0001_1_2 = dr["fua_0001_1_2"].ToString();
            }
        }
        else
            ide_0001_1_2 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {

        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alprga_0001_1_2 WHERE ide_0001_1_2 = {_ide_0001_1_2}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_1_2 SET");
        strSQL += string.Concat(" ide_0001 = @ide_0001");
        strSQL += string.Concat(", ide_0001_1 = @ide_0001_1");
        strSQL += string.Concat(", eje_0001_1_2 = @eje_0001_1_2");
        strSQL += string.Concat(", fec_0001_1_2 = @fec_0001_1_2");
        strSQL += string.Concat(", tim_0001_1_2 = @tim_0001_1_2");
        strSQL += string.Concat(", con_0001_1_2 = @con_0001_1_2");
        strSQL += string.Concat(", imp_0001_1_2 = @imp_0001_1_2");
        strSQL += string.Concat(", ira_0001_1_2 = @ira_0001_1_2");
        strSQL += string.Concat(", dip_0001_1_2 = @dip_0001_1_2");
        strSQL += string.Concat(", uua_0001_1_2 = @uua_0001_1_2");
        strSQL += string.Concat(", fua_0001_1_2 = @fua_0001_1_2");
        strSQL += string.Concat(" WHERE ide_0001_1_2 = ", _ide_0001_1_2);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@ide_0001_1", _ide_0001_1);
        Command.Parameters.AddWithValue("@eje_0001_1_2", _eje_0001_1_2);
        Command.Parameters.AddWithValue("@fec_0001_1_2", _fec_0001_1_2);
        Command.Parameters.AddWithValue("@tim_0001_1_2", _tim_0001_1_2);
        Command.Parameters.AddWithValue("@con_0001_1_2", _con_0001_1_2);
        Command.Parameters.AddWithValue("@imp_0001_1_2", _imp_0001_1_2);

        if (_ira_0001_1_2 == null)
            Command.Parameters.Add("@ira_0001_1_2", SqlDbType.VarChar).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ira_0001_1_2", _ira_0001_1_2);

        Command.Parameters.AddWithValue("@dip_0001_1_2", _dip_0001_1_2);
        Command.Parameters.AddWithValue("@uua_0001_1_2", _uua_0001_1_2);
        Command.Parameters.AddWithValue("@fua_0001_1_2", _fua_0001_1_2);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza registro
    /// </summary>
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_1_2 SET");
        strSQL += string.Concat(" eje_0001_1_2 = @eje_0001_1_2");
        strSQL += string.Concat(", fec_0001_1_2 = @fec_0001_1_2");
        strSQL += string.Concat(", tim_0001_1_2 = @tim_0001_1_2");
        strSQL += string.Concat(", con_0001_1_2 = @con_0001_1_2");
        strSQL += string.Concat(", imp_0001_1_2 = @imp_0001_1_2");
        strSQL += string.Concat(", ira_0001_1_2 = @ira_0001_1_2");
        strSQL += string.Concat(", dip_0001_1_2 = @dip_0001_1_2");
        strSQL += string.Concat(", uua_0001_1_2 = @uua_0001_1_2");
        strSQL += string.Concat(", fua_0001_1_2 = @fua_0001_1_2");
        strSQL += string.Concat(" WHERE ide_0001_1_2 = ", _ide_0001_1_2);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_0001_1_2", _eje_0001_1_2);
        Command.Parameters.AddWithValue("@fec_0001_1_2", _fec_0001_1_2);
        Command.Parameters.AddWithValue("@tim_0001_1_2", _tim_0001_1_2);
        Command.Parameters.AddWithValue("@con_0001_1_2", _con_0001_1_2);
        Command.Parameters.AddWithValue("@imp_0001_1_2", _imp_0001_1_2);

        if (_ira_0001_1_2 == null)
            Command.Parameters.Add("@ira_0001_1_2", SqlDbType.VarChar).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ira_0001_1_2", _ira_0001_1_2);

        Command.Parameters.AddWithValue("@dip_0001_1_2", _dip_0001_1_2);
        Command.Parameters.AddWithValue("@uua_0001_1_2", _uua_0001_1_2);
        Command.Parameters.AddWithValue("@fua_0001_1_2", _fua_0001_1_2);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}