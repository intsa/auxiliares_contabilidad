using System;
using System.Data;
using System.Data.SqlClient;

public class Alprga_0001_1
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_1;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private short _eje_0001_1;                                      // 3    
    private int? _exp_0001_1;                                       // 4    
    private string _org_0001_1;                                     // 5    
    private string _fun_0001_1;                                     // 6    
    private string _eco_0001_1;                                     // 7    
    private string _den_0001_1;                                     // 8    
    private decimal _prg_0001_1;                                    // 9    
    private decimal _com_0001_1;                                    // 10    
    private decimal _rec_0001_1;                                    // 11    
    private decimal _pag_0001_1;                                    // 12    
    private string _ira_0001_1;                                     // 13    
    private int _uar_0001_1;                                        // 14    
    private string _far_0001_1;                                     // 15    
    private string _dip_0001_1;                                     // 16    
    private int _uua_0001_1;                                        // 17    
    private string _fua_0001_1;                                     // 18    

    #endregion

    #region Propiedades

    public int ide_0001_1
    {
        get { return _ide_0001_1; }
        set { _ide_0001_1 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public short eje_0001_1
    {
        get { return _eje_0001_1; }
        set { _eje_0001_1 = value; }
    }
    public int? exp_0001_1
    {
        get { return _exp_0001_1; }
        set { _exp_0001_1 = value; }
    }
    public string org_0001_1
    {
        get { return _org_0001_1; }
        set { _org_0001_1 = value; }
    }
    public string fun_0001_1
    {
        get { return _fun_0001_1; }
        set { _fun_0001_1 = value; }
    }
    public string eco_0001_1
    {
        get { return _eco_0001_1; }
        set { _eco_0001_1 = value; }
    }
    public string den_0001_1
    {
        get { return _den_0001_1; }
        set { _den_0001_1 = value; }
    }
    public decimal prg_0001_1
    {
        get { return _prg_0001_1; }
        set { _prg_0001_1 = value; }
    }
    public decimal com_0001_1
    {
        get { return _com_0001_1; }
        set { _com_0001_1 = value; }
    }
    public decimal rec_0001_1
    {
        get { return _rec_0001_1; }
        set { _rec_0001_1 = value; }
    }
    public decimal pag_0001_1
    {
        get { return _pag_0001_1; }
        set { _pag_0001_1 = value; }
    }
    public string ira_0001_1
    {
        get { return _ira_0001_1; }
        set { _ira_0001_1 = value; }
    }
    public int uar_0001_1
    {
        get { return _uar_0001_1; }
        set { _uar_0001_1 = value; }
    }
    public string far_0001_1
    {
        get { return _far_0001_1; }
        set { _far_0001_1 = value; }
    }
    public string dip_0001_1
    {
        get { return _dip_0001_1; }
        set { _dip_0001_1 = value; }
    }
    public int uua_0001_1
    {
        get { return _uua_0001_1; }
        set { _uua_0001_1 = value; }
    }
    public string fua_0001_1
    {
        get { return _fua_0001_1; }
        set { _fua_0001_1 = value; }
    }

    #endregion

    #region Constructores

    public Alprga_0001_1(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_1 = 0;
        _ide_0001 = 0;
        _eje_0001_1 = 0;
        _exp_0001_1 = null;
        _org_0001_1 = "";
        _fun_0001_1 = "";
        _eco_0001_1 = "";
        _den_0001_1 = "";
        _prg_0001_1 = 0;
        _com_0001_1 = 0;
        _rec_0001_1 = 0;
        _pag_0001_1 = 0;
        _ira_0001_1 = null;
        _uar_0001_1 = 0;
        _far_0001_1 = "";
        _dip_0001_1 = "";
        _uua_0001_1 = 0;
        _fua_0001_1 = "";
    }

    #endregion

    #region Métodos públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alprga_0001_1 (ide_0001, eje_0001_1, exp_0001_1, org_0001_1, fun_0001_1, eco_0001_1, den_0001_1, prg_0001_1, com_0001_1, rec_0001_1, pag_0001_1, ira_0001_1, uar_0001_1, far_0001_1, dip_0001_1, uua_0001_1, fua_0001_1)" +
            $" OUTPUT INSERTED.ide_0001_1 VALUES (@ide_0001, @eje_0001_1, @exp_0001_1, @org_0001_1, @fun_0001_1, @eco_0001_1, @den_0001_1, @prg_0001_1, @com_0001_1, @rec_0001_1, @pag_0001_1, @ira_0001_1, @uar_0001_1, @far_0001_1, @dip_0001_1, @uua_0001_1, @fua_0001_1)"
        };

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@eje_0001_1", _eje_0001_1);

        if (_exp_0001_1 == null)
            Command.Parameters.Add("@exp_0001_1", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@exp_0001_1", _exp_0001_1);

        Command.Parameters.AddWithValue("@org_0001_1", _org_0001_1);
        Command.Parameters.AddWithValue("@fun_0001_1", _fun_0001_1);
        Command.Parameters.AddWithValue("@eco_0001_1", _eco_0001_1);
        Command.Parameters.AddWithValue("@den_0001_1", _den_0001_1);
        Command.Parameters.AddWithValue("@prg_0001_1", _prg_0001_1);
        Command.Parameters.AddWithValue("@com_0001_1", _com_0001_1);
        Command.Parameters.AddWithValue("@rec_0001_1", _rec_0001_1);
        Command.Parameters.AddWithValue("@pag_0001_1", _pag_0001_1);

        if (_ira_0001_1 == null)
            Command.Parameters.Add("@ira_0001_1", SqlDbType.VarChar).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ira_0001_1", _ira_0001_1);

        Command.Parameters.AddWithValue("@uar_0001_1", _uar_0001_1);
        Command.Parameters.AddWithValue("@far_0001_1", _far_0001_1);
        Command.Parameters.AddWithValue("@dip_0001_1", _dip_0001_1);
        Command.Parameters.AddWithValue("@uua_0001_1", _uua_0001_1);
        Command.Parameters.AddWithValue("@fua_0001_1", _fua_0001_1);

        Command.Connection = _sqlCon;
        _ide_0001_1 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alprga_0001_1 WHERE ide_0001_1 = {_ide_0001_1}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_1 = (int)dr["ide_0001_1"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                eje_0001_1 = (short)dr["eje_0001_1"];
                exp_0001_1 = DBNull.Value.Equals(dr["exp_0001_1"]) ? null : (int?)dr["exp_0001_1"];
                org_0001_1 = (string)dr["org_0001_1"];
                fun_0001_1 = (string)dr["fun_0001_1"];
                eco_0001_1 = (string)dr["eco_0001_1"];
                den_0001_1 = (string)dr["den_0001_1"];
                prg_0001_1 = (decimal)dr["prg_0001_1"];
                com_0001_1 = (decimal)dr["com_0001_1"];
                rec_0001_1 = (decimal)dr["rec_0001_1"];
                pag_0001_1 = (decimal)dr["pag_0001_1"];
                ira_0001_1 = DBNull.Value.Equals(dr["ira_0001_1"]) ? null : (string)dr["ira_0001_1"];
                uar_0001_1 = (int)dr["uar_0001_1"];
                far_0001_1 = dr["far_0001_1"].ToString();
                dip_0001_1 = (string)dr["dip_0001_1"];
                uua_0001_1 = (int)dr["uua_0001_1"];
                fua_0001_1 = dr["fua_0001_1"].ToString();
            }
        }
        else
            ide_0001_1 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alprga_0001_1 WHERE ide_0001_1 = {_ide_0001_1}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_1 SET");
        strSQL += string.Concat(" ide_0001 = @ide_0001");
        strSQL += string.Concat(", eje_0001_1 = @eje_0001_1");
        strSQL += string.Concat(", exp_0001_1 = @exp_0001_1");
        strSQL += string.Concat(", org_0001_1 = @org_0001_1");
        strSQL += string.Concat(", fun_0001_1 = @fun_0001_1");
        strSQL += string.Concat(", eco_0001_1 = @eco_0001_1");
        strSQL += string.Concat(", den_0001_1 = @den_0001_1");
        strSQL += string.Concat(", prg_0001_1 = @prg_0001_1");
        strSQL += string.Concat(", com_0001_1 = @com_0001_1");
        strSQL += string.Concat(", rec_0001_1 = @rec_0001_1");
        strSQL += string.Concat(", pag_0001_1 = @pag_0001_1");
        strSQL += string.Concat(", ira_0001_1 = @ira_0001_1");
        strSQL += string.Concat(", dip_0001_1 = @dip_0001_1");
        strSQL += string.Concat(", uua_0001_1 = @uua_0001_1");
        strSQL += string.Concat(", fua_0001_1 = @fua_0001_1");
        strSQL += string.Concat(" WHERE ide_0001_1 = ", _ide_0001_1);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@eje_0001_1", _eje_0001_1);

        if (_exp_0001_1 == null)
            Command.Parameters.Add("@exp_0001_1", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@exp_0001_1", _exp_0001_1);

        Command.Parameters.AddWithValue("@org_0001_1", _org_0001_1);
        Command.Parameters.AddWithValue("@fun_0001_1", _fun_0001_1);
        Command.Parameters.AddWithValue("@eco_0001_1", _eco_0001_1);
        Command.Parameters.AddWithValue("@den_0001_1", _den_0001_1);
        Command.Parameters.AddWithValue("@prg_0001_1", _prg_0001_1);
        Command.Parameters.AddWithValue("@com_0001_1", _com_0001_1);
        Command.Parameters.AddWithValue("@rec_0001_1", _rec_0001_1);
        Command.Parameters.AddWithValue("@pag_0001_1", _pag_0001_1);

        if (_ira_0001_1 == null)
            Command.Parameters.Add("@ira_0001_1", SqlDbType.VarChar).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ira_0001_1", _ira_0001_1);

        Command.Parameters.AddWithValue("@dip_0001_1", _dip_0001_1);
        Command.Parameters.AddWithValue("@uua_0001_1", _uua_0001_1);
        Command.Parameters.AddWithValue("@fua_0001_1", _fua_0001_1);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza registro
    /// </summary>
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alprga_0001_1 SET");
        strSQL += string.Concat(" eje_0001_1 = @eje_0001_1");
        strSQL += string.Concat(", exp_0001_1 = @exp_0001_1");
        strSQL += string.Concat(", org_0001_1 = @org_0001_1");
        strSQL += string.Concat(", fun_0001_1 = @fun_0001_1");
        strSQL += string.Concat(", eco_0001_1 = @eco_0001_1");
        strSQL += string.Concat(", den_0001_1 = @den_0001_1");
        strSQL += string.Concat(", prg_0001_1 = @prg_0001_1");
        strSQL += string.Concat(", com_0001_1 = @com_0001_1");
        strSQL += string.Concat(", rec_0001_1 = @rec_0001_1");
        strSQL += string.Concat(", pag_0001_1 = @pag_0001_1");
        strSQL += string.Concat(", ira_0001_1 = @ira_0001_1");
        strSQL += string.Concat(", dip_0001_1 = @dip_0001_1");
        strSQL += string.Concat(", uua_0001_1 = @uua_0001_1");
        strSQL += string.Concat(", fua_0001_1 = @fua_0001_1");
        strSQL += string.Concat(" WHERE ide_0001_1 = ", _ide_0001_1);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_0001_1", _eje_0001_1);

        if (_exp_0001_1 == null)
            Command.Parameters.Add("@exp_0001_1", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@exp_0001_1", _exp_0001_1);

        Command.Parameters.AddWithValue("@org_0001_1", _org_0001_1);
        Command.Parameters.AddWithValue("@fun_0001_1", _fun_0001_1);
        Command.Parameters.AddWithValue("@eco_0001_1", _eco_0001_1);
        Command.Parameters.AddWithValue("@den_0001_1", _den_0001_1);
        Command.Parameters.AddWithValue("@prg_0001_1", _prg_0001_1);
        Command.Parameters.AddWithValue("@com_0001_1", _com_0001_1);
        Command.Parameters.AddWithValue("@rec_0001_1", _rec_0001_1);
        Command.Parameters.AddWithValue("@pag_0001_1", _pag_0001_1);

        if (_ira_0001_1 == null)
            Command.Parameters.Add("@ira_0001_1", SqlDbType.VarChar).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ira_0001_1", _ira_0001_1);

        Command.Parameters.AddWithValue("@dip_0001_1", _dip_0001_1);
        Command.Parameters.AddWithValue("@uua_0001_1", _uua_0001_1);
        Command.Parameters.AddWithValue("@fua_0001_1", _fua_0001_1);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza registro
    /// </summary>
    /// <param name="Campos">Matriz de campos a actualizar</param>
    /// <param name="Importe">Importe a actualizar</param>
    public void Actualizar_Importes(string[] Campos, decimal Importe)
    {
        string Cadena_Sql = $"UPDATE {_BaseDatos}.dbo.alprga_0001_1 SET";

        for (byte i = 0; i < Campos.Length; i++)
            Cadena_Sql = $"{Cadena_Sql} {Campos[i]} = {Campos[i]} + {Convert.ToString(Importe).Replace(",", ".")},";

        Cadena_Sql += $" dip_0001_1 = '{_dip_0001_1}',";
        Cadena_Sql += $" uua_0001_1 = {_uua_0001_1},";
        Cadena_Sql += $" fua_0001_1 = '{_fua_0001_1}'";
        Cadena_Sql += $" WHERE ide_0001_1 = {_ide_0001_1}";

        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = Cadena_Sql
        };

        Command.ExecuteNonQuery();
    }

    #endregion

}