﻿using ClaseIntsa.Propiedades;
using System;
using System.Data.SqlClient;

namespace Proyectos_Contabilidad
{
    public static class Desencadenantes_Proyectos
    {
        #region Tabla Alprga_0001 'PROYECTOS DE GASTO E INVERSIÓN'

        /// <summary>
        /// Inserta registros tabla 'PROYECTOS DE GASTO E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Codigo">Código de proyecto de gasto e inversión</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Referencia">Referencia</param>
        /// <param name="Pid_5000">Identificador registro 'TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Fecha">Fecha inicio ejecución prevista</param>
        /// <param name="Duracion">Duración prevista ejecución</param>
        /// <param name="Coeficiente">Coeficiente de financiación</param>
        /// <param name="Total_Previsto">Total importe previsto</param>
        /// <param name="Desviacion">Semáforo cálculo desviación</param>
        /// <param name="Id_Trasaso">Identificador registro traspaso</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alprga_0001(string CadenaConexion, string Bdd_Proyectos, int Codigo, string Denominacion, string Referencia, int Pid_5000, DateTime Fecha, byte Duracion,
                                                                                decimal Coeficiente, decimal Total_Previsto, bool Desviacion, string Id_Trasaso = null)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001 Temporal = new Alprga_0001(Cadena_Conexion, Bdd_Proyectos)
                    {
                        cod_0001 = Codigo,
                        den_0001 = Denominacion,
                        ref_0001 = Referencia,
                        ide_5000 = Pid_5000,
                        fei_0001 = Fecha.ToString("dd/MM/yyyy"),
                        dur_0001 = Duracion,
                        cfi_0001 = Coeficiente,
                        tot_0001 = Total_Previsto,
                        scd_0001 = Desviacion ? (byte)1 : (byte)0,
                        ira_0001 = Id_Trasaso,
                        uar_0001 = Variables_Globales.IdUsuario,
                        far_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_0001 = Variables_Globales.IpPrivada,
                        uua_0001 = Variables_Globales.IdUsuario,
                        fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_0001 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0001;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'PROYECTOS DE GASTO E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Referencia">Referencia</param>
        /// <param name="Pid_5000">Identificador registro 'TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Fecha">Fecha inicio ejecución prevista</param>
        /// <param name="Fecha">Duración previste ejecución</param>
        /// <param name="Duracion">Duración previste ejecución</param>
        /// <param name="Coeficiente">Coeficiente de financiación</param>
        /// <param name="Total_Previsto">Total importe previsto</param>
        /// <param name="Desviacion">Semáforo cálculo desviación</param>
        /// <param name="Id_Trasaso">Identificador registro traspaso</param>
        public static void Actualizar_Alprga_0001(string CadenaConexion, string Bdd_Proyectos, int Pid_0001, string Denominacion, string Referencia, int Pid_5000, DateTime Fecha, byte Duracion,
                                                                                      decimal Coeficiente, decimal Total_Previsto, bool Desviacion, string Id_Trasaso = null)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001 Temporal = new Alprga_0001(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001 = Pid_0001,

                        den_0001 = Denominacion,
                        ref_0001 = Referencia,
                        ide_5000 = Pid_5000,
                        fei_0001 = Fecha.ToString("dd/MM/yyyy"),
                        dur_0001 = Duracion,
                        cfi_0001 = Coeficiente,
                        tot_0001 = Total_Previsto,
                        scd_0001 = Desviacion ? (byte)1 : (byte)0,
                        ira_0001 = Id_Trasaso,
                        dip_0001 = Variables_Globales.IpPrivada,
                        uua_0001 = Variables_Globales.IdUsuario,
                        fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'PROYECTOS DE GASTO E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        public static void Eliminar_Alprga_0001(string CadenaConexion, string Bdd_Proyectos, int Pid_0001)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001 Temporal = new Alprga_0001(Cadena_Conexion, Bdd_Proyectos) { ide_0001 = Pid_0001 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alprga_0001 'PROYECTOS DE GASTO E INVERSIÓN'

        #region Tabla Alprga_0001_1 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'

        /// <summary>
        /// Inserta registros tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Ejercicio">Ejercicio ejecución</param>
        /// <param name="Expediente">Número expediente contable</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        /// <param name="Gasto_Previsto">Total importe gastos previstos</param>
        /// <param name="Comprometido">Importe comprometido</param>
        /// <param name="Reconocido">Importe reconocido</param>
        /// <param name="Pagado">Importe pagado</param>
        /// <param name="Id_Trasaso">Identificador registro traspaso</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alprga_0001_1(string CadenaConexion, string Bdd_Proyectos, int Pid_0001, short Ejercicio, int? Expediente, string Clave_Organica, string Clave_Funcional, string Clave_Economica, string Denominacion,
                                                                                     decimal Gasto_Previsto, decimal Comprometido, decimal Reconocido, decimal Pagado, string Id_Trasaso = null)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1 Temporal = new Alprga_0001_1(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001 = Pid_0001,
                        eje_0001_1 = Ejercicio,
                        exp_0001_1 = Expediente,
                        org_0001_1 = Clave_Organica,
                        fun_0001_1 = Clave_Funcional,
                        eco_0001_1 = Clave_Economica,
                        den_0001_1 = Denominacion,
                        prg_0001_1 = Gasto_Previsto,
                        com_0001_1 = Comprometido,
                        rec_0001_1 = Reconocido,
                        pag_0001_1 = Pagado,
                        ira_0001_1 = Id_Trasaso,
                        uar_0001_1 = Variables_Globales.IdUsuario,
                        far_0001_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_0001_1 = Variables_Globales.IpPrivada,
                        uua_0001_1 = Variables_Globales.IdUsuario,
                        fua_0001_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_0001_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0001_1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_1">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'</param>
        /// <param name="Ejercicio">Ejercicio ejecución</param>
        /// <param name="Expediente">Número expediente contable</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        /// <param name="Gasto_Previsto">Total importe gastos previstos</param>
        /// <param name="Comprometido">Importe comprometido</param>
        /// <param name="Reconocido">Importe reconocido</param>
        /// <param name="Pagado">Importe pagado</param>
        /// <param name="Id_Trasaso">Identificador registro traspaso</param>
        public static void Actualizar_Alprga_0001_1(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_1, short Ejercicio, int? Expediente, string Clave_Organica, string Clave_Funcional, string Clave_Economica, string Denominacion,
                                                                                          decimal Gasto_Previsto, decimal Comprometido, decimal Reconocido, decimal Pagado, string Id_Trasaso = null)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1 Temporal = new Alprga_0001_1(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001_1 = Pid_0001_1,

                        eje_0001_1 = Ejercicio,
                        exp_0001_1 = Expediente,
                        org_0001_1 = Clave_Organica,
                        fun_0001_1 = Clave_Funcional,
                        eco_0001_1 = Clave_Economica,
                        den_0001_1 = Denominacion,
                        prg_0001_1 = Gasto_Previsto,
                        com_0001_1 = Comprometido,
                        rec_0001_1 = Reconocido,
                        pag_0001_1 = Pagado,
                        ira_0001_1 = Id_Trasaso,
                        dip_0001_1 = Variables_Globales.IpPrivada,
                        uua_0001_1 = Variables_Globales.IdUsuario,
                        fua_0001_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_1">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'</param>
        /// <param name="Campos">Matriz de nombres de columnas a actualizar</param>
        /// <param name="Importe">Importe a actualizar</param>
        public static void Actualizar_Alprga_0001_1(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_1, string[] Campos, decimal Importe)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1 Temporal = new Alprga_0001_1(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001_1 = Pid_0001_1,

                        dip_0001_1 = Variables_Globales.IpPrivada,
                        uua_0001_1 = Variables_Globales.IdUsuario,
                        fua_0001_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Importes(Campos, Importe);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_1">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'</param>
        public static void Eliminar_Alprga_0001_1(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_1)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1 Temporal = new Alprga_0001_1(Cadena_Conexion, Bdd_Proyectos) { ide_0001_1 = Pid_0001_1 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alprga_0001_1 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'

        #region Tabla Alprga_0001_1_2 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. GASTOS NO IMPUTADOS'

        /// <summary>
        /// Inserta registros tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. GASTOS NO IMPUTADOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Pid_0001_1">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'</param>
        /// <param name="Ejercicio">Ejercicio ejecución</param>
        /// <param name="Fecha">Fecha imputación</param>
        /// <param name="Tipo">Tipo de imputación</param>
        /// <param name="Concepto">Concepto imputación</param>
        /// <param name="Importe">Importe imputación</param>
        /// <param name="Id_Trasaso">Identificador registro traspaso</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alprga_0001_1_2(string CadenaConexion, string Bdd_Proyectos, int Pid_0001, int Pid_0001_1, short Ejercicio, DateTime Fecha, byte Tipo, string Concepto, decimal Importe, string Id_Trasaso = null)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1_2 Temporal = new Alprga_0001_1_2(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001 = Pid_0001,
                        ide_0001_1 = Pid_0001_1,
                        eje_0001_1_2 = Ejercicio,
                        fec_0001_1_2 = Fecha.ToString("dd/MM/yyyy"),
                        tim_0001_1_2 = Tipo,
                        con_0001_1_2 = Concepto,
                        imp_0001_1_2 = Importe,
                        ira_0001_1_2 = Id_Trasaso,
                        uar_0001_1_2 = Variables_Globales.IdUsuario,
                        far_0001_1_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_0001_1_2 = Variables_Globales.IpPrivada,
                        uua_0001_1_2 = Variables_Globales.IdUsuario,
                        fua_0001_1_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_0001_1_2 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0001_1_2;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. GASTOS NO IMPUTADOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_1_2">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. GASTOS NO IMPUTADOS'</param>
        /// <param name="Ejercicio">Ejercicio ejecución</param>
        /// <param name="Fecha">Fecha imputación</param>
        /// <param name="Tipo">Tipo de imputación</param>
        /// <param name="Concepto">Concepto imputación</param>
        /// <param name="Importe">Importe imputación</param>
        /// <param name="Id_Trasaso">Identificador registro traspaso</param>
        public static void Actualizar_Alprga_0001_1_2(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_1_2, short Ejercicio, DateTime Fecha, byte Tipo, string Concepto, decimal Importe, string Id_Trasaso = null)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1_2 Temporal = new Alprga_0001_1_2(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001_1_2 = Pid_0001_1_2,

                        eje_0001_1_2 = Ejercicio,
                        fec_0001_1_2 = Fecha.ToString("dd/MM/yyyy"),
                        tim_0001_1_2 = Tipo,
                        con_0001_1_2 = Concepto,
                        imp_0001_1_2 = Importe,
                        ira_0001_1_2 = Id_Trasaso,
                        dip_0001_1_2 = Variables_Globales.IpPrivada,
                        uua_0001_1_2 = Variables_Globales.IdUsuario,
                        fua_0001_1_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. GASTOS NO IMPUTADOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_1_2">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. GASTOS NO IMPUTADOS'</param>
        public static void Eliminar_Alprga_0001_1_2(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_1_2)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1_2 Temporal = new Alprga_0001_1_2(Cadena_Conexion, Bdd_Proyectos) { ide_0001_1_2 = Pid_0001_1_2 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alprga_0001_1_2 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. GASTOS NO IMPUTADOS'

        #region Tabla Alprga_0001_1_3 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. IMPUTACIONES CONTABLES''

        /// <summary>
        /// Inserta registros tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. IMPUTACIONES CONTABLES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Pid_0001_1">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'</param>
        /// <param name="Pid_a0001">Identificador registro 'FACTURAS/DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_n0001_1">Identificador registro 'NOMINAS. DETALLE'</param>
        /// <param name="Ejercicio">Ejercicio de contable</param>
        /// <param name="Expediente">Datos contables. Número de expediente</param>
        /// <param name="Documento">Datos contables. Número de documento</param>
        /// <param name="Fecha">Fecha contabilización</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Tipo_Movimiento">Tipo de movimiento</param>
        /// <param name="Importe_Movimiento">Importe </param>
        /// <param name="Porcentaje">Porcentaje de imputación</param>
        /// <param name="Imputacion">Importe imputado</param>
        /// <returns>Identificador registro</returns>
        public static long Insertar_Alprga_0001_1_3(string CadenaConexion, string Bdd_Proyectos, int Pid_0001, int Pid_0001_1, int? Pid_a0001, int? Pid_n0001_1, short Ejercicio, int Expediente, int Documento, 
                                                                                           DateTime Fecha, string Tipo_Movimiento, decimal Importe_Movimiento, decimal? Porcentaje, decimal Imputacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1_3 Temporal = new Alprga_0001_1_3(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001 = Pid_0001,
                        ide_0001_1 = Pid_0001_1,
                        ifa_0001_1_3 = Pid_a0001,
                        ino_0001_1_3 = Pid_n0001_1,
                        eje_0001_1_3 = Ejercicio,
                        exp_0001_1_3 = Expediente,
                        doc_0001_1_3 = Documento,
                        fec_0001_1_3 = Fecha.ToString("dd/MM/yyyy"),
                        tmo_0001_1_3 = Tipo_Movimiento,
                        imp_0001_1_3 = Importe_Movimiento,
                        por_0001_1_3 = Porcentaje,
                        iim_0001_1_3 = Imputacion,
                        uar_0001_1_3 = Variables_Globales.IdUsuario,
                        far_0001_1_3 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_0001_1_3 = Variables_Globales.IpPrivada,
                        uua_0001_1_3 = Variables_Globales.IdUsuario,
                        fua_0001_1_3 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_0001_1_3 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0001_1_3;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. IMPUTACIONES CONTABLES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_1_3">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. IMPUTACIONES CONTABLES'</param>
        /// <param name="Pid_a0001">Identificador registro 'FACTURAS/DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_n0001_1">Identificador registro 'NOMINAS. DETALLE'</param>
        /// <param name="Ejercicio">Ejercicio de contable</param>
        /// <param name="Expediente">Datos contables. Número de expediente</param>
        /// <param name="Documento">Datos contables. Número de documento</param>
        /// <param name="Fecha">Fecha contabilización</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Tipo_Movimiento">Tipo de movimiento</param>
        /// <param name="Importe_Movimiento">Importe </param>
        /// <param name="Porcentaje">Porcentaje de imputación</param>
        /// <param name="Imputacion">Importe imputado</param>
        public static void Actualizar_Alprga_0001_1_3(string CadenaConexion, string Bdd_Proyectos, long Pid_0001_1_3, int? Pid_a0001, int? Pid_n0001_1, short Ejercicio, int Expediente, int Documento,
                                                                                                DateTime Fecha, string Tipo_Movimiento, decimal Importe_Movimiento, decimal? Porcentaje, decimal Imputacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1_3 Temporal = new Alprga_0001_1_3(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001_1_3 = Pid_0001_1_3,

                        ifa_0001_1_3 = Pid_a0001,
                        ino_0001_1_3 = Pid_n0001_1,
                        eje_0001_1_3 = Ejercicio,
                        exp_0001_1_3 = Expediente,
                        doc_0001_1_3 = Documento,
                        fec_0001_1_3 = Fecha.ToString("dd/MM/yyyy"),
                        tmo_0001_1_3 = Tipo_Movimiento,
                        imp_0001_1_3 = Importe_Movimiento,
                        por_0001_1_3 = Porcentaje,
                        iim_0001_1_3 = Imputacion,
                        dip_0001_1_3 = Variables_Globales.IpPrivada,
                        uua_0001_1_3 = Variables_Globales.IdUsuario,
                        fua_0001_1_3 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. IMPUTACIONES CONTABLES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_1_3">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. IMPUTACIONES CONTABLES'</param>
        public static void Eliminar_Alprga_0001_1_3(string CadenaConexion, string Bdd_Proyectos, long Pid_0001_1_3)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_1_3 Temporal = new Alprga_0001_1_3(Cadena_Conexion, Bdd_Proyectos) { ide_0001_1_3 = Pid_0001_1_3 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alprga_0001_1_3 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS. IMPUTACIONES CONTABLES'

        #region Tabla Alprga_0001_2 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES'

        /// <summary>
        /// Inserta registros tabla 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Pid_5010">Identificador registro 'AGENTES FINANCIADORS'</param>
        /// <param name="Ejercicio">Ejercicio de financiación</param>
        /// <param name="Coeficiente">Coeficiente de financiación</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alprga_0001_2(string CadenaConexion, string Bdd_Proyectos, int Pid_0001, int Pid_5010, decimal Coeficiente)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_2 Temporal = new Alprga_0001_2(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001 = Pid_0001,
                        ide_5010 = Pid_5010,
                        cfi_0001_2 = Coeficiente,
                        uar_0001_2 = Variables_Globales.IdUsuario,
                        far_0001_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_0001_2 = Variables_Globales.IpPrivada,
                        uua_0001_2 = Variables_Globales.IdUsuario,
                        fua_0001_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_0001_2 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0001_2;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_2">Identificador registro'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES'</param>
        /// <param name="Pid_5010">Identificador registro 'AGENTES FINANCIADORS'</param>
        /// <param name="Ejercicio">Ejercicio de financiación</param>
        /// <param name="Coeficiente">Coeficiente de financiación</param>
        public static void Actualizar_Alprga_0001_2(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_2, int Pid_5010, decimal Coeficiente)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_2 Temporal = new Alprga_0001_2(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001_2 = Pid_0001_2,

                        ide_5010 = Pid_5010,
                        cfi_0001_2 = Coeficiente,
                        dip_0001_2 = Variables_Globales.IpPrivada,
                        uua_0001_2 = Variables_Globales.IdUsuario,
                        fua_0001_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_2">Identificador registro 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES'</param>
        public static void Eliminar_Alprga_0001_2(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_2)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_2 Temporal = new Alprga_0001_2(Cadena_Conexion, Bdd_Proyectos) { ide_0001_2 = Pid_0001_2 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alprga_0001_2  'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES'

        #region Tabla Alprga_0001_2_1 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES. FINANCIACIÓN'

        /// <summary>
        /// Inserta registros tabla 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES. FINANCIACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS EINVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Pid_0001_2">Identificador registro 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES'</param>
        /// <param name="Ejercicio">Ejercicio ejecución</param>
        /// <param name="Expediente">Número expediente contable</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        /// <param name="Ingreso_Previsto">Total importe ingresos previstos</param>
        /// <param name="Comprometido">Importe comprometido</param>
        /// <param name="Reconocido">Importe reconocido</param>
        /// <param name="Aplicado">Importe aplicado</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alprga_0001_2_1(string CadenaConexion, string Bdd_Proyectos, int Pid_0001, int Pid_0001_2, short? Ejercicio, int? Expediente, string Clave_Organica, string Clave_Economica, string Denominacion,
                                                                                       decimal Ingreso_Previsto, decimal Comprometido, decimal Reconocido, decimal Aplicado)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_2_1 Temporal = new Alprga_0001_2_1(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001 = Pid_0001,
                        ide_0001_2 = Pid_0001_2,
                        eje_0001_2_1 = Ejercicio,
                        exp_0001_2_1 = Expediente,
                        org_0001_2_1 = Clave_Organica,
                        eco_0001_2_1 = Clave_Economica,
                        den_0001_2_1 = Denominacion,
                        pri_0001_2_1 = Ingreso_Previsto,
                        com_0001_2_1 = Comprometido,
                        rec_0001_2_1 = Reconocido,
                        iia_0001_2_1 = Aplicado,
                        uar_0001_2_1 = Variables_Globales.IdUsuario,
                        far_0001_2_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_0001_2_1 = Variables_Globales.IpPrivada,
                        uua_0001_2_1 = Variables_Globales.IdUsuario,
                        fua_0001_2_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_0001_2_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0001_2_1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES. FINANCIACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_2_1">Identificador registro 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES. FINANCIACIÓN'</param>
        /// <param name="Ejercicio">Ejercicio ejecución</param>
        /// <param name="Expediente">Número expediente contable</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        /// <param name="Ingreso_Previsto">Total importe ingresos previstos</param>
        /// <param name="Comprometido">Importe comprometido</param>
        /// <param name="Reconocido">Importe reconocido</param>
        /// <param name="Aplicado">Importe aplicado</param>
        public static void Actualizar_Alprga_0001_2_1(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_2_1, short? Ejercicio, int? Expediente, string Clave_Organica, string Clave_Economica, string Denominacion,
                                                                                              decimal Ingreso_Previsto, decimal Comprometido, decimal Reconocido, decimal Aplicado)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_2_1 Temporal = new Alprga_0001_2_1(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_0001_2_1 = Pid_0001_2_1,

                        eje_0001_2_1 = Ejercicio,
                        exp_0001_2_1 = Expediente,
                        org_0001_2_1 = Clave_Organica,
                        eco_0001_2_1 = Clave_Economica,
                        den_0001_2_1 = Denominacion,
                        pri_0001_2_1 = Ingreso_Previsto,
                        com_0001_2_1 = Comprometido,
                        rec_0001_2_1 = Reconocido,
                        iia_0001_2_1 = Aplicado,
                        dip_0001_2_1 = Variables_Globales.IpPrivada,
                        uua_0001_2_1 = Variables_Globales.IdUsuario,
                        fua_0001_2_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES. FINANCIACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_2_1">Identificador registro 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES. FINANCIACIÓN'</param>
        public static void Eliminar_Alprga_0001_2_1(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_2_1)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_0001_2_1 Temporal = new Alprga_0001_2_1(Cadena_Conexion, Bdd_Proyectos) { ide_0001_2_1 = Pid_0001_2_1 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alprga_0001_2_1 'PROYECTOS DE GASTOS E INVERSIÓN. AGENTES FINANCIADORES. FINANCIACIÓN'

        #region Tabla Alprga_5000 'TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'

        /// <summary>
        /// Inserta registros tabla 'TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Codigo">Código tipo de proyecto</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Desviacion">Semáforo cálculo desviación</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_5000(string CadenaConexion, string Bdd_Proyectos, int Codigo, string Denominacion, bool Desviacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_5000 Temporal = new Alprga_5000(Cadena_Conexion, Bdd_Proyectos)
                    {
                        cod_5000 = Codigo,
                        den_5000 = Denominacion,
                        scd_5000 = Desviacion ? (byte)1 : (byte)0,
                        uar_5000 = Variables_Globales.IdUsuario,
                        far_5000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_5000 = Variables_Globales.IpPrivada,
                        uua_5000 = Variables_Globales.IdUsuario,
                        fua_5000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_5000 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_5000;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_5000">Identificador registro 'TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Desviacion">Semáforo cálculo desviación</param>
        public static void Actualizar_Alprga_5000(string CadenaConexion, string Bdd_Proyectos, int Pid_5000, string Denominacion, bool Desviacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_5000 Temporal = new Alprga_5000(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_5000 = Pid_5000,

                        den_5000 = Denominacion,
                        scd_5000 = Desviacion ? (byte)1 : (byte)0,
                        uar_5000 = Variables_Globales.IdUsuario,
                        far_5000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_5000 = Variables_Globales.IpPrivada,
                        uua_5000 = Variables_Globales.IdUsuario,
                        fua_5000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla'TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_5000">Identificador registro 'TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'</param>
        public static void Eliminar_Alprga_5000(string CadenaConexion, string Bdd_Proyectos, int Pid_5000)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_5000 Temporal = new Alprga_5000(Cadena_Conexion, Bdd_Proyectos) { ide_5000 = Pid_5000 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alprga_5000 'TIPOS DE PROYECTOS DE GASTO E INVERSIÓN'

        #region Tabla Alprga_5010 'AGENTES FINANCIADORES'

        /// <summary>
        /// Inserta registros tabla 'AGENTES FINANCIADORES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Codigo">Código agente financiador</param>
        /// <param name="Pid_a0001">Identificador reggistro 'AUXILIARES. TERCEROS</param>
        /// <param name="Numero_Identificacion">Número de identificación fiscal</param>
        /// <param name="Razon_Social">Razón social</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_5010(string CadenaConexion, string Bdd_Proyectos, int Codigo, int Pid_a0001, string Numero_Identificacion, string Razon_Social)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_5010 Temporal = new Alprga_5010(Cadena_Conexion, Bdd_Proyectos)
                    {
                        cod_5010 = Codigo,
                        irt_5010 = Pid_a0001,
                        nid_5010 = Numero_Identificacion,
                        raz_5010 = Razon_Social,
                        uar_5010 = Variables_Globales.IdUsuario,
                        far_5010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_5010 = Variables_Globales.IpPrivada,
                        uua_5010 = Variables_Globales.IdUsuario,
                        fua_5010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_5010 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_5010;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'AGENTES FINANCIADORES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_5010">Identificador registro 'AGENTES FINANCIADORES'</param>
        /// <param name="Pid_a0001">Identificador reggistro 'AUXILIARES. TERCEROS</param>
        /// <param name="Numero_Identificacion">Número de identificación fiscal</param>
        /// <param name="Razon_Social">Razón social</param>
        public static void Actualizar_Alprga_5010(string CadenaConexion, string Bdd_Proyectos, int Pid_5010, int Pid_a0001, string Numero_Identificacion, string Razon_Social)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_5010 Temporal = new Alprga_5010(Cadena_Conexion, Bdd_Proyectos)
                    {
                        ide_5010 = Pid_5010,

                        irt_5010 = Pid_a0001,
                        nid_5010 = Numero_Identificacion,
                        raz_5010 = Razon_Social,
                        dip_5010 = Variables_Globales.IpPrivada,
                        uua_5010 = Variables_Globales.IdUsuario,
                        fua_5010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla'AGENTES FINANCIADORES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_5010">Identificador registro 'AGENTES FINANCIADORES'</param>
        public static void Eliminar_Alprga_5010(string CadenaConexion, string Bdd_Proyectos, int Pid_5010)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alprga_5010 Temporal = new Alprga_5010(Cadena_Conexion, Bdd_Proyectos) { ide_5010 = Pid_5010 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alprga_5010 'AGENTES FINANCIADORES'

    }

}