﻿using ClaseIntsa.DataBase;
using System;
using System.Data.SqlClient;

namespace Proyectos_Contabilidad
{
    public static class Auxiliares_Proyectos
    {
        /// <summary>
        /// Obtiene datos 'PROYECTOS DE GASTO E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTO E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Proyecto"> Identificador registro/Código 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Tipo">Tipo de búsqueda<br/>
        /// 0 - Identificador registro<br/>
        /// 1 - Código tipo de proyecto
        /// </param>
        /// <returns>
        /// Item1 : Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN' [ide_0001]<br/>
        /// Item2 : Matriz de datos enteros<br/>
        /// · 0 - Codigo tipo de proyecto de gasto e inversión [cod_0001]<br/>
        /// · 1 - Identificador registro 'TIPOS DE PROYECTO DE GASTO E INVERSIÓN' [ide_5000]<br/>
        /// Item3 : Matriz de datos alfabéticos<br/>
        /// · 0 - Denominación del proyecto de gasto e inversión [den_0001]<br/>
        /// · 1 - Referencia del proyecto de gasto e inversión [ref_0001]<br/>
        /// Item4 : Fecha inicio [fei_0001]<br/>
        /// Item5 : Duración [dur_0001]<br/>
        /// Item6 : Matriz de datos numéricos<br/>
        /// · 0 - Coeficiente de financiación [cfi_0001]<br/>
        /// · 1 - Importe total previsto del poyecto [tot_0001]<br/>
        /// Item7 : Semáforo cálculo de desviaciones [scd_0001]<br/>
        /// </returns>
        public static Tuple<int, int[], string[], DateTime, byte, decimal[], bool> Datos_Alprga_0001(string CadenaConexion, string Bdd_Proyectos, int Proyecto, byte Tipo = 0)
        {
            int Id_0001 = 0;
            int[] Identificadores = { 0, 0 };
            string[] Denominaciones = { "", "" };
            DateTime Fecha = DateTime.MinValue;
            byte Duracion = 0;
            decimal[] Importes = { 0, 0 };
            bool Desviacion = false;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001, cod_0001, ide_5000, den_0001, ref_0001, fei_0001, dur_0001, cfi_0001, tot_0001, scd_0001";
            string Cadena_From = $" FROM {Bdd_Proyectos}.dbo.Alprga_0001";
            string Restriccion_Codigo = $"cod_0001 = {Proyecto}";
            string Restriccion_Identificador = $"ide_0001 = {Proyecto}";
            string Cadena_Where = Tipo == 0 ? $" WHERE {Restriccion_Identificador}" : $" WHERE {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_0001 = (int)Data_Reader["ide_0001"];
                        Identificadores[0] = (int)Data_Reader["cod_0001"];
                        Identificadores[1] = (int)Data_Reader["ide_5000"];
                        Denominaciones[0] = (string)Data_Reader["den_0001"];
                        Denominaciones[1] = (string)Data_Reader["ref_0001"];
                        Fecha = Convert.ToDateTime(Data_Reader["fei_0001"]);
                        Duracion = (byte)Data_Reader["dur_0001"];
                        Importes[0] = Convert.ToDecimal(Data_Reader["cfi_0001"]);
                        Importes[1] = Convert.ToDecimal(Data_Reader["tot_0001"]);
                        Desviacion = Convert.ToBoolean(Data_Reader["scd_0001"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int[], string[], DateTime, byte, decimal[], bool>(Id_0001, Identificadores, Denominaciones, Fecha, Duracion, Importes, Desviacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'PROYECTOS DE GASTO E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTO E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Proyecto"> Identificador registro/Código 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Tipo">Tipo de búsqueda<br/>
        /// 0 - Identificador registro<br/>
        /// 1 - Código tipo de proyecto
        /// </param>
        /// <returns>
        /// Item1 : Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN' [ide_0001]<br/>
        /// Item2 : Matriz de datos enteros<br/>
        /// · 0 - Codigo tipo de proyecto de gasto e inversión [cod_0001]<br/>
        /// · 1 - Identificador registro 'TIPOS DE PROYECTO DE GASTO E INVERSIÓN' [ide_5000]<br/>
        /// · 2 - Ejercicio de finalización [anofin]<br/>
        /// Item3 : Matriz de datos alfabéticos<br/>
        /// · 0 - Denominación del proyecto de gasto e inversión [den_0001]<br/>
        /// · 1 - Referencia del proyecto de gasto e inversión [ref_0001]<br/>
        /// · 2 - Denominación tipo de proyecto de gasto e inversión [den_5000]<br/>
        /// Item4 : Fecha inicio [fei_0001]<br/>
        /// Item5 : Duración [dur_0001]<br/>
        /// Item6 : Matriz de datos numéricos<br/>
        /// · 0 - Coeficiente de financiación [cfi_0001]<br/>
        /// · 1 - Importe total previsto del poyecto [tot_0001]<br/>
        /// · 2 - Importe total previsto del poyecto [tot_0001]<br/>
        /// · 3 - Importe total previsto del poyecto [tot_0001]<br/>
        /// Item7 : Semáforo cálculo de desviaciones [scd_0001]
        /// </returns>
        public static Tuple<int, int[], string[], DateTime, byte, decimal[], bool> Datos_Alprga_v_0001(string CadenaConexion, string Bdd_Proyectos, int Proyecto, byte Tipo = 0)
        {
            int Id_0001 = 0;
            int[] Identificadores = { 0, 0, 0 };
            string[] Denominaciones = { "", "", "" };
            DateTime Fecha = DateTime.MinValue;
            byte Duracion = 0;
            decimal[] Importes = { 0, 0, 0, 0 };
            bool Desviacion = false;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001, cod_0001, ide_5000, anofin, den_0001, ref_0001, den_5000, fei_0001, dur_0001,  cfi_0001, tot_0001, pregas, preing, scd_0001";
            string Cadena_From = $" FROM {Bdd_Proyectos}.dbo.Alprga_v_0001";
            string Restriccion_Codigo = $"cod_0001 = {Proyecto}";
            string Restriccion_Identificador = $"ide_0001 = {Proyecto}";
            string Cadena_Where = Tipo == 0 ? $" WHERE {Restriccion_Identificador}" : $" WHERE {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_0001 = (int)Data_Reader["ide_0001"];
                        Identificadores[0] = (int)Data_Reader["cod_0001"];
                        Identificadores[1] = (int)Data_Reader["ide_5000"];
                        Identificadores[2] = (int)Data_Reader["anofin"];
                        Denominaciones[0] = (string)Data_Reader["den_0001"];
                        Denominaciones[1] = (string)Data_Reader["ref_0001"];
                        Denominaciones[2] = (string)Data_Reader["den_5000"];
                        Fecha = Convert.ToDateTime(Data_Reader["fei_0001"]);
                        Duracion = (byte)Data_Reader["dur_0001"];
                        Importes[0] = Convert.ToDecimal(Data_Reader["cfi_0001"]);
                        Importes[1] = Convert.ToDecimal(Data_Reader["tot_0001"]);
                        Importes[2] = Convert.ToDecimal(Data_Reader["pregas"]);
                        Importes[3] = Convert.ToDecimal(Data_Reader["preing"]);
                        Desviacion = Convert.ToBoolean(Data_Reader["scd_0001"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int[], string[], DateTime, byte, decimal[], bool>(Id_0001, Identificadores, Denominaciones, Fecha, Duracion, Importes, Desviacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTO E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_1">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'</param>
        /// <returns>
        /// Item1 : Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS' [ide_0001_1]<br/>
        /// Item2 : Ejercicio de ejecución [eje_0001_1]<br/>
        /// Item3 : Número de expediente contable [exp_0001_1]<br/>
        /// Item4 : Matriz de datos alfabéticos<br/>
        /// · 0 - Clave orgánica [org_0001_1]<br/>
        /// · 1 - Clave funcional [fun_0001_1]<br/>
        /// · 2 - Clave ECONÓMICA [eco_0001_1]<br/>
        /// · 3 - Aplicación presupuestaria normalizada (OOOOO-FFFFF-EEEEEEE) [aplpre]<br/>
        /// · 4 - Denominación de la aplicación presupuestaria [den_0001_1]<br/>
        /// Item5 : Matriz de datos numéricos<br/>
        /// · 0 - Total importe PREVISTO de gastos [prg_0001_1]<br/>
        /// · 1 - Importe compromisos [com_0001_1]<br/>
        /// · 2 - Importe reconocientos [com_0001_1]<br/>
        /// · 3 - Importe pagos [pag_0001_1]
        /// </returns>
        public static Tuple<int, short, int?, string[], decimal[]> Datos_Alprga_0001_1(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_1)
        {
            int Id_0001_1 = 0;
            short Ejercicio = 0;
            int? Expediente = null;
            string[] Denominaciones = { "", "", "", "", "" };
            decimal[] Importes = { 0, 0, 0, 0 };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001_1, eje_0001_1, exp_0001_1, org_0001_1, fun_0001_1, eco_0001_1, Funciones_Generales.dbo.Normaliza_Aplicacion_3 (org_0001_1, fun_0001_1, eco_0001_1) aplpre,";
            Cadena_Select += " den_0001_1, prg_0001_1, com_0001_1, rec_0001_1, pag_0001_1";
            string Cadena_From = $" FROM {Bdd_Proyectos}.dbo.Alprga_0001_1";
            string Restriccion_Identificador = $"ide_0001_1 = {Pid_0001_1}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_0001_1 = (int)Data_Reader["ide_0001_1"];
                        Ejercicio = (short)Data_Reader["eje_0001_1"];
                        Expediente = Data_Reader["exp_0001_1"] == DBNull.Value ? null : (int?)Convert.ToInt32(Data_Reader["exp_0001_1"]);
                        Denominaciones[0] = (string)Data_Reader["org_0001_1"];
                        Denominaciones[1] = (string)Data_Reader["fun_0001_1"];
                        Denominaciones[2] = (string)Data_Reader["eco_0001_1"];
                        Denominaciones[3] = (string)Data_Reader["aplpre"];
                        Denominaciones[4] = (string)Data_Reader["den_0001_1"];
                        Importes[0] = Convert.ToDecimal(Data_Reader["prg_0001_1"]);
                        Importes[1] = Convert.ToDecimal(Data_Reader["com_0001_1"]);
                        Importes[2] = Convert.ToDecimal(Data_Reader["rec_0001_1"]);
                        Importes[3] = Convert.ToDecimal(Data_Reader["pag_0001_1"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, int?, string[], decimal[]>(Id_0001_1, Ejercicio, Expediente, Denominaciones, Importes);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'PROYECTOS DE GASTO E INVERSIÓN. AGENTES FINANCIADORES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTO E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN'</param>
        /// <param name="Pid_5010">Identificador registro 'AGENTES FINANCIADORES'</param>
        /// <param name="Codido_Agente">Código agente financiador</param>
        /// <returns>
        /// Item1 : Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. AGENTES FINANCIADORES' [ide_0001_2]<br/>
        /// Item2 : Matriz de datos identificadores<br/>
        /// · 0 - Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN' [ide_0001]<br/>
        /// · 1 - Identificador registro 'AGENTES FINANCIADORES' [ide_5010]<br/>
        /// · 2 - Identificador registro 'TERCEROS' [irt_5010]<br/>
        /// Item3 : Código agente financiador [cod_5010]<br/>
        /// Item4 : Número de identificación fiscal<br/>
        /// Item5 : Razón social/Nombre<br/>
        /// Item6 : Coeficiente de financiación
        /// </returns>
        public static Tuple<int, int[], int, string, string, decimal> Datos_v_Alprga_0001_2(string CadenaConexion, string Bdd_Proyectos, int Pid_0001, int Pid_5010, int Codido_Agente)
        {
            int Id_0001_2 = 0;
            int[] Identificadores = { 0, 0, 0 };
            int Agente = 0;
            string Numero_Identificacion = "";
            string Razon_Social = "";
            decimal Coeficiente = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001_2, ide_0001, ide_5010, irt_5010,  cod_5010, nid_5010, raz_5010, cfi_0001_2";
            string Cadena_From = $" FROM {Bdd_Proyectos}.dbo.Alprga_v_0001_2";
            string Restriccion_Identificador = $"ide_0001 = {Pid_0001}";
            string Restriccion_Id_Agente = $"ide_5010 = {Pid_5010}";
            string Restriccion_Agente = $"cod_5010 = {Codido_Agente}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador} AND {Restriccion_Id_Agente} AND {Restriccion_Agente}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_0001_2 = (int)Data_Reader["ide_0001_2"];
                        Identificadores[0] = (int)Data_Reader["ide_0001"];
                        Identificadores[1] = (int)Data_Reader["ide_5010"];
                        Identificadores[2] = (int)Data_Reader["irt_5010"];
                        Agente = (int)Data_Reader["cod_5010"];
                        Numero_Identificacion = (string)Data_Reader["nid_5010"];
                        Razon_Social = (string)Data_Reader["raz_5010"];
                        Coeficiente = Convert.ToDecimal(Data_Reader["cfi_0001_2"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int[], int, string, string, decimal>(Id_0001_2, Identificadores, Agente, Numero_Identificacion, Razon_Social, Coeficiente);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'PROYECTOS DE GASTO E INVERSIÓN. AGENTES FINANCIADORES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTO E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_0001_2">Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. AGENTES FINANCIADORES'</param>
        /// <returns>
        /// Item1 : Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN. AGENTES FINANCIADORES' [ide_0001_2]<br/>
        /// Item2 : Matriz de datos identificadores<br/>
        /// · 0 - Identificador registro 'PROYECTOS DE GASTO E INVERSIÓN' [ide_0001]<br/>
        /// · 1 - Identificador registro 'AGENTES FINANCIADORES' [ide_5010]<br/>
        /// · 2 - Identificador registro 'TERCEROS' [irt_5010]<br/>
        /// Item3 : Código agente financiador [cod_5010]<br/>
        /// Item4 : Número de identificación fiscal<br/>
        /// Item5 : Razón social/Nombre<br/>
        /// Item6 : Coeficiente de financiación
        /// </returns>
        public static Tuple<int, int[], int, string, string, decimal> Datos_v_Alprga_0001_2(string CadenaConexion, string Bdd_Proyectos, int Pid_0001_2)
        {
            int Id_0001_2 = 0;
            int[] Identificadores = { 0, 0, 0 };
            int Agente = 0;
            string Numero_Identificacion = "";
            string Razon_Social = "";
            decimal Coeficiente = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001_2, ide_0001, ide_5010, irt_5010, cod_5010, nid_5010, raz_5010, cfi_0001_2";
            string Cadena_From = $" FROM {Bdd_Proyectos}.dbo.Alprga_v_0001_2";
            string Restriccion_Identificador = $"ide_0001_2 = {Pid_0001_2}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_0001_2 = (int)Data_Reader["ide_0001_1"];
                        Identificadores[0] = (int)Data_Reader["ide_0001"];
                        Identificadores[1] = (int)Data_Reader["ide_5010"];
                        Identificadores[2] = (int)Data_Reader["irt_5010"];
                        Agente = (int)Data_Reader["cod_5010"];
                        Numero_Identificacion = (string)Data_Reader["nid_5010"];
                        Razon_Social = (string)Data_Reader["raz_5010"];
                        Coeficiente = Convert.ToDecimal(Data_Reader["cfi_0001_2"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int[], int, string, string, decimal>(Id_0001_2, Identificadores, Agente, Numero_Identificacion, Razon_Social, Coeficiente);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE PROYECTO DE GASTO E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Valor_Campo">Identificador registro 'TIPOS DE PROYECTO DE GASTO E INVERSIÓN'</param>
        /// <param name="Tipo">Tipo de búsqueda<br/>
        /// 0 - Identificador registro<br/>
        /// 1 - Código tipo de proyecto
        /// </param>
        /// <returns>
        /// Item1 : Identificador registro 'TIPOS DE PROYECTO DE GASTO E INVERSIÓN' [ide_5000]<br/>
        /// Item2 : Código tipo de proyecto [cod_5000]<br/>
        /// Item3 : Denominación [den_5000]
        /// Item4 : Semáforo cálculo desviaciones [scd_5000]
        /// </returns>
        public static Tuple<int, int, string, bool> Datos_Alprga_5000(string CadenaConexion, string Bdd_Proyectos, int Valor_Campo, byte Tipo = 0)
        {
            int Id_5000 = 0;
            int Codigo = 0;
            string Denominacion = "";
            bool Desviacion = false;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5000, cod_5000, den_5000, scd_5000";
            string Cadena_From = $" FROM {Bdd_Proyectos}.dbo.Alprga_5000";
            string Restriccion_Codigo = $"cod_5000 = {Valor_Campo}";
            string Restriccion_Identificador = $"ide_5000 = {Valor_Campo}";
            string Cadena_Where = Tipo == 0 ? $" WHERE {Restriccion_Identificador}" : $" WHERE {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5000 = (int)Data_Reader["ide_5000"];
                        Codigo = (int)Data_Reader["cod_5000"];
                        Denominacion = (string)Data_Reader["den_5000"];
                        Desviacion = Convert.ToBoolean(Data_Reader["scd_5000"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int, string, bool>(Id_5000, Codigo, Denominacion, Desviacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'AGENTES FINANCIADORES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS/INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Valor_Campo">Identificador registro 'AGENTES FINANCIADORES'</param>
        /// <param name="Tipo">Tipo de búsqueda<br/>
        /// 0 - Identificador registro<br/>
        /// 1 - Código de agente financiador<br/>
        /// 2 - Identificador registro 'AUXILIARES. TERCEROS'
        /// </param>
        /// <returns>
        /// Item1 : Identificador registro 'AGENTES FINANCIADORES' [ide_5010]<br/>
        /// Item2 : Código de agente financiador [cod_5010]<br/>
        /// Item3 : Identificador reggistro 'AUXILIARES. TERCEROS' [irt_5010]<br/>
        /// Item4 : Número de identificación fiscal [nid_5010]
        /// Item5 : Razón social [raz_5010]
        /// </returns>
        public static Tuple<int, int, int, string, string> Datos_Alprga_5010(string CadenaConexion, string Bdd_Proyectos, int Valor_Campo, byte Tipo = 0)
        {
            int Id_5010 = 0;
            int Codigo = 0;
            int Id_a0001 = 0;
            string Numero_Identificacion = "";
            string Razon_Social = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5010, cod_5010, irt_5010, nid_5010, raz_5010";
            string Cadena_From = $" FROM {Bdd_Proyectos}.dbo.Alprga_5010";
            string Restriccion_Identificador = $"ide_5010 = {Valor_Campo}";
            string Restriccion_Codigo = $"cod_5010 = {Valor_Campo}";
            string Restriccion_Tercero = $"irt_5010 = {Valor_Campo}";
            string Cadena_Where = Tipo == 0 ? $" WHERE {Restriccion_Identificador}" : Tipo == 1 ? $" WHERE {Restriccion_Codigo}" : $" WHERE {Restriccion_Tercero}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5010 = (int)Data_Reader["ide_5010"];
                        Codigo = (int)Data_Reader["cod_5010"];
                        Id_a0001 = (int)Data_Reader["irt_5010"];
                        Numero_Identificacion = (string)Data_Reader["nid_5010"];
                        Razon_Social = (string)Data_Reader["raz_5010"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int, int, string, string>(Id_5010, Codigo, Id_a0001, Numero_Identificacion, Razon_Social);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica  el siguiente número de la secuencia
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Secuencia">Nombre de la secuencia</param>
        /// <param name="Id_Funcion">Identificador de funcion a utilizar<br/>
        /// 1 - Código proyecto de gasto e inversión<br/>
        /// 2 - Código tipo de proyecto de gastos e inversión
        /// 3 - Código agente financiador
        /// </param>
        /// <returns>Numero de secuencia válido</returns>
        public static int Verificar_Secuencia(string CadenaConexion, string Bdd_Proyectos, string Secuencia, byte Id_Funcion)
        {
            bool Secuencia_Valida = false;
            int Numero_Secuencia = 0;
            int Id_Secuencia = 0;

            while (!Secuencia_Valida)
            {
                Numero_Secuencia = Secuencias.Siguiente_Secuencia(CadenaConexion, Bdd_Proyectos, Secuencia);

                if (Numero_Secuencia != 0)
                {
                    switch (Id_Funcion)
                    {
                        case 1:             // CÓDIGO PROYECTO DE GASTOS E INVERSIÓN
                            Id_Secuencia = Datos_Alprga_0001(CadenaConexion, Bdd_Proyectos, Numero_Secuencia, (byte)1).Item1;

                            break;

                        case 2:             // CÓDIGO TIPOS DE PROYECTO DE GASTOS E INVERSIÓN
                            Id_Secuencia = Datos_Alprga_5000(CadenaConexion, Bdd_Proyectos, Numero_Secuencia, (byte)1).Item1;

                            break;

                        case 3:             // CÓDIGO AGENTES FINANCIADORES
                            Id_Secuencia = Datos_Alprga_5010(CadenaConexion, Bdd_Proyectos, Numero_Secuencia, (byte)1).Item1;

                            break;
                    }

                    if (Id_Secuencia == 0)
                        Secuencia_Valida = true;

                }
            }

            return Numero_Secuencia;
        }

        /// <summary>
        /// Verifica si existe el tipo de proyecto 'PROYECTOS DE GASTOS E INVERSIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_5000">Identificador registro 'TIPOS DE PROYECTOS DE GASTOS E INVERSIÓN'</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Ocurrencias_Alprga_0001(string CadenaConexion, string Bdd_Proyectos, int Pid_5000)
        {
            bool Existe;
            string Cadena_Sql;
            string Cadena_Select = "SELECT TOP (1) ide_0001";
            string Cadena_From = $" FROM {Bdd_Proyectos}.dbo.Alprga_0001";
            string Restriccion_Identificador = $"ide_5000 = {Pid_5000}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica si existe el agente financiador 'PROYECTOS DE GASTO E INVERSIÓN. FINANCIACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTOS E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Pid_5010">Identificador registro 'AGENTES FINANCIADORES'</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Ocurrencias_Alprga_0001_1_1(string CadenaConexion, string Bdd_Proyectos, int Pid_5010)
        {
            bool Existe;
            string Cadena_Sql;
            string Cadena_Select = "SELECT TOP (1) ide_0001_1_1";
            string Cadena_From = $" FROM {Bdd_Proyectos}.dbo.Alprga_0001_1_1";
            string Restriccion_Identificador = $"ide_5010 = {Pid_5010}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene la matriz de los campos a actualizar según el tipo de movimiento 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'
        /// </summary>
        /// <param name="Tipo_Movimiento">Tipo de movimiento</param>
        /// <param name="Importe">Importe movimiento</param>
        /// <returns>
        /// Item1 :  Matriz de campos a actualizar<br/>
        /// Item2 : Importe a actualizar con signo
        /// </returns>
        public static Tuple<string[], decimal> Campos_Alprga_0001_1(string Tipo_Movimiento, decimal Importe)
        {
            string[] Campos = { "" };
            decimal Importe_Actualizacion = Tipo_Movimiento.Trim().Contains("/") ? -Importe : Importe;

            switch (Tipo_Movimiento.Trim())
            {
                case "ADOP":
                case "ADOR":
                case "ADOP/":
                case "ADOR/":
                    Array.Resize(ref Campos, 2);

                    Campos[0] = "com_0001_1";
                    Campos[1] = "rec_0001_1";

                    break;

                case "O":
                case "O/":
                    Campos[0] = "rec_0001_1";

                    break;

                case "RP":
                case "RP/":
                    Campos[0] = "pag_0001_1";

                    break;

                default:
                    Campos[0] = "";
                    Importe_Actualizacion = 0;

                    break;
            }

            return Tuple.Create<string[], decimal>(Campos, Importe_Actualizacion);
        }

        /// <summary>
        /// Obtiene la matriz de los campos a actualizar según el tipo de movimiento 'PROYECTOS DE GASTO E INVERSIÓN. GASTOS'
        /// </summary>
        /// <param name="Tipo_Movimiento">Tipo de movimiento<br/>
        /// 1 - COMPROMETIDO Y RECONOCIDO (+)<br/>
        /// 2 - COMPROMETIDO Y RECONOCIDO(-)<br/>
        /// 3 - COMPROMETIDO(+)<br/>
        /// 4 - COMPROMETIDO(-)<br/>
        /// 5 - RECONOCIDO(+)<br/>
        /// 6 - RECONOCIDO(-)
        /// </param>
        /// <param name="Importe">Importe movimiento</param>
        /// <returns>
        /// Item1 :  Matriz de campos a actualizar<br/>
        /// Item2 : Importe a actualizar con signo
        /// </returns>
        public static Tuple<string[], byte, decimal> Campos_Alprga_0001_1(byte Tipo_Movimiento, decimal Importe)
        {
            string[] Campos = { "" };
            byte Tipo_Imputacion = Tipo_Movimiento;
            decimal Importe_Actualizacion = 0;

            if (Importe < 0
                 &&
                 Tipo_Imputacion % 2 == 1)
            {
                Tipo_Imputacion++;
                Importe = Math.Abs(Importe);
            }

            switch (Tipo_Imputacion)
            {
                case 1:         // COMPROMETIDO Y RECONOCIDO(+)
                    Array.Resize(ref Campos, 2);

                    Campos[0] = "com_0001_1";
                    Campos[1] = "rec_0001_1";
                    Importe_Actualizacion = Importe;

                    break;

                case 2:         // COMPROMETIDO Y RECONOCIDO(-)
                    Array.Resize(ref Campos, 2);

                    Campos[0] = "com_0001_1";
                    Campos[1] = "rec_0001_1";
                    Importe_Actualizacion = -Importe;

                    break;

                case 3:         // COMPROMETIDO (+)
                    Campos[0] = "com_0001_1";
                    Importe_Actualizacion = Importe;

                    break;

                case 4:         // COMPROMETIDO (-)
                    Campos[0] = "com_0001_1";
                    Importe_Actualizacion = -Importe;

                    break;

                case 5:         // RECONOCIDO(+)
                    Campos[0] = "rec_0001_1";
                    Importe_Actualizacion = Importe;

                    break;

                case 6:         // RECONOCIDO(-)
                    Campos[0] = "rec_0001_1";
                    Importe_Actualizacion = -Importe;

                    break;

                default:
                    Campos[0] = "";

                    break;
            }

            return Tuple.Create<string[], byte, decimal>(Campos, Tipo_Imputacion, Importe_Actualizacion);
        }
    }

    public static class Desviaciones
    {
        /// <summary>
        /// Genera la SQL de cáculo de las desviaciones de financiación
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTO E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Ejercicio">Ejercicio actual</param>
        /// <param name="Tipo_Calculo">Tipo de cálculo<br/>
        /// 1 - Desviación acumulada (Hasta el ejercicio actual)<br/>
        /// 2 - Desviación ejercicio actual<br/>
        /// 3 - Desviación ejercicios anteriores (Hasta el ejercicio anterior al actual)<br/>
        /// 4 - Desviación todos los ejercicios (Anteriores y posteriores)
        /// </param>
        /// <param name="Tipo_Desviacion">Cálculo de tipo de desviación<br/>
        /// 1 - Desviaciones positivas<br/>
        /// 2 - Desviaciones negativas<br/>
        /// 3 - Desviaciones cero<br/>
        /// 4 - Desviaciones positivas y negativas
        /// </param>
        /// <param name="Proyectos">Matriz de códigos de proyectos de gastos e inversión</param>
        /// <param name="Agentes">Matriz de códigos de agentes financiadores</param>
        /// <returns>Cadena 'SQL'</returns>
        public static string Calcular_Desviaciones_Old(string CadenaConexion, string Bdd_Proyectos, short Ejercicio, byte Tipo_Calculo, byte Tipo_Desviacion, int[] Proyectos, int[] Agentes)
        {
            string[] Sub_Cadena_Sql = { "", "", "" };
            string[] Sub_Cadena_Select = { "", "", "" };
            string[] Sub_Cadena_From = { "", "", "" };

            string[] Cadena_Ejercicio = { "", "" };

            string Restriccion_Fechas = $"{Ejercicio} BETWEEN anoini AND anofin";
            string Restriccion_Calculo = "scd_0001 = 1";
            string Restricccion_Desviaciones;
            string Restricccion_Proyectos;
            string Cadena_Sql;
            string Cadena_Select;
            string Cadena_From;
            string Cadena_Where = $"{Restriccion_Fechas} AND {Restriccion_Calculo}";

            if (Proyectos.Length == 1)
                Restricccion_Proyectos = Proyectos[0] != 0 ? $"cod_0001 = {Proyectos[0]}" : "";
            else
                Restricccion_Proyectos = Proyectos[0] == 0 && Proyectos[1] == 0 ? "" : $"cod_0001 BETWEEN {Proyectos[0]} AND {Proyectos[1]}";

            Cadena_Where += Restricccion_Proyectos != string.Empty ? $" AND {Restricccion_Proyectos}" : "";

            switch (Tipo_Calculo)
            {
                case 1:             // DESVIACIÓN ACUMULADA
                    Cadena_Ejercicio[0] = $"* IIF (eje_0001_1 <= {Ejercicio}, 1, 0)";
                    Cadena_Ejercicio[1] = $"* IIF (eje_0001_2 <= {Ejercicio}, 1, 0)";

                    break;

                case 2:             // DESVIACIÓN EJERCICIO ACTUAL
                    Cadena_Ejercicio[0] = $"* IIF (eje_0001_1 = {Ejercicio}, 1, 0)";
                    Cadena_Ejercicio[1] = $"* IIF (eje_0001_2 = {Ejercicio}, 1, 0)";

                    break;

                case 3:             // DESVIACIÓN EJERCICIOS ANTERIORES
                    Cadena_Ejercicio[0] = $"* IIF (eje_0001_1 < {Ejercicio}, 1, 0)";
                    Cadena_Ejercicio[1] = $"* IIF (eje_0001_2 < {Ejercicio}, 1, 0)";

                    break;

                case 4:             // DESVIACIÓN TODOS LOS EJERCICIOS
                    Cadena_Ejercicio[0] = $" * 1";
                    Cadena_Ejercicio[1] = $" * 1";

                    break;

            }

            // SUBCADENA SQL DE OBLIGACIONES RECONOCIDAS
            Sub_Cadena_Select[0] = $"ISNULL ((SELECT SUM (ISNULL (rec_0001_1, 0) {Cadena_Ejercicio[0]})";
            Sub_Cadena_From[0] = $" FROM {Bdd_Proyectos}.dbo.alprga_0001_1 WHERE alprga_0001_1.ide_0001 = alprga_v_0001.ide_0001), 0)";

            // SUBCADENA SQL DE DERECHOS RECONOCIDOS
            Sub_Cadena_Select[1] = $"ISNULL ((SELECT SUM (ISNULL (rec_0001_2, 0) {Cadena_Ejercicio[1]})";
            Sub_Cadena_From[1] = $" FROM {Bdd_Proyectos}.dbo.alprga_0001_2 WHERE alprga_0001_2.ide_0001 = alprga_v_0001.ide_0001), 0)";

            // SUBCADENA SQL DE ACUMULADO DE COEFICIENTES
            Sub_Cadena_Select[2] = $"(SELECT IIF (SUM (cfi_0001_2) > 100, 100.00, SUM (cfi_0001_2))";
            Sub_Cadena_From[2] = $" FROM {Bdd_Proyectos}.dbo.alprga_0001_2 WHERE alprga_0001_2.ide_0001 = alprga_v_0001.ide_0001)";

            for (byte i = 0; i < Sub_Cadena_Sql.Length; i++)
                Sub_Cadena_Sql[i] = $"{Sub_Cadena_Select[i]}{Sub_Cadena_From[i]}";

            switch (Tipo_Desviacion)
            {
                case 1:             // DESVIACIONES POSITIVAS
                    Restricccion_Desviaciones = $"{Sub_Cadena_Sql[1]} - ROUND ({Sub_Cadena_Sql[0]} * {Sub_Cadena_Sql[2]} / 100, 02) > 0";
                    Cadena_Where += $" AND {Restricccion_Desviaciones}";
                    break;

                case 2:             // DESVIACIONES NEGATIVAS
                    Restricccion_Desviaciones = $"{Sub_Cadena_Sql[1]} - ROUND ({Sub_Cadena_Sql[0]} * {Sub_Cadena_Sql[2]} / 100, 02) < 0";
                    Cadena_Where += $" AND {Restricccion_Desviaciones}";

                    break;

                case 3:             // DESVIACIONES CERO
                    Restricccion_Desviaciones = $"{Sub_Cadena_Sql[1]} - ROUND ({Sub_Cadena_Sql[0]} * {Sub_Cadena_Sql[2]} / 100, 02) = 0";
                    Cadena_Where += $" AND {Restricccion_Desviaciones}";

                    break;

                default:             // TODAS LAS DESVIACIONES NEGATIVAS

                    break;
            }

            Cadena_Select = "SELECT ide_0001, cod_0001, den_0001, anoini, anofin,";
            Cadena_Select += $" {Sub_Cadena_Sql[0]} oblrec, {Sub_Cadena_Sql[1]} derrec, {Sub_Cadena_Sql[2]} coefin,";
            Cadena_Select += $" ABS ({Sub_Cadena_Sql[1]} - ROUND ({Sub_Cadena_Sql[0]}  * {Sub_Cadena_Sql[2]} / 100, 02)) desabs,";
            Cadena_Select += $" {Sub_Cadena_Sql[1]} - ROUND ({Sub_Cadena_Sql[0]}  * {Sub_Cadena_Sql[2]} / 100, 02) impdes, cfi_0001";
            Cadena_From = $" FROM {Bdd_Proyectos}.dbo.alprga_v_0001";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From} WHERE {Cadena_Where}";

            return Cadena_Sql;
        }

        /// <summary>
        /// Genera la SQL de cáculo de las desviaciones de financiación
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Proyectos">Nombre BDD 'PROYECTOS DE GASTO E INVERSIÓN (FINANCIACIÓN AFECTADA)'</param>
        /// <param name="Ejercicio">Ejercicio actual</param>
        /// <param name="Tipo_Calculo">Tipo de cálculo<br/>
        /// 1 - Desviación acumulada (Hasta el ejercicio actual)<br/>
        /// 2 - Desviación ejercicio actual<br/>
        /// 3 - Desviación ejercicios anteriores (Hasta el ejercicio anterior al actual)<br/>
        /// 4 - Desviación todos los ejercicios (Anteriores y posteriores)
        /// </param>
        /// <param name="Tipo_Desviacion">Cálculo de tipo de desviación<br/>
        /// 1 - Desviaciones positivas<br/>
        /// 2 - Desviaciones negativas<br/>
        /// 3 - Desviaciones cero<br/>
        /// 4 - Desviaciones positivas y negativas
        /// </param>
        /// <param name="Proyectos">Matriz de códigos de proyectos de gastos e inversión</param>
        /// <param name="Agentes">Matriz de códigos de agentes financiadores</param>
        /// <returns>Cadena 'SQL'</returns>
        public static string Calcular_Desviaciones(string CadenaConexion, string Bdd_Proyectos, short Ejercicio, byte Tipo_Calculo, byte Tipo_Desviacion, int[] Proyectos, int[] Agentes)
        {
            string[] Sub_Cadena_Sql = { "", "", "" };
            string[] Sub_Cadena_Select = { "", "", "" };
            string[] Sub_Cadena_From = { "", "", "" };
            string[] Sub_Cadena_Where = { "", "", "" };
            string[] Sub_Cadena_Group = { "", "", "" };

            string[] Cadena_Ejercicio = { "", "" };

            string Restriccion_Fechas = $"{Ejercicio} BETWEEN anoini AND anofin";
            string Restriccion_Calculo = "scd_0001 = 1";
            string Restricccion_Desviaciones;
            string Restricccion_Proyectos = "";
            string Cadena_Sql;
            string Cadena_Select;
            string Cadena_From;
            string Cadena_Where;

            if (Proyectos.Length == 1)
                Restricccion_Proyectos = Proyectos[0] != 0 ? $"cod_0001 = {Proyectos[0]}" : "";
            else
            {
                if (Proyectos[0] == 0 && Proyectos[1] == 0)
                    Restricccion_Proyectos = "";

                if (Proyectos[0] == 0 && Proyectos[1] != 0)
                    Restricccion_Proyectos = $"cod_0001 BETWEEN {Proyectos[0]} AND {Proyectos[1]}";

                if (Proyectos[0] != 0 && Proyectos[1] == 0)
                    Restricccion_Proyectos = $"cod_0001 BETWEEN {Proyectos[0]} AND {int.MaxValue}";

                if (Proyectos[0] != 0 && Proyectos[1] != 0)
                    Restricccion_Proyectos = $"cod_0001 BETWEEN {Proyectos[0]} AND {Proyectos[1]}";
            }

            if (Restricccion_Proyectos != string.Empty)
                Cadena_Where = $"{Restricccion_Proyectos}";
            else
                Cadena_Where = "";

            switch (Tipo_Calculo)
            {
                case 1:             // DESVIACIÓN ACUMULADA
                    Cadena_Ejercicio[0] = $" * IIF (ISNULL (eje_0001_1, 0) <= {Ejercicio}, 1, 0)";
                    Cadena_Ejercicio[1] = $"* IIF (ISNULL (eje_0001_2_1, 0) <= {Ejercicio}, 1, 0)";

                    break;

                case 2:             // DESVIACIÓN EJERCICIO ACTUAL
                    Cadena_Ejercicio[0] = $" * IIF (ISNULL (eje_0001_1, 0) = {Ejercicio}, 1, 0)";
                    Cadena_Ejercicio[1] = $" * IIF (ISNULL (eje_0001_2_1, 0) = {Ejercicio}, 1, 0)";

                    break;

                case 3:             // DESVIACIÓN EJERCICIOS ANTERIORES
                    Cadena_Ejercicio[0] = $" * IIF (ISNULL (eje_0001_1, 0) < {Ejercicio}, 1, 0)";
                    Cadena_Ejercicio[1] = $" * IIF (ISNULL (eje_0001_2_1, 0) < {Ejercicio}, 1, 0)";

                    break;

                case 4:             // DESVIACIÓN TODOS LOS EJERCICIOS
                    Cadena_Ejercicio[0] = $" * 1";
                    Cadena_Ejercicio[1] = $" * 1";
                    break;
            }

            // SUBCADENA SQL DE ACUMULADO DE COEFICIENTES
            Sub_Cadena_Select[0] = $"ISNULL ((SELECT MAX (cfi_0001_2)";
            Sub_Cadena_From[0] = $" FROM {Bdd_Proyectos}.dbo.alprga_v_0001_2";
            Sub_Cadena_Where[0] = " WHERE alprga_v_0001_2.ide_0001_2 = MAX (alprga_v_0001_2_1.ide_0001_2)), 0)";

            Sub_Cadena_Sql[0] = $"{Sub_Cadena_Select[0]}{Sub_Cadena_From[0]}{Sub_Cadena_Where[0]}";

            // SUBCADENA SQL DE OBLIGACIONES RECONOCIDAS
            Sub_Cadena_Select[1] = $"SELECT alprga_v_0001.ide_0001, cod_0001, MAX (den_0001) denapl, MAX (anoini) AS anoini, MAX (anofin) AS anofin, SUM (ISNULL (rec_0001_1, 0){Cadena_Ejercicio[0]}) AS oblrec";
            Sub_Cadena_From[1] = $" FROM {Bdd_Proyectos}.dbo.alprga_v_0001 LEFT OUTER JOIN {Bdd_Proyectos}.dbo.alprga_0001_1";
            Sub_Cadena_From[1] += " ON alprga_v_0001.ide_0001 = alprga_0001_1.ide_0001";
            Sub_Cadena_Where[1] = $" WHERE {Restriccion_Fechas} AND {Restriccion_Calculo}";
            Sub_Cadena_Group[1] = " GROUP BY alprga_v_0001.ide_0001, cod_0001";

            // SUBCADENA SQL DE DERECHOS RECONOCIDOS
            Sub_Cadena_Select[2] = $"SELECT alprga_v_0001.ide_0001, SUM (ISNULL (rec_0001_2_1, 0){Cadena_Ejercicio[1]}) AS derrec,{Sub_Cadena_Sql[0]} coefin";
            Sub_Cadena_From[2] = $" FROM {Bdd_Proyectos}.dbo.alprga_v_0001 LEFT OUTER JOIN {Bdd_Proyectos}.dbo.alprga_v_0001_2_1";
            Sub_Cadena_From[2] += " ON alprga_v_0001.ide_0001 = alprga_v_0001_2_1.ide_0001";
            Sub_Cadena_Where[2] = $" WHERE {Restriccion_Fechas} AND {Restriccion_Calculo}";
            Sub_Cadena_Group[2] = " GROUP BY alprga_v_0001.ide_0001, cod_0001";


            for (byte i = 1; i < Sub_Cadena_Sql.Length; i++)
                Sub_Cadena_Sql[i] = $"{Sub_Cadena_Select[i]}{Sub_Cadena_From[i]}{Sub_Cadena_Where[i]}{Sub_Cadena_Group[i]}";

            switch (Tipo_Desviacion)
            {
                case 1:             // DESVIACIONES POSITIVAS
                    Restricccion_Desviaciones = $"derrec - ROUND (oblrec * coefin / 100, 02) > 0";
                    Cadena_Where += Cadena_Where != string.Empty ? $" AND {Restricccion_Desviaciones}" : $"{Restricccion_Desviaciones}";
                    break;

                case 2:             // DESVIACIONES NEGATIVAS
                    Restricccion_Desviaciones = $"derrec - ROUND (oblrec * coefin / 100, 02) < 0";
                    Cadena_Where += Cadena_Where != string.Empty ? $" AND {Restricccion_Desviaciones}" : $"{Restricccion_Desviaciones}";

                    break;

                case 3:             // DESVIACIONES CERO
                    Restricccion_Desviaciones = $"derrec - ROUND (oblrec * coefin / 100, 02) = 0";
                    Cadena_Where += Cadena_Where != string.Empty ? $" AND {Restricccion_Desviaciones}" : $"{Restricccion_Desviaciones}";

                    break;

                case 4:             // DESVIACIONES DIFERENTES DE CERO
                    Restricccion_Desviaciones = $"derrec - ROUND (oblrec * coefin / 100, 02) != 0";
                    Cadena_Where += Cadena_Where != string.Empty ? $" AND {Restricccion_Desviaciones}" : $"{Restricccion_Desviaciones}";

                    break;

                default:             // TODAS LAS DESVIACIONES

                    break;
            }

            Cadena_Select = "SELECT Gastos.ide_0001, cod_0001, denapl, anoini, anofin, oblrec, coefin, derrec,";
            Cadena_Select += " derrec - ROUND (oblrec * coefin / 100, 02) impdes , ABS (derrec - ROUND (oblrec * coefin / 100, 02)) desabs";
            Cadena_From = $" FROM ({Sub_Cadena_Sql[1]}) Gastos INNER JOIN ({Sub_Cadena_Sql[2]}) Ingresos ON Gastos.ide_0001 = Ingresos.ide_0001 ";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}";

            if (Cadena_Where != string.Empty)
                Cadena_Sql += $" WHERE {Cadena_Where}";

            return Cadena_Sql;
        }
    }

}