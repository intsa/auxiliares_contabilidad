using System;
using System.Data;
using System.Data.SqlClient;

public class Gesfac_0001_4
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_4;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private string _frs_0001_4;                                     // 3    
    private string _fat_0001_4;                                     // 4    
    private string _fao_0001_4;                                     // 5    
    private string _fca_0001_4;                                     // 6    
    private string _fpp_0001_4;                                     // 7    
    private string _fpr_0001_4;                                     // 8    
    private short _dpl_0001_4;                                      // 9    
    private int _uar_0001_4;                                        // 10    
    private string _far_0001_4;                                     // 11    
    private string _dip_0001_4;                                     // 12    
    private int _uua_0001_4;                                        // 13    
    private string _fua_0001_4;                                     // 14    

    #endregion

    #region Propiedades

    public int ide_0001_4
    {
        get { return _ide_0001_4; }
        set { _ide_0001_4 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public string frs_0001_4
    {
        get { return _frs_0001_4; }
        set { _frs_0001_4 = value; }
    }
    public string fat_0001_4
    {
        get { return _fat_0001_4; }
        set { _fat_0001_4 = value; }
    }
    public string fao_0001_4
    {
        get { return _fao_0001_4; }
        set { _fao_0001_4 = value; }
    }
    public string fca_0001_4
    {
        get { return _fca_0001_4; }
        set { _fca_0001_4 = value; }
    }
    public string fpp_0001_4
    {
        get { return _fpp_0001_4; }
        set { _fpp_0001_4 = value; }
    }
    public string fpr_0001_4
    {
        get { return _fpr_0001_4; }
        set { _fpr_0001_4 = value; }
    }
    public short dpl_0001_4
    {
        get { return _dpl_0001_4; }
        set { _dpl_0001_4 = value; }
    }
    public int uar_0001_4
    {
        get { return _uar_0001_4; }
        set { _uar_0001_4 = value; }
    }
    public string far_0001_4
    {
        get { return _far_0001_4; }
        set { _far_0001_4 = value; }
    }
    public string dip_0001_4
    {
        get { return _dip_0001_4; }
        set { _dip_0001_4 = value; }
    }
    public int uua_0001_4
    {
        get { return _uua_0001_4; }
        set { _uua_0001_4 = value; }
    }
    public string fua_0001_4
    {
        get { return _fua_0001_4; }
        set { _fua_0001_4 = value; }
    }

    #endregion

    #region Constructores

    public Gesfac_0001_4(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_4 = 0;
        _ide_0001 = 0;
        _frs_0001_4 = "";
        _fat_0001_4 = null;
        _fao_0001_4 = null;
        _fca_0001_4 = null;
        _fpp_0001_4 = null;
        _fpr_0001_4 = null;
        _dpl_0001_4 = 0;
        _uar_0001_4 = 0;
        _far_0001_4 = "";
        _dip_0001_4 = "";
        _uua_0001_4 = 0;
        _fua_0001_4 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.gesfac_0001_4 (ide_0001, frs_0001_4, fat_0001_4, fao_0001_4, fca_0001_4, fpp_0001_4, fpr_0001_4, dpl_0001_4, uar_0001_4, far_0001_4, dip_0001_4, uua_0001_4, fua_0001_4)" +
            $" OUTPUT INSERTED.ide_0001_4 VALUES (  @ide_0001, @frs_0001_4, @fat_0001_4, @fao_0001_4, @fca_0001_4, @fpp_0001_4, @fpr_0001_4, @dpl_0001_4, @uar_0001_4, @far_0001_4, @dip_0001_4, @uua_0001_4, @fua_0001_4)"
        };

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@frs_0001_4", _frs_0001_4);

        if (_fat_0001_4 == null)
            Command.Parameters.Add("@fat_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fat_0001_4", _fat_0001_4);

        if (_fao_0001_4 == null)
            Command.Parameters.Add("@fao_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fao_0001_4", _fao_0001_4);

        if (_fca_0001_4 == null)
            Command.Parameters.Add("@fca_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fca_0001_4", _fca_0001_4);

        if (_fpp_0001_4 == null)
            Command.Parameters.Add("@fpp_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fpp_0001_4", _fpp_0001_4);

        if (_fpr_0001_4 == null)
            Command.Parameters.Add("@fpr_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fpr_0001_4", _fpr_0001_4);

        Command.Parameters.AddWithValue("@dpl_0001_4", _dpl_0001_4);
        Command.Parameters.AddWithValue("@uar_0001_4", _uar_0001_4);
        Command.Parameters.AddWithValue("@far_0001_4", _far_0001_4);
        Command.Parameters.AddWithValue("@dip_0001_4", _dip_0001_4);
        Command.Parameters.AddWithValue("@uua_0001_4", _uua_0001_4);
        Command.Parameters.AddWithValue("@fua_0001_4", _fua_0001_4);

        Command.Connection = _sqlCon;
        _ide_0001_4 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.gesfac_0001_4 WHERE ide_0001_4 = {_ide_0001_4}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_4 = (int)dr["ide_0001_4"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                frs_0001_4 = dr["frs_0001_4"].ToString();
                fat_0001_4 = DBNull.Value.Equals(dr["fat_0001_4"]) ? null : dr["fat_0001_4"].ToString();
                fao_0001_4 = DBNull.Value.Equals(dr["fao_0001_4"]) ? null : dr["fao_0001_4"].ToString();
                fca_0001_4 = DBNull.Value.Equals(dr["fca_0001_4"]) ? null : dr["fca_0001_4"].ToString();
                fpp_0001_4 = DBNull.Value.Equals(dr["fpp_0001_4"]) ? null : dr["fpp_0001_4"].ToString();
                fpr_0001_4 = DBNull.Value.Equals(dr["fpr_0001_4"]) ? null : dr["fpr_0001_4"].ToString();
                dpl_0001_4 = (short)dr["dpl_0001_4"];
                uar_0001_4 = (int)dr["uar_0001_4"];
                far_0001_4 = dr["far_0001_4"].ToString();
                dip_0001_4 = (string)dr["dip_0001_4"];
                uua_0001_4 = (int)dr["uua_0001_4"];
                fua_0001_4 = dr["fua_0001_4"].ToString();
            }
        }
        else
            ide_0001_4 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.gesfac_0001_4 WHERE ide_0001_4 = {_ide_0001_4}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.gesfac_0001_4 SET");
        strSQL += string.Concat(" ide_0001 = @ide_0001");
        strSQL += string.Concat(", frs_0001_4 = @frs_0001_4");
        strSQL += string.Concat(", fat_0001_4 = @fat_0001_4");
        strSQL += string.Concat(", fao_0001_4 = @fao_0001_4");
        strSQL += string.Concat(", fca_0001_4 = @fca_0001_4");
        strSQL += string.Concat(", fpp_0001_4 = @fpp_0001_4");
        strSQL += string.Concat(", fpr_0001_4 = @fpr_0001_4");
        strSQL += string.Concat(", dpl_0001_4 = @dpl_0001_4");
        strSQL += string.Concat(", dip_0001_4 = @dip_0001_4");
        strSQL += string.Concat(", uua_0001_4 = @uua_0001_4");
        strSQL += string.Concat(", fua_0001_4 = @fua_0001_4");
        strSQL += string.Concat(" WHERE ide_0001_4 = ", _ide_0001_4);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@frs_0001_4", _frs_0001_4);

        if (_fat_0001_4 == null)
            Command.Parameters.Add("@fat_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fat_0001_4", _fat_0001_4);

        if (_fao_0001_4 == null)
            Command.Parameters.Add("@fao_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fao_0001_4", _fao_0001_4);

        if (_fca_0001_4 == null)
            Command.Parameters.Add("@fca_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fca_0001_4", _fca_0001_4);

        if (_fpp_0001_4 == null)
            Command.Parameters.Add("@fpp_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fpp_0001_4", _fpp_0001_4);

        if (_fpr_0001_4 == null)
            Command.Parameters.Add("@fpr_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fpr_0001_4", _fpr_0001_4);

        Command.Parameters.AddWithValue("@dpl_0001_4", _dpl_0001_4);
        Command.Parameters.AddWithValue("@dip_0001_4", _dip_0001_4);
        Command.Parameters.AddWithValue("@uua_0001_4", _uua_0001_4);
        Command.Parameters.AddWithValue("@fua_0001_4", _fua_0001_4);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}