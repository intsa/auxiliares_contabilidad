using System;
using System.Data;
using System.Data.SqlClient;

public class Gesfac_0001_4_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_4;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private string _frs_0001_4;                                     // 3    
    private string _fat_0001_4;                                     // 4    
    private string _fao_0001_4;                                     // 5    
    private string _fca_0001_4;                                     // 6    
    private string _fpp_0001_4;                                     // 7    
    private string _fpr_0001_4;                                     // 8    
    private short _dpl_0001_4;                                      // 9    
    private int _uar_0001_4;                                        // 10    
    private string _far_0001_4;                                     // 11    
    private string _dip_0001_4;                                     // 12    
    private int _uua_0001_4;                                        // 13    
    private string _fua_0001_4;                                     // 14    

    #endregion

    #region Propiedades

    public int ide_0001_4
    {
        get { return _ide_0001_4; }
        set { _ide_0001_4 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public string frs_0001_4
    {
        get { return _frs_0001_4; }
        set { _frs_0001_4 = value; }
    }
    public string fat_0001_4
    {
        get { return _fat_0001_4; }
        set { _fat_0001_4 = value; }
    }
    public string fao_0001_4
    {
        get { return _fao_0001_4; }
        set { _fao_0001_4 = value; }
    }
    public string fca_0001_4
    {
        get { return _fca_0001_4; }
        set { _fca_0001_4 = value; }
    }
    public string fpp_0001_4
    {
        get { return _fpp_0001_4; }
        set { _fpp_0001_4 = value; }
    }
    public string fpr_0001_4
    {
        get { return _fpr_0001_4; }
        set { _fpr_0001_4 = value; }
    }
    public short dpl_0001_4
    {
        get { return _dpl_0001_4; }
        set { _dpl_0001_4 = value; }
    }
    public int uar_0001_4
    {
        get { return _uar_0001_4; }
        set { _uar_0001_4 = value; }
    }
    public string far_0001_4
    {
        get { return _far_0001_4; }
        set { _far_0001_4 = value; }
    }
    public string dip_0001_4
    {
        get { return _dip_0001_4; }
        set { _dip_0001_4 = value; }
    }
    public int uua_0001_4
    {
        get { return _uua_0001_4; }
        set { _uua_0001_4 = value; }
    }
    public string fua_0001_4
    {
        get { return _fua_0001_4; }
        set { _fua_0001_4 = value; }
    }

    #endregion

    #region Constructores

    public Gesfac_0001_4_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_4 = 0;
        _ide_0001 = 0;
        _frs_0001_4 = "";
        _fat_0001_4 = null;
        _fao_0001_4 = null;
        _fca_0001_4 = null;
        _fpp_0001_4 = null;
        _fpr_0001_4 = null;
        _dpl_0001_4 = 0;
        _uar_0001_4 = 0;
        _far_0001_4 = "";
        _dip_0001_4 = "";
        _uua_0001_4 = 0;
        _fua_0001_4 = "";
    }

    #endregion

    #region Metodos Públicos

     //METODO QUE ACTUALIZAR
    public void Actualizar_Pago_Real()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.gesfac_0001_4 SET");
        strSQL += string.Concat(" fpr_0001_4 = @fpr_0001_4");
        strSQL += string.Concat(", dip_0001_4 = @dip_0001_4");
        strSQL += string.Concat(", uua_0001_4 = @uua_0001_4");
        strSQL += string.Concat(", fua_0001_4 = @fua_0001_4");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS

        if (_fpr_0001_4 == null)
            Command.Parameters.Add("@fpr_0001_4", SqlDbType.DateTime).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fpr_0001_4", _fpr_0001_4);

        Command.Parameters.AddWithValue("@dip_0001_4", _dip_0001_4);
        Command.Parameters.AddWithValue("@uua_0001_4", _uua_0001_4);
        Command.Parameters.AddWithValue("@fua_0001_4", _fua_0001_4);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    #endregion

}