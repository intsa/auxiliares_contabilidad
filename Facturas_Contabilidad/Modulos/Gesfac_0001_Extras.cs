using System;
using System.Data;
using System.Data.SqlClient;

public class Gesfac_0001_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _eje_0001;                                        // 2   CLAVE_UNICA 
    private byte _lfr_0001;                                         // 3   CLAVE_UNICA 
    private int _ord_0001;                                          // 4   CLAVE_UNICA 
    private string _nrs_0001;                                       // 5    
    private string _frs_0001;                                       // 6    
    private string _fec_0001;                                       // 7    
    private string _nfa_0001;                                       // 8    
    private long? _nrf_0001;                                       // 9    
    private short _tdf_0001;                                         // 10    
    private byte _tpr_0001;                                         // 11    
    private byte _tfa_0001;                                         // 12    
    private int? _nag_0001;                                         // 13    
    private byte _edp_0001;                                         // 14    
    private short? _ecm_0001;                                       // 15    
    private int? _cme_0001;                                         // 16    
    private short _tco_0001;                                         // 17    
    private string _con_0001;                                       // 18    
    private decimal _bas_0001;                                      // 19    
    private decimal _imd_0001;                                      // 20    
    private decimal _imn_0001;                                      // 21    
    private decimal? _ipr_0001;                                     // 22    CALCULADO
    private decimal _ibr_0001;                                      // 23    
    private decimal _ret_0001;                                      // 24    
    private decimal _car_0001;                                      // 25    
    private decimal _dto_0001;                                      // 26    
    private decimal? _tot_0001;                                     // 27    CALCULADO
    private decimal? _liq_0001;                                     // 28    CALCULADO
    private byte _sco_0001;                                         // 29    
    private byte _sap_0001;                                         // 30    
    private short? _cej_0001;                                       // 31    
    private int? _nex_0001;                                         // 32    
    private int? _ndr_0001;                                         // 33    
    private string _res_0001;                                       // 34    
    private byte _sgi_0001;                                         // 35    
    private short _ejp_0001;                                        // 36    
    private string _cor_0001;                                       // 37    
    private string _cfu_0001;                                       // 38    
    private string _cec_0001;                                       // 39    
    private string _cta_0001;                                       // 40    
    private int _idt_0001;                                          // 41    
    private string _nif_0001;                                       // 42    
    private string _raz_0001;                                       // 43    
    private string _iba_0001;                                       // 44    
    private string _bic_0001;                                       // 45    
    private short? _can_0001;                                       // 46    
    private short? _fop_0001;                                       // 47    
    private short _tse_0001;                                        // 48    
    private short? _ere_0001;                                       // 49    
    private int? _nre_0001;                                         // 50    
    private string _rfe_0001;                                       // 51    
    private byte _rsi_0001;                                         // 52    
    private byte _sdr_0001;                                         // 53    
    private byte _sfc_0001;                                         // 54    
    private string _ida_0001;                                       // 55    
    private decimal _ifa_0001;                                      // 56    
    private decimal _prf_0001;                                      // 57    
    private int _spp_0001;                                          // 58    CALCULADO
    private int _uar_0001;                                          // 59    
    private string _far_0001;                                       // 60    
    private string _dip_0001;                                       // 61    
    private int _uua_0001;                                          // 62    
    private string _fua_0001;                                       // 63    

    #endregion

    #region Propiedades

    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public short eje_0001
    {
        get { return _eje_0001; }
        set { _eje_0001 = value; }
    }
    public byte lfr_0001
    {
        get { return _lfr_0001; }
        set { _lfr_0001 = value; }
    }
    public int ord_0001
    {
        get { return _ord_0001; }
        set { _ord_0001 = value; }
    }
    public string nrs_0001
    {
        get { return _nrs_0001; }
        set { _nrs_0001 = value; }
    }
    public string frs_0001
    {
        get { return _frs_0001; }
        set { _frs_0001 = value; }
    }
    public string fec_0001
    {
        get { return _fec_0001; }
        set { _fec_0001 = value; }
    }
    public string nfa_0001
    {
        get { return _nfa_0001; }
        set { _nfa_0001 = value; }
    }
    public long? nrf_0001
    {
        get { return _nrf_0001; }
        set { _nrf_0001 = value; }
    }
    public short tdf_0001
    {
        get { return _tdf_0001; }
        set { _tdf_0001 = value; }
    }
    public byte tpr_0001
    {
        get { return _tpr_0001; }
        set { _tpr_0001 = value; }
    }
    public byte tfa_0001
    {
        get { return _tfa_0001; }
        set { _tfa_0001 = value; }
    }
    public int? nag_0001
    {
        get { return _nag_0001; }
        set { _nag_0001 = value; }
    }
    public byte edp_0001
    {
        get { return _edp_0001; }
        set { _edp_0001 = value; }
    }
    public short? ecm_0001
    {
        get { return _ecm_0001; }
        set { _ecm_0001 = value; }
    }
    public int? cme_0001
    {
        get { return _cme_0001; }
        set { _cme_0001 = value; }
    }
    public short tco_0001
    {
        get { return _tco_0001; }
        set { _tco_0001 = value; }
    }
    public string con_0001
    {
        get { return _con_0001; }
        set { _con_0001 = value; }
    }
    public decimal bas_0001
    {
        get { return _bas_0001; }
        set { _bas_0001 = value; }
    }
    public decimal imd_0001
    {
        get { return _imd_0001; }
        set { _imd_0001 = value; }
    }
    public decimal imn_0001
    {
        get { return _imn_0001; }
        set { _imn_0001 = value; }
    }
    public decimal? ipr_0001
    {
        get { return _ipr_0001; }
        set { _ipr_0001 = value; }
    }
    public decimal ibr_0001
    {
        get { return _ibr_0001; }
        set { _ibr_0001 = value; }
    }
    public decimal ret_0001
    {
        get { return _ret_0001; }
        set { _ret_0001 = value; }
    }
    public decimal car_0001
    {
        get { return _car_0001; }
        set { _car_0001 = value; }
    }
    public decimal dto_0001
    {
        get { return _dto_0001; }
        set { _dto_0001 = value; }
    }
    public decimal? tot_0001
    {
        get { return _tot_0001; }
        set { _tot_0001 = value; }
    }
    public decimal? liq_0001
    {
        get { return _liq_0001; }
        set { _liq_0001 = value; }
    }
    public byte sco_0001
    {
        get { return _sco_0001; }
        set { _sco_0001 = value; }
    }
    public byte sap_0001
    {
        get { return _sap_0001; }
        set { _sap_0001 = value; }
    }
    public short? cej_0001
    {
        get { return _cej_0001; }
        set { _cej_0001 = value; }
    }
    public int? nex_0001
    {
        get { return _nex_0001; }
        set { _nex_0001 = value; }
    }
    public int? ndr_0001
    {
        get { return _ndr_0001; }
        set { _ndr_0001 = value; }
    }
    public string res_0001
    {
        get { return _res_0001; }
        set { _res_0001 = value; }
    }
    public byte sgi_0001
    {
        get { return _sgi_0001; }
        set { _sgi_0001 = value; }
    }
    public short ejp_0001
    {
        get { return _ejp_0001; }
        set { _ejp_0001 = value; }
    }
    public string cor_0001
    {
        get { return _cor_0001; }
        set { _cor_0001 = value; }
    }
    public string cfu_0001
    {
        get { return _cfu_0001; }
        set { _cfu_0001 = value; }
    }
    public string cec_0001
    {
        get { return _cec_0001; }
        set { _cec_0001 = value; }
    }
    public string cta_0001
    {
        get { return _cta_0001; }
        set { _cta_0001 = value; }
    }
    public int idt_0001
    {
        get { return _idt_0001; }
        set { _idt_0001 = value; }
    }
    public string nif_0001
    {
        get { return _nif_0001; }
        set { _nif_0001 = value; }
    }
    public string raz_0001
    {
        get { return _raz_0001; }
        set { _raz_0001 = value; }
    }
    public string iba_0001
    {
        get { return _iba_0001; }
        set { _iba_0001 = value; }
    }
    public string bic_0001
    {
        get { return _bic_0001; }
        set { _bic_0001 = value; }
    }
    public short? can_0001
    {
        get { return _can_0001; }
        set { _can_0001 = value; }
    }
    public short? fop_0001
    {
        get { return _fop_0001; }
        set { _fop_0001 = value; }
    }
    public short tse_0001
    {
        get { return _tse_0001; }
        set { _tse_0001 = value; }
    }
    public short? ere_0001
    {
        get { return _ere_0001; }
        set { _ere_0001 = value; }
    }
    public int? nre_0001
    {
        get { return _nre_0001; }
        set { _nre_0001 = value; }
    }
    public string rfe_0001
    {
        get { return _rfe_0001; }
        set { _rfe_0001 = value; }
    }
    public byte rsi_0001
    {
        get { return _rsi_0001; }
        set { _rsi_0001 = value; }
    }
    public byte sdr_0001
    {
        get { return _sdr_0001; }
        set { _sdr_0001 = value; }
    }
    public byte sfc_0001
    {
        get { return _sfc_0001; }
        set { _sfc_0001 = value; }
    }
    public string ida_0001
    {
        get { return _ida_0001; }
        set { _ida_0001 = value; }
    }
    public decimal ifa_0001
    {
        get { return _ifa_0001; }
        set { _ifa_0001 = value; }
    }
    public decimal prf_0001
    {
        get { return _prf_0001; }
        set { _prf_0001 = value; }
    }
    public int spp_0001
    {
        get { return _spp_0001; }
        set { _spp_0001 = value; }
    }
    public int uar_0001
    {
        get { return _uar_0001; }
        set { _uar_0001 = value; }
    }
    public string far_0001
    {
        get { return _far_0001; }
        set { _far_0001 = value; }
    }
    public string dip_0001
    {
        get { return _dip_0001; }
        set { _dip_0001 = value; }
    }
    public int uua_0001
    {
        get { return _uua_0001; }
        set { _uua_0001 = value; }
    }
    public string fua_0001
    {
        get { return _fua_0001; }
        set { _fua_0001 = value; }
    }

    #endregion

    #region Constructores

    public Gesfac_0001_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001 = 0;
        _eje_0001 = 0;
        _lfr_0001 = 0;
        _ord_0001 = 0;
        _nrs_0001 = "";
        _frs_0001 = null;
        _fec_0001 = "";
        _nfa_0001 = "";
        _nrf_0001 = null;
        _tdf_0001 = 0;
        _tpr_0001 = 0;
        _tfa_0001 = 0;
        _nag_0001 = null;
        _edp_0001 = 0;
        _ecm_0001 = null;
        _cme_0001 = null;
        _tco_0001 = 0;
        _con_0001 = "";
        _bas_0001 = 0;
        _imd_0001 = 0;
        _imn_0001 = 0;
        _ipr_0001 = null;
        _ibr_0001 = 0;
        _ret_0001 = 0;
        _car_0001 = 0;
        _dto_0001 = 0;
        _tot_0001 = null;
        _liq_0001 = null;
        _sco_0001 = 0;
        _sap_0001 = 0;
        _cej_0001 = null;
        _nex_0001 = null;
        _ndr_0001 = null;
        _res_0001 = "";
        _sgi_0001 = 0;
        _ejp_0001 = 0;
        _cor_0001 = "";
        _cfu_0001 = "";
        _cec_0001 = "";
        _cta_0001 = "";
        _idt_0001 = 0;
        _nif_0001 = "";
        _raz_0001 = "";
        _iba_0001 = "";
        _bic_0001 = "";
        _can_0001 = null;
        _fop_0001 = null;
        _tse_0001 = 0;
        _ere_0001 = null;
        _nre_0001 = null;
        _rfe_0001 = null;
        _rsi_0001 = 0;
        _sdr_0001 = 0;
        _sfc_0001 = 0;
        _ida_0001 = "";
        _ifa_0001 = 0;
        _prf_0001 = 0;
        _spp_0001 = 0;
        _uar_0001 = 0;
        _far_0001 = "";
        _dip_0001 = "";
        _uua_0001 = 0;
        _fua_0001 = "";
    }

    #endregion

    #region Métodos públicos

    /// <summary>
    /// Carga datos presentación factura
    /// </summary>
    public void Cargar_Extras()
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = $"SELECT ide_0001, lfr_0001, eje_0001, ord_0001, cor_0001, cfu_0001, cec_0001, fec_0001, nfa_0001, sco_0001, idt_0001, nif_0001, raz_0001, cta_0001," +
                                                        $" ipr_0001, imd_0001, imn_0001, ret_0001, car_0001, dto_0001 FROM {_BaseDatos}.dbo.gesfac_0001 WHERE ide_0001 = {_ide_0001}";
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            lfr_0001 = Convert.ToByte(dr["lfr_0001"]);
            eje_0001 = (short)dr["eje_0001"];
            ord_0001 = (int)dr["ord_0001"];
            cor_0001 = dr["cor_0001"].ToString();
            cfu_0001 = dr["cfu_0001"].ToString();
            cec_0001 = dr["cec_0001"].ToString();
            fec_0001 = dr["fec_0001"].ToString();
            nfa_0001 = (string)dr["nfa_0001"];
            sco_0001 = (byte)dr["sco_0001"];
            cta_0001 = (string)dr["cta_0001"];
            idt_0001 = (int)dr["idt_0001"];
            nif_0001 = (string)dr["nif_0001"];
            ipr_0001 = DBNull.Value.Equals(dr["ipr_0001"]) ? null : (decimal?)dr["ipr_0001"];
            imd_0001 = (decimal)dr["imd_0001"];
            imn_0001 = (decimal)dr["imn_0001"];
            ret_0001 = (decimal)dr["ret_0001"];
            car_0001 = (decimal)dr["car_0001"];
            dto_0001 = (decimal)dr["dto_0001"];
        }
        else
            ide_0001 = 0;

        dr.Close();
    }

    /// <summary>
    /// Actualiza los importes de la factura
    /// </summary>
    public void Actualizar_Importes()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.gesfac_0001 SET");
        strSQL += string.Concat(" bas_0001 = @bas_0001");
        strSQL += string.Concat(", imd_0001 = @imd_0001");
        strSQL += string.Concat(", imn_0001 = @imn_0001");
        strSQL += string.Concat(", ibr_0001 = @ibr_0001");
        strSQL += string.Concat(", ret_0001 = @ret_0001");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@bas_0001", _bas_0001);
        Command.Parameters.AddWithValue("@imd_0001", _imd_0001);
        Command.Parameters.AddWithValue("@imn_0001", _imn_0001);
        Command.Parameters.AddWithValue("@ibr_0001", _ibr_0001);
        Command.Parameters.AddWithValue("@ret_0001", _ret_0001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza el importe pagado de una factura
    /// </summary>
    /// <param name="Importe_Pagado">Importe pagado</param>
    public void Actualizar_Extras_Pago_Parcial(decimal Importe_Pagado)
    {
        string Cadena_Sql;
        string Cadena_Update = $"UPDATE {_BaseDatos}.dbo.gesfac_0001";
        string Cadena_Valores;
        string Cadena_Where;

        Cadena_Valores = $" SET prf_0001 += {Importe_Pagado.ToString().Replace(",", ".")}";
        Cadena_Where = $" WHERE ide_0001 = {_ide_0001}";
        Cadena_Sql = $"{Cadena_Update}{Cadena_Valores}{Cadena_Where}";

        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = Cadena_Sql
        };

        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza la situación contable a 'RECONOCIDO'
    /// </summary>
    public void Actualizar_Extras_Reconocido()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.gesfac_0001 SET");
        strSQL += string.Concat(" sco_0001 = @sco_0001");
        strSQL += string.Concat(", cej_0001 = @cej_0001");
        strSQL += string.Concat(", nex_0001 = @nex_0001");
        strSQL += string.Concat(", ndr_0001 = @ndr_0001");
        strSQL += string.Concat(", dip_0001 = @dip_0001");
        strSQL += string.Concat(", uua_0001 = @uua_0001");
        strSQL += string.Concat(", fua_0001 = @fua_0001");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@sco_0001", _sco_0001);

        if (_cej_0001 == null)
            Command.Parameters.Add("@cej_0001", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@cej_0001", _cej_0001);

        if (_nex_0001 == null)
            Command.Parameters.Add("@nex_0001", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@nex_0001", _nex_0001);

        if (_ndr_0001 == null)
            Command.Parameters.Add("@ndr_0001", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ndr_0001", _ndr_0001);

        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza la situación contable a 'PAGADO'
    /// </summary>
    public void Actualizar_Extras_Pagado()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.gesfac_0001 SET");
        strSQL += string.Concat(" sco_0001 = @sco_0001");
        strSQL += string.Concat(", dip_0001 = @dip_0001");
        strSQL += string.Concat(", uua_0001 = @uua_0001");
        strSQL += string.Concat(", fua_0001 = @fua_0001");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@sco_0001", _sco_0001);
        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza la situación contable a 'PAGADO'
    /// </summary>
    public void Actualizar_Extras_Situacion()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.gesfac_0001 SET");
        strSQL += string.Concat(" sco_0001 = @sco_0001");
        strSQL += string.Concat(", dip_0001 = @dip_0001");
        strSQL += string.Concat(", uua_0001 = @uua_0001");
        strSQL += string.Concat(", fua_0001 = @fua_0001");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@sco_0001", _sco_0001);
        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}