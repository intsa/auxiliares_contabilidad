using System;
using System.Data.SqlClient;

public class Gesfac_0001_2
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_2;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2   CLAVE_UNICA 
    private int _idr_0001_2;                                        // 3    
    private string _mdr_0001_2;                                     // 4   CLAVE_UNICA 
    private string _den_0001_2;                                     // 5    
    private string _ima_0001_2;                                     // 6    
    private int _uar_0001_2;                                        // 7    
    private string _far_0001_2;                                     // 8    
    private string _dip_0001_2;                                     // 9    
    private int _uua_0001_2;                                        // 10    
    private string _fua_0001_2;                                     // 11    

    #endregion

    #region Propiedades

    public int ide_0001_2
    {
        get { return _ide_0001_2; }
        set { _ide_0001_2 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public int idr_0001_2
    {
        get { return _idr_0001_2; }
        set { _idr_0001_2 = value; }
    }
    public string mdr_0001_2
    {
        get { return _mdr_0001_2; }
        set { _mdr_0001_2 = value; }
    }
    public string den_0001_2
    {
        get { return _den_0001_2; }
        set { _den_0001_2 = value; }
    }
    public string ima_0001_2
    {
        get { return _ima_0001_2; }
        set { _ima_0001_2 = value; }
    }
    public int uar_0001_2
    {
        get { return _uar_0001_2; }
        set { _uar_0001_2 = value; }
    }
    public string far_0001_2
    {
        get { return _far_0001_2; }
        set { _far_0001_2 = value; }
    }
    public string dip_0001_2
    {
        get { return _dip_0001_2; }
        set { _dip_0001_2 = value; }
    }
    public int uua_0001_2
    {
        get { return _uua_0001_2; }
        set { _uua_0001_2 = value; }
    }
    public string fua_0001_2
    {
        get { return _fua_0001_2; }
        set { _fua_0001_2 = value; }
    }

    #endregion

    #region Constructores

    public Gesfac_0001_2(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_2 = 0;
        _ide_0001 = 0;
        _idr_0001_2 = 0;
        _mdr_0001_2 = "";
        _den_0001_2 = "";
        _ima_0001_2 = "";
        _uar_0001_2 = 0;
        _far_0001_2 = "";
        _dip_0001_2 = "";
        _uua_0001_2 = 0;
        _fua_0001_2 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {

        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.gesfac_0001_2 (" +

        "  ide_0001" + ", idr_0001_2" + ", mdr_0001_2" + ", den_0001_2" + ", ima_0001_2" + ", uar_0001_2" + ", far_0001_2" + ", dip_0001_2" + ", uua_0001_2" + ", fua_0001_2" +

        ") OUTPUT INSERTED.ide_0001_2 VALUES (" +

        "  @ide_0001" + ", @idr_0001_2" + ", @mdr_0001_2" + ", @den_0001_2" + ", @ima_0001_2" + ", @uar_0001_2" + ", @far_0001_2" + ", @dip_0001_2" + ", @uua_0001_2" + ", @fua_0001_2" +

         ")";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@idr_0001_2", _idr_0001_2);
        Command.Parameters.AddWithValue("@mdr_0001_2", _mdr_0001_2);
        Command.Parameters.AddWithValue("@den_0001_2", _den_0001_2);
        Command.Parameters.AddWithValue("@ima_0001_2", _ima_0001_2);
        Command.Parameters.AddWithValue("@uar_0001_2", _uar_0001_2);
        Command.Parameters.AddWithValue("@far_0001_2", _far_0001_2);
        Command.Parameters.AddWithValue("@dip_0001_2", _dip_0001_2);
        Command.Parameters.AddWithValue("@uua_0001_2", _uua_0001_2);
        Command.Parameters.AddWithValue("@fua_0001_2", _fua_0001_2);
        Command.Connection = _sqlCon;

        _ide_0001_2 = (int)Command.ExecuteScalar();

        Command = null;

    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {

        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "Select * FROM " + _BaseDatos + ".dbo.gesfac_0001_2 WHERE ide_0001_2 = " + _ide_0001_2;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_2 = (int)dr["ide_0001_2"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                idr_0001_2 = (int)dr["idr_0001_2"];
                mdr_0001_2 = (string)dr["mdr_0001_2"];
                den_0001_2 = (string)dr["den_0001_2"];
                ima_0001_2 = (string)dr["ima_0001_2"];
                uar_0001_2 = (int)dr["uar_0001_2"];
                far_0001_2 = dr["far_0001_2"].ToString();
                dip_0001_2 = (string)dr["dip_0001_2"];
                uua_0001_2 = (int)dr["uua_0001_2"];
                fua_0001_2 = dr["fua_0001_2"].ToString();
            }
        }
        else
        {
            ide_0001_2 = 0;
        }

        dr.Close();

        Command = null;

    }

    //METODO ELIMINAR
    public void Eliminar()
    {

        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.gesfac_0001_2 WHERE ide_0001_2 = " + _ide_0001_2;
        Command.ExecuteNonQuery();

        Command = null;

    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.gesfac_0001_2 SET");
        strSQL += String.Concat(" ide_0001 = @ide_0001");
        strSQL += String.Concat(", idr_0001_2 = @idr_0001_2");
        strSQL += String.Concat(", mdr_0001_2 = @mdr_0001_2");
        strSQL += String.Concat(", den_0001_2 = @den_0001_2");
        strSQL += String.Concat(", ima_0001_2 = @ima_0001_2");
        strSQL += String.Concat(", dip_0001_2 = @dip_0001_2");
        strSQL += String.Concat(", uua_0001_2 = @uua_0001_2");
        strSQL += String.Concat(", fua_0001_2 = @fua_0001_2");
        strSQL += String.Concat(" WHERE ide_0001_2 = ", _ide_0001_2);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@idr_0001_2", _idr_0001_2);
        Command.Parameters.AddWithValue("@mdr_0001_2", _mdr_0001_2);
        Command.Parameters.AddWithValue("@den_0001_2", _den_0001_2);
        Command.Parameters.AddWithValue("@ima_0001_2", _ima_0001_2);
        Command.Parameters.AddWithValue("@dip_0001_2", _dip_0001_2);
        Command.Parameters.AddWithValue("@uua_0001_2", _uua_0001_2);
        Command.Parameters.AddWithValue("@fua_0001_2", _fua_0001_2);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();

        Command = null;

    }

    //METODO CARGAR CLAVE UNICA: ide_0001 mdr_0001_2
    public void Cargar_ide_0001_mdr_0001_2(bool AsignarPropiedades = true)
    {

        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.gesfac_0001_2 WHERE ide_0001 = " + _ide_0001 + " AND mdr_0001_2 = '" + _mdr_0001_2 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {

            ide_0001_2 = (int)dr["ide_0001_2"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                idr_0001_2 = (int)dr["idr_0001_2"];
                mdr_0001_2 = (string)dr["mdr_0001_2"];
                den_0001_2 = (string)dr["den_0001_2"];
                ima_0001_2 = (string)dr["ima_0001_2"];
                uar_0001_2 = (int)dr["uar_0001_2"];
                far_0001_2 = dr["far_0001_2"].ToString();
                dip_0001_2 = (string)dr["dip_0001_2"];
                uua_0001_2 = (int)dr["uua_0001_2"];
                fua_0001_2 = dr["fua_0001_2"].ToString();
            }
        }
        else
        {
            ide_0001_2 = 0;
        }

        dr.Close();

        Command = null;

    }

    #endregion

}