using System;
using System.Data.SqlClient;

public class Gesfac_0001_1
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_1;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private byte _tim_0001_1;                                       // 3    
    private int _ipo_0001_1;                                        // 4    
    private byte _sde_0001_1;                                       // 5    
    private decimal _bas_0001_1;                                    // 6    
    private decimal _po1_0001_1;                                    // 7    
    private decimal _im1_0001_1;                                    // 8    
    private decimal _po2_0001_1;                                    // 9    
    private decimal _im2_0001_1;                                    // 10    
    private string _ida_0001_1;                                     // 11    
    private int _uar_0001_1;                                        // 12    
    private string _far_0001_1;                                     // 13    
    private string _dip_0001_1;                                     // 14    
    private int _uua_0001_1;                                        // 15    
    private string _fua_0001_1;                                     // 16    

    #endregion

    #region Propiedades

    public int ide_0001_1
    {
        get { return _ide_0001_1; }
        set { _ide_0001_1 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public byte tim_0001_1
    {
        get { return _tim_0001_1; }
        set { _tim_0001_1 = value; }
    }
    public int ipo_0001_1
    {
        get { return _ipo_0001_1; }
        set { _ipo_0001_1 = value; }
    }
    public byte sde_0001_1
    {
        get { return _sde_0001_1; }
        set { _sde_0001_1 = value; }
    }
    public decimal bas_0001_1
    {
        get { return _bas_0001_1; }
        set { _bas_0001_1 = value; }
    }
    public decimal po1_0001_1
    {
        get { return _po1_0001_1; }
        set { _po1_0001_1 = value; }
    }
    public decimal im1_0001_1
    {
        get { return _im1_0001_1; }
        set { _im1_0001_1 = value; }
    }
    public decimal po2_0001_1
    {
        get { return _po2_0001_1; }
        set { _po2_0001_1 = value; }
    }
    public decimal im2_0001_1
    {
        get { return _im2_0001_1; }
        set { _im2_0001_1 = value; }
    }
    public string ida_0001_1
    {
        get { return _ida_0001_1; }
        set { _ida_0001_1 = value; }
    }
    public int uar_0001_1
    {
        get { return _uar_0001_1; }
        set { _uar_0001_1 = value; }
    }
    public string far_0001_1
    {
        get { return _far_0001_1; }
        set { _far_0001_1 = value; }
    }
    public string dip_0001_1
    {
        get { return _dip_0001_1; }
        set { _dip_0001_1 = value; }
    }
    public int uua_0001_1
    {
        get { return _uua_0001_1; }
        set { _uua_0001_1 = value; }
    }
    public string fua_0001_1
    {
        get { return _fua_0001_1; }
        set { _fua_0001_1 = value; }
    }

    #endregion

    #region Constructores

    public Gesfac_0001_1(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_1 = 0;
        _ide_0001 = 0;
        _tim_0001_1 = 0;
        _ipo_0001_1 = 0;
        _sde_0001_1 = 0;
        _bas_0001_1 = 0;
        _po1_0001_1 = 0;
        _im1_0001_1 = 0;
        _po2_0001_1 = 0;
        _im2_0001_1 = 0;
        _ida_0001_1 = "";
        _uar_0001_1 = 0;
        _far_0001_1 = "";
        _dip_0001_1 = "";
        _uua_0001_1 = 0;
        _fua_0001_1 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.gesfac_0001_1 (" +
        "  ide_0001" + ", tim_0001_1" + ", ipo_0001_1" + ", sde_0001_1" + ", bas_0001_1" + ", po1_0001_1" + ", im1_0001_1" + ", po2_0001_1" + ", im2_0001_1" + ", ida_0001_1" +
        ", uar_0001_1" + ", far_0001_1" + ", dip_0001_1" + ", uua_0001_1" + ", fua_0001_1" +
        ") OUTPUT INSERTED.ide_0001_1 VALUES (" +
        "  @ide_0001" + ", @tim_0001_1" + ", @ipo_0001_1" + ", @sde_0001_1" + ", @bas_0001_1" + ", @po1_0001_1" + ", @im1_0001_1" + ", @po2_0001_1" + ", @im2_0001_1" + ", @ida_0001_1" +
        ", @uar_0001_1" + ", @far_0001_1" + ", @dip_0001_1" + ", @uua_0001_1" + ", @fua_0001_1" +
         ")";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@tim_0001_1", _tim_0001_1);
        Command.Parameters.AddWithValue("@ipo_0001_1", _ipo_0001_1);
        Command.Parameters.AddWithValue("@sde_0001_1", _sde_0001_1);
        Command.Parameters.AddWithValue("@bas_0001_1", _bas_0001_1);
        Command.Parameters.AddWithValue("@po1_0001_1", _po1_0001_1);
        Command.Parameters.AddWithValue("@im1_0001_1", _im1_0001_1);
        Command.Parameters.AddWithValue("@po2_0001_1", _po2_0001_1);
        Command.Parameters.AddWithValue("@im2_0001_1", _im2_0001_1);
        Command.Parameters.AddWithValue("@ida_0001_1", _ida_0001_1);
        Command.Parameters.AddWithValue("@uar_0001_1", _uar_0001_1);
        Command.Parameters.AddWithValue("@far_0001_1", _far_0001_1);
        Command.Parameters.AddWithValue("@dip_0001_1", _dip_0001_1);
        Command.Parameters.AddWithValue("@uua_0001_1", _uua_0001_1);
        Command.Parameters.AddWithValue("@fua_0001_1", _fua_0001_1);
        Command.Connection = _sqlCon;

        _ide_0001_1 = (int)Command.ExecuteScalar();
        Command = null;
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.gesfac_0001_1 WHERE ide_0001_1 = " + _ide_0001_1;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001_1 = (int)dr["ide_0001_1"];

            if (AsignarPropiedades)
            {
                ide_0001 = (int)dr["ide_0001"];
                tim_0001_1 = (byte)Convert.ToByte(dr["tim_0001_1"]);
                ipo_0001_1 = (int)dr["ipo_0001_1"];
                sde_0001_1 = Convert.ToByte(dr["sde_0001_1"]);
                bas_0001_1 = (decimal)dr["bas_0001_1"];
                po1_0001_1 = (decimal)dr["po1_0001_1"];
                im1_0001_1 = (decimal)dr["im1_0001_1"];
                po2_0001_1 = (decimal)dr["po2_0001_1"];
                im2_0001_1 = (decimal)dr["im2_0001_1"];
                ida_0001_1 = (string)dr["ida_0001_1"];
                uar_0001_1 = (int)dr["uar_0001_1"];
                far_0001_1 = dr["far_0001_1"].ToString();
                dip_0001_1 = (string)dr["dip_0001_1"];
                uua_0001_1 = (int)dr["uua_0001_1"];
                fua_0001_1 = dr["fua_0001_1"].ToString();
            }
        }
        else
        {
            ide_0001_1 = 0;
        }

        dr.Close();
        Command = null;
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.gesfac_0001_1 WHERE ide_0001_1 = " + _ide_0001_1;

        Command.ExecuteNonQuery();
        Command = null;
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.gesfac_0001_1 SET ");
        strSQL += String.Concat("  ide_0001 = @ide_0001");
        strSQL += String.Concat(", tim_0001_1 = @tim_0001_1");
        strSQL += String.Concat(", ipo_0001_1 = @ipo_0001_1");
        strSQL += String.Concat(", sde_0001_1 = @sde_0001_1");
        strSQL += String.Concat(", bas_0001_1 = @bas_0001_1");
        strSQL += String.Concat(", po1_0001_1 = @po1_0001_1");
        strSQL += String.Concat(", im1_0001_1 = @im1_0001_1");
        strSQL += String.Concat(", po2_0001_1 = @po2_0001_1");
        strSQL += String.Concat(", im2_0001_1 = @im2_0001_1");
        strSQL += String.Concat(", ida_0001_1 = @ida_0001_1");
        strSQL += String.Concat(", dip_0001_1 = @dip_0001_1");
        strSQL += String.Concat(", uua_0001_1 = @uua_0001_1");
        strSQL += String.Concat(", fua_0001_1 = @fua_0001_1");
        strSQL += String.Concat(" WHERE ide_0001_1 = ", _ide_0001_1);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_0001", _ide_0001);
        Command.Parameters.AddWithValue("@tim_0001_1", _tim_0001_1);
        Command.Parameters.AddWithValue("@ipo_0001_1", _ipo_0001_1);
        Command.Parameters.AddWithValue("@sde_0001_1", _sde_0001_1);
        Command.Parameters.AddWithValue("@bas_0001_1", _bas_0001_1);
        Command.Parameters.AddWithValue("@po1_0001_1", _po1_0001_1);
        Command.Parameters.AddWithValue("@im1_0001_1", _im1_0001_1);
        Command.Parameters.AddWithValue("@po2_0001_1", _po2_0001_1);
        Command.Parameters.AddWithValue("@im2_0001_1", _im2_0001_1);
        Command.Parameters.AddWithValue("@ida_0001_1", _ida_0001_1);
        Command.Parameters.AddWithValue("@dip_0001_1", _dip_0001_1);
        Command.Parameters.AddWithValue("@uua_0001_1", _uua_0001_1);
        Command.Parameters.AddWithValue("@fua_0001_1", _fua_0001_1);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
        Command = null;
    }

    #endregion
}