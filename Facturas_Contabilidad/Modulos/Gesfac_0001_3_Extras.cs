using System;
using System.Data;
using System.Data.SqlClient;

public class Gesfac_0001_3_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001_3;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_0001;                                          // 2    
    private decimal _bas_0001_3;                                    // 3    
    private decimal _imd_0001_3;                                    // 4    
    private decimal _imn_0001_3;                                    // 5    
    private decimal _ica_0001_3;                                    // 6    
    private decimal _idt_0001_3;                                    // 7    
    private decimal _bre_0001_3;                                    // 8    
    private decimal _pre_0001_3;                                    // 9    
    private decimal _ret_0001_3;                                    // 10    
    private decimal? _liq_0001_3;                                   // 11    CALCULADO
    private string _fpr_0001_3;                                     // 12    
    private short? _cej_0001_3;                                     // 13    
    private int? _nex_0001_3;                                       // 14    
    private int? _ndr_0001_3;                                       // 15    
    private int? _ndp_0001_3;                                       // 16    
    private byte _sdo_0001_3;                                       // 17    
    private byte _spp_0001_3;                                       // 18    
    private byte _edr_0001_3;                                       // 19    
    private int _uar_0001_3;                                        // 20    
    private string _far_0001_3;                                     // 21    
    private string _dip_0001_3;                                     // 22    
    private int _uua_0001_3;                                        // 23    
    private string _fua_0001_3;                                     // 24    

    #endregion

    #region Propiedades

    public int ide_0001_3
    {
        get { return _ide_0001_3; }
        set { _ide_0001_3 = value; }
    }
    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public decimal bas_0001_3
    {
        get { return _bas_0001_3; }
        set { _bas_0001_3 = value; }
    }
    public decimal imd_0001_3
    {
        get { return _imd_0001_3; }
        set { _imd_0001_3 = value; }
    }
    public decimal imn_0001_3
    {
        get { return _imn_0001_3; }
        set { _imn_0001_3 = value; }
    }
    public decimal ica_0001_3
    {
        get { return _ica_0001_3; }
        set { _ica_0001_3 = value; }
    }
    public decimal idt_0001_3
    {
        get { return _idt_0001_3; }
        set { _idt_0001_3 = value; }
    }
    public decimal bre_0001_3
    {
        get { return _bre_0001_3; }
        set { _bre_0001_3 = value; }
    }
    public decimal pre_0001_3
    {
        get { return _pre_0001_3; }
        set { _pre_0001_3 = value; }
    }
    public decimal ret_0001_3
    {
        get { return _ret_0001_3; }
        set { _ret_0001_3 = value; }
    }
    public decimal? liq_0001_3
    {
        get { return _liq_0001_3; }
        set { _liq_0001_3 = value; }
    }
    public string fpr_0001_3
    {
        get { return _fpr_0001_3; }
        set { _fpr_0001_3 = value; }
    }
    public short? cej_0001_3
    {
        get { return _cej_0001_3; }
        set { _cej_0001_3 = value; }
    }
    public int? nex_0001_3
    {
        get { return _nex_0001_3; }
        set { _nex_0001_3 = value; }
    }
    public int? ndr_0001_3
    {
        get { return _ndr_0001_3; }
        set { _ndr_0001_3 = value; }
    }
    public int? ndp_0001_3
    {
        get { return _ndp_0001_3; }
        set { _ndp_0001_3 = value; }
    }
    public byte sdo_0001_3
    {
        get { return _sdo_0001_3; }
        set { _sdo_0001_3 = value; }
    }
    public byte spp_0001_3
    {
        get { return _spp_0001_3; }
        set { _spp_0001_3 = value; }
    }
    public byte edr_0001_3
    {
        get { return _edr_0001_3; }
        set { _edr_0001_3 = value; }
    }
    public int uar_0001_3
    {
        get { return _uar_0001_3; }
        set { _uar_0001_3 = value; }
    }
    public string far_0001_3
    {
        get { return _far_0001_3; }
        set { _far_0001_3 = value; }
    }
    public string dip_0001_3
    {
        get { return _dip_0001_3; }
        set { _dip_0001_3 = value; }
    }
    public int uua_0001_3
    {
        get { return _uua_0001_3; }
        set { _uua_0001_3 = value; }
    }
    public string fua_0001_3
    {
        get { return _fua_0001_3; }
        set { _fua_0001_3 = value; }
    }

    #endregion

    #region Constructores

    public Gesfac_0001_3_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001_3 = 0;
        _ide_0001 = 0;
        _bas_0001_3 = 0;
        _imd_0001_3 = 0;
        _imn_0001_3 = 0;
        _ica_0001_3 = 0;
        _idt_0001_3 = 0;
        _bre_0001_3 = 0;
        _pre_0001_3 = 0;
        _ret_0001_3 = 0;
        _liq_0001_3 = null;
        _fpr_0001_3 = null;
        _cej_0001_3 = null;
        _nex_0001_3 = null;
        _ndr_0001_3 = null;
        _ndp_0001_3 = null;
        _sdo_0001_3 = 0;
        _spp_0001_3 = 0;
        _edr_0001_3 = 0;
        _uar_0001_3 = 0;
        _far_0001_3 = "";
        _dip_0001_3 = "";
        _uua_0001_3 = 0;
        _fua_0001_3 = "";
    }

    #endregion

    #region Metodos Públicos

    /// <summary>
    /// Actualiza el/los registros de una factura<br/>
    /// · Ejercicio contable<br/>
    /// · Número de expediente<br/>
    /// · Número de documento reconocimiento
    /// </summary>
    public void Actualizar_Extras_Reconocido()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.gesfac_0001_3 SET");
        strSQL += string.Concat(" cej_0001_3 = @cej_0001_3");
        strSQL += string.Concat(", nex_0001_3 = @nex_0001_3");
        strSQL += string.Concat(", ndr_0001_3 = @ndr_0001_3");
        strSQL += string.Concat(", dip_0001_3 = @dip_0001_3");
        strSQL += string.Concat(", uua_0001_3 = @uua_0001_3");
        strSQL += string.Concat(", fua_0001_3 = @fua_0001_3");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        if (_cej_0001_3 == null)
            Command.Parameters.Add("@cej_0001_3", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@cej_0001_3", _cej_0001_3);

        if (_nex_0001_3 == null)
            Command.Parameters.Add("@nex_0001_3", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@nex_0001_3", _nex_0001_3);

        if (_ndr_0001_3 == null)
            Command.Parameters.Add("@ndr_0001_3", SqlDbType.Int).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ndr_0001_3", _ndr_0001_3);

        Command.Parameters.AddWithValue("@dip_0001_3", _dip_0001_3);
        Command.Parameters.AddWithValue("@uua_0001_3", _uua_0001_3);
        Command.Parameters.AddWithValue("@fua_0001_3", _fua_0001_3);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza documento de pago de una factura<br/>
    /// · Fecha pago real<br/>
    /// · Número de documento de pago<br/>
    /// · Situación documento de pago
    /// </summary>
    /// <param name="Situacion">Suma/resta a la situacion documento de pago</param>
    public void Actualizar_Extras_Pagado(sbyte Situacion)
    {
        string Cadena_Sql;
        string Cadena_Update = $"UPDATE {_BaseDatos}.dbo.gesfac_0001_3";
        string Cadena_Valores;
        string Cadena_Where;

        Cadena_Valores = _fpr_0001_3 == null ? $" SET fpr_0001_3 = {DBNull.Value}," : $" SET fpr_0001_3 = {_fpr_0001_3},";

        if (_ndp_0001_3 == null)
            Cadena_Valores += $" ndp_0001_3 = {DBNull.Value},";
        else
            Cadena_Valores += $" ndp_0001_3 = {_ndp_0001_3},";

        Cadena_Valores += $" sdo_0001_3 += {Situacion},";
        Cadena_Valores += $" dip_0001_3 = '{_dip_0001_3}',";
        Cadena_Valores += $" uua_0001_3 = {_uua_0001_3},";
        Cadena_Valores += $" uua_0001_3 = '{_fua_0001_3}'";
        Cadena_Where = $" WHERE ide_0001 = {_ide_0001}";

        Cadena_Sql = $"{Cadena_Update}{Cadena_Valores}{Cadena_Where}";

        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = Cadena_Sql
        };

        Command.ExecuteNonQuery();
    }

    #endregion

}