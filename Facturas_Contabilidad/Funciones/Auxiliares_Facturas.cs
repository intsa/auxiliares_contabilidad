﻿using System;
using System.Data.SqlClient;

namespace Facturas_Contabilidad
{
    public static class Auxiliares_Facturas
    {
        #region Obtención de datos

        /// <summary>
        /// Obtiene datos 'FACTURAS/DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Libro_Facturas">Libro facturas<br/>
        /// · 1 - Compras y gastos<br/>
        /// · 2 - Intracomunitarias<br/>
        /// · 3 - Bienes de inversión<br/>
        /// · 4 - Subvenciones
        /// </param>
        /// <param name="Ejercicio">Ejercicio factura</param>
        /// <param name="Numero_Orden">Número de orden</param>
        /// <returns>
        /// Item1 : Identificador registro 'FACTURAS/DOCUMENTOS RECIBIDOS' [ide_0001]<br/>
        /// Item2 : Tipo de factura [tfa_0001]<br/>
        /// · 1 - Normal<br/>
        /// · 2 - Anticipo caja<br/>
        /// · 3 - Gasto justificar<br/>
        /// · 4 - Suplidos<br/>
        /// · 5 - Pagos fijos<br/>
        /// Item3 : Situación contable [sco_0001]<br/>
        /// · 0 - Anulado<br/>
        /// · 1 - NO contabilizado<br/>
        /// · 2 - Retenido<br/>
        /// · 3 - Comprometido<br/>
        /// · 4 - Reconocido<br/>
        /// · 5 - Pagado<br/>
        /// · 6 - Reintegro de pago<br/>
        /// · 7 - Contabilizado<br/>
        /// · 8 - Pendiente cumplimentar face<br/>
        /// ·  9 - Rechazado<br/>
        /// · 10 - Pago anulado<br/>
        /// · 11 - Pago compensado<br/>
        /// Item4 : Situación pago parcial [spp_0001]<br/>
        /// · 1 - Pagada<br/>
        /// · 2 - Pago parcial<br/>
        /// · 3 - Pendiente<br/>
        /// · 4 - Factura importe cero<br/>
        /// · 5 - Situación errónea<br/>
        /// Item5 : Cuenta contable imputación [cta_0001]<br/>
        /// Item6 : Importe factura [tot_0001]
        /// </returns>
        public static Tuple<int, byte, byte, int, string, decimal> Datos_Gesfac_0001(string CadenaConexion, string Nombre_Bdd, byte Libro_Facturas, short Ejercicio, int Numero_Orden)
        {
            int Id_0001 = 0;
            byte Tipo_Factura = 0;
            byte Situacion_Contable = 0;
            int Situacion_Pagos = 0;
            string Cuenta = "";
            decimal Importe = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001, tfa_0001, sco_0001, spp_0001, cta_0001, tot_0001";
            string Cadena_From = $" FROM {Nombre_Bdd}.dbo.gesfac_0001";
            string Restriccion_Libro = $"lfr_0001 = {Libro_Facturas}";
            string Restriccion_Ejercicio = $"eje_0001 = {Ejercicio}";
            string Restriccion_Orden = $"ord_0001 = {Numero_Orden}";
            string Cadena_Restricciones = $"{Restriccion_Libro} AND {Restriccion_Ejercicio} AND {Restriccion_Orden}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_0001 = (int)Data_Reader["ide_0001"];
                        Tipo_Factura = (byte)Data_Reader["tfa_0001"];
                        Situacion_Contable = (byte)Data_Reader["sco_0001"];
                        Situacion_Pagos = (int)Data_Reader["spp_0001"];
                        Cuenta = (string)Data_Reader["cta_0001"];
                        Importe = (decimal)Data_Reader["tot_0001"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, byte, byte, int, string, decimal>(Id_0001, Tipo_Factura, Situacion_Contable, Situacion_Pagos, Cuenta, Importe);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'FACTURAS/DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001">Identificador registro 'FACTURAS/DOCUMENTOS RECIBIDOS'</param>
        /// <returns>
        /// Item1 : Identificador registro 'FACTURAS/DOCUMENTOS RECIBIDOS' [ide_0001]<br/>
        /// Item2 : Tipo de factura [tfa_0001]<br/>
        /// · 1 - Normal<br/>
        /// · 2 - Anticipo caja<br/>
        /// · 3 - Gasto justificar<br/>
        /// · 4 - Suplidos<br/>
        /// · 5 - Pagos fijos<br/>
        /// Item3 : Situación contable [sco_0001]<br/>
        /// · 0 - Anulado<br/>
        /// · 1 - NO contabilizado<br/>
        /// · 2 - Retenido<br/>
        /// · 3 - Comprometido<br/>
        /// · 4 - Reconocido<br/>
        /// · 5 - Pagado<br/>
        /// · 6 - Reintegro de pago<br/>
        /// · 7 - Contabilizado<br/>
        /// · 8 - Pendiente cumplimentar face<br/>
        /// ·  9 - Rechazado<br/>
        /// · 10 - Pago anulado<br/>
        /// · 11 - Pago compensado<br/>
        /// Item4 : Situación pago parcial [spp_0001]<br/>
        /// · 1 - Pagada<br/>
        /// · 2 - Pago parcial<br/>
        /// · 3 - Pendiente<br/>
        /// · 4 - Factura importe cero<br/>
        /// · 5 - Situación errónea<br/>
        /// Item5 : Cuenta contable imputación [cta_0001]<br/>
        /// Item6 : Importe factura [tot_0001]
        /// </returns>
        public static Tuple<int, byte, byte, int, string, decimal> Datos_Gesfac_0001(string CadenaConexion, string Nombre_Bdd, int Pid_f0001)
        {
            int Id_0001 = 0;
            byte Tipo_Factura = 0;
            byte Situacion_Contable = 0;
            int Situacion_Pagos = 0;
            string Cuenta = "";
            decimal Importe = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001, tfa_0001, sco_0001, spp_0001, cta_0001, tot_0001";
            string Cadena_From = $" FROM {Nombre_Bdd}.dbo.gesfac_0001";
            string Restriccion_Identificador = $"ide_0001 = {Pid_f0001}";
            string Cadena_Restricciones = $"{Restriccion_Identificador}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_0001 = (int)Data_Reader["ide_0001"];
                        Tipo_Factura = (byte)Data_Reader["tfa_0001"];
                        Situacion_Contable = (byte)Data_Reader["sco_0001"];
                        Situacion_Pagos = (int)Data_Reader["spp_0001"];
                        Cuenta = (string)Data_Reader["cta_0001"];
                        Importe = (decimal)Data_Reader["tot_0001"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, byte, byte, int, string, decimal>(Id_0001, Tipo_Factura, Situacion_Contable, Situacion_Pagos, Cuenta, Importe);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador 'FACTURAS/DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Id_Traspaso">Identificador traspaso</param>
        /// <returns></returns>
        public static int Identificador_Gesfac_0001(string CadenaConexion, string Nombre_Bdd, string Id_Traspaso)
        {
            int Id_Registro;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001";
            string Cadena_From = $" FROM {Nombre_Bdd}.dbo.gesfac_0001";
            string Restriccion_Antiguo = $"ida_0001 = '{Id_Traspaso}'";
            string Cadena_Restricciones = Restriccion_Antiguo;
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Id_Registro = Data_Reader.Read() ? (int)Data_Reader["ide_0001"] : 0;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_Registro;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Obtención de datos

        #region Verificación de datos

        /// <summary>
        /// Obtiene el número de registros relacionados de una factura-documento
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conextion</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001">Identificador registro 'FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <returns>Número de registros relacionados</returns>
        public static int Registros_Relacionados_Gesfac_0001(string CadenaConexion, string Nombre_Bdd, int Pid_f0001)
        {
            int Numero_Registros;

            string Cadena_Sql;
            string Cadena_Select;
            string Cadena_From;
            string Restriccion_Identificador = $"ide_0001 = {Pid_f0001}";
            string Cadena_Restricciones = Restriccion_Identificador;
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            Cadena_Select = "SELECT gesfac_0001.ide_0001,";
            Cadena_Select = Cadena_Select + $" ((SELECT count (*) FROM {Nombre_Bdd}.dbo.gesfac_0001_1 WHERE gesfac_0001_1.ide_0001 = gesfac_0001.ide_0001) +";
            Cadena_Select = Cadena_Select + $" (SELECT count (*) FROM {Nombre_Bdd}.dbo.gesfac_0001_2 WHERE gesfac_0001_2.ide_0001 = gesfac_0001.ide_0001) +";
            Cadena_Select = Cadena_Select + $" (SELECT count (*) FROM {Nombre_Bdd}.dbo.gesfac_0001_3 WHERE gesfac_0001_3.ide_0001 = gesfac_0001.ide_0001) +";
            Cadena_Select = Cadena_Select + $" (SELECT count (*) FROM {Nombre_Bdd}.dbo.gesfac_0001_4 WHERE gesfac_0001_4.ide_0001 = gesfac_0001.ide_0001) +";
            Cadena_Select = Cadena_Select + $" (SELECT count (*) FROM {Nombre_Bdd}.dbo.gesfac_0001_5 WHERE gesfac_0001_5.ide_0001 = gesfac_0001.ide_0001)) numreg";
            Cadena_From = $" FROM {Nombre_Bdd}.dbo.gesfac_0001";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    Numero_Registros = Data_Reader.Read() ? (int)Data_Reader["numreg"] : 0;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Numero_Registros;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica si existe la forma de pago/cobro en 'FACTURAS/DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Nombre_Bdd">Nombre BDD 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param> 
        /// <param name="Codigo">Código forma de pago/cobro</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Verificar_Forma_Pago(string CadenaConexion, string Nombre_Bdd, short Codigo)
        {
            bool Existe;

            string Cadena_Sql;
            string[] Sql_Cadena = new string[1];
            string[] Cadena_Select = { "SELECT TOP (1) fop_0001 forpag" };
            string[] Cadena_From = { $" FROM {Nombre_Bdd}.dbo.gesfac_0001" };
            string[] Restriccion_Forma_Pago = { $"fop_0001 =  {Codigo}" };
            string[] Cadena_Where = { $" WHERE {Restriccion_Forma_Pago[0]}" };

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand { Connection = Cadena_Conexion };

                    for (byte i = 0; i < Sql_Cadena.Length; i++)
                        Sql_Cadena[i] = $"{Cadena_Select[i]}{Cadena_From[i]}{Cadena_Where[i]}";

                    Cadena_Sql = Sql_Cadena[0];

                    for (byte i = 1; i < Sql_Cadena.Length; i++)
                        Cadena_Sql += $" UNION ALL {Sql_Cadena[i]}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica si existe el canal de pago/cobro en 'FACTURAS/DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Nombre_Bdd">Nombre BDD 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param> 
        /// <param name="Ejercicio">Ejercicio a comprobar/cobro</param>
        /// <param name="Codigo">Código canal de pago/cobro</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Verificar_Canal_Pago(string CadenaConexion, string Nombre_Bdd, short Ejercicio, short Codigo)
        {
            bool Existe;

            string Cadena_Sql;
            string[] Sql_Cadena = new string[1];
            string[] Cadena_Select = { "SELECT TOP (1) can_0001 forpag" };
            string[] Cadena_From = { $" FROM {Nombre_Bdd}.dbo.gesfac_0001" };
            string[] Restriccion_Ejercicio = { $"eje_0001 =  {Ejercicio}" };
            string[] Restriccion_Canal_Pago = { $"can_0001 =  {Codigo}" };
            string[] Cadena_Where = { $" WHERE {Restriccion_Ejercicio[0]} AND {Restriccion_Canal_Pago[0]}" };

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand { Connection = Cadena_Conexion };

                    for (byte i = 0; i < Sql_Cadena.Length; i++)
                        Sql_Cadena[i] = $"{Cadena_Select[i]}{Cadena_From[i]}{Cadena_Where[i]}";

                    Cadena_Sql = Sql_Cadena[0];

                    for (byte i = 1; i < Sql_Cadena.Length; i++)
                        Cadena_Sql += $" UNION ALL {Sql_Cadena[i]}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica si existe el tipo de factura en 'FACTURAS/DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Nombre_Bdd">Nombre BDD 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param> 
        /// <param name="Codigo">Código tipo de factura/cobro</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Verificar_Tipo_Factura(string CadenaConexion, string Nombre_Bdd, short Codigo)
        {
            bool Existe;

            string Cadena_Sql;
            string Cadena_Select = "SELECT TOP (1) ide_0001 idereg";
            string Cadena_From = $" FROM {Nombre_Bdd}.dbo.gesfac_0001";
            string Restriccion_Tipo_Factura = $"tdf_0001 =  {Codigo}";
            string Cadena_Where = $" WHERE {Restriccion_Tipo_Factura}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica si existe el tipo de contrato en 'FACTURAS/DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Nombre_Bdd">Nombre BDD 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param> 
        /// <param name="Codigo">Código tipo de contrato</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Verificar_Tipo_Contrato(string CadenaConexion, string Nombre_Bdd, short Codigo)
        {
            bool Existe;

            string Cadena_Sql;
            string Cadena_Select = "SELECT TOP (1) ide_0001 idereg";
            string Cadena_From = $" FROM {Nombre_Bdd}.dbo.gesfac_0001";
            string Restriccion_Tipo_Factura = $"tco_0001 =  {Codigo}";
            string Cadena_Where = $" WHERE {Restriccion_Tipo_Factura}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica si existe el tipo de sesión en 'FACTURAS/DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Nombre_Bdd">Nombre BDD 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param> 
        /// <param name="Codigo">Código tipo de sesión/cobro</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Verificar_Tipo_Sesion(string CadenaConexion, string Nombre_Bdd, short Codigo)
        {
            bool Existe;

            string Cadena_Sql;
            string Cadena_Select = "SELECT TOP (1) ide_0001 idereg";
            string Cadena_From = $" FROM {Nombre_Bdd}.dbo.gesfac_0001";
            string Restriccion_Tipo_Factura = $"tse_0001 =  {Codigo}";
            string Cadena_Where = $" WHERE {Restriccion_Tipo_Factura}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica si existe el tipo de sesión en 'FACTURAS/DOCUMENTOS RECIBIDOS. MOTIVO DE DISCONFORMIDAD/REPARO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Nombre_Bdd">Nombre BDD 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param> 
        /// <param name="Pid_2050_1">Identificador registro 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO. DETALLE'</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Verificar_Motivo(string CadenaConexion, string Nombre_Bdd, int Pid_2050_1)
        {
            bool Existe;

            string Cadena_Sql;
            string Cadena_Select = "SELECT TOP (1) ide_0001_2 idereg";
            string Cadena_From = $" FROM {Nombre_Bdd}.dbo.gesfac_0001_2";
            string Restriccion_Identificador = $"idr_0001_2 =  {Pid_2050_1}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Verificación de datos

    }

}