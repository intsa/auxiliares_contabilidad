﻿using ClaseIntsa.Propiedades;
using System;
using System.Data.SqlClient;

namespace Facturas_Contabilidad
{
    public static class Desencadenantes_Facturas
    {
        #region Gesfac_0001 'FACTURAS Y DOCUMENTOS RECIBIDOS'

        /// <summary>
        /// Actualiza los importes tabla 'FACTURAS Y DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001">Identificador registro tabla 'FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Base_Imponible">Importe base imponible</param>
        /// <param name="Impuesto_Deducible">Importe impuestos deducibles</param>
        /// <param name="Impuesto_No_Deducible">Importe impuestos NO deducibles</param>
        /// <param name="Base_Retenciones">Importe base imponible I.R.P.F.</param>
        /// <param name="Retenciones">Importe impuestos retenciones</param>
        public static void Actualizar_Gesfac_0001_Importes(string CadenaConexion, string Nombre_Bdd, int Pid_f0001, decimal Base_Imponible, decimal Impuesto_Deducible, decimal Impuesto_No_Deducible, decimal Base_Retenciones, decimal Retenciones)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Gesfac_0001_Extras Temporal = new Gesfac_0001_Extras(Cadena_Conexion, Nombre_Bdd);

                    Cadena_Conexion.Open();
                    Temporal.ide_0001 = Pid_f0001;
                    Temporal.bas_0001 = Base_Imponible;
                    Temporal.imd_0001 = Impuesto_Deducible;
                    Temporal.imn_0001 = Impuesto_No_Deducible;
                    Temporal.ibr_0001 = Base_Retenciones;
                    Temporal.ret_0001 = Retenciones;

                    Temporal.Actualizar_Importes();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza el importe pagado tabla 'FACTURAS Y DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001">Identificador registro tabla 'FACTURAS/DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Presupuestario">Importe presupuestario pagado</param>
        public static void Actualizar_Gesfac_0001_Pago_Parcial(string CadenaConexion, string Nombre_Bdd, int Pid_f0001, decimal Presupuestario)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Gesfac_0001_Extras Temporal = new Gesfac_0001_Extras(Cadena_Conexion, Nombre_Bdd);

                    Cadena_Conexion.Open();
                    Temporal.ide_0001 = Pid_f0001;

                    Temporal.Actualizar_Extras_Pago_Parcial(Presupuestario);

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Actualiza la situción contable y los datos contables  tabla 'FACTURAS Y DOCUMENTOS RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001">Identificador registro tabla 'FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Ejercicio">Ejercicio expediente contable</param>
        /// <param name="Expediente">Número expediente contable</param>
        /// <param name="Documento">Número expediente contable (Reconocimiento de la obligación)</param>
        /// <param name="Situacion">Situación contable (Reconocimiento de la obligación)</param>
        public static void Actualizar_Gesfac_0001_Reconocido(string CadenaConexion, string Nombre_Bdd, int Pid_f0001, short Ejercicio, int Expediente, int Documento, byte Situacion = 4)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Gesfac_0001_Extras Temporal = new Gesfac_0001_Extras(Cadena_Conexion, Nombre_Bdd)
                    {
                        ide_0001 = Pid_f0001,

                        sco_0001 = Situacion,
                        cej_0001 = Ejercicio,
                        nex_0001 = Expediente,
                        ndr_0001 = Documento,
                        dip_0001 = Variables_Globales.IpPrivada,
                        uua_0001 = Variables_Globales.IdUsuario,
                        fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras_Reconocido();

                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        /// <summary>
        /// Actualiza la situción contable y los datos contables  tabla 'FACTURAS Y DOCUMENTOS. RECIBIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001">Identificador registro tabla 'FACTURAS Y /DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Situacion">Situación contable (Reconocimiento de la obligación)</param>
        public static void Actualizar_Gesfac_0001_Pagado(string CadenaConexion, string Nombre_Bdd, int Pid_f0001, byte Situacion = 5)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Gesfac_0001_Extras Temporal = new Gesfac_0001_Extras(Cadena_Conexion, Nombre_Bdd)
                    {
                        ide_0001 = Pid_f0001,

                        sco_0001 = Situacion,
                        dip_0001 = Variables_Globales.IpPrivada,
                        uua_0001 = Variables_Globales.IdUsuario,
                        fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras_Pagado();

                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        /// <summary>
        /// Actualizar registro tabla 'FACTURAS Y DOCUMENTOS. RECIBIDOS'<br/>
        /// Semáforo disconforme o reparada [sdr_0001]
        /// </summary>
        /// <param name="CadenaConexion"></param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001"></param>
        public static void Actualizar_Gesfac_0001(string CadenaConexion, string Nombre_Bdd, int Pid_f0001)
        {
            string Cadena_Sql;
            string Cadena_Update = $"UPDATE {Nombre_Bdd}.dbo.gesfac_0001";
            string Cadena_Valores = " SET sdr_0001 = 1";
            string Restriccion_Identificador = $"ide_0001 = {Pid_f0001}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Update}{Cadena_Valores}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    Comando_Sql.Connection = Cadena_Conexion;
                    Comando_Sql.CommandText = Cadena_Sql;
                    Comando_Sql.ExecuteNonQuery();

                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Gesfac_0001 'FACTURAS/DOCUMENTOS RECIBIDOS'

        #region Gesfac_0001_3 'FACTURAS/DOCUMENTOS RECIBIDOS. DOCUMENTOS DE PAGO'

        /// <summary>
        /// Actualiza la situción contable y los datos contables  tabla 'FACTURAS Y DOCUMENTOS RECIBIDOS. DOCUMENTOS DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001">Identificador registro tabla 'FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Ejercicio">Ejercicio expediente contable</param>
        /// <param name="Expediente">Número expediente contable</param>
        /// <param name="Documento">Número expediente contable (Reconocimiento de la obligación)</param>
        public static void Actualizar_Gesfac_0001_3_Reconocido(string CadenaConexion, string Nombre_Bdd, int Pid_f0001, short Ejercicio, int Expediente, int Documento)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Gesfac_0001_3_Extras Temporal = new Gesfac_0001_3_Extras(Cadena_Conexion, Nombre_Bdd)
                    {
                        ide_0001 = Pid_f0001,

                        cej_0001_3 = Ejercicio,
                        nex_0001_3 = Expediente,
                        ndr_0001_3 = Documento,
                        dip_0001_3 = Variables_Globales.IpPrivada,
                        uua_0001_3 = Variables_Globales.IdUsuario,
                        fua_0001_3 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras_Reconocido();

                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        /// <summary>
        /// Actualiza la situción contable y los datos contables  tabla 'FACTURAS Y DOCUMENTOS RECIBIDOS. DOCUMENTOS DE PAGO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001_3">Identificador registro 'FACTURAS Y DOCUMENTOS RECIBIDOS. DOCUMENTOS DE PAGO'</param>
        /// <param name="Documento">Documento de pago</param>
        /// <param name="Situacion">Situación documento de pago<br/>
        /// · 0 - Pendiente de pago<br/>
        /// · 1 - Pagado y sin contabilizar<br/>
        /// · 2 - Pagado y contabilizado<br/>
        /// · 3 - NO DEFINIDO
        /// </param>
        public static void Actualizar_Gesfac_0001_3_Pagado(string CadenaConexion, string Nombre_Bdd, int Pid_f0001_3, int? Documento, DateTime? Fecha, sbyte Situacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Gesfac_0001_3_Extras Temporal = new Gesfac_0001_3_Extras(Cadena_Conexion, Nombre_Bdd)
                    {
                        ide_0001_3 = Pid_f0001_3,

                        fpr_0001_3 = Fecha.ToString(),
                        ndp_0001_3 = Documento,
                        dip_0001_3 = Variables_Globales.IpPrivada,
                        uua_0001_3 = Variables_Globales.IdUsuario,
                        fua_0001_3 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras_Pagado(Situacion);

                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        #endregion Gesfac_0001_3 'FACTURAS/DOCUMENTOS RECIBIDOS. DOCUMENTOS DE PAGO'

        #region Gesfac_0001_4 'FACTURAS/DOCUMENTOS RECIBIDOS. FECHAS PREVISTAS'

        /// <summary>
        /// Actualiza la situción contable y los datos contables  tabla 'FACTURAS Y DOCUMENTOS RECIBIDOS. FECHAS PREVISTAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Nombre_Bdd">Nombre base de datos 'GESTIÓN DE FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Pid_f0001">Identificador registro 'FACTURAS Y DOCUMENTOS RECIBIDOS'</param>
        /// <param name="Fecha">Fecha pago real total factura</param>
        /// </param>
        public static void Actualizar_Gesfac_0001_4(string CadenaConexion, string Nombre_Bdd, int Pid_f0001, DateTime? Fecha)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Gesfac_0001_4_Extras Temporal = new Gesfac_0001_4_Extras(Cadena_Conexion, Nombre_Bdd)
                    {
                        ide_0001 = Pid_f0001,

                        fpr_0001_4 = Fecha.ToString(),
                        dip_0001_4 = Variables_Globales.IpPrivada,
                        uua_0001_4 = Variables_Globales.IdUsuario,
                        fua_0001_4= DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Pago_Real();

                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        #endregion Gesfac_0001_4 'FACTURAS/DOCUMENTOS RECIBIDOS. FECHAS PREVISTAS'

    }

}