using System;
using System.Data;
using System.Data.SqlClient;

public class Alccom_5001
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private short _ide_5001;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private byte _niv_5001;                                         // 2    
    private string _des_5001;                                       // 3    
    private byte _lnc_5001;                                         // 4    
    private short? _aiv_5001;                                       // 5    
    private short? _afv_5001;                                       // 6    
    private int _uar_5001;                                          // 7    
    private string _far_5001;                                       // 8    
    private string _dip_5001;                                       // 9    
    private int _uua_5001;                                          // 10    
    private string _fua_5001;                                       // 11    

    #endregion

    #region Propiedades

    public short ide_5001
    {
        get { return _ide_5001; }
        set { _ide_5001 = value; }
    }
    public byte niv_5001
    {
        get { return _niv_5001; }
        set { _niv_5001 = value; }
    }
    public string des_5001
    {
        get { return _des_5001; }
        set { _des_5001 = value; }
    }
    public byte lnc_5001
    {
        get { return _lnc_5001; }
        set { _lnc_5001 = value; }
    }
    public short? aiv_5001
    {
        get { return _aiv_5001; }
        set { _aiv_5001 = value; }
    }
    public short? afv_5001
    {
        get { return _afv_5001; }
        set { _afv_5001 = value; }
    }
    public int uar_5001
    {
        get { return _uar_5001; }
        set { _uar_5001 = value; }
    }
    public string far_5001
    {
        get { return _far_5001; }
        set { _far_5001 = value; }
    }
    public string dip_5001
    {
        get { return _dip_5001; }
        set { _dip_5001 = value; }
    }
    public int uua_5001
    {
        get { return _uua_5001; }
        set { _uua_5001 = value; }
    }
    public string fua_5001
    {
        get { return _fua_5001; }
        set { _fua_5001 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_5001(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_5001 = 0;
        _niv_5001 = 0;
        _des_5001 = "";
        _lnc_5001 = 0;
        _aiv_5001 = null;
        _afv_5001 = null;
        _uar_5001 = 0;
        _far_5001 = "";
        _dip_5001 = "";
        _uua_5001 = 0;
        _fua_5001 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_5001 (niv_5001, des_5001, lnc_5001, aiv_5001, afv_5001, uar_5001, far_5001, dip_5001, uua_5001, fua_5001)" +
            " OUTPUT INSERTED.ide_5001 VALUES (@niv_5001, @des_5001, @lnc_5001, @aiv_5001, @afv_5001, @uar_5001, @far_5001, @dip_5001, @uua_5001, @fua_5001)";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@niv_5001", _niv_5001);
        Command.Parameters.AddWithValue("@des_5001", _des_5001);
        Command.Parameters.AddWithValue("@lnc_5001", _lnc_5001);

        if (_aiv_5001 == null)
            Command.Parameters.Add("@aiv_5001", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@aiv_5001", _aiv_5001);

        if (_afv_5001 == null)
            Command.Parameters.Add("@afv_5001", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@afv_5001", _afv_5001);

        Command.Parameters.AddWithValue("@uar_5001", _uar_5001);
        Command.Parameters.AddWithValue("@far_5001", _far_5001);
        Command.Parameters.AddWithValue("@dip_5001", _dip_5001);
        Command.Parameters.AddWithValue("@uua_5001", _uua_5001);
        Command.Parameters.AddWithValue("@fua_5001", _fua_5001);

        Command.Connection = _sqlCon;
        _ide_5001 = (short)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_5001 WHERE ide_5001 = {_ide_5001}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_5001 = (short)dr["ide_5001"];

            if (AsignarPropiedades)
            {
                niv_5001 = Convert.ToByte(dr["niv_5001"]);
                des_5001 = (string)dr["des_5001"];
                lnc_5001 = Convert.ToByte(dr["lnc_5001"]);
                aiv_5001 = DBNull.Value.Equals(dr["aiv_5001"]) ? null : (short?)dr["aiv_5001"];
                afv_5001 = DBNull.Value.Equals(dr["afv_5001"]) ? null : (short?)dr["afv_5001"];
                uar_5001 = (int)dr["uar_5001"];
                far_5001 = dr["far_5001"].ToString();
                dip_5001 = (string)dr["dip_5001"];
                uua_5001 = (int)dr["uua_5001"];
                fua_5001 = dr["fua_5001"].ToString();
            }
        }
        else
            ide_5001 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_5001 WHERE ide_5001 = {_ide_5001}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_5001 SET");
        strSQL += string.Concat(" niv_5001 = @niv_5001");
        strSQL += string.Concat(", des_5001 = @des_5001");
        strSQL += string.Concat(", lnc_5001 = @lnc_5001");
        strSQL += string.Concat(", aiv_5001 = @aiv_5001");
        strSQL += string.Concat(", afv_5001 = @afv_5001");
        strSQL += string.Concat(", dip_5001 = @dip_5001");
        strSQL += string.Concat(", uua_5001 = @uua_5001");
        strSQL += string.Concat(", fua_5001 = @fua_5001");
        strSQL += string.Concat(" WHERE ide_5001 = ", _ide_5001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@niv_5001", _niv_5001);
        Command.Parameters.AddWithValue("@des_5001", _des_5001);
        Command.Parameters.AddWithValue("@lnc_5001", _lnc_5001);

        if (_aiv_5001 == null)
            Command.Parameters.Add("@aiv_5001", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@aiv_5001", _aiv_5001);

        if (_afv_5001 == null)
            Command.Parameters.Add("@afv_5001", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@afv_5001", _afv_5001);

        Command.Parameters.AddWithValue("@dip_5001", _dip_5001);
        Command.Parameters.AddWithValue("@uua_5001", _uua_5001);
        Command.Parameters.AddWithValue("@fua_5001", _fua_5001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}