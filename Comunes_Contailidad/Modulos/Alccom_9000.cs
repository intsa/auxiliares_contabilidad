using System;
using System.Data;
using System.Data.SqlClient;

public class Alccom_9000
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_9000;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _cod_9000;                                        // 2    
    private string _ap1_9000;                                       // 3    
    private string _ap2_9000;                                       // 4    
    private string _nom_9000;                                       // 5    
    private string _car_9000;                                       // 6    
    private string _fiv_9000;                                         // 7    
    private string _ffv_9000;                                         // 8    
    private int _uar_9000;                                          // 9    
    private string _far_9000;                                       // 10    
    private string _dip_9000;                                       // 11    
    private int _uua_9000;                                          // 12    
    private string _fua_9000;                                       // 13    

    #endregion

    #region Propiedades

    public int ide_9000
    {
        get { return _ide_9000; }
        set { _ide_9000 = value; }
    }
    public short cod_9000
    {
        get { return _cod_9000; }
        set { _cod_9000 = value; }
    }
    public string ap1_9000
    {
        get { return _ap1_9000; }
        set { _ap1_9000 = value; }
    }
    public string ap2_9000
    {
        get { return _ap2_9000; }
        set { _ap2_9000 = value; }
    }
    public string nom_9000
    {
        get { return _nom_9000; }
        set { _nom_9000 = value; }
    }
    public string car_9000
    {
        get { return _car_9000; }
        set { _car_9000 = value; }
    }
    public string fiv_9000
    {
        get { return _fiv_9000; }
        set
        { _fiv_9000 = value; }
    }
    public string ffv_9000
    {
        get { return _ffv_9000; }
        set { _ffv_9000 = value; }
    }
    public int uar_9000
    {
        get { return _uar_9000; }
        set { _uar_9000 = value; }
    }
    public string far_9000
    {
        get { return _far_9000; }
        set { _far_9000 = value; }
    }
    public string dip_9000
    {
        get { return _dip_9000; }
        set { _dip_9000 = value; }
    }
    public int uua_9000
    {
        get { return _uua_9000; }
        set { _uua_9000 = value; }
    }
    public string fua_9000
    {
        get { return _fua_9000; }
        set { _fua_9000 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_9000(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_9000 = 0;
        _cod_9000 = 0;
        _ap1_9000 = "";
        _ap2_9000 = "";
        _nom_9000 = "";
        _car_9000 = "";
        _fiv_9000 = "";
        _ffv_9000 = "";
        _uar_9000 = 0;
        _far_9000 = "";
        _dip_9000 = "";
        _uua_9000 = 0;
        _fua_9000 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_9000 (cod_9000, ap1_9000, ap2_9000, nom_9000, car_9000, fiv_9000, ffv_9000, uar_9000, far_9000, dip_9000, uua_9000, fua_9000)" +
            $" OUTPUT INSERTED.ide_9000 VALUES (@cod_9000, @ap1_9000, @ap2_9000, @nom_9000, @car_9000, @fiv_9000, @ffv_9000, @uar_9000, @far_9000, @dip_9000, @uua_9000, @fua_9000)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_9000", _cod_9000);
        Command.Parameters.AddWithValue("@ap1_9000", _ap1_9000);
        Command.Parameters.AddWithValue("@ap2_9000", _ap2_9000);
        Command.Parameters.AddWithValue("@nom_9000", _nom_9000);
        Command.Parameters.AddWithValue("@car_9000", _car_9000);

        if (_fiv_9000 == null)
            Command.Parameters.Add("@fiv_9000", SqlDbType.DateTime2).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fiv_9000", _fiv_9000);

        if (_ffv_9000 == null)
            Command.Parameters.Add("@ffv_9000", SqlDbType.DateTime2).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ffv_9000", _ffv_9000);

        Command.Parameters.AddWithValue("@uar_9000", _uar_9000);
        Command.Parameters.AddWithValue("@far_9000", _far_9000);
        Command.Parameters.AddWithValue("@dip_9000", _dip_9000);
        Command.Parameters.AddWithValue("@uua_9000", _uua_9000);
        Command.Parameters.AddWithValue("@fua_9000", _fua_9000);

        Command.Connection = _sqlCon;
        _ide_9000 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_9000 WHERE ide_9000 = {_ide_9000}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_9000 = (int)dr["ide_9000"];

            if (AsignarPropiedades)
            {
                cod_9000 = (short)dr["cod_9000"];
                ap1_9000 = (string)dr["ap1_9000"];
                ap2_9000 = (string)dr["ap2_9000"];
                nom_9000 = (string)dr["nom_9000"];
                car_9000 = (string)dr["car_9000"];
                fiv_9000 = DBNull.Value.Equals(dr["fiv_9000"]) ? null : (string)dr["fiv_9000"];
                ffv_9000 = DBNull.Value.Equals(dr["ffv_9000"]) ? null : (string)dr["ffv_9000"];
                uar_9000 = (int)dr["uar_9000"];
                far_9000 = dr["far_9000"].ToString();
                dip_9000 = (string)dr["dip_9000"];
                uua_9000 = (int)dr["uua_9000"];
                fua_9000 = dr["fua_9000"].ToString();
            }
        }
        else
            ide_9000 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_9000 WHERE ide_9000 = {_ide_9000}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_9000 SET");
        strSQL += string.Concat(" cod_9000 = @cod_9000");
        strSQL += string.Concat(", ap1_9000 = @ap1_9000");
        strSQL += string.Concat(", ap2_9000 = @ap2_9000");
        strSQL += string.Concat(", nom_9000 = @nom_9000");
        strSQL += string.Concat(", car_9000 = @car_9000");
        strSQL += string.Concat(", fiv_9000 = @fiv_9000");
        strSQL += string.Concat(", ffv_9000 = @ffv_9000");
        strSQL += string.Concat(", dip_9000 = @dip_9000");
        strSQL += string.Concat(", uua_9000 = @uua_9000");
        strSQL += string.Concat(", fua_9000 = @fua_9000");
        strSQL += string.Concat(" WHERE ide_9000 = ", _ide_9000);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_9000", _cod_9000);
        Command.Parameters.AddWithValue("@ap1_9000", _ap1_9000);
        Command.Parameters.AddWithValue("@ap2_9000", _ap2_9000);
        Command.Parameters.AddWithValue("@nom_9000", _nom_9000);
        Command.Parameters.AddWithValue("@car_9000", _car_9000);

        if (_fiv_9000 == null)
            Command.Parameters.Add("@fiv_9000", SqlDbType.DateTime2).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fiv_9000", _fiv_9000);

        if (_ffv_9000 == null)
            Command.Parameters.Add("@ffv_9000", SqlDbType.DateTime2).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@ffv_9000", _ffv_9000);

        Command.Parameters.AddWithValue("@dip_9000", _dip_9000);
        Command.Parameters.AddWithValue("@uua_9000", _uua_9000);
        Command.Parameters.AddWithValue("@fua_9000", _fua_9000);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}