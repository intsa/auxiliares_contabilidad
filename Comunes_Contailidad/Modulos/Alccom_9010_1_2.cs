using System;
using System.Data.SqlClient;

public class Alccom_9010_1_2
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_9010_1_2;                                      // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_9010_1;                                        // 2   CLAVE_UNICA 
    private int _ide_9000;                                          // 3   CLAVE_UNICA 
    private short _cod_9010_1_2;                                    // 4    
    private byte _ord_9010_1_2;                                     // 5   CLAVE_UNICA 
    private int _uar_9010_1_2;                                      // 6    
    private string _far_9010_1_2;                                   // 7    
    private string _dip_9010_1_2;                                   // 8    
    private int _uua_9010_1_2;                                      // 9    
    private string _fua_9010_1_2;                                   // 10    

    #endregion

    #region Propiedades

    public int ide_9010_1_2
    {
        get { return _ide_9010_1_2; }
        set { _ide_9010_1_2 = value; }
    }
    public int ide_9010_1
    {
        get { return _ide_9010_1; }
        set { _ide_9010_1 = value; }
    }
    public int ide_9000
    {
        get { return _ide_9000; }
        set { _ide_9000 = value; }
    }
    public short cod_9010_1_2
    {
        get { return _cod_9010_1_2; }
        set { _cod_9010_1_2 = value; }
    }
    public byte ord_9010_1_2
    {
        get { return _ord_9010_1_2; }
        set { _ord_9010_1_2 = value; }
    }
    public int uar_9010_1_2
    {
        get { return _uar_9010_1_2; }
        set { _uar_9010_1_2 = value; }
    }
    public string far_9010_1_2
    {
        get { return _far_9010_1_2; }
        set { _far_9010_1_2 = value; }
    }
    public string dip_9010_1_2
    {
        get { return _dip_9010_1_2; }
        set { _dip_9010_1_2 = value; }
    }
    public int uua_9010_1_2
    {
        get { return _uua_9010_1_2; }
        set { _uua_9010_1_2 = value; }
    }
    public string fua_9010_1_2
    {
        get { return _fua_9010_1_2; }
        set { _fua_9010_1_2 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_9010_1_2(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_9010_1_2 = 0;
        _ide_9010_1 = 0;
        _ide_9000 = 0;
        _cod_9010_1_2 = 0;
        _ord_9010_1_2 = 0;
        _uar_9010_1_2 = 0;
        _far_9010_1_2 = "";
        _dip_9010_1_2 = "";
        _uua_9010_1_2 = 0;
        _fua_9010_1_2 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_9010_1_2 (  ide_9010_1, ide_9000, cod_9010_1_2, ord_9010_1_2, uar_9010_1_2, far_9010_1_2, dip_9010_1_2, uua_9010_1_2, fua_9010_1_2)" +
            $" OUTPUT INSERTED.ide_9010_1_2 VALUES (  @ide_9010_1, @ide_9000, @cod_9010_1_2, @ord_9010_1_2, @uar_9010_1_2, @far_9010_1_2, @dip_9010_1_2, @uua_9010_1_2, @fua_9010_1_2)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_9010_1", _ide_9010_1);
        Command.Parameters.AddWithValue("@ide_9000", _ide_9000);
        Command.Parameters.AddWithValue("@cod_9010_1_2", _cod_9010_1_2);
        Command.Parameters.AddWithValue("@ord_9010_1_2", _ord_9010_1_2);
        Command.Parameters.AddWithValue("@uar_9010_1_2", _uar_9010_1_2);
        Command.Parameters.AddWithValue("@far_9010_1_2", _far_9010_1_2);
        Command.Parameters.AddWithValue("@dip_9010_1_2", _dip_9010_1_2);
        Command.Parameters.AddWithValue("@uua_9010_1_2", _uua_9010_1_2);
        Command.Parameters.AddWithValue("@fua_9010_1_2", _fua_9010_1_2);

        Command.Connection = _sqlCon;
        _ide_9010_1_2 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_9010_1_2 WHERE ide_9010_1_2 = {_ide_9010_1_2}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_9010_1_2 = (int)dr["ide_9010_1_2"];

            if (AsignarPropiedades)
            {
                ide_9010_1 = (int)dr["ide_9010_1"];
                ide_9000 = (int)dr["ide_9000"];
                cod_9010_1_2 = (short)dr["cod_9010_1_2"];
                ord_9010_1_2 = Convert.ToByte(dr["ord_9010_1_2"]);
                uar_9010_1_2 = (int)dr["uar_9010_1_2"];
                far_9010_1_2 = dr["far_9010_1_2"].ToString();
                dip_9010_1_2 = (string)dr["dip_9010_1_2"];
                uua_9010_1_2 = (int)dr["uua_9010_1_2"];
                fua_9010_1_2 = dr["fua_9010_1_2"].ToString();
            }
        }
        else
            ide_9010_1_2 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_9010_1_2 WHERE ide_9010_1_2 = {_ide_9010_1_2}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_9010_1_2 SET");
        strSQL += string.Concat(" ide_9010_1 = @ide_9010_1");
        strSQL += string.Concat(", ide_9000 = @ide_9000");
        strSQL += string.Concat(", cod_9010_1_2 = @cod_9010_1_2");
        strSQL += string.Concat(", ord_9010_1_2 = @ord_9010_1_2");
        strSQL += string.Concat(", dip_9010_1_2 = @dip_9010_1_2");
        strSQL += string.Concat(", uua_9010_1_2 = @uua_9010_1_2");
        strSQL += string.Concat(", fua_9010_1_2 = @fua_9010_1_2");
        strSQL += string.Concat(" WHERE ide_9010_1_2 = ", _ide_9010_1_2);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_9010_1", _ide_9010_1);
        Command.Parameters.AddWithValue("@ide_9000", _ide_9000);
        Command.Parameters.AddWithValue("@cod_9010_1_2", _cod_9010_1_2);
        Command.Parameters.AddWithValue("@ord_9010_1_2", _ord_9010_1_2);
        Command.Parameters.AddWithValue("@dip_9010_1_2", _dip_9010_1_2);
        Command.Parameters.AddWithValue("@uua_9010_1_2", _uua_9010_1_2);
        Command.Parameters.AddWithValue("@fua_9010_1_2", _fua_9010_1_2);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: ide_9010_1 ide_9000 ord_9010_1_2
    public void Cargar_ide_9010_1_ide_9000_ord_9010_1_2(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_9010_1_2 WHERE ide_9010_1 = {_ide_9010_1} AND ide_9000 = {_ide_9000} AND ord_9010_1_2 = {_ord_9010_1_2}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_9010_1_2 = (int)dr["ide_9010_1_2"];

            if (AsignarPropiedades)
            {
                ide_9010_1 = (int)dr["ide_9010_1"];
                ide_9000 = (int)dr["ide_9000"];
                cod_9010_1_2 = (short)dr["cod_9010_1_2"];
                ord_9010_1_2 = Convert.ToByte(dr["ord_9010_1_2"]);
                uar_9010_1_2 = (int)dr["uar_9010_1_2"];
                far_9010_1_2 = dr["far_9010_1_2"].ToString();
                dip_9010_1_2 = (string)dr["dip_9010_1_2"];
                uua_9010_1_2 = (int)dr["uua_9010_1_2"];
                fua_9010_1_2 = dr["fua_9010_1_2"].ToString();
            }
        }
        else
            ide_9010_1_2 = 0;

        dr.Close();
    }

    #endregion

}