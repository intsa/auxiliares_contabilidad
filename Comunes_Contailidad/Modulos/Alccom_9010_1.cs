using System;
using System.Data;
using System.Data.SqlClient;

public class Alccom_9010_1
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_9010_1;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_9010;                                          // 2   CLAVE_UNICA 
    private string _tmo_9010_1;                                     // 3   CLAVE_UNICA 
    private string _des_9010_1;                                     // 4    
    private short? _aiv_9010_1;                                     // 5    
    private short? _afv_9010_1;                                     // 6    
    private byte _spt_9010_1;                                       // 7    
    private int _uar_9010_1;                                        // 8    
    private string _far_9010_1;                                     // 9    
    private string _dip_9010_1;                                     // 10    
    private int _uua_9010_1;                                        // 11    
    private string _fua_9010_1;                                     // 12    

    #endregion

    #region Propiedades

    public int ide_9010_1
    {
        get { return _ide_9010_1; }
        set { _ide_9010_1 = value; }
    }
    public int ide_9010
    {
        get { return _ide_9010; }
        set { _ide_9010 = value; }
    }
    public string tmo_9010_1
    {
        get { return _tmo_9010_1; }
        set { _tmo_9010_1 = value; }
    }
    public string des_9010_1
    {
        get { return _des_9010_1; }
        set { _des_9010_1 = value; }
    }
    public short? aiv_9010_1
    {
        get { return _aiv_9010_1; }
        set { _aiv_9010_1 = value; }
    }
    public short? afv_9010_1
    {
        get { return _afv_9010_1; }
        set { _afv_9010_1 = value; }
    }
    public byte spt_9010_1
    {
        get { return _spt_9010_1; }
        set { _spt_9010_1 = value; }
    }
    public int uar_9010_1
    {
        get { return _uar_9010_1; }
        set { _uar_9010_1 = value; }
    }
    public string far_9010_1
    {
        get { return _far_9010_1; }
        set { _far_9010_1 = value; }
    }
    public string dip_9010_1
    {
        get { return _dip_9010_1; }
        set { _dip_9010_1 = value; }
    }
    public int uua_9010_1
    {
        get { return _uua_9010_1; }
        set { _uua_9010_1 = value; }
    }
    public string fua_9010_1
    {
        get { return _fua_9010_1; }
        set { _fua_9010_1 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_9010_1(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_9010_1 = 0;
        _ide_9010 = 0;
        _tmo_9010_1 = "";
        _des_9010_1 = "";
        _aiv_9010_1 = null;
        _afv_9010_1 = null;
        _spt_9010_1 = 0;
        _uar_9010_1 = 0;
        _far_9010_1 = "";
        _dip_9010_1 = "";
        _uua_9010_1 = 0;
        _fua_9010_1 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_9010_1 (ide_9010, tmo_9010_1, des_9010_1, aiv_9010_1, afv_9010_1, spt_9010_1, uar_9010_1, far_9010_1, dip_9010_1, uua_9010_1, fua_9010_1)" +
            $" OUTPUT INSERTED.ide_9010_1 VALUES (@ide_9010, @tmo_9010_1, @des_9010_1, @aiv_9010_1, @afv_9010_1, @spt_9010_1, @uar_9010_1, @far_9010_1, @dip_9010_1, @uua_9010_1, @fua_9010_1)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_9010", _ide_9010);
        Command.Parameters.AddWithValue("@tmo_9010_1", _tmo_9010_1);
        Command.Parameters.AddWithValue("@des_9010_1", _des_9010_1);

        if (_aiv_9010_1 == null)
            Command.Parameters.Add("@aiv_9010_1", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@aiv_9010_1", _aiv_9010_1);

        if (_afv_9010_1 == null)
            Command.Parameters.Add("@afv_9010_1", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@afv_9010_1", _afv_9010_1);

        Command.Parameters.AddWithValue("@spt_9010_1", _spt_9010_1);
        Command.Parameters.AddWithValue("@uar_9010_1", _uar_9010_1);
        Command.Parameters.AddWithValue("@far_9010_1", _far_9010_1);
        Command.Parameters.AddWithValue("@dip_9010_1", _dip_9010_1);
        Command.Parameters.AddWithValue("@uua_9010_1", _uua_9010_1);
        Command.Parameters.AddWithValue("@fua_9010_1", _fua_9010_1);

        Command.Connection = _sqlCon;
        _ide_9010_1 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_9010_1 WHERE ide_9010_1 = {_ide_9010_1}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_9010_1 = (int)dr["ide_9010_1"];

            if (AsignarPropiedades)
            {
                ide_9010 = (int)dr["ide_9010"];
                tmo_9010_1 = (string)dr["tmo_9010_1"];
                des_9010_1 = (string)dr["des_9010_1"];
                aiv_9010_1 = DBNull.Value.Equals(dr["aiv_9010_1"]) ? null : (short?)dr["aiv_9010_1"];
                afv_9010_1 = DBNull.Value.Equals(dr["afv_9010_1"]) ? null : (short?)dr["afv_9010_1"];
                spt_9010_1 = Convert.ToByte(dr["spt_9010_1"]);
                uar_9010_1 = (int)dr["uar_9010_1"];
                far_9010_1 = dr["far_9010_1"].ToString();
                dip_9010_1 = (string)dr["dip_9010_1"];
                uua_9010_1 = (int)dr["uua_9010_1"];
                fua_9010_1 = dr["fua_9010_1"].ToString();
            }
        }
        else
            ide_9010_1 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_9010_1 WHERE ide_9010_1 = {_ide_9010_1}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_9010_1 SET");
        strSQL += string.Concat(" ide_9010 = @ide_9010");
        strSQL += string.Concat(", tmo_9010_1 = @tmo_9010_1");
        strSQL += string.Concat(", des_9010_1 = @des_9010_1");
        strSQL += string.Concat(", aiv_9010_1 = @aiv_9010_1");
        strSQL += string.Concat(", afv_9010_1 = @afv_9010_1");
        strSQL += string.Concat(", spt_9010_1 = @spt_9010_1");
        strSQL += string.Concat(", dip_9010_1 = @dip_9010_1");
        strSQL += string.Concat(", uua_9010_1 = @uua_9010_1");
        strSQL += string.Concat(", fua_9010_1 = @fua_9010_1");
        strSQL += string.Concat(" WHERE ide_9010_1 = ", _ide_9010_1);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_9010", _ide_9010);
        Command.Parameters.AddWithValue("@tmo_9010_1", _tmo_9010_1);
        Command.Parameters.AddWithValue("@des_9010_1", _des_9010_1);

        if (_aiv_9010_1 == null)
            Command.Parameters.Add("@aiv_9010_1", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@aiv_9010_1", _aiv_9010_1);

        if (_afv_9010_1 == null)
            Command.Parameters.Add("@afv_9010_1", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@afv_9010_1", _afv_9010_1);

        Command.Parameters.AddWithValue("@spt_9010_1", _spt_9010_1);
        Command.Parameters.AddWithValue("@dip_9010_1", _dip_9010_1);
        Command.Parameters.AddWithValue("@uua_9010_1", _uua_9010_1);
        Command.Parameters.AddWithValue("@fua_9010_1", _fua_9010_1);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: ide_9010 tmo_9010_1
    public void Cargar_ide_9010_tmo_9010_1(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_9010_1 WHERE ide_9010 = {_ide_9010} AND tmo_9010_1 = '{_tmo_9010_1}'"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_9010_1 = (int)dr["ide_9010_1"];

            if (AsignarPropiedades)
            {
                ide_9010 = (int)dr["ide_9010"];
                tmo_9010_1 = (string)dr["tmo_9010_1"];
                des_9010_1 = (string)dr["des_9010_1"];
                aiv_9010_1 = DBNull.Value.Equals(dr["aiv_9010_1"]) ? null : (short?)dr["aiv_9010_1"];
                afv_9010_1 = DBNull.Value.Equals(dr["afv_9010_1"]) ? null : (short?)dr["afv_9010_1"];
                spt_9010_1 = Convert.ToByte(dr["spt_9010_1"]);
                uar_9010_1 = (int)dr["uar_9010_1"];
                far_9010_1 = dr["far_9010_1"].ToString();
                dip_9010_1 = (string)dr["dip_9010_1"];
                uua_9010_1 = (int)dr["uua_9010_1"];
                fua_9010_1 = dr["fua_9010_1"].ToString();
            }
        }
        else
            ide_9010_1 = 0;

        dr.Close();
    }

    #endregion

}