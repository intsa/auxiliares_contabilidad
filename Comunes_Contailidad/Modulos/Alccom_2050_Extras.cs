using System;
using System.Data;
using System.Data.SqlClient;

public class Alccom_2050_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_2050;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _cod_2050;                                        // 2   CLAVE_UNICA 
    private string _den_2050;                                       // 3    
    private string _fvi_2050;                                       // 4    
    private string _fvf_2050;                                       // 5    
    private int _uar_2050;                                          // 6    
    private string _far_2050;                                       // 7    
    private string _dip_2050;                                       // 8    
    private int _uua_2050;                                          // 9    
    private string _fua_2050;                                       // 10    

    #endregion

    #region Propiedades

    public int ide_2050
    {
        get { return _ide_2050; }
        set { _ide_2050 = value; }
    }
    public short cod_2050
    {
        get { return _cod_2050; }
        set { _cod_2050 = value; }
    }
    public string den_2050
    {
        get { return _den_2050; }
        set { _den_2050 = value; }
    }
    public string fvi_2050
    {
        get { return _fvi_2050; }
        set { _fvi_2050 = value; }
    }
    public string fvf_2050
    {
        get { return _fvf_2050; }
        set { _fvf_2050 = value; }
    }
    public int uar_2050
    {
        get { return _uar_2050; }
        set { _uar_2050 = value; }
    }
    public string far_2050
    {
        get { return _far_2050; }
        set { _far_2050 = value; }
    }
    public string dip_2050
    {
        get { return _dip_2050; }
        set { _dip_2050 = value; }
    }
    public int uua_2050
    {
        get { return _uua_2050; }
        set { _uua_2050 = value; }
    }
    public string fua_2050
    {
        get { return _fua_2050; }
        set { _fua_2050 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_2050_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_2050 = 0;
        _cod_2050 = 0;
        _den_2050 = "";
        _fvi_2050 = null;
        _fvf_2050 = null;
        _uar_2050 = 0;
        _far_2050 = "";
        _dip_2050 = "";
        _uua_2050 = 0;
        _fua_2050 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO ACTUALIZAR
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_2050 SET");
        strSQL += string.Concat(" den_2050 = @den_2050");
        strSQL += string.Concat(", fvi_2050 = @fvi_2050");
        strSQL += string.Concat(", fvf_2050 = @fvf_2050");
        strSQL += string.Concat(", dip_2050 = @dip_2050");
        strSQL += string.Concat(", uua_2050 = @uua_2050");
        strSQL += string.Concat(", fua_2050 = @fua_2050");
        strSQL += string.Concat(" WHERE ide_2050 = ", _ide_2050);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_2050", _den_2050);

        if (_fvi_2050 == null)
            Command.Parameters.Add("@fvi_2050", SqlDbType.DateTime2).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fvi_2050", _fvi_2050);

        if (_fvf_2050 == null)
            Command.Parameters.Add("@fvf_2050", SqlDbType.DateTime2).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@fvf_2050", _fvf_2050);

        Command.Parameters.AddWithValue("@dip_2050", _dip_2050);
        Command.Parameters.AddWithValue("@uua_2050", _uua_2050);
        Command.Parameters.AddWithValue("@fua_2050", _fua_2050);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}