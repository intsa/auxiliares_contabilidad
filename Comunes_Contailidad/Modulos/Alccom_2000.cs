using System;
using System.Data.SqlClient;

public class Alccom_2000
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_2000;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _cod_2000;                                        // 2   CLAVE_UNICA 
    private byte _tpc_2000;                                         // 3    
    private string _den_2000;                                       // 4    
    private byte _tdo_2000;                                         // 5    
    private short _ndi_2000;                                         // 6    
    private int _uar_2000;                                          // 7    
    private string _far_2000;                                       // 8    
    private string _dip_2000;                                       // 9    
    private int _uua_2000;                                          // 10    
    private string _fua_2000;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_2000
    {
        get { return _ide_2000; }
        set { _ide_2000 = value; }
    }
    public short cod_2000
    {
        get { return _cod_2000; }
        set { _cod_2000 = value; }
    }
    public byte tpc_2000
    {
        get { return _tpc_2000; }
        set { _tpc_2000 = value; }
    }
    public string den_2000
    {
        get { return _den_2000; }
        set { _den_2000 = value; }
    }
    public byte tdo_2000
    {
        get { return _tdo_2000; }
        set { _tdo_2000 = value; }
    }
    public short ndi_2000
    {
        get { return _ndi_2000; }
        set { _ndi_2000 = value; }
    }
    public int uar_2000
    {
        get { return _uar_2000; }
        set { _uar_2000 = value; }
    }
    public string far_2000
    {
        get { return _far_2000; }
        set { _far_2000 = value; }
    }
    public string dip_2000
    {
        get { return _dip_2000; }
        set { _dip_2000 = value; }
    }
    public int uua_2000
    {
        get { return _uua_2000; }
        set { _uua_2000 = value; }
    }
    public string fua_2000
    {
        get { return _fua_2000; }
        set { _fua_2000 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_2000(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_2000 = 0;
        _cod_2000 = 0;
        _tpc_2000 = 0;
        _den_2000 = "";
        _tdo_2000 = 0;
        _ndi_2000 = 0;
        _uar_2000 = 0;
        _far_2000 = "";
        _dip_2000 = "";
        _uua_2000 = 0;
        _fua_2000 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_2000 (cod_2000, tpc_2000, den_2000, tdo_2000, ndi_2000, uar_2000, far_2000, dip_2000, uua_2000, fua_2000)" +
            $" OUTPUT INSERTED.ide_2000 VALUES (  @cod_2000, @tpc_2000, @den_2000, @tdo_2000, @ndi_2000, @uar_2000, @far_2000, @dip_2000, @uua_2000, @fua_2000)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_2000", _cod_2000);
        Command.Parameters.AddWithValue("@tpc_2000", _tpc_2000);
        Command.Parameters.AddWithValue("@den_2000", _den_2000);
        Command.Parameters.AddWithValue("@tdo_2000", _tdo_2000);
        Command.Parameters.AddWithValue("@ndi_2000", _ndi_2000);
        Command.Parameters.AddWithValue("@uar_2000", _uar_2000);
        Command.Parameters.AddWithValue("@far_2000", _far_2000);
        Command.Parameters.AddWithValue("@dip_2000", _dip_2000);
        Command.Parameters.AddWithValue("@uua_2000", _uua_2000);
        Command.Parameters.AddWithValue("@fua_2000", _fua_2000);

        Command.Connection = _sqlCon;
        _ide_2000 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_2000 WHERE ide_2000 = {_ide_2000}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2000 = (int)dr["ide_2000"];

            if (AsignarPropiedades)
            {
                cod_2000 = (short)dr["cod_2000"];
                tpc_2000 = Convert.ToByte(dr["tpc_2000"]);
                den_2000 = (string)dr["den_2000"];
                tdo_2000 = Convert.ToByte(dr["tdo_2000"]);
                ndi_2000 = (short)dr["ndi_2000"];
                uar_2000 = (int)dr["uar_2000"];
                far_2000 = dr["far_2000"].ToString();
                dip_2000 = (string)dr["dip_2000"];
                uua_2000 = (int)dr["uua_2000"];
                fua_2000 = dr["fua_2000"].ToString();
            }
        }
        else
            ide_2000 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_2000 WHERE ide_2000 = {_ide_2000}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_2000 SET");
        strSQL += string.Concat(" cod_2000 = @cod_2000");
        strSQL += string.Concat(", tpc_2000 = @tpc_2000");
        strSQL += string.Concat(", den_2000 = @den_2000");
        strSQL += string.Concat(", tdo_2000 = @tdo_2000");
        strSQL += string.Concat(", ndi_2000 = @ndi_2000");
        strSQL += string.Concat(", dip_2000 = @dip_2000");
        strSQL += string.Concat(", uua_2000 = @uua_2000");
        strSQL += string.Concat(", fua_2000 = @fua_2000");
        strSQL += string.Concat(" WHERE ide_2000 = ", _ide_2000);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_2000", _cod_2000);
        Command.Parameters.AddWithValue("@tpc_2000", _tpc_2000);
        Command.Parameters.AddWithValue("@den_2000", _den_2000);
        Command.Parameters.AddWithValue("@tdo_2000", _tdo_2000);
        Command.Parameters.AddWithValue("@ndi_2000", _ndi_2000);
        Command.Parameters.AddWithValue("@dip_2000", _dip_2000);
        Command.Parameters.AddWithValue("@uua_2000", _uua_2000);
        Command.Parameters.AddWithValue("@fua_2000", _fua_2000);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: cod_2000
    public void Cargar_cod_2000(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_2000 WHERE cod_2000 = {_cod_2000}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2000 = (int)dr["ide_2000"];

            if (AsignarPropiedades)
            {
                cod_2000 = (short)dr["cod_2000"];
                tpc_2000 = Convert.ToByte(dr["tpc_2000"]);
                den_2000 = (string)dr["den_2000"];
                tdo_2000 = Convert.ToByte(dr["tdo_2000"]);
                ndi_2000 = (short)dr["ndi_2000"];
                uar_2000 = (int)dr["uar_2000"];
                far_2000 = dr["far_2000"].ToString();
                dip_2000 = (string)dr["dip_2000"];
                uua_2000 = (int)dr["uua_2000"];
                fua_2000 = dr["fua_2000"].ToString();
            }
        }
        else
            ide_2000 = 0;

        dr.Close();
    }

    #endregion

}