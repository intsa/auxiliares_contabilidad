using System.Data.SqlClient;

public class Alccom_2040
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_2040;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _cod_2040;                                        // 2   CLAVE_UNICA 
    private string _den_2040;                                       // 4    
    private int _uar_2040;                                          // 7    
    private string _far_2040;                                       // 8    
    private string _dip_2040;                                       // 9    
    private int _uua_2040;                                          // 10    
    private string _fua_2040;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_2040
    {
        get { return _ide_2040; }
        set { _ide_2040 = value; }
    }
    public short cod_2040
    {
        get { return _cod_2040; }
        set { _cod_2040 = value; }
    }
    public string den_2040
    {
        get { return _den_2040; }
        set { _den_2040 = value; }
    }
    public int uar_2040
    {
        get { return _uar_2040; }
        set { _uar_2040 = value; }
    }
    public string far_2040
    {
        get { return _far_2040; }
        set { _far_2040 = value; }
    }
    public string dip_2040
    {
        get { return _dip_2040; }
        set { _dip_2040 = value; }
    }
    public int uua_2040
    {
        get { return _uua_2040; }
        set { _uua_2040 = value; }
    }
    public string fua_2040
    {
        get { return _fua_2040; }
        set { _fua_2040 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_2040(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_2040 = 0;
        _cod_2040 = 0;
        _den_2040 = "";
        _uar_2040 = 0;
        _far_2040 = "";
        _dip_2040 = "";
        _uua_2040 = 0;
        _fua_2040 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_2040 (cod_2040, den_2040, uar_2040, far_2040, dip_2040, uua_2040, fua_2040)" +
            $" OUTPUT INSERTED.ide_2040 VALUES (@cod_2040, @den_2040, @uar_2040, @far_2040, @dip_2040, @uua_2040, @fua_2040)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_2040", _cod_2040);
        Command.Parameters.AddWithValue("@den_2040", _den_2040);
        Command.Parameters.AddWithValue("@uar_2040", _uar_2040);
        Command.Parameters.AddWithValue("@far_2040", _far_2040);
        Command.Parameters.AddWithValue("@dip_2040", _dip_2040);
        Command.Parameters.AddWithValue("@uua_2040", _uua_2040);
        Command.Parameters.AddWithValue("@fua_2040", _fua_2040);

        Command.Connection = _sqlCon;
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_2040 WHERE ide_2040 = {_ide_2040}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2040 = (int)dr["ide_2040"];

            if (AsignarPropiedades)
            {
                cod_2040 = (short)dr["cod_2040"];
                den_2040 = (string)dr["den_2040"];
                uar_2040 = (int)dr["uar_2040"];
                far_2040 = dr["far_2040"].ToString();
                dip_2040 = (string)dr["dip_2040"];
                uua_2040 = (int)dr["uua_2040"];
                fua_2040 = dr["fua_2040"].ToString();
            }
        }
        else
            ide_2040 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_2040 WHERE ide_2040 = {_ide_2040}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_2040 SET");
        strSQL += string.Concat(" cod_2040 = @cod_2040");
        strSQL += string.Concat(", den_2040 = @den_2040");
        strSQL += string.Concat(", dip_2040 = @dip_2040");
        strSQL += string.Concat(", uua_2040 = @uua_2040");
        strSQL += string.Concat(", fua_2040 = @fua_2040");
        strSQL += string.Concat(" WHERE ide_2040 = ", _ide_2040);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@cod_2040", _cod_2040);
        Command.Parameters.AddWithValue("@den_2040", _den_2040);
        Command.Parameters.AddWithValue("@dip_2040", _dip_2040);
        Command.Parameters.AddWithValue("@uua_2040", _uua_2040);
        Command.Parameters.AddWithValue("@fua_2040", _fua_2040);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE ÚNICA: cod_2040
    public void Cargar_cod_2040(bool AsignarPropiedades = true)
    {

        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_2040 WHERE cod_2040 = {_cod_2040}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2040 = (int)dr["ide_2040"];

            if (AsignarPropiedades)
            {
                cod_2040 = (short)dr["cod_2040"];
                den_2040 = (string)dr["den_2040"];
                uar_2040 = (int)dr["uar_2040"];
                far_2040 = dr["far_2040"].ToString();
                dip_2040 = (string)dr["dip_2040"];
                uua_2040 = (int)dr["uua_2040"];
                fua_2040 = dr["fua_2040"].ToString();
            }
        }
        else
            ide_2040 = 0;

        dr.Close();
    }

    #endregion

}