using System.Data.SqlClient;

public class Alccom_2030_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_2030;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _tco_2030;                                        // 2   CLAVE_UNICA 
    private string _den_2030;                                       // 3    
    private byte _scm_2030;                                         // 4    
    private byte _sps_2030;                                         // 5    
    private byte _spc_2030;                                         // 6    
    private byte _spf_2030;                                         // 7    
    private int _uar_2030;                                          // 8    
    private string _far_2030;                                       // 9    
    private string _dip_2030;                                       // 10    
    private int _uua_2030;                                          // 11    
    private string _fua_2030;                                       // 12    

    #endregion

    #region Propiedades

    public int ide_2030
    {
        get { return _ide_2030; }
        set { _ide_2030 = value; }
    }
    public short tco_2030
    {
        get { return _tco_2030; }
        set { _tco_2030 = value; }
    }
    public string den_2030
    {
        get { return _den_2030; }
        set { _den_2030 = value; }
    }
    public byte scm_2030
    {
        get { return _scm_2030; }
        set { _scm_2030 = value; }
    }
    public byte sps_2030
    {
        get { return _sps_2030; }
        set { _sps_2030 = value; }
    }
    public byte spc_2030
    {
        get { return _spc_2030; }
        set { _spc_2030 = value; }
    }
    public byte spf_2030
    {
        get { return _spf_2030; }
        set { _spf_2030 = value; }
    }
    public int uar_2030
    {
        get { return _uar_2030; }
        set { _uar_2030 = value; }
    }
    public string far_2030
    {
        get { return _far_2030; }
        set { _far_2030 = value; }
    }
    public string dip_2030
    {
        get { return _dip_2030; }
        set { _dip_2030 = value; }
    }
    public int uua_2030
    {
        get { return _uua_2030; }
        set { _uua_2030 = value; }
    }
    public string fua_2030
    {
        get { return _fua_2030; }
        set { _fua_2030 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_2030_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_2030 = 0;
        _tco_2030 = 0;
        _den_2030 = "";
        _scm_2030 = 0;
        _sps_2030 = 0;
        _spc_2030 = 0;
        _spf_2030 = 0;
        _uar_2030 = 0;
        _far_2030 = "";
        _dip_2030 = "";
        _uua_2030 = 0;
        _fua_2030 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO ACTUALIZAR
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_2030 SET");
        strSQL += string.Concat(" den_2030 = @den_2030");
        strSQL += string.Concat(", scm_2030 = @scm_2030");
        strSQL += string.Concat(", sps_2030 = @sps_2030");
        strSQL += string.Concat(", spc_2030 = @spc_2030");
        strSQL += string.Concat(", spf_2030 = @spf_2030");
        strSQL += string.Concat(", dip_2030 = @dip_2030");
        strSQL += string.Concat(", uua_2030 = @uua_2030");
        strSQL += string.Concat(", fua_2030 = @fua_2030");
        strSQL += string.Concat(" WHERE ide_2030 = ", _ide_2030);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_2030", _den_2030);
        Command.Parameters.AddWithValue("@scm_2030", _scm_2030);
        Command.Parameters.AddWithValue("@sps_2030", _sps_2030);
        Command.Parameters.AddWithValue("@spc_2030", _spc_2030);
        Command.Parameters.AddWithValue("@spf_2030", _spf_2030);
        Command.Parameters.AddWithValue("@dip_2030", _dip_2030);
        Command.Parameters.AddWithValue("@uua_2030", _uua_2030);
        Command.Parameters.AddWithValue("@fua_2030", _fua_2030);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}