using System;
using System.Data.SqlClient;

namespace Comunes_Contabilidad.Modulos
{
    public class Alccom_2010_Extras
    {
        #region Atributos Conexión

        private SqlConnection _sqlCon;
        private string _BaseDatos;

        #endregion

        #region Atributos

        private int _ide_2010;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
        private short _eje_2010;                                        // 2   CLAVE_UNICA 
        private short _cod_2010;                                        // 3   CLAVE_UNICA 
        private string _den_2010;                                       // 4    
        private byte _tca_2010;                                         // 5    
        private byte _cca_2010;                                         // 6    
        private string _iba_2010;                                       // 7    
        private string _cta_2010;                                       // 8    
        private int _uar_2010;                                          // 9    
        private string _far_2010;                                       // 10    
        private string _dip_2010;                                       // 11    
        private int _uua_2010;                                          // 12    
        private string _fua_2010;                                       // 13    

        #endregion

        #region Propiedades

        public int ide_2010
        {
            get { return _ide_2010; }
            set { _ide_2010 = value; }
        }
        public short eje_2010
        {
            get { return _eje_2010; }
            set { _eje_2010 = value; }
        }
        public short cod_2010
        {
            get { return _cod_2010; }
            set { _cod_2010 = value; }
        }
        public string den_2010
        {
            get { return _den_2010; }
            set { _den_2010 = value; }
        }
        public byte tca_2010
        {
            get { return _tca_2010; }
            set { _tca_2010 = value; }
        }
        public byte cca_2010
        {
            get { return _cca_2010; }
            set { _cca_2010 = value; }
        }
        public string iba_2010
        {
            get { return _iba_2010; }
            set { _iba_2010 = value; }
        }
        public string cta_2010
        {
            get { return _cta_2010; }
            set { _cta_2010 = value; }
        }
        public int uar_2010
        {
            get { return _uar_2010; }
            set { _uar_2010 = value; }
        }
        public string far_2010
        {
            get { return _far_2010; }
            set { _far_2010 = value; }
        }
        public string dip_2010
        {
            get { return _dip_2010; }
            set { _dip_2010 = value; }
        }
        public int uua_2010
        {
            get { return _uua_2010; }
            set { _uua_2010 = value; }
        }
        public string fua_2010
        {
            get { return _fua_2010; }
            set { _fua_2010 = value; }
        }

        #endregion

        #region Constructores

        public Alccom_2010_Extras(SqlConnection sqlCon, string BaseDatos = null)
        {
            _sqlCon = sqlCon;
            _BaseDatos = BaseDatos ?? sqlCon.Database;

            _ide_2010 = 0;
            _eje_2010 = 0;
            _cod_2010 = 0;
            _den_2010 = "";
            _tca_2010 = 0;
            _cca_2010 = 0;
            _iba_2010 = "";
            _cta_2010 = "";
            _uar_2010 = 0;
            _far_2010 = "";
            _dip_2010 = "";
            _uua_2010 = 0;
            _fua_2010 = "";
        }

        #endregion

        #region Metodos Públicos

          //METODO QUE ACTUALIZAR
        public void Actualizar_Extras()
        {
            string strSQL = "";
            SqlCommand Command = new SqlCommand{Connection = _sqlCon};

            strSQL = string.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alccom_2010 SET");
            strSQL += string.Concat(" den_2010 = @den_2010");
            strSQL += string.Concat(", tca_2010 = @tca_2010");
            strSQL += string.Concat(", cca_2010 = @cca_2010");
            strSQL += string.Concat(", iba_2010 = @iba_2010");
            strSQL += string.Concat(", cta_2010 = @cta_2010");
            strSQL += string.Concat(", dip_2010 = @dip_2010");
            strSQL += string.Concat(", uua_2010 = @uua_2010");
            strSQL += string.Concat(", fua_2010 = @fua_2010");
            strSQL += string.Concat(" WHERE ide_2010 = ", _ide_2010);

            // ASIGNACIÓN DE PARÁMETROS
            Command.Parameters.AddWithValue("@den_2010", _den_2010);
            Command.Parameters.AddWithValue("@tca_2010", _tca_2010);
            Command.Parameters.AddWithValue("@cca_2010", _cca_2010);
            Command.Parameters.AddWithValue("@iba_2010", _iba_2010);
            Command.Parameters.AddWithValue("@cta_2010", _cta_2010);
            Command.Parameters.AddWithValue("@dip_2010", _dip_2010);
            Command.Parameters.AddWithValue("@uua_2010", _uua_2010);
            Command.Parameters.AddWithValue("@fua_2010", _fua_2010);

            Command.CommandText = strSQL;
            Command.ExecuteNonQuery();
        }

        #endregion
    }
}