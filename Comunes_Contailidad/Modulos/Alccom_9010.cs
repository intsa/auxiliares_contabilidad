using System;
using System.Data.SqlClient;

public class Alccom_9010
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_9010;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private string _tdo_9010;                                       // 2   CLAVE_UNICA 
    private string _des_9010;                                       // 3    
    private string _sdo_9010;                                       // 4    
    private byte _smp_9010;                                         // 5    
    private string _tda_9010;                                       // 6    
    private int _uar_9010;                                          // 7    
    private string _far_9010;                                       // 8    
    private string _dip_9010;                                       // 9    
    private int _uua_9010;                                          // 10    
    private string _fua_9010;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_9010
    {
        get { return _ide_9010; }
        set { _ide_9010 = value; }
    }
    public string tdo_9010
    {
        get { return _tdo_9010; }
        set { _tdo_9010 = value; }
    }
    public string des_9010
    {
        get { return _des_9010; }
        set { _des_9010 = value; }
    }
    public string sdo_9010
    {
        get { return _sdo_9010; }
        set { _sdo_9010 = value; }
    }
    public byte smp_9010
    {
        get { return _smp_9010; }
        set { _smp_9010 = value; }
    }
    public string tda_9010
    {
        get { return _tda_9010; }
        set { _tda_9010 = value; }
    }
    public int uar_9010
    {
        get { return _uar_9010; }
        set { _uar_9010 = value; }
    }
    public string far_9010
    {
        get { return _far_9010; }
        set { _far_9010 = value; }
    }
    public string dip_9010
    {
        get { return _dip_9010; }
        set { _dip_9010 = value; }
    }
    public int uua_9010
    {
        get { return _uua_9010; }
        set { _uua_9010 = value; }
    }
    public string fua_9010
    {
        get { return _fua_9010; }
        set { _fua_9010 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_9010(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_9010 = 0;
        _tdo_9010 = "";
        _des_9010 = "";
        _sdo_9010 = "";
        _smp_9010 = 0;
        _tda_9010 = "";
        _uar_9010 = 0;
        _far_9010 = "";
        _dip_9010 = "";
        _uua_9010 = 0;
        _fua_9010 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_9010 (tdo_9010, des_9010, sdo_9010, smp_9010, tda_9010, uar_9010, far_9010, dip_9010, uua_9010, fua_9010)" +
            $" OUTPUT INSERTED.ide_9010 VALUES (@tdo_9010, @des_9010, @sdo_9010, @smp_9010, @tda_9010, @uar_9010, @far_9010, @dip_9010, @uua_9010, @fua_9010)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@tdo_9010", _tdo_9010);
        Command.Parameters.AddWithValue("@des_9010", _des_9010);
        Command.Parameters.AddWithValue("@sdo_9010", _sdo_9010);
        Command.Parameters.AddWithValue("@smp_9010", _smp_9010);
        Command.Parameters.AddWithValue("@tda_9010", _tda_9010);
        Command.Parameters.AddWithValue("@uar_9010", _uar_9010);
        Command.Parameters.AddWithValue("@far_9010", _far_9010);
        Command.Parameters.AddWithValue("@dip_9010", _dip_9010);
        Command.Parameters.AddWithValue("@uua_9010", _uua_9010);
        Command.Parameters.AddWithValue("@fua_9010", _fua_9010);

        Command.Connection = _sqlCon;
        _ide_9010 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_9010 WHERE ide_9010 = {_ide_9010}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_9010 = (int)dr["ide_9010"];

            if (AsignarPropiedades)
            {
                tdo_9010 = (string)dr["tdo_9010"];
                des_9010 = (string)dr["des_9010"];
                sdo_9010 = (string)dr["sdo_9010"];
                smp_9010 = Convert.ToByte(dr["smp_9010"]);
                tda_9010 = (string)dr["tda_9010"];
                uar_9010 = (int)dr["uar_9010"];
                far_9010 = dr["far_9010"].ToString();
                dip_9010 = (string)dr["dip_9010"];
                uua_9010 = (int)dr["uua_9010"];
                fua_9010 = dr["fua_9010"].ToString();
            }
        }
        else
            ide_9010 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_9010 WHERE ide_9010 = {_ide_9010}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_9010 SET");
        strSQL += string.Concat(" tdo_9010 = @tdo_9010");
        strSQL += string.Concat(", des_9010 = @des_9010");
        strSQL += string.Concat(", sdo_9010 = @sdo_9010");
        strSQL += string.Concat(", smp_9010 = @smp_9010");
        strSQL += string.Concat(", tda_9010 = @tda_9010");
        strSQL += string.Concat(", dip_9010 = @dip_9010");
        strSQL += string.Concat(", uua_9010 = @uua_9010");
        strSQL += string.Concat(", fua_9010 = @fua_9010");
        strSQL += string.Concat(" WHERE ide_9010 = ", _ide_9010);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@tdo_9010", _tdo_9010);
        Command.Parameters.AddWithValue("@des_9010", _des_9010);
        Command.Parameters.AddWithValue("@sdo_9010", _sdo_9010);
        Command.Parameters.AddWithValue("@smp_9010", _smp_9010);
        Command.Parameters.AddWithValue("@tda_9010", _tda_9010);
        Command.Parameters.AddWithValue("@dip_9010", _dip_9010);
        Command.Parameters.AddWithValue("@uua_9010", _uua_9010);
        Command.Parameters.AddWithValue("@fua_9010", _fua_9010);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: tdo_9010
    public void Cargar_tdo_9010(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_9010 WHERE tdo_9010 = '{_tdo_9010}'"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_9010 = (int)dr["ide_9010"];

            if (AsignarPropiedades)
            {
                tdo_9010 = (string)dr["tdo_9010"];
                des_9010 = (string)dr["des_9010"];
                sdo_9010 = (string)dr["sdo_9010"];
                smp_9010 = Convert.ToByte(dr["smp_9010"]);
                tda_9010 = (string)dr["tda_9010"];
                uar_9010 = (int)dr["uar_9010"];
                far_9010 = dr["far_9010"].ToString();
                dip_9010 = (string)dr["dip_9010"];
                uua_9010 = (int)dr["uua_9010"];
                fua_9010 = dr["fua_9010"].ToString();
            }
        }
        else
            ide_9010 = 0;

        dr.Close();
    }

    #endregion
}