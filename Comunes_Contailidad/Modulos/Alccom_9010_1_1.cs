using System;
using System.Data.SqlClient;

public class Alccom_9010_1_1
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_9010_1_1;                                      // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_9010_1;                                        // 2    
    private string _cta_9010_1_1;                                   // 3    
    private string _doh_9010_1_1;                                   // 4    
    private short _sig_9010_1_1;                                    // 5    
    private byte _tcu_9010_1_1;                                     // 6    
    private int _uar_9010_1_1;                                      // 7    
    private string _far_9010_1_1;                                   // 8    
    private string _dip_9010_1_1;                                   // 9    
    private int _uua_9010_1_1;                                      // 10    
    private string _fua_9010_1_1;                                   // 11    

    #endregion

    #region Propiedades

    public int ide_9010_1_1
    {
        get { return _ide_9010_1_1; }
        set { _ide_9010_1_1 = value; }
    }
    public int ide_9010_1
    {
        get { return _ide_9010_1; }
        set { _ide_9010_1 = value; }
    }
    public string cta_9010_1_1
    {
        get { return _cta_9010_1_1; }
        set { _cta_9010_1_1 = value; }
    }
    public string doh_9010_1_1
    {
        get { return _doh_9010_1_1; }
        set { _doh_9010_1_1 = value; }
    }
    public short sig_9010_1_1
    {
        get { return _sig_9010_1_1; }
        set { _sig_9010_1_1 = value; }
    }
    public byte tcu_9010_1_1
    {
        get { return _tcu_9010_1_1; }
        set { _tcu_9010_1_1 = value; }
    }
    public int uar_9010_1_1
    {
        get { return _uar_9010_1_1; }
        set { _uar_9010_1_1 = value; }
    }
    public string far_9010_1_1
    {
        get { return _far_9010_1_1; }
        set { _far_9010_1_1 = value; }
    }
    public string dip_9010_1_1
    {
        get { return _dip_9010_1_1; }
        set { _dip_9010_1_1 = value; }
    }
    public int uua_9010_1_1
    {
        get { return _uua_9010_1_1; }
        set { _uua_9010_1_1 = value; }
    }
    public string fua_9010_1_1
    {
        get { return _fua_9010_1_1; }
        set { _fua_9010_1_1 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_9010_1_1(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_9010_1_1 = 0;
        _ide_9010_1 = 0;
        _cta_9010_1_1 = "";
        _doh_9010_1_1 = "";
        _sig_9010_1_1 = 0;
        _tcu_9010_1_1 = 0;
        _uar_9010_1_1 = 0;
        _far_9010_1_1 = "";
        _dip_9010_1_1 = "";
        _uua_9010_1_1 = 0;
        _fua_9010_1_1 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_9010_1_1 (ide_9010_1, cta_9010_1_1, doh_9010_1_1, sig_9010_1_1, tcu_9010_1_1, uar_9010_1_1, far_9010_1_1, dip_9010_1_1, uua_9010_1_1, fua_9010_1_1)" +
            $" OUTPUT INSERTED.ide_9010_1_1 VALUES (@ide_9010_1, @cta_9010_1_1, @doh_9010_1_1, @sig_9010_1_1, @tcu_9010_1_1, @uar_9010_1_1, @far_9010_1_1, @dip_9010_1_1, @uua_9010_1_1, @fua_9010_1_1)";

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_9010_1", _ide_9010_1);
        Command.Parameters.AddWithValue("@cta_9010_1_1", _cta_9010_1_1);
        Command.Parameters.AddWithValue("@doh_9010_1_1", _doh_9010_1_1);
        Command.Parameters.AddWithValue("@sig_9010_1_1", _sig_9010_1_1);
        Command.Parameters.AddWithValue("@tcu_9010_1_1", _tcu_9010_1_1);
        Command.Parameters.AddWithValue("@uar_9010_1_1", _uar_9010_1_1);
        Command.Parameters.AddWithValue("@far_9010_1_1", _far_9010_1_1);
        Command.Parameters.AddWithValue("@dip_9010_1_1", _dip_9010_1_1);
        Command.Parameters.AddWithValue("@uua_9010_1_1", _uua_9010_1_1);
        Command.Parameters.AddWithValue("@fua_9010_1_1", _fua_9010_1_1);

        Command.Connection = _sqlCon;
        _ide_9010_1_1 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_9010_1_1 WHERE ide_9010_1_1 = {_ide_9010_1_1}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_9010_1_1 = (int)dr["ide_9010_1_1"];

            if (AsignarPropiedades)
            {
                ide_9010_1 = (int)dr["ide_9010_1"];
                cta_9010_1_1 = (string)dr["cta_9010_1_1"];
                doh_9010_1_1 = (string)dr["doh_9010_1_1"];
                sig_9010_1_1 = (short)dr["sig_9010_1_1"];
                tcu_9010_1_1 = Convert.ToByte(dr["tcu_9010_1_1"]);
                uar_9010_1_1 = (int)dr["uar_9010_1_1"];
                far_9010_1_1 = dr["far_9010_1_1"].ToString();
                dip_9010_1_1 = (string)dr["dip_9010_1_1"];
                uua_9010_1_1 = (int)dr["uua_9010_1_1"];
                fua_9010_1_1 = dr["fua_9010_1_1"].ToString();
            }
        }
        else
            ide_9010_1_1 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_9010_1_1 WHERE ide_9010_1_1 = {_ide_9010_1_1}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_9010_1_1 SET");
        strSQL += string.Concat(" ide_9010_1 = @ide_9010_1");
        strSQL += string.Concat(", cta_9010_1_1 = @cta_9010_1_1");
        strSQL += string.Concat(", doh_9010_1_1 = @doh_9010_1_1");
        strSQL += string.Concat(", sig_9010_1_1 = @sig_9010_1_1");
        strSQL += string.Concat(", tcu_9010_1_1 = @tcu_9010_1_1");
        strSQL += string.Concat(", dip_9010_1_1 = @dip_9010_1_1");
        strSQL += string.Concat(", uua_9010_1_1 = @uua_9010_1_1");
        strSQL += string.Concat(", fua_9010_1_1 = @fua_9010_1_1");
        strSQL += string.Concat(" WHERE ide_9010_1_1 = ", _ide_9010_1_1);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_9010_1", _ide_9010_1);
        Command.Parameters.AddWithValue("@cta_9010_1_1", _cta_9010_1_1);
        Command.Parameters.AddWithValue("@doh_9010_1_1", _doh_9010_1_1);
        Command.Parameters.AddWithValue("@sig_9010_1_1", _sig_9010_1_1);
        Command.Parameters.AddWithValue("@tcu_9010_1_1", _tcu_9010_1_1);
        Command.Parameters.AddWithValue("@dip_9010_1_1", _dip_9010_1_1);
        Command.Parameters.AddWithValue("@uua_9010_1_1", _uua_9010_1_1);
        Command.Parameters.AddWithValue("@fua_9010_1_1", _fua_9010_1_1);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}