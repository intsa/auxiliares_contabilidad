using System;
using System.Data;
using System.Data.SqlClient;

public class Alccom_5003
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private short _ide_5003;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _ide_5000;                                        // 2    
    private byte _ldo_5003;                                         // 3    
    private short _ide_5001;                                        // 4    
    private byte _ldf_5003;                                         // 5    
    private short _ide_5002;                                        // 6    
    private byte _lde_5003;                                         // 7    
    private short? _aiv_5003;                                       // 8    
    private short? _afv_5003;                                       // 9    
    private int _uar_5003;                                          // 10    
    private string _far_5003;                                       // 11    
    private string _dip_5003;                                       // 12    
    private int _uua_5003;                                          // 13    
    private string _fua_5003;                                       // 14    

    #endregion

    #region Propiedades

    public short ide_5003
    {
        get { return _ide_5003; }
        set { _ide_5003 = value; }
    }
    public short ide_5000
    {
        get { return _ide_5000; }
        set { _ide_5000 = value; }
    }
    public byte ldo_5003
    {
        get { return _ldo_5003; }
        set { _ldo_5003 = value; }
    }
    public short ide_5001
    {
        get { return _ide_5001; }
        set { _ide_5001 = value; }
    }
    public byte ldf_5003
    {
        get { return _ldf_5003; }
        set { _ldf_5003 = value; }
    }
    public short ide_5002
    {
        get { return _ide_5002; }
        set { _ide_5002 = value; }
    }
    public byte lde_5003
    {
        get { return _lde_5003; }
        set { _lde_5003 = value; }
    }
    public short? aiv_5003
    {
        get { return _aiv_5003; }
        set { _aiv_5003 = value; }
    }
    public short? afv_5003
    {
        get { return _afv_5003; }
        set { _afv_5003 = value; }
    }
    public int uar_5003
    {
        get { return _uar_5003; }
        set { _uar_5003 = value; }
    }
    public string far_5003
    {
        get { return _far_5003; }
        set { _far_5003 = value; }
    }
    public string dip_5003
    {
        get { return _dip_5003; }
        set { _dip_5003 = value; }
    }
    public int uua_5003
    {
        get { return _uua_5003; }
        set { _uua_5003 = value; }
    }
    public string fua_5003
    {
        get { return _fua_5003; }
        set { _fua_5003 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_5003(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_5003 = 0;
        _ide_5000 = 0;
        _ldo_5003 = 0;
        _ide_5001 = 0;
        _ldf_5003 = 0;
        _ide_5002 = 0;
        _lde_5003 = 0;
        _aiv_5003 = null;
        _afv_5003 = null;
        _uar_5003 = 0;
        _far_5003 = "";
        _dip_5003 = "";
        _uua_5003 = 0;
        _fua_5003 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_5003 (  ide_5000, ldo_5003, ide_5001, ldf_5003, ide_5002, lde_5003, aiv_5003, afv_5003, uar_5003, far_5003, dip_5003, uua_5003, fua_5003) " +
            $"OUTPUT INSERTED.ide_5003 VALUES (  @ide_5000, @ldo_5003, @ide_5001, @ldf_5003, @ide_5002, @lde_5003, @aiv_5003, @afv_5003, @uar_5003, @far_5003, @dip_5003, @uua_5003, @fua_5003)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_5000", _ide_5000);
        Command.Parameters.AddWithValue("@ldo_5003", _ldo_5003);
        Command.Parameters.AddWithValue("@ide_5001", _ide_5001);
        Command.Parameters.AddWithValue("@ldf_5003", _ldf_5003);
        Command.Parameters.AddWithValue("@ide_5002", _ide_5002);
        Command.Parameters.AddWithValue("@lde_5003", _lde_5003);

        if (_aiv_5003 == null)
            Command.Parameters.Add("@aiv_5003", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@aiv_5003", _aiv_5003);

        if (_afv_5003 == null)
            Command.Parameters.Add("@afv_5003", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@afv_5003", _afv_5003);

        Command.Parameters.AddWithValue("@uar_5003", _uar_5003);
        Command.Parameters.AddWithValue("@far_5003", _far_5003);
        Command.Parameters.AddWithValue("@dip_5003", _dip_5003);
        Command.Parameters.AddWithValue("@uua_5003", _uua_5003);
        Command.Parameters.AddWithValue("@fua_5003", _fua_5003);

        Command.Connection = _sqlCon;
        _ide_5003 = (short)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_5003 WHERE ide_5003 = {_ide_5003}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_5003 = (short)dr["ide_5003"];

            if (AsignarPropiedades)
            {
                ide_5000 = (short)dr["ide_5000"];
                ldo_5003 = Convert.ToByte(dr["ldo_5003"]);
                ide_5001 = (short)dr["ide_5001"];
                ldf_5003 = Convert.ToByte(dr["ldf_5003"]);
                ide_5002 = (short)dr["ide_5002"];
                lde_5003 = Convert.ToByte(dr["lde_5003"]);
                aiv_5003 = DBNull.Value.Equals(dr["aiv_5003"]) ? null : (short?)dr["aiv_5003"];
                afv_5003 = DBNull.Value.Equals(dr["afv_5003"]) ? null : (short?)dr["afv_5003"];
                uar_5003 = (int)dr["uar_5003"];
                far_5003 = dr["far_5003"].ToString();
                dip_5003 = (string)dr["dip_5003"];
                uua_5003 = (int)dr["uua_5003"];
                fua_5003 = dr["fua_5003"].ToString();
            }
        }
        else
            ide_5003 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_5003 WHERE ide_5003 = {_ide_5003}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {

        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_5003 SET");
        strSQL += string.Concat(" ide_5000 = @ide_5000");
        strSQL += string.Concat(", ldo_5003 = @ldo_5003");
        strSQL += string.Concat(", ide_5001 = @ide_5001");
        strSQL += string.Concat(", ldf_5003 = @ldf_5003");
        strSQL += string.Concat(", ide_5002 = @ide_5002");
        strSQL += string.Concat(", lde_5003 = @lde_5003");
        strSQL += string.Concat(", aiv_5003 = @aiv_5003");
        strSQL += string.Concat(", afv_5003 = @afv_5003");
        strSQL += string.Concat(", dip_5003 = @dip_5003");
        strSQL += string.Concat(", uua_5003 = @uua_5003");
        strSQL += string.Concat(", fua_5003 = @fua_5003");
        strSQL += string.Concat(" WHERE ide_5003 = ", _ide_5003);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_5000", _ide_5000);
        Command.Parameters.AddWithValue("@ldo_5003", _ldo_5003);
        Command.Parameters.AddWithValue("@ide_5001", _ide_5001);
        Command.Parameters.AddWithValue("@ldf_5003", _ldf_5003);
        Command.Parameters.AddWithValue("@ide_5002", _ide_5002);
        Command.Parameters.AddWithValue("@lde_5003", _lde_5003);
   
        if (_aiv_5003 == null)
            Command.Parameters.Add("@aiv_5003", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@aiv_5003", _aiv_5003);

        if (_afv_5003 == null)
            Command.Parameters.Add("@afv_5003", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@afv_5003", _afv_5003);

        Command.Parameters.AddWithValue("@dip_5003", _dip_5003);
        Command.Parameters.AddWithValue("@uua_5003", _uua_5003);
        Command.Parameters.AddWithValue("@fua_5003", _fua_5003);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion
}