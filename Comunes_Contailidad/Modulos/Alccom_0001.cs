using System;
using System.Data;
using System.Data.SqlClient;

namespace Comunes_Contabilidad.Modulos
{
    public class Alccom_0001
    {
        #region Atributos Conexión

        private readonly SqlConnection _sqlCon;
        private readonly string _BaseDatos;

        #endregion

        #region Atributos

        private int _ide_0001;                                          // 1 CLAVE_PRIMARIA   
        private byte _tip_0001;                                         // 2    
        private string _cna_0001;                                       // 3    
        private string _cnp_0001;                                       // 4    
        private string _des_0001;                                       // 5    
        private string _nin_0001;                                       // 6    
        private string _cin_0001;                                       // 7    
        private byte? _lcl_0001;                                        // 8    
        private string _sql_0001;                                       // 9    
        private short _aiv_0001;                                        // 10    
        private short _afv_0001;                                        // 11    
        private int _uar_0001;                                          // 12    
        private string _far_0001;                                       // 13    
        private string _dip_0001;                                       // 14    
        private int _uua_0001;                                          // 15    
        private string _fua_0001;                                       // 16    

        #endregion

        #region Propiedades

        public int ide_0001
        {
            get { return _ide_0001; }
            set { _ide_0001 = value; }
        }
        public byte tip_0001
        {
            get { return _tip_0001; }
            set { _tip_0001 = value; }
        }
        public string cna_0001
        {
            get { return _cna_0001; }
            set { _cna_0001 = value; }
        }
        public string cnp_0001
        {
            get { return _cnp_0001; }
            set { _cnp_0001 = value; }
        }
        public string des_0001
        {
            get { return _des_0001; }
            set { _des_0001 = value; }
        }
        public string nin_0001
        {
            get { return _nin_0001; }
            set { _nin_0001 = value; }
        }
        public string cin_0001
        {
            get { return _cin_0001; }
            set { _cin_0001 = value; }
        }
        public byte? lcl_0001
        {
            get { return _lcl_0001; }
            set { _lcl_0001 = value; }
        }
        public string sql_0001
        {
            get { return _sql_0001; }
            set { _sql_0001 = value; }
        }
        public short aiv_0001
        {
            get { return _aiv_0001; }
            set { _aiv_0001 = value; }
        }
        public short afv_0001
        {
            get { return _afv_0001; }
            set { _afv_0001 = value; }
        }
        public int uar_0001
        {
            get { return _uar_0001; }
            set { _uar_0001 = value; }
        }
        public string far_0001
        {
            get { return _far_0001; }
            set { _far_0001 = value; }
        }
        public string dip_0001
        {
            get { return _dip_0001; }
            set { _dip_0001 = value; }
        }
        public int uua_0001
        {
            get { return _uua_0001; }
            set { _uua_0001 = value; }
        }
        public string fua_0001
        {
            get { return _fua_0001; }
            set { _fua_0001 = value; }
        }

        #endregion

        #region Constructores

        public Alccom_0001(SqlConnection sqlCon, string BaseDatos = null)
        {
            _sqlCon = sqlCon;
            _BaseDatos = BaseDatos ?? "Adm_Loc_Con_Com_000999";

            _ide_0001 = 0;
            _tip_0001 = 0;
            _cna_0001 = "";
            _cnp_0001 = "";
            _des_0001 = "";
            _nin_0001 = "";
            _cin_0001 = "";
            _lcl_0001 = null;
            _sql_0001 = "";
            _aiv_0001 = 0;
            _afv_0001 = 0;
            _uar_0001 = 0;
            _far_0001 = "";
            _dip_0001 = "";
            _uua_0001 = 0;
            _fua_0001 = "";
        }

        #endregion

        #region Metodos Públicos

        //METODO INSERTAR
        public void Insertar()
        {
            SqlCommand Command = new SqlCommand
            {
                CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_0001 (tip_0001, cna_0001, cnp_0001, des_0001, nin_0001, cin_0001, lcl_0001, sql_0001, aiv_0001, afv_0001, uar_0001, far_0001, dip_0001, uua_0001, fua_0001)" +
                $" OUTPUT INSERTED.ide_0001 VALUES (@tip_0001, @cna_0001, @cnp_0001, @des_0001, @nin_0001, @cin_0001, @lcl_0001, @sql_0001, @aiv_0001, @afv_0001, @uar_0001, @far_0001, @dip_0001, @uua_0001, @fua_0001)"
            };

            // ASIGNACIÓN DE PARÁMETROS
            Command.Parameters.AddWithValue("@tip_0001", _tip_0001);
            Command.Parameters.AddWithValue("@cna_0001", _cna_0001);
            Command.Parameters.AddWithValue("@cnp_0001", _cnp_0001);
            Command.Parameters.AddWithValue("@des_0001", _des_0001);
            Command.Parameters.AddWithValue("@nin_0001", _nin_0001);
            Command.Parameters.AddWithValue("@cin_0001", _cin_0001);

            if (_lcl_0001 == null)
                Command.Parameters.Add("@lcl_0001", SqlDbType.TinyInt).Value = DBNull.Value;
            else
                Command.Parameters.AddWithValue("@lcl_0001", _lcl_0001);

            Command.Parameters.AddWithValue("@sql_0001", _sql_0001);
            Command.Parameters.AddWithValue("@aiv_0001", _aiv_0001);
            Command.Parameters.AddWithValue("@afv_0001", _afv_0001);
            Command.Parameters.AddWithValue("@uar_0001", _uar_0001);
            Command.Parameters.AddWithValue("@far_0001", _far_0001);
            Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
            Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
            Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

            Command.Connection = _sqlCon;
            _ide_0001 = (int)Command.ExecuteScalar();
        }

        //METODO CARGAR
        public void Cargar(bool AsignarPropiedades = true)
        {
            SqlCommand Command = new SqlCommand
            {
                Connection = _sqlCon,
                CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_0001 WHERE ide_0001 = {_ide_0001}"
            };

            SqlDataReader dr;
            dr = Command.ExecuteReader();

            if (dr.Read())
            {
                ide_0001 = (int)dr["ide_0001"];

                if (AsignarPropiedades)
                {
                    tip_0001 = (byte)Convert.ToByte(dr["tip_0001"]);
                    cna_0001 = (string)dr["cna_0001"];
                    cnp_0001 = (string)dr["cnp_0001"];
                    des_0001 = (string)dr["des_0001"];
                    nin_0001 = (string)dr["nin_0001"];
                    cin_0001 = (string)dr["cin_0001"];
                    lcl_0001 = DBNull.Value.Equals(dr["lcl_0001"]) ? (byte?)null : (byte?)Convert.ToByte(dr["lcl_0001"]);
                    sql_0001 = (string)dr["sql_0001"];
                    aiv_0001 = (short)dr["aiv_0001"];
                    afv_0001 = (short)dr["afv_0001"];
                    uar_0001 = (int)dr["uar_0001"];
                    far_0001 = dr["far_0001"].ToString();
                    dip_0001 = (string)dr["dip_0001"];
                    uua_0001 = (int)dr["uua_0001"];
                    fua_0001 = dr["fua_0001"].ToString();
                }
            }
            else
                ide_0001 = 0;

            dr.Close();
        }

        //METODO ELIMINAR
        public void Eliminar()
        {
            SqlCommand Command = new SqlCommand
            {
                Connection = _sqlCon,
                CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_0001 WHERE ide_0001 = {_ide_0001}"
            };

            Command.ExecuteNonQuery();
        }

        //METODO ACTUALIZAR
        public void Actualizar()
        {
            string strSQL = "";
            SqlCommand Command = new SqlCommand{Connection = _sqlCon};

            strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_0001 SET");
            strSQL += string.Concat(" tip_0001 = @tip_0001");
            strSQL += string.Concat(", cna_0001 = @cna_0001");
            strSQL += string.Concat(", cnp_0001 = @cnp_0001");
            strSQL += string.Concat(", des_0001 = @des_0001");
            strSQL += string.Concat(", nin_0001 = @nin_0001");
            strSQL += string.Concat(", cin_0001 = @cin_0001");
            strSQL += string.Concat(", lcl_0001 = @lcl_0001");
            strSQL += string.Concat(", sql_0001 = @sql_0001");
            strSQL += string.Concat(", aiv_0001 = @aiv_0001");
            strSQL += string.Concat(", afv_0001 = @afv_0001");
            strSQL += string.Concat(", dip_0001 = @dip_0001");
            strSQL += string.Concat(", uua_0001 = @uua_0001");
            strSQL += string.Concat(", fua_0001 = @fua_0001");
            strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

            // ASIGNACIÓN DE PARÁMETROS
            Command.Parameters.AddWithValue("@tip_0001", _tip_0001);
            Command.Parameters.AddWithValue("@cna_0001", _cna_0001);
            Command.Parameters.AddWithValue("@cnp_0001", _cnp_0001);
            Command.Parameters.AddWithValue("@des_0001", _des_0001);
            Command.Parameters.AddWithValue("@nin_0001", _nin_0001);
            Command.Parameters.AddWithValue("@cin_0001", _cin_0001);

            if (_lcl_0001 == null)
                Command.Parameters.Add("@lcl_0001", SqlDbType.TinyInt).Value = DBNull.Value;
            else
                Command.Parameters.AddWithValue("@lcl_0001", _lcl_0001);

            Command.Parameters.AddWithValue("@sql_0001", _sql_0001);
            Command.Parameters.AddWithValue("@aiv_0001", _aiv_0001);
            Command.Parameters.AddWithValue("@afv_0001", _afv_0001);
            Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
            Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
            Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

            Command.CommandText = strSQL;
            Command.ExecuteNonQuery();
        }

        #endregion

    }
}