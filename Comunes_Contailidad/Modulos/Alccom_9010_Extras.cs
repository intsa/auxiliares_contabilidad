using System;
using System.Data.SqlClient;

public class Alccom_9010_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_9010;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private string _tdo_9010;                                       // 2   CLAVE_UNICA 
    private string _des_9010;                                       // 3    
    private string _sdo_9010;                                       // 4    
    private byte _smp_9010;                                         // 5    
    private string _tda_9010;                                       // 6    
    private int _uar_9010;                                          // 7    
    private string _far_9010;                                       // 8    
    private string _dip_9010;                                       // 9    
    private int _uua_9010;                                          // 10    
    private string _fua_9010;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_9010
    {
        get { return _ide_9010; }
        set { _ide_9010 = value; }
    }
    public string tdo_9010
    {
        get { return _tdo_9010; }
        set { _tdo_9010 = value; }
    }
    public string des_9010
    {
        get { return _des_9010; }
        set { _des_9010 = value; }
    }
    public string sdo_9010
    {
        get { return _sdo_9010; }
        set { _sdo_9010 = value; }
    }
    public byte smp_9010
    {
        get { return _smp_9010; }
        set { _smp_9010 = value; }
    }
    public string tda_9010
    {
        get { return _tda_9010; }
        set { _tda_9010 = value; }
    }
    public int uar_9010
    {
        get { return _uar_9010; }
        set { _uar_9010 = value; }
    }
    public string far_9010
    {
        get { return _far_9010; }
        set { _far_9010 = value; }
    }
    public string dip_9010
    {
        get { return _dip_9010; }
        set { _dip_9010 = value; }
    }
    public int uua_9010
    {
        get { return _uua_9010; }
        set { _uua_9010 = value; }
    }
    public string fua_9010
    {
        get { return _fua_9010; }
        set { _fua_9010 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_9010_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_9010 = 0;
        _tdo_9010 = "";
        _des_9010 = "";
        _sdo_9010 = "";
        _smp_9010 = 0;
        _tda_9010 = "";
        _uar_9010 = 0;
        _far_9010 = "";
        _dip_9010 = "";
        _uua_9010 = 0;
        _fua_9010 = "";
    }

    #endregion

    #region Métodos públicos

    /// <summary>
    /// Actualiza registro<br/>
    /// Denominación [des_9010]<br/>
    /// Serie documental [sdo_9010]<br/>
    /// </summary>
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_9010 SET");
        strSQL += string.Concat(" des_9010 = @des_9010");
        strSQL += string.Concat(", sdo_9010 = @sdo_9010");
        strSQL += string.Concat(", dip_9010 = @dip_9010");
        strSQL += string.Concat(", uua_9010 = @uua_9010");
        strSQL += string.Concat(", fua_9010 = @fua_9010");
        strSQL += string.Concat(" WHERE ide_9010 = ", _ide_9010);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@des_9010", _des_9010);
        Command.Parameters.AddWithValue("@sdo_9010", _sdo_9010);
        Command.Parameters.AddWithValue("@dip_9010", _dip_9010);
        Command.Parameters.AddWithValue("@uua_9010", _uua_9010);
        Command.Parameters.AddWithValue("@fua_9010", _fua_9010);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

     #endregion
}