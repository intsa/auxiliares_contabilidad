using System;
using System.Data;
using System.Data.SqlClient;

namespace Comunes_Contabilidad.Modulos
{
    public class Alccom_0002
    {
        #region Atributos Conexión

        private SqlConnection _sqlCon;
        private string _BaseDatos;

        #endregion

        #region Atributos

        private int _ide_0002;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
        private byte _tip_0002;                                         // 2    
        private byte _ord_0002;                                         // 3    
        private byte _esp_0002;                                         // 4    
        private string _tit_0002;                                       // 5    
        private byte _sne_0002;                                         // 7    
        private byte _sto_0002;                                         // 8    
        private string _sql_0002;                                       // 9    
        private short? _aiv_0002;                                       // 10    
        private short? _afv_0002;                                       // 11    
        private int _uar_0002;                                          // 12    
        private string _far_0002;                                       // 13    
        private string _dip_0002;                                       // 14    
        private int _uua_0002;                                          // 15    
        private string _fua_0002;                                       // 16    

        #endregion

        #region Propiedades

        public int ide_0002
        {
            get { return _ide_0002; }
            set { _ide_0002 = value; }
        }
        public byte tip_0002
        {
            get { return _tip_0002; }
            set { _tip_0002 = value; }
        }
        public byte ord_0002
        {
            get { return _ord_0002; }
            set { _ord_0002 = value; }
        }
        public byte esp_0002
        {
            get { return _esp_0002; }
            set { _esp_0002 = value; }
        }
        public string tit_0002
        {
            get { return _tit_0002; }
            set { _tit_0002 = value; }
        }
        public byte sne_0002
        {
            get { return _sne_0002; }
            set { _sne_0002 = value; }
        }
        public byte sto_0002
        {
            get { return _sto_0002; }
            set { _sto_0002 = value; }
        }
        public string sql_0002
        {
            get { return _sql_0002; }
            set { _sql_0002 = value; }
        }
        public short? aiv_0002
        {
            get { return _aiv_0002; }
            set { _aiv_0002 = value; }
        }
        public short? afv_0002
        {
            get { return _afv_0002; }
            set { _afv_0002 = value; }
        }
        public int uar_0002
        {
            get { return _uar_0002; }
            set { _uar_0002 = value; }
        }
        public string far_0002
        {
            get { return _far_0002; }
            set { _far_0002 = value; }
        }
        public string dip_0002
        {
            get { return _dip_0002; }
            set { _dip_0002 = value; }
        }
        public int uua_0002
        {
            get { return _uua_0002; }
            set { _uua_0002 = value; }
        }
        public string fua_0002
        {
            get { return _fua_0002; }
            set { _fua_0002 = value; }
        }

        #endregion

        #region Constructores

        public Alccom_0002(SqlConnection sqlCon, string BaseDatos = null)
        {
            _sqlCon = sqlCon;
            _BaseDatos = BaseDatos ?? sqlCon.Database;

            _ide_0002 = 0;
            _tip_0002 = 0;
            _ord_0002 = 0;
            _esp_0002 = 0;
            _tit_0002 = "";
            _sne_0002 = 0;
            _sto_0002 = 0;
            _sql_0002 = "";
            _aiv_0002 = null;
            _afv_0002 = null;
            _uar_0002 = 0;
            _far_0002 = "";
            _dip_0002 = "";
            _uua_0002 = 0;
            _fua_0002 = "";
        }

        #endregion

        #region Metodos Públicos

        //METODO INSERTAR
        public void Insertar()
        {
            SqlCommand Command = new SqlCommand();

            Command.CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_0002 (tip_0002, ord_0002, esp_0002, tit_0002, sne_0002, sto_0002, sql_0002, aiv_0002, afv_0002, uar_0002, far_0002, dip_0002, uua_0002, fua_0002)" +
                $" OUTPUT INSERTED.ide_0002 VALUES (@tip_0002, @ord_0002, @esp_0002, @tit_0002, @sne_0002, @sto_0002, @sql_0002, @aiv_0002, @afv_0002, @uar_0002, @far_0002, @dip_0002, @uua_0002, @fua_0002)";

            // ASIGNACIÓN DE PARÁMETROS
            Command.Parameters.AddWithValue("@tip_0002", _tip_0002);
            Command.Parameters.AddWithValue("@ord_0002", _ord_0002);
            Command.Parameters.AddWithValue("@esp_0002", _esp_0002);
            Command.Parameters.AddWithValue("@tit_0002", _tit_0002);
            Command.Parameters.AddWithValue("@sne_0002", _sne_0002);
            Command.Parameters.AddWithValue("@sto_0002", _sto_0002);
            Command.Parameters.AddWithValue("@sql_0002", _sql_0002);

            if (_aiv_0002 == null)
                Command.Parameters.Add("@aiv_0002", SqlDbType.SmallInt).Value = DBNull.Value;
            else
                Command.Parameters.AddWithValue("@aiv_0002", _aiv_0002);

            if (_afv_0002 == null)
                Command.Parameters.Add("@afv_0002", SqlDbType.SmallInt).Value = DBNull.Value;
            else
                Command.Parameters.AddWithValue("@afv_0002", _afv_0002);

            Command.Parameters.AddWithValue("@uar_0002", _uar_0002);
            Command.Parameters.AddWithValue("@far_0002", _far_0002);
            Command.Parameters.AddWithValue("@dip_0002", _dip_0002);
            Command.Parameters.AddWithValue("@uua_0002", _uua_0002);
            Command.Parameters.AddWithValue("@fua_0002", _fua_0002);

            Command.Connection = _sqlCon;
            _ide_0002 = (int)Command.ExecuteScalar();
        }

        //METODO CARGAR
        public void Cargar(bool AsignarPropiedades = true)
        {
            SqlCommand Command = new SqlCommand
            {
                Connection = _sqlCon,
                CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_0002 WHERE ide_0002 = {_ide_0002}"
            };

            SqlDataReader dr;
            dr = Command.ExecuteReader();

            if (dr.Read())
            {
                ide_0002 = (int)dr["ide_0002"];

                if (AsignarPropiedades)
                {
                    tip_0002 = (byte)Convert.ToByte(dr["tip_0002"]);
                    ord_0002 = (byte)Convert.ToByte(dr["ord_0002"]);
                    esp_0002 = (byte)Convert.ToByte(dr["esp_0002"]);
                    tit_0002 = (string)dr["tit_0002"];
                    sne_0002 = Convert.ToByte(dr["sne_0002"]);
                    sto_0002 = Convert.ToByte(dr["sto_0002"]);
                    sql_0002 = (string)dr["sql_0002"];
                    aiv_0002 = DBNull.Value.Equals(dr["aiv_0002"]) ? (short?)null : (short?)dr["aiv_0002"];
                    afv_0002 = DBNull.Value.Equals(dr["afv_0002"]) ? (short?)null : (short?)dr["afv_0002"];
                    uar_0002 = (int)dr["uar_0002"];
                    far_0002 = dr["far_0002"].ToString();
                    dip_0002 = (string)dr["dip_0002"];
                    uua_0002 = (int)dr["uua_0002"];
                    fua_0002 = dr["fua_0002"].ToString();
                }
            }
            else
                ide_0002 = 0;

            dr.Close();
        }

        //METODO ELIMINAR
        public void Eliminar()
        {
            SqlCommand Command = new SqlCommand
            {
                Connection = _sqlCon,
                CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_0002 WHERE ide_0002 = {_ide_0002}"
            };

            Command.ExecuteNonQuery();
        }

        //METODO QUE ACTUALIZAR
        public void Actualizar()
        {
            string strSQL = "";
            SqlCommand Command = new SqlCommand();

            Command.Connection = _sqlCon;

            strSQL = string.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alccom_0002 Set ");
            strSQL += string.Concat("  tip_0002 = @tip_0002");
            strSQL += string.Concat(", ord_0002 = @ord_0002");
            strSQL += string.Concat(", esp_0002 = @esp_0002");
            strSQL += string.Concat(", tit_0002 = @tit_0002");
            strSQL += string.Concat(", sne_0002 = @sne_0002");
            strSQL += string.Concat(", sto_0002 = @sto_0002");
            strSQL += string.Concat(", sql_0002 = @sql_0002");
            strSQL += string.Concat(", aiv_0002 = @aiv_0002");
            strSQL += string.Concat(", afv_0002 = @afv_0002");
            strSQL += string.Concat(", dip_0002 = @dip_0002");
            strSQL += string.Concat(", uua_0002 = @uua_0002");
            strSQL += string.Concat(", fua_0002 = @fua_0002");
            strSQL += string.Concat(" WHERE ide_0002 = ", _ide_0002);

            // ASIGNACIÓN DE PARÁMETROS
            Command.Parameters.AddWithValue("@tip_0002", _tip_0002);
            Command.Parameters.AddWithValue("@ord_0002", _ord_0002);
            Command.Parameters.AddWithValue("@esp_0002", _esp_0002);
            Command.Parameters.AddWithValue("@tit_0002", _tit_0002);
            Command.Parameters.AddWithValue("@sne_0002", _sne_0002);
            Command.Parameters.AddWithValue("@sto_0002", _sto_0002);
            Command.Parameters.AddWithValue("@sql_0002", _sql_0002);

            if (_aiv_0002 == null)
                Command.Parameters.Add("@aiv_0002", SqlDbType.SmallInt).Value = DBNull.Value;
            else
                Command.Parameters.AddWithValue("@aiv_0002", _aiv_0002);

            if (_afv_0002 == null)
                Command.Parameters.Add("@afv_0002", SqlDbType.SmallInt).Value = DBNull.Value;
            else
                Command.Parameters.AddWithValue("@afv_0002", _afv_0002);

            Command.Parameters.AddWithValue("@dip_0002", _dip_0002);
            Command.Parameters.AddWithValue("@uua_0002", _uua_0002);
            Command.Parameters.AddWithValue("@fua_0002", _fua_0002);

            Command.CommandText = strSQL;
            Command.ExecuteNonQuery();
        }

        #endregion

    }
}