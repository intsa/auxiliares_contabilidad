using System;
using System.Data.SqlClient;

public class Alccom_2050_1
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_2050_1;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_2050;                                          // 2   CLAVE_UNICA 
    private string _tdr_2050_1;                                     // 3   CLAVE_UNICA 
    private string _den_2050_1;                                     // 4    
    private byte _sun_2050_1;                                       // 5    
    private int _uar_2050_1;                                        // 6    
    private string _far_2050_1;                                     // 7    
    private string _dip_2050_1;                                     // 8    
    private int _uua_2050_1;                                        // 9    
    private string _fua_2050_1;                                     // 10    

    #endregion

    #region Propiedades

    public int ide_2050_1
    {
        get { return _ide_2050_1; }
        set { _ide_2050_1 = value; }
    }
    public int ide_2050
    {
        get { return _ide_2050; }
        set { _ide_2050 = value; }
    }
    public string tdr_2050_1
    {
        get { return _tdr_2050_1; }
        set { _tdr_2050_1 = value; }
    }
    public string den_2050_1
    {
        get { return _den_2050_1; }
        set { _den_2050_1 = value; }
    }
    public byte sun_2050_1
    {
        get { return _sun_2050_1; }
        set { _sun_2050_1 = value; }
    }
    public int uar_2050_1
    {
        get { return _uar_2050_1; }
        set { _uar_2050_1 = value; }
    }
    public string far_2050_1
    {
        get { return _far_2050_1; }
        set { _far_2050_1 = value; }
    }
    public string dip_2050_1
    {
        get { return _dip_2050_1; }
        set { _dip_2050_1 = value; }
    }
    public int uua_2050_1
    {
        get { return _uua_2050_1; }
        set { _uua_2050_1 = value; }
    }
    public string fua_2050_1
    {
        get { return _fua_2050_1; }
        set { _fua_2050_1 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_2050_1(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_2050_1 = 0;
        _ide_2050 = 0;
        _tdr_2050_1 = "";
        _den_2050_1 = "";
        _sun_2050_1 = 0;
        _uar_2050_1 = 0;
        _far_2050_1 = "";
        _dip_2050_1 = "";
        _uua_2050_1 = 0;
        _fua_2050_1 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_2050_1 (ide_2050, tdr_2050_1, den_2050_1, sun_2050_1, uar_2050_1, far_2050_1, dip_2050_1, uua_2050_1, fua_2050_1)" +
            $" OUTPUT INSERTED.ide_2050_1 VALUES (@ide_2050, @tdr_2050_1, @den_2050_1, @sun_2050_1, @uar_2050_1, @far_2050_1, @dip_2050_1, @uua_2050_1, @fua_2050_1)"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_2050", _ide_2050);
        Command.Parameters.AddWithValue("@tdr_2050_1", _tdr_2050_1);
        Command.Parameters.AddWithValue("@den_2050_1", _den_2050_1);
        Command.Parameters.AddWithValue("@sun_2050_1", _sun_2050_1);
        Command.Parameters.AddWithValue("@uar_2050_1", _uar_2050_1);
        Command.Parameters.AddWithValue("@far_2050_1", _far_2050_1);
        Command.Parameters.AddWithValue("@dip_2050_1", _dip_2050_1);
        Command.Parameters.AddWithValue("@uua_2050_1", _uua_2050_1);
        Command.Parameters.AddWithValue("@fua_2050_1", _fua_2050_1);

        Command.Connection = _sqlCon;
        _ide_2050_1 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_2050_1 WHERE ide_2050_1 = {_ide_2050_1}"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2050_1 = (int)dr["ide_2050_1"];

            if (AsignarPropiedades)
            {
                ide_2050 = (int)dr["ide_2050"];
                tdr_2050_1 = (string)dr["tdr_2050_1"];
                den_2050_1 = (string)dr["den_2050_1"];
                sun_2050_1 = Convert.ToByte(dr["sun_2050_1"]);
                uar_2050_1 = (int)dr["uar_2050_1"];
                far_2050_1 = dr["far_2050_1"].ToString();
                dip_2050_1 = (string)dr["dip_2050_1"];
                uua_2050_1 = (int)dr["uua_2050_1"];
                fua_2050_1 = dr["fua_2050_1"].ToString();
            }
        }
        else
            ide_2050_1 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_2050_1 WHERE ide_2050_1 = {_ide_2050_1}"
        };

        Command.ExecuteNonQuery();
    }

    //METODO ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_2050_1 SET");
        strSQL += string.Concat(" ide_2050 = @ide_2050");
        strSQL += string.Concat(", tdr_2050_1 = @tdr_2050_1");
        strSQL += string.Concat(", den_2050_1 = @den_2050_1");
        strSQL += string.Concat(", sun_2050_1 = @sun_2050_1");
        strSQL += string.Concat(", dip_2050_1 = @dip_2050_1");
        strSQL += string.Concat(", uua_2050_1 = @uua_2050_1");
        strSQL += string.Concat(", fua_2050_1 = @fua_2050_1");
        strSQL += string.Concat(" WHERE ide_2050_1 = ", _ide_2050_1);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_2050", _ide_2050);
        Command.Parameters.AddWithValue("@tdr_2050_1", _tdr_2050_1);
        Command.Parameters.AddWithValue("@den_2050_1", _den_2050_1);
        Command.Parameters.AddWithValue("@sun_2050_1", _sun_2050_1);
        Command.Parameters.AddWithValue("@dip_2050_1", _dip_2050_1);
        Command.Parameters.AddWithValue("@uua_2050_1", _uua_2050_1);
        Command.Parameters.AddWithValue("@fua_2050_1", _fua_2050_1);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE ÚNICA: ide_2050 tdr_2050_1
    public void Cargar_ide_2050_tdr_2050_1(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = $"Select * FROM {_BaseDatos}.dbo.alccom_2050_1 WHERE ide_2050 = {_ide_2050} AND tdr_2050_1 = '{_tdr_2050_1}'"
        };

        SqlDataReader dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2050_1 = (int)dr["ide_2050_1"];

            if (AsignarPropiedades)
            {
                ide_2050 = (int)dr["ide_2050"];
                tdr_2050_1 = (string)dr["tdr_2050_1"];
                den_2050_1 = (string)dr["den_2050_1"];
                sun_2050_1 = Convert.ToByte(dr["sun_2050_1"]);
                uar_2050_1 = (int)dr["uar_2050_1"];
                far_2050_1 = dr["far_2050_1"].ToString();
                dip_2050_1 = (string)dr["dip_2050_1"];
                uua_2050_1 = (int)dr["uua_2050_1"];
                fua_2050_1 = dr["fua_2050_1"].ToString();
            }
        }
        else
            ide_2050_1 = 0;

        dr.Close();
    }

    #endregion

}