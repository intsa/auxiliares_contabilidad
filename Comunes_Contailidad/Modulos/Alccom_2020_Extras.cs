using System;
using System.Data.SqlClient;

public class Alccom_2020_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_2020;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _tdf_2020;                                        // 2   CLAVE_UNICA 
    private string _den_2020;                                       // 3    
    private byte _fca_2020;                                         // 4    
    private short _dia_2020;                                        // 5    
    private byte _smo_2020;                                         // 6    
    private byte _sgi_2020;                                         // 7    
    private byte _sre_2020;                                         // 8    
    private int _uar_2020;                                          // 9    
    private string _far_2020;                                       // 10    
    private string _dip_2020;                                       // 11    
    private int _uua_2020;                                          // 12    
    private string _fua_2020;                                       // 13    

    #endregion

    #region Propiedades

    public int ide_2020
    {
        get { return _ide_2020; }
        set { _ide_2020 = value; }
    }
    public short tdf_2020
    {
        get { return _tdf_2020; }
        set { _tdf_2020 = value; }
    }
    public string den_2020
    {
        get { return _den_2020; }
        set { _den_2020 = value; }
    }
    public byte fca_2020
    {
        get { return _fca_2020; }
        set { _fca_2020 = value; }
    }
    public short dia_2020
    {
        get { return _dia_2020; }
        set { _dia_2020 = value; }
    }
    public byte smo_2020
    {
        get { return _smo_2020; }
        set { _smo_2020 = value; }
    }
    public byte sgi_2020
    {
        get { return _sgi_2020; }
        set { _sgi_2020 = value; }
    }
    public byte sre_2020
    {
        get { return _sre_2020; }
        set { _sre_2020 = value; }
    }
    public int uar_2020
    {
        get { return _uar_2020; }
        set { _uar_2020 = value; }
    }
    public string far_2020
    {
        get { return _far_2020; }
        set { _far_2020 = value; }
    }
    public string dip_2020
    {
        get { return _dip_2020; }
        set { _dip_2020 = value; }
    }
    public int uua_2020
    {
        get { return _uua_2020; }
        set { _uua_2020 = value; }
    }
    public string fua_2020
    {
        get { return _fua_2020; }
        set { _fua_2020 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_2020_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_2020 = 0;
        _tdf_2020 = 0;
        _den_2020 = "";
        _fca_2020 = 0;
        _dia_2020 = 0;
        _smo_2020 = 0;
        _sgi_2020 = 0;
        _sre_2020 = 0;
        _uar_2020 = 0;
        _far_2020 = "";
        _dip_2020 = "";
        _uua_2020 = 0;
        _fua_2020 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO ACTUALIZAR
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_2020 SET");
        strSQL += string.Concat(" den_2020 = @den_2020");
        strSQL += string.Concat(", fca_2020 = @fca_2020");
        strSQL += string.Concat(", dia_2020 = @dia_2020");
        strSQL += string.Concat(", smo_2020 = @smo_2020");
        strSQL += string.Concat(", sgi_2020 = @sgi_2020");
        strSQL += string.Concat(", sre_2020 = @sre_2020");
        strSQL += string.Concat(", dip_2020 = @dip_2020");
        strSQL += string.Concat(", uua_2020 = @uua_2020");
        strSQL += string.Concat(", fua_2020 = @fua_2020");
        strSQL += string.Concat(" WHERE ide_2020 = ", _ide_2020);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_2020", _den_2020);
        Command.Parameters.AddWithValue("@fca_2020", _fca_2020);
        Command.Parameters.AddWithValue("@dia_2020", _dia_2020);
        Command.Parameters.AddWithValue("@smo_2020", _smo_2020);
        Command.Parameters.AddWithValue("@sgi_2020", _sgi_2020);
        Command.Parameters.AddWithValue("@sre_2020", _sre_2020);
        Command.Parameters.AddWithValue("@dip_2020", _dip_2020);
        Command.Parameters.AddWithValue("@uua_2020", _uua_2020);
        Command.Parameters.AddWithValue("@fua_2020", _fua_2020);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion

}