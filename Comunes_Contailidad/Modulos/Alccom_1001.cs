using System;
using System.Data.SqlClient;

namespace Comunes_Contabilidad.Modulos
{
    public class Alccom_1001
    {
        #region Atributos Conexión

        private SqlConnection _sqlCon;
        private string _BaseDatos;

        #endregion

        #region Atributos

        private int _ide_1001;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
        private byte _niv_1001;                                         // 2    
        private string _cta_1001;                                       // 3    
        private string _des_1001;                                       // 4    
        private byte _sun_1001;                                         // 5    
        private short _acv_1001;                                        // 6    
        private short _afv_1001;                                        // 7    
        private int _uar_1001;                                          // 8    
        private string _far_1001;                                       // 9    
        private string _dip_1001;                                       // 10    
        private int _uua_1001;                                          // 11    
        private string _fua_1001;                                       // 12    

        #endregion

        #region Propiedades

        public int ide_1001
        {
            get { return _ide_1001; }
            set { _ide_1001 = value; }
        }
        public byte niv_1001
        {
            get { return _niv_1001; }
            set { _niv_1001 = value; }
        }
        public string cta_1001
        {
            get { return _cta_1001; }
            set { _cta_1001 = value; }
        }
        public string des_1001
        {
            get { return _des_1001; }
            set { _des_1001 = value; }
        }
        public byte sun_1001
        {
            get { return _sun_1001; }
            set { _sun_1001 = value; }
        }
        public short acv_1001
        {
            get { return _acv_1001; }
            set { _acv_1001 = value; }
        }
        public short afv_1001
        {
            get { return _afv_1001; }
            set { _afv_1001 = value; }
        }
        public int uar_1001
        {
            get { return _uar_1001; }
            set { _uar_1001 = value; }
        }
        public string far_1001
        {
            get { return _far_1001; }
            set { _far_1001 = value; }
        }
        public string dip_1001
        {
            get { return _dip_1001; }
            set { _dip_1001 = value; }
        }
        public int uua_1001
        {
            get { return _uua_1001; }
            set { _uua_1001 = value; }
        }
        public string fua_1001
        {
            get { return _fua_1001; }
            set { _fua_1001 = value; }
        }

        #endregion

        #region Constructores

        public Alccom_1001(SqlConnection sqlCon, string BaseDatos = null)
        {
            _sqlCon = sqlCon;
            _BaseDatos = BaseDatos ?? sqlCon.Database;

            _ide_1001 = 0;
            _niv_1001 = 0;
            _cta_1001 = "";
            _des_1001 = "";
            _sun_1001 = 0;
            _acv_1001 = 0;
            _afv_1001 = 0;
            _uar_1001 = 0;
            _far_1001 = "";
            _dip_1001 = "";
            _uua_1001 = 0;
            _fua_1001 = "";
        }

        #endregion

        #region Metodos Públicos

        //METODO INSERTAR
        public void Insertar()
        {
            SqlCommand Command = new SqlCommand
            {
                CommandText = $"INSERT INTO {_BaseDatos}.dbo.alccom_1001 (niv_1001, cta_1001, des_1001, sun_1001, acv_1001, afv_1001, uar_1001, far_1001, dip_1001, uua_1001, fua_1001)" +
                $" OUTPUT INSERTED.ide_1001 VALUES (@niv_1001, @cta_1001, @des_1001, @sun_1001, @acv_1001, @afv_1001, @uar_1001, @far_1001, @dip_1001, @uua_1001, @fua_1001)"
            };

            // ASIGNACIÓN DE PARÁMETROS
            Command.Parameters.AddWithValue("@niv_1001", _niv_1001);
            Command.Parameters.AddWithValue("@cta_1001", _cta_1001);
            Command.Parameters.AddWithValue("@des_1001", _des_1001);
            Command.Parameters.AddWithValue("@sun_1001", _sun_1001);
            Command.Parameters.AddWithValue("@acv_1001", _acv_1001);
            Command.Parameters.AddWithValue("@afv_1001", _afv_1001);
            Command.Parameters.AddWithValue("@uar_1001", _uar_1001);
            Command.Parameters.AddWithValue("@far_1001", _far_1001);
            Command.Parameters.AddWithValue("@dip_1001", _dip_1001);
            Command.Parameters.AddWithValue("@uua_1001", _uua_1001);
            Command.Parameters.AddWithValue("@fua_1001", _fua_1001);

            Command.Connection = _sqlCon;
            _ide_1001 = (int)Command.ExecuteScalar();
        }

        //METODO CARGAR
        public void Cargar(bool AsignarPropiedades = true)
        {
            SqlCommand Command = new SqlCommand
            {
                Connection = _sqlCon,
                CommandText = $"SELECT * FROM {_BaseDatos}.dbo.alccom_1001 WHERE ide_1001 = {_ide_1001}"
            };

            SqlDataReader dr;
            dr = Command.ExecuteReader();

            if (dr.Read())
            {
                ide_1001 = (int)dr["ide_1001"];

                if (AsignarPropiedades)
                {
                    niv_1001 = (byte)Convert.ToByte(dr["niv_1001"]);
                    cta_1001 = (string)dr["cta_1001"];
                    des_1001 = (string)dr["des_1001"];
                    sun_1001 = Convert.ToByte(dr["sun_1001"]);
                    acv_1001 = (short)dr["acv_1001"];
                    afv_1001 = (short)dr["afv_1001"];
                    uar_1001 = (int)dr["uar_1001"];
                    far_1001 = dr["far_1001"].ToString();
                    dip_1001 = (string)dr["dip_1001"];
                    uua_1001 = (int)dr["uua_1001"];
                    fua_1001 = dr["fua_1001"].ToString();
                }
            }
            else
                ide_1001 = 0;

            dr.Close();
        }

        //METODO ELIMINAR
        public void Eliminar()
        {
            SqlCommand Command = new SqlCommand
            {
                Connection = _sqlCon,
                CommandText = $"DELETE FROM {_BaseDatos}.dbo.alccom_1001 WHERE ide_1001 = {_ide_1001}"
            };

            Command.ExecuteNonQuery();
        }

        //METODO QUE ACTUALIZAR
        public void Actualizar()
        {
            string strSQL = "";
            SqlCommand Command = new SqlCommand
            {
                Connection = _sqlCon
            };

            strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_1001 SET");
            strSQL += string.Concat(" niv_1001 = @niv_1001");
            strSQL += string.Concat(", cta_1001 = @cta_1001");
            strSQL += string.Concat(", des_1001 = @des_1001");
            strSQL += string.Concat(", sun_1001 = @sun_1001");
            strSQL += string.Concat(", acv_1001 = @acv_1001");
            strSQL += string.Concat(", afv_1001 = @afv_1001");
            strSQL += string.Concat(", dip_1001 = @dip_1001");
            strSQL += string.Concat(", uua_1001 = @uua_1001");
            strSQL += string.Concat(", fua_1001 = @fua_1001");
            strSQL += string.Concat(" WHERE ide_1001 = ", _ide_1001);

            // ASIGNACIÓN DE PARÁMETROS
            Command.Parameters.AddWithValue("@niv_1001", _niv_1001);
            Command.Parameters.AddWithValue("@cta_1001", _cta_1001);
            Command.Parameters.AddWithValue("@des_1001", _des_1001);
            Command.Parameters.AddWithValue("@sun_1001", _sun_1001);
            Command.Parameters.AddWithValue("@acv_1001", _acv_1001);
            Command.Parameters.AddWithValue("@afv_1001", _afv_1001);
            Command.Parameters.AddWithValue("@dip_1001", _dip_1001);
            Command.Parameters.AddWithValue("@uua_1001", _uua_1001);
            Command.Parameters.AddWithValue("@fua_1001", _fua_1001);

            Command.CommandText = strSQL;
            Command.ExecuteNonQuery();
        }

        #endregion

    }
}