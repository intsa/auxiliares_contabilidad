using System;
using System.Data;
using System.Data.SqlClient;

namespace Comunes_Contabilidad.Modulos
{
    public class Alccom_0001_Extras
    {
        #region Atributos Conexión

        private SqlConnection _sqlCon;
        private string _BaseDatos;

        #endregion

        #region Atributos

        private int _ide_0001;                                          // 1 CLAVE_PRIMARIA   
        private byte _tip_0001;                                         // 2    
        private string _cna_0001;                                       // 3    
        private string _cnp_0001;                                       // 4    
        private string _des_0001;                                       // 5    
        private string _nin_0001;                                       // 6    
        private string _cin_0001;                                       // 7    
        private byte? _lcl_0001;                                        // 8    
        private string _sql_0001;                                       // 9    
        private short _aiv_0001;                                        // 10    
        private short _afv_0001;                                        // 11    
        private int _uar_0001;                                          // 12    
        private string _far_0001;                                       // 13    
        private string _dip_0001;                                       // 14    
        private int _uua_0001;                                          // 15    
        private string _fua_0001;                                       // 16    

        #endregion

        #region Propiedades

        public int ide_0001
        {
            get { return _ide_0001; }
            set { _ide_0001 = value; }
        }
        public byte tip_0001
        {
            get { return _tip_0001; }
            set { _tip_0001 = value; }
        }
        public string cna_0001
        {
            get { return _cna_0001; }
            set { _cna_0001 = value; }
        }
        public string cnp_0001
        {
            get { return _cnp_0001; }
            set { _cnp_0001 = value; }
        }
        public string des_0001
        {
            get { return _des_0001; }
            set { _des_0001 = value; }
        }
        public string nin_0001
        {
            get { return _nin_0001; }
            set { _nin_0001 = value; }
        }
        public string cin_0001
        {
            get { return _cin_0001; }
            set { _cin_0001 = value; }
        }
        public byte? lcl_0001
        {
            get { return _lcl_0001; }
            set { _lcl_0001 = value; }
        }
        public string sql_0001
        {
            get { return _sql_0001; }
            set { _sql_0001 = value; }
        }
        public short aiv_0001
        {
            get { return _aiv_0001; }
            set { _aiv_0001 = value; }
        }
        public short afv_0001
        {
            get { return _afv_0001; }
            set { _afv_0001 = value; }
        }
        public int uar_0001
        {
            get { return _uar_0001; }
            set { _uar_0001 = value; }
        }
        public string far_0001
        {
            get { return _far_0001; }
            set { _far_0001 = value; }
        }
        public string dip_0001
        {
            get { return _dip_0001; }
            set { _dip_0001 = value; }
        }
        public int uua_0001
        {
            get { return _uua_0001; }
            set { _uua_0001 = value; }
        }
        public string fua_0001
        {
            get { return _fua_0001; }
            set { _fua_0001 = value; }
        }

        #endregion

        #region Constructores

        public Alccom_0001_Extras(SqlConnection sqlCon, string BaseDatos = null)
        {
            _sqlCon = sqlCon;
            _BaseDatos = BaseDatos ?? "Adm_Loc_Con_Com_000999";

            _ide_0001 = 0;
            _tip_0001 = 0;
            _cna_0001 = "";
            _cnp_0001 = "";
            _des_0001 = "";
            _nin_0001 = "";
            _cin_0001 = "";
            _lcl_0001 = null;
            _sql_0001 = "";
            _aiv_0001 = 0;
            _afv_0001 = 0;
            _uar_0001 = 0;
            _far_0001 = "";
            _dip_0001 = "";
            _uua_0001 = 0;
            _fua_0001 = "";
        }

        #endregion

        #region Metodos Públicos

        //METODO CARGAR
        public void Cargar_Extras()
        {
            SqlCommand Command = new SqlCommand
            {
                Connection = _sqlCon,
                CommandText = $"SELECT ide_0001, nin_0001,  sql_0001 FROM {_BaseDatos}.dbo.alccom_0001 WHERE ide_0001 = {_ide_0001}"
            };

            SqlDataReader dr;
            dr = Command.ExecuteReader();

            if (dr.Read())
            {
                ide_0001 = (int)dr["ide_0001"];
                nin_0001 = (string)dr["nin_0001"];
                sql_0001 = (string)dr["sql_0001"];
            }
            else
                ide_0001 = 0;

            dr.Close();
        }

        #endregion

    }
}