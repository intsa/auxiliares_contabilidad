using System;
using System.Data;
using System.Data.SqlClient;

public class Alccom_5002_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private short _ide_5002;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private byte _niv_5002;                                         // 2    
    private string _des_5002;                                       // 3    
    private byte _lnc_5002;                                         // 4    
    private short? _aiv_5002;                                       // 5    
    private short? _afv_5002;                                       // 6    
    private int _uar_5002;                                          // 7    
    private string _far_5002;                                       // 8    
    private string _dip_5002;                                       // 9    
    private int _uua_5002;                                          // 10    
    private string _fua_5002;                                       // 11    

    #endregion

    #region Propiedades

    public short ide_5002
    {
        get { return _ide_5002; }
        set { _ide_5002 = value; }
    }
    public byte niv_5002
    {
        get { return _niv_5002; }
        set { _niv_5002 = value; }
    }
    public string des_5002
    {
        get { return _des_5002; }
        set { _des_5002 = value; }
    }
    public byte lnc_5002
    {
        get { return _lnc_5002; }
        set { _lnc_5002 = value; }
    }
    public short? aiv_5002
    {
        get { return _aiv_5002; }
        set { _aiv_5002 = value; }
    }
    public short? afv_5002
    {
        get { return _afv_5002; }
        set { _afv_5002 = value; }
    }
    public int uar_5002
    {
        get { return _uar_5002; }
        set { _uar_5002 = value; }
    }
    public string far_5002
    {
        get { return _far_5002; }
        set { _far_5002 = value; }
    }
    public string dip_5002
    {
        get { return _dip_5002; }
        set { _dip_5002 = value; }
    }
    public int uua_5002
    {
        get { return _uua_5002; }
        set { _uua_5002 = value; }
    }
    public string fua_5002
    {
        get { return _fua_5002; }
        set { _fua_5002 = value; }
    }

    #endregion

    #region Constructores

    public Alccom_5002_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_5002 = 0;
        _niv_5002 = 0;
        _des_5002 = "";
        _lnc_5002 = 0;
        _aiv_5002 = null;
        _afv_5002 = null;
        _uar_5002 = 0;
        _far_5002 = "";
        _dip_5002 = "";
        _uua_5002 = 0;
        _fua_5002 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO ACTUALIZAR
    public void Actualizar_Extras()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, $"UPDATE {_BaseDatos}.dbo.alccom_5002 SET");
        strSQL += string.Concat(" des_5002 = @des_5002");
        strSQL += string.Concat(", lnc_5002 = @lnc_5002");
        strSQL += string.Concat(", aiv_5002 = @aiv_5002");
        strSQL += string.Concat(", afv_5002 = @afv_5002");
        strSQL += string.Concat(", dip_5002 = @dip_5002");
        strSQL += string.Concat(", uua_5002 = @uua_5002");
        strSQL += string.Concat(", fua_5002 = @fua_5002");
        strSQL += string.Concat(" WHERE ide_5002 = ", _ide_5002);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@des_5002", _des_5002);
        Command.Parameters.AddWithValue("@lnc_5002", _lnc_5002);

        if (_aiv_5002 == null)
            Command.Parameters.Add("@aiv_5002", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@aiv_5002", _aiv_5002);

        if (_afv_5002 == null)
            Command.Parameters.Add("@afv_5002", SqlDbType.SmallInt).Value = DBNull.Value;
        else
            Command.Parameters.AddWithValue("@afv_5002", _afv_5002);

        Command.Parameters.AddWithValue("@dip_5002", _dip_5002);
        Command.Parameters.AddWithValue("@uua_5002", _uua_5002);
        Command.Parameters.AddWithValue("@fua_5002", _fua_5002);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    #endregion
}