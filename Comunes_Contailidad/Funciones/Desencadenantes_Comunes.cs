﻿using ClaseIntsa.Propiedades;
using Comunes_Contabilidad.Modulos;
using System;
using System.Data.SqlClient;

namespace Comunes_Contabilidad
{
    public static class Desencadenantes_Comunes
    {
        #region Tabla Alccom_1001 'CUENTAS CONTABLES. MAYOR. COMUNES'

        /// <summary>
        /// Inserta registros tabla 'CUENTAS CONTABLES. MAYOR'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Nivel">Nivel de la cuenta</param>
        /// <param name="Cuenta">Código cuenta contable</param>
        /// <param name="Denominacion">Denominación cuenta contable</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <param name="Inicio">Año comienzo validez</param>
        /// <param name="Final">Año finalización validez</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_1001(string CadenaConexion, string Bdd_Comunes, byte Nivel, string Cuenta, string Denominacion, byte Ultimo_Nivel, short Inicio, short Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_1001 Temporal = new Alccom_1001(Cadena_Conexion, Bdd_Comunes)
                    {
                        niv_1001 = Nivel,
                        cta_1001 = Cuenta,
                        des_1001 = Denominacion,
                        sun_1001 = Ultimo_Nivel,
                        acv_1001 = Inicio,
                        afv_1001 = Final,
                        uar_1001 = Variables_Globales.IdUsuario,
                        far_1001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_1001 = Variables_Globales.IpPrivada,
                        uua_1001 = Variables_Globales.IdUsuario,
                        fua_1001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_1001 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_1001;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualizar registros tabla 'CUENTAS CONTABLES. MAYOR'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_1001">Identificador registro 'CUENTAS CONTABLES. MAYOR'</param>
        /// <param name="Denominacion">Denominación cuenta contable</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <param name="Inicio">Año comienzo validez</param>
        /// <param name="Final">Año finalización validez</param>
        public static void Actualizar_Alccom_1001(string CadenaConexion, string Bdd_Comunes, int Id_1001, string Denominacion, byte Ultimo_Nivel, short Inicio, short Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_1001_Extras Temporal = new Alccom_1001_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_1001 = Id_1001,
                        des_1001 = Denominacion,
                        sun_1001 = Ultimo_Nivel,
                        acv_1001 = Inicio,
                        afv_1001 = Final,
                        dip_1001 = Variables_Globales.IpPrivada,
                        uua_1001 = Variables_Globales.IdUsuario,
                        fua_1001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'CUENTAS CONTABLES. MAYOR'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_1001">Identificador registro 'CUENTAS CONTABLES. MAYOR'</param>
        public static void Eliminar_Alccom_1001(string CadenaConexion, string Bdd_Comunes, int Id_1001)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_1001 Temporal = new Alccom_1001(Cadena_Conexion, Bdd_Comunes) { ide_1001 = Id_1001 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_1001 'CUENTAS CONTABLES. MAYOR. COMUNES'

        #region Tabla Alccom_2000 'FORMAS DE PAGO/COBRO'

        /// <summary>
        /// Inserta registros tabla 'FORMA DE PAGO/COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Codigo">Código</param>
        /// <param name="Tipo">Tipo<br/>
        /// · 1  Pago<br/>
        /// · 2 Cobro<br/>
        /// · 3 Ambas
        /// </param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Tipo_Documento">Tipo de documento<br/>
        /// · 1 Transferencia<br/>
        /// · 2 Cheque<br/>
        /// · 3 Pagaré<br/>
        /// · 4 Otros
        /// </param>
        /// <param name="Dias">Número de días a vencimiento</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_2000(string CadenaConexion, string Bdd_Comunes, short Codigo, byte Tipo, string Denominacion, byte Tipo_Documento, short Dias)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2000 Temporal = new Alccom_2000(Cadena_Conexion, Bdd_Comunes)
                    {
                        cod_2000 = Codigo,
                        tpc_2000 = Tipo,
                        den_2000 = Denominacion,
                        tdo_2000 = Tipo_Documento,
                        ndi_2000 = Dias,
                        uar_2000 = Variables_Globales.IdUsuario,
                        far_2000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_2000 = Variables_Globales.IpPrivada,
                        uua_2000 = Variables_Globales.IdUsuario,
                        fua_2000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_2000 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_2000;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'FORMA DE PAGO/COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2000">Identificador registro 'FORMA DE PAGO/COBRO'</param>
        /// <param name="Tipo">Tipo<br/>
        /// · 1  Pago<br/>
        /// · 2 Cobro<br/>
        /// · 3 Ambas
        /// </param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Tipo_Documento">Tipo de documento<br/>
        /// · 1 Transferencia<br/>
        /// · 2 Cheque<br/>
        /// · 3 Pagaré<br/>
        /// · 4 Otros
        /// </param>
        /// <param name="Dias">Número de días a vencimiento</param>
        /// <returns>Identificador registro</returns>
        public static void Actualizar_Alccom_2000(string CadenaConexion, string Bdd_Comunes, int Pid_2000, byte Tipo, string Denominacion, byte Tipo_Documento, short Dias)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2000_Extras Temporal = new Alccom_2000_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_2000 = Pid_2000,

                        tpc_2000 = Tipo,
                        den_2000 = Denominacion,
                        tdo_2000 = Tipo_Documento,
                        ndi_2000 = Dias,
                        uar_2000 = Variables_Globales.IdUsuario,
                        far_2000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_2000 = Variables_Globales.IpPrivada,
                        uua_2000 = Variables_Globales.IdUsuario,
                        fua_2000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'FORMA DE PAGO/COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2000">Identificador registro 'FORMA DE PAGO/COBRO'</param>
        public static void Eliminar_Alccom_2000(string CadenaConexion, string Bdd_Comunes, int Pid_2000)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2000 Temporal = new Alccom_2000(Cadena_Conexion, Bdd_Comunes) { ide_2000 = Pid_2000 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_2000 'FORMAS DE PAGO/COBRO'

        #region Tabla Alccom_2010 'CANALES DE PAGO/COBRO'

        /// <summary>
        /// Inserta registros tabla 'CANALES DE PAGO/COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio actual</param>
        /// <param name="Codigo">Código</param>
        /// <param name="Tipo">Tipo<br/>
        /// · 1  Pago<br/>
        /// · 2 Cobro<br/>
        /// · 3 Ambas
        /// </param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Clase">Clase de canal<br/>
        /// · 1 Entidades financieras<br/>
        /// · 2 Otras
        /// </param>
        /// <param name="Iban">I.B.A.N. normalizado</param>
        /// <param name="Cuenta">Cuenta contable</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_2010(string CadenaConexion, string Bdd_Comunes, short Ejercicio, short Codigo, byte Tipo, string Denominacion, byte Clase, string Iban, string Cuenta)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2010 Temporal = new Alccom_2010(Cadena_Conexion, Bdd_Comunes)
                    {
                        eje_2010 = Ejercicio,
                        cod_2010 = Codigo,
                        tca_2010 = Tipo,
                        den_2010 = Denominacion,
                        cca_2010 = Clase,
                        iba_2010 = Iban,
                        cta_2010 = Cuenta,
                        uar_2010 = Variables_Globales.IdUsuario,
                        far_2010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_2010 = Variables_Globales.IpPrivada,
                        uua_2010 = Variables_Globales.IdUsuario,
                        fua_2010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_2010 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_2010;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'CANALES DE PAGO/COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2010">Identificador registro 'CANALES DE PAGO/COBRO'</param>
        /// <param name="Tipo">Tipo<br/>
        /// · 1 Pago<br/>
        /// · 2 Cobro<br/>
        /// · 3 Pago y cobro
        /// </param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Clase">Clase de canal<br/>
        /// · 1 Entidades financieras<br/>
        /// · 2 Otras
        /// </param>
        /// <param name="Iban">I.B.A.N. normalizado</param>
        /// <param name="Cuenta">Cuenta contable</param>
        public static void Actualizar_Alccom_2010(string CadenaConexion, string Bdd_Comunes, int Pid_2010, byte Tipo, string Denominacion, byte Clase, string Iban, string Cuenta)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2010_Extras Temporal = new Alccom_2010_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_2010 = Pid_2010,

                        tca_2010 = Tipo,
                        den_2010 = Denominacion,
                        cca_2010 = Clase,
                        iba_2010 = Iban,
                        cta_2010 = Cuenta,
                        dip_2010 = Variables_Globales.IpPrivada,
                        uua_2010 = Variables_Globales.IdUsuario,
                        fua_2010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'CANALES DE PAGO/COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2010">Identificador registro 'CANALES DE PAGO/COBRO'</param>
        public static void Eliminar_Alccom_2010(string CadenaConexion, string Bdd_Comunes, int Pid_2010)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2010 Temporal = new Alccom_2010(Cadena_Conexion, Bdd_Comunes) { ide_2010 = Pid_2010 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_2010 'CANALES DE PAGO/COBRO'

        #region Tabla Alccom_2020 'TIPOS DE FACTURAS'

        /// <summary>
        /// Inserta registros tabla 'TIPOS DE FACTURAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Codigo">Código</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Fecha_Calculo">Fecha para el cáculo de la fecha tope de pago<br/>
        /// · 0 No aplicable<br/>
        /// · 1 Fecha registro<br/>
        /// · 2 Fecha aprobación
        /// </param>
        /// <param name="Dias">Días a vencimiento</param>
        /// <param name="Morosidad">Semáforo inclusión en morosidad</param>
        /// <param name="Asigna_347">Semáforo asignación '347'</param>
        /// <param name="Rectificativa">Semáforo factura rectificativa</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_2020(string CadenaConexion, string Bdd_Comunes, short Codigo, string Denominacion, byte Fecha_Calculo, short Dias, bool Morosidad, bool Asigna_347, bool Rectificativa)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2020 Temporal = new Alccom_2020(Cadena_Conexion, Bdd_Comunes)
                    {
                        tdf_2020 = Codigo,
                        den_2020 = Denominacion,
                        fca_2020 = Fecha_Calculo,
                        dia_2020 = Dias,
                        smo_2020 = Morosidad ? (byte)1 : (byte)0,
                        sgi_2020 = Asigna_347 ? (byte)1 : (byte)0,
                        sre_2020 = Rectificativa ? (byte)1 : (byte)0,
                        uar_2020 = Variables_Globales.IdUsuario,
                        far_2020 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_2020 = Variables_Globales.IpPrivada,
                        uua_2020 = Variables_Globales.IdUsuario,
                        fua_2020 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_2020 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_2020;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'TIPOS DE FACTURAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2020">Identificador registro 'TIPOS DE FACTURAS'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Fecha_Calculo">Fecha para el cáculo de la fecha tope de pago<br/>
        /// · 0 No aplicable<br/>
        /// · 1 Fecha registro<br/>
        /// · 2 Fecha aprobación
        /// </param>
        /// <param name="Dias">Días a vencimiento</param>
        /// <param name="Morosidad">Semáforo inclusión en morosidad</param>
        /// <param name="Asigna_347">Semáforo asignación '347'</param>
        /// <param name="Rectificativa">Semáforo factura rectificativa</param>
        public static void Actualizar_Alccom_2020(string CadenaConexion, string Bdd_Comunes, int Pid_2020, string Denominacion, byte Fecha_Calculo, short Dias, bool Morosidad, bool Asigna_347, bool Rectificativa)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2020_Extras Temporal = new Alccom_2020_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_2020 = Pid_2020,

                        den_2020 = Denominacion,
                        fca_2020 = Fecha_Calculo,
                        dia_2020 = Dias,
                        smo_2020 = Morosidad ? (byte)1 : (byte)0,
                        sgi_2020 = Asigna_347 ? (byte)1 : (byte)0,
                        sre_2020 = Rectificativa ? (byte)1 : (byte)0,
                        dip_2020 = Variables_Globales.IpPrivada,
                        uua_2020 = Variables_Globales.IdUsuario,
                        fua_2020 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla'TIPOS DE FACTURAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2020">Identificador registro 'TIPOS DE FACTURAS'</param>
        public static void Eliminar_Alccom_2020(string CadenaConexion, string Bdd_Comunes, int Pid_2020)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2020 Temporal = new Alccom_2020(Cadena_Conexion, Bdd_Comunes) { ide_2020 = Pid_2020 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_2020 'TIPOS DE FACTURAS'

        #region Tabla Alccom_2030 'TIPOS DE CONTRATOS'

        /// <summary>
        /// Inserta registros tabla 'TIPOS DE CONTRATOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Codigo">Código</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Contraro_Menor">Semáforo contrato menor</param>
        /// <param name="Servicios">Semáforo de prestación servicios</param>
        /// <param name="Capacidad">Semáforo petición capacidad y aptitud profesional</param>
        /// <param name="Presentacion">Semáforo presentación en facturas</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_2030(string CadenaConexion, string Bdd_Comunes, short Codigo, string Denominacion, bool Contraro_Menor, bool Servicios, bool Capacidad, bool Presentacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2030 Temporal = new Alccom_2030(Cadena_Conexion, Bdd_Comunes)
                    {
                        tco_2030 = Codigo,
                        den_2030 = Denominacion,
                        scm_2030 = Contraro_Menor ? (byte)1 : (byte)0,
                        sps_2030 = Servicios ? (byte)1 : (byte)0,
                        spc_2030 = Capacidad ? (byte)1 : (byte)0,
                        spf_2030 = Presentacion ? (byte)1 : (byte)0,
                        uar_2030 = Variables_Globales.IdUsuario,
                        far_2030 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_2030 = Variables_Globales.IpPrivada,
                        uua_2030 = Variables_Globales.IdUsuario,
                        fua_2030 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_2030 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_2030;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'TIPOS DE CONTRATOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2030">Identificador registro 'TIPOS DE CONTRATOS'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Contraro_Menor">Semáforo contrato menor</param>
        /// <param name="Servicios">Semáforo de prestación servicios</param>
        /// <param name="Capacidad">Semáforo petición capacidad y aptitud profesional</param>
        /// <param name="Presentacion">Semáforo presentación en facturas</param>
        public static void Actualizar_Alccom_2030(string CadenaConexion, string Bdd_Comunes, int Pid_2030, string Denominacion, bool Contraro_Menor, bool Servicios, bool Capacidad, bool Presentacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2030_Extras Temporal = new Alccom_2030_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_2030 = Pid_2030,

                        den_2030 = Denominacion,
                        scm_2030 = Contraro_Menor ? (byte)1 : (byte)0,
                        sps_2030 = Servicios ? (byte)1 : (byte)0,
                        spc_2030 = Capacidad ? (byte)1 : (byte)0,
                        spf_2030 = Presentacion ? (byte)1 : (byte)0,
                        dip_2030 = Variables_Globales.IpPrivada,
                        uua_2030 = Variables_Globales.IdUsuario,
                        fua_2030 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla'TIPOS DE CONTRATOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2030">Identificador registro 'TIPOS DE CONTRATOS'</param>
        public static void Eliminar_Alccom_2030(string CadenaConexion, string Bdd_Comunes, int Pid_2030)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2030 Temporal = new Alccom_2030(Cadena_Conexion, Bdd_Comunes) { ide_2030 = Pid_2030 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_2030 'TIPOS DE CONTRATOS'

        #region Tabla Alccom_2040 'TIPOS DE SESIONES'

        /// <summary>
        /// Inserta registros tabla 'TIPOS DE SESIONES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Codigo">Código</param>
        /// <param name="Denominacion">Denominación</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_2040(string CadenaConexion, string Bdd_Comunes, short Codigo, string Denominacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2040 Temporal = new Alccom_2040(Cadena_Conexion, Bdd_Comunes)
                    {
                        cod_2040 = Codigo,
                        den_2040 = Denominacion,
                        uar_2040 = Variables_Globales.IdUsuario,
                        far_2040 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_2040 = Variables_Globales.IpPrivada,
                        uua_2040 = Variables_Globales.IdUsuario,
                        fua_2040 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_2040 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_2040;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'TIPOS DE SESIONES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2040">Identificador registro 'TIPOS DE SESIONES'</param>
        /// <param name="Denominacion">Denominación</param>
        public static void Actualizar_Alccom_2040(string CadenaConexion, string Bdd_Comunes, int Pid_2040, string Denominacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2040_Extras Temporal = new Alccom_2040_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_2040 = Pid_2040,

                        den_2040 = Denominacion,
                        dip_2040 = Variables_Globales.IpPrivada,
                        uua_2040 = Variables_Globales.IdUsuario,
                        fua_2040 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'TIPOS DE SESIONES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2040">Identificador registro 'TIPOS DE SESIONES'</param>
        public static void Eliminar_Alccom_2040(string CadenaConexion, string Bdd_Comunes, int Pid_2040)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2040 Temporal = new Alccom_2040(Cadena_Conexion, Bdd_Comunes) { ide_2040 = Pid_2040 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_2040 'TIPOS DE SESIONES'

        #region Tabla Alccom_2050 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'

        /// <summary>
        /// Inserta registros tabla 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Codigo">Código</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Inicio">Fecha inicial de validez</param>
        /// <param name="Final">Fecha final de validez</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_2050(string CadenaConexion, string Bdd_Comunes, short Codigo, string Denominacion, DateTime? Inicio, DateTime? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2050 Temporal = new Alccom_2050(Cadena_Conexion, Bdd_Comunes)
                    {
                        cod_2050 = Codigo,
                        den_2050 = Denominacion,
                        fvi_2050 = Inicio?.ToString(),
                        fvf_2050 = Final?.ToString(),
                        uar_2050 = Variables_Globales.IdUsuario,
                        far_2050 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_2050 = Variables_Globales.IpPrivada,
                        uua_2050 = Variables_Globales.IdUsuario,
                        fua_2050 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_2050 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_2050;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2050">Identificador registro 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Inicio">Fecha inicial de validez</param>
        /// <param name="Final">Fecha final de validez</param>
        public static void Actualizar_Alccom_2050(string CadenaConexion, string Bdd_Comunes, int Pid_2050, string Denominacion, DateTime? Inicio, DateTime? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2050_Extras Temporal = new Alccom_2050_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_2050 = Pid_2050,

                        den_2050 = Denominacion,
                        fvi_2050 = Inicio?.ToString(),
                        fvf_2050 = Final?.ToString(),
                        dip_2050 = Variables_Globales.IpPrivada,
                        uua_2050 = Variables_Globales.IdUsuario,
                        fua_2050 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2050">Identificador registro 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'</param>
        public static void Eliminar_Alccom_2050(string CadenaConexion, string Bdd_Comunes, int Pid_2050)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2050 Temporal = new Alccom_2050(Cadena_Conexion, Bdd_Comunes) { ide_2050 = Pid_2050 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_2050 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'

        #region Tabla Alccom_2050_1 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO. DETALLE'

        /// <summary>
        /// Inserta registros tabla 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO. DETALLE'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2050">Identificador registro 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'</param>
        /// <param name="Codigo">Código</param>
        /// <param name="Descripcion">Descripción</param>
        /// <param name="Ultimo_Nivel">Último nivel</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_2050_1(string CadenaConexion, string Bdd_Comunes, int Pid_2050, string Codigo, string Descripcion, bool Ultimo_Nivel)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2050_1 Temporal = new Alccom_2050_1(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_2050 = Pid_2050,
                        tdr_2050_1 = Codigo,
                        den_2050_1 = Descripcion,
                        sun_2050_1 = Ultimo_Nivel ? (byte)1 : (byte)0,
                        uar_2050_1 = Variables_Globales.IdUsuario,
                        far_2050_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_2050_1 = Variables_Globales.IpPrivada,
                        uua_2050_1 = Variables_Globales.IdUsuario,
                        fua_2050_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_2050_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_2050_1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO. DETALLE'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2050_1">Identificador registro 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO.DETALLE'</param>
        /// <param name="Descripcion">Descripción</param>
        /// <param name="Ultimo_Nivel">Último nivel</param>
        public static void Actualizar_Alccom_2050_1(string CadenaConexion, string Bdd_Comunes, int Pid_2050_1, string Descripcion, bool Ultimo_Nivel)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2050_1_Extras Temporal = new Alccom_2050_1_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_2050_1 = Pid_2050_1,

                        den_2050_1 = Descripcion,
                        sun_2050_1 = Ultimo_Nivel ? (byte)1 : (byte)0,
                        dip_2050_1 = Variables_Globales.IpPrivada,
                        uua_2050_1 = Variables_Globales.IdUsuario,
                        fua_2050_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_2050_1">Identificador registro 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO. DETALLE'</param>
        public static void Eliminar_Alccom_2050_1(string CadenaConexion, string Bdd_Comunes, int Pid_2050_1)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_2050_1 Temporal = new Alccom_2050_1(Cadena_Conexion, Bdd_Comunes) { ide_2050_1 = Pid_2050_1 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_2050_1 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO. DETALLE'

        #region Tabla Alccom_5000 'NIVELES ORGÁNICOS'

        /// <summary>
        /// Inserta registros tabla 'NIVELES ORGÁNICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Nivel">Código nivel</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Longitud">Longitud en dígitos de la clave</param>
        /// <param name="Inicio">Periodo validez. Inicio</param>
        /// <param name="Final">Periodo validez. Final</param>
        /// <returns>Identificador registro</returns>
        public static short Insertar_Alccom_5000(string CadenaConexion, string Bdd_Comunes, byte Nivel, string Denominacion, byte Longitud, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5000 Temporal = new Alccom_5000(Cadena_Conexion, Bdd_Comunes)
                    {
                        niv_5000 = Nivel,
                        des_5000 = Denominacion,
                        lnc_5000 = Longitud,
                        aiv_5000 = Inicio,
                        afv_5000 = Final,
                        uar_5000 = Variables_Globales.IdUsuario,
                        far_5000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_5000 = Variables_Globales.IpPrivada,
                        uua_5000 = Variables_Globales.IdUsuario,
                        fua_5000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_5000 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_5000;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Inserta registros tabla 'NIVELES ORGÁNICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_5000">Identificador registro 'NIVELES ORGÁNICOS'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Longitud">Longitud en dígitos de la clave</param>
        /// <param name="Inicio">Periodo validez. Inicio</param>
        /// <param name="Final">Periodo validez. Final</param>
        public static void Actualizar_Alccom_5000(string CadenaConexion, string Bdd_Comunes, short Pid_5000, string Denominacion, byte Longitud, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5000_Extras Temporal = new Alccom_5000_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_5000 = Pid_5000,

                        des_5000 = Denominacion,
                        lnc_5000 = Longitud,
                        aiv_5000 = Inicio,
                        afv_5000 = Final,
                        dip_5000 = Variables_Globales.IpPrivada,
                        uua_5000 = Variables_Globales.IdUsuario,
                        fua_5000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registros tabla 'NIVELES ORGÁNICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_5000">Identificador registro 'NIVELES ORGÁNICOS'</param>
        public static void Eliminar_Alccom_5000(string CadenaConexion, string Bdd_Comunes, short Pid_5000)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5000 Temporal = new Alccom_5000(Cadena_Conexion, Bdd_Comunes) { ide_5000 = Pid_5000 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_5000 'NIVELES ORGÁNICOS'

        #region Tabla Alccom_5001 'NIVELES FUNCIONALES'

        /// <summary>
        /// Inserta registros tabla 'NIVELES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Nivel">Código nivel</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Longitud">Longitud en dígitos de la clave</param>
        /// <param name="Inicio">Periodo validez. Inicio</param>
        /// <param name="Final">Periodo validez. Final</param>
        /// <returns>Identificador registro</returns>
        public static short Insertar_Alccom_5001(string CadenaConexion, string Bdd_Comunes, byte Nivel, string Denominacion, byte Longitud, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5001 Temporal = new Alccom_5001(Cadena_Conexion, Bdd_Comunes)
                    {
                        niv_5001 = Nivel,
                        des_5001 = Denominacion,
                        lnc_5001 = Longitud,
                        aiv_5001 = Inicio,
                        afv_5001 = Final,
                        uar_5001 = Variables_Globales.IdUsuario,
                        far_5001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_5001 = Variables_Globales.IpPrivada,
                        uua_5001 = Variables_Globales.IdUsuario,
                        fua_5001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_5001 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_5001;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Inserta registros tabla 'NIVELES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_5001">Identificador registro 'NIVELES FUNCIONALES'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Longitud">Longitud en dígitos de la clave</param>
        /// <param name="Inicio">Periodo validez. Inicio</param>
        /// <param name="Final">Periodo validez. Final</param>
        public static void Actualizar_Alccom_5001(string CadenaConexion, string Bdd_Comunes, short Pid_5001, string Denominacion, byte Longitud, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5001_Extras Temporal = new Alccom_5001_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_5001 = Pid_5001,

                        des_5001 = Denominacion,
                        lnc_5001 = Longitud,
                        aiv_5001 = Inicio,
                        afv_5001 = Final,
                        dip_5001 = Variables_Globales.IpPrivada,
                        uua_5001 = Variables_Globales.IdUsuario,
                        fua_5001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registros tabla 'NIVELES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_5001">Identificador registro 'NIVELES FUNCIONALES'</param>
        public static void Eliminar_Alccom_5001(string CadenaConexion, string Bdd_Comunes, short Pid_5001)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5001 Temporal = new Alccom_5001(Cadena_Conexion, Bdd_Comunes) { ide_5001 = Pid_5001 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_5001 'NIVELES FUNCIONALES'

        #region Tabla Alccom_5002 'NIVELES ECONÓMICOS'

        /// <summary>
        /// Inserta registros tabla 'NIVELES ECONÓMICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Nivel">Código nivel</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Longitud">Longitud en dígitos de la clave</param>
        /// <param name="Inicio">Periodo validez. Inicio</param>
        /// <param name="Final">Periodo validez. Final</param>
        /// <returns>Identificador registro</returns>
        public static short Insertar_Alccom_5002(string CadenaConexion, string Bdd_Comunes, byte Nivel, string Denominacion, byte Longitud, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5002 Temporal = new Alccom_5002(Cadena_Conexion, Bdd_Comunes)
                    {
                        niv_5002 = Nivel,
                        des_5002 = Denominacion,
                        lnc_5002 = Longitud,
                        aiv_5002 = Inicio,
                        afv_5002 = Final,
                        uar_5002 = Variables_Globales.IdUsuario,
                        far_5002 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_5002 = Variables_Globales.IpPrivada,
                        uua_5002 = Variables_Globales.IdUsuario,
                        fua_5002 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_5002 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_5002;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Inserta registros tabla 'NIVELES ECONÓMICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_5002">Identificador registro 'NIVELES ECONÓMICOS'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Longitud">Longitud en dígitos de la clave</param>
        /// <param name="Inicio">Periodo validez. Inicio</param>
        /// <param name="Final">Periodo validez. Final</param>
        public static void Actualizar_Alccom_5002(string CadenaConexion, string Bdd_Comunes, short Pid_5002, string Denominacion, byte Longitud, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5002_Extras Temporal = new Alccom_5002_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_5002 = Pid_5002,

                        des_5002 = Denominacion,
                        lnc_5002 = Longitud,
                        aiv_5002 = Inicio,
                        afv_5002 = Final,
                        dip_5002 = Variables_Globales.IpPrivada,
                        uua_5002 = Variables_Globales.IdUsuario,
                        fua_5002 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registros tabla 'NIVELES ECONÓMICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_5002">Identificador registro 'NIVELES ECONÓMICOS'</param>
        public static void Eliminar_Alccom_5002(string CadenaConexion, string Bdd_Comunes, short Pid_5002)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5002 Temporal = new Alccom_5002(Cadena_Conexion, Bdd_Comunes) { ide_5002 = Pid_5002 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_5002 'NIVELES FUNCIONALES'

        #region Tabla Alccom_5003 'NIVELES DE VINCULACIÓN'

        /// <summary>
        /// Inserta registros tabla 'NIVELES DE VINCULACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_5000">Identificador registro 'NIVELES ORGÁNICOS'</param>
        /// <param name="Longitud_Organica">Longitud nivel orgánico</param>
        /// <param name="Pid_5001">Identificador registro 'NIVELES FUNCIONALES'</param>
        /// <param name="Longitud_Funcional">Longitud nivel funcional</param>
        /// <param name="Pid_5002">Identificador registro 'NIVELES ECONÓMICOS'</param>
        /// <param name="Longitud_Economica">Longitud nivel económico</param>
        /// <param name="Inicio">Periodo validez. Inicio</param>
        /// <param name="Final">Periodo validez. Final</param>
        /// <returns>Identificador registro</returns>
        public static short Insertar_Alccom_5003(string CadenaConexion, string Bdd_Comunes, short Pid_5000, byte Longitud_Organica, short Pid_5001, byte Longitud_Funcional, short Pid_5002, byte Longitud_Economica, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5003 Temporal = new Alccom_5003(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_5000 = Pid_5000,
                        ldo_5003 = Longitud_Organica,
                        ide_5001 = Pid_5001,
                        ldf_5003 = Longitud_Funcional,
                        ide_5002 = Pid_5002,
                        lde_5003 = Longitud_Economica,
                        aiv_5003 = Inicio,
                        afv_5003 = Final,
                        uar_5003 = Variables_Globales.IdUsuario,
                        far_5003 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_5003 = Variables_Globales.IpPrivada,
                        uua_5003 = Variables_Globales.IdUsuario,
                        fua_5003 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_5003 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_5003;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Inserta registros tabla 'NIVELES DE VINCULACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_5003">Identificador registro 'NIVELES DE VINCULACIÓN'</param>
        /// <param name="Pid_5000">Identificador registro 'NIVELES ORGÁNICOS'</param>
        /// <param name="Longitud_Organica">Longitud nivel orgánico</param>
        /// <param name="Pid_5001">Identificador registro 'NIVELES FUNCIONALES'</param>
        /// <param name="Longitud_Funcional">Longitud nivel funcional</param>
        /// <param name="Pid_5002">Identificador registro 'NIVELES ECONÓMICOS'</param>
        /// <param name="Longitud_Economica">Longitud nivel económico</param>
        /// <param name="Inicio">Periodo validez. Inicio</param>
        /// <param name="Final">Periodo validez. Final</param>
        public static void Actualizar_Alccom_5003(string CadenaConexion, string Bdd_Comunes, short Pid_5003, short Pid_5000, byte Longitud_Organica, short Pid_5001, byte Longitud_Funcional, short Pid_5002, byte Longitud_Economica, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5003 Temporal = new Alccom_5003(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_5003 = Pid_5003,

                        ide_5000 = Pid_5000,
                        ldo_5003 = Longitud_Organica,
                        ide_5001 = Pid_5001,
                        ldf_5003 = Longitud_Funcional,
                        ide_5002 = Pid_5002,
                        lde_5003 = Longitud_Economica,
                        aiv_5003 = Inicio,
                        afv_5003 = Final,
                        dip_5003 = Variables_Globales.IpPrivada,
                        uua_5003 = Variables_Globales.IdUsuario,
                        fua_5003 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registros tabla 'NIVELES ECONÓMICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_5003">Identificador registro 'NIVELES DE VINCULACIÓN'</param>
        public static void Eliminar_Alccom_5003(string CadenaConexion, string Bdd_Comunes, short Pid_5003)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_5003 Temporal = new Alccom_5003(Cadena_Conexion, Bdd_Comunes) { ide_5003 = Pid_5003 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_5003 'NIVELES DE VINCULACIÓN'

        #region Tabla Alccom_9000 'FIRMATES'

        /// <summary>
        /// Inserta registros tabla 'FIRMATES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Codigo">Código</param>
        /// <param name="Apellido_1">Primer apellido</param>
        /// <param name="Apellido_2">Segundo apellido</param>
        /// <param name="Nombre">Nombre</param>
        /// <param name="Cargo">Cargo</param>
        /// <param name="Inicio">Año comienzo validez</param>
        /// <param name="Final">Año finalización validez</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_9000(string CadenaConexion, string Bdd_Comunes, short Codigo, string Apellido_1, string Apellido_2, string Nombre, string Cargo, DateTime? Inicio, DateTime? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9000 Temporal = new Alccom_9000(Cadena_Conexion, Bdd_Comunes)
                    {
                        cod_9000 = Codigo,
                        ap1_9000 = Apellido_1,
                        ap2_9000 = Apellido_2,
                        nom_9000 = Nombre,
                        car_9000 = Cargo,
                        fiv_9000 = Inicio?.ToString(),
                        ffv_9000 = Final?.ToString(),
                        uar_9000 = Variables_Globales.IdUsuario,
                        far_9000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_9000 = Variables_Globales.IpPrivada,
                        uua_9000 = Variables_Globales.IdUsuario,
                        fua_9000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_9000 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_9000;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'FIRMATES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9000">Identificador registro 'FIRMATES'</param>
        /// <param name="Apellido_1">Primer apellido</param>
        /// <param name="Apellido_2">Segundo apellido</param>
        /// <param name="Nombre">Nombre</param>
        /// <param name="Cargo">Cargo</param>
        /// <param name="Inicio">Año comienzo validez</param>
        /// <param name="Final">Año finalización validez</param>
        public static void Actualizar_Alccom_9000(string CadenaConexion, string Bdd_Comunes, int Pid_9000, string Apellido_1, string Apellido_2, string Nombre, string Cargo, DateTime? Inicio, DateTime? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9000_Extras Temporal = new Alccom_9000_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_9000 = Pid_9000,

                        ap1_9000 = Apellido_1,
                        ap2_9000 = Apellido_2,
                        nom_9000 = Nombre,
                        car_9000 = Cargo,
                        fiv_9000 = Inicio?.ToString(),
                        ffv_9000 = Final?.ToString(),
                        dip_9000 = Variables_Globales.IpPrivada,
                        uua_9000 = Variables_Globales.IdUsuario,
                        fua_9000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'FIRMATES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9000">Identificador registro 'FIRMATES'</param>
        public static void Eliminar_Alccom_9000(string CadenaConexion, string Bdd_Comunes, int Pid_9000)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9000 Temporal = new Alccom_9000(Cadena_Conexion, Bdd_Comunes) { ide_9000 = Pid_9000 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_9000 'FIRMATES'

        #region Tabla Alccom_9010 'TIPOS DE DOCUMENTOS'

        /// <summary>
        /// Inserta registros tabla 'TIPOS DE DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Codigo">Código TIDO DE DOCUMENTO</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Serie">Serie documental</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_9010(string CadenaConexion, string Bdd_Comunes, string Codigo, string Denominacion, string Serie)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010 Temporal = new Alccom_9010(Cadena_Conexion, Bdd_Comunes)
                    {
                        tdo_9010 = Codigo,
                        des_9010 = Denominacion,
                        sdo_9010 = Serie,
                        smp_9010 = 0,
                        tda_9010 = "",
                        uar_9010 = Variables_Globales.IdUsuario,
                        far_9010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_9010 = Variables_Globales.IpPrivada,
                        uua_9010 = Variables_Globales.IdUsuario,
                        fua_9010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_9010 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_9010;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'TIPOS DE DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010">Identificador registro 'TIPOS DE DOCUMENTOS'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Serie">Serie documental</param>
        public static void Actualizar_Alccom_9010(string CadenaConexion, string Bdd_Comunes, int Pid_9010, string Denominacion, string Serie)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_Extras Temporal = new Alccom_9010_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_9010 = Pid_9010,

                        des_9010 = Denominacion,
                        sdo_9010 = Serie,
                        dip_9010 = Variables_Globales.IpPrivada,
                        uua_9010 = Variables_Globales.IdUsuario,
                        fua_9010 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'TIPOS DE DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010">Identificador registro 'TIPOS DE DOCUMENTOS'</param>
        public static void Eliminar_Alccom_9010(string CadenaConexion, string Bdd_Comunes, int Pid_9010)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010 Temporal = new Alccom_9010(Cadena_Conexion, Bdd_Comunes) { ide_9010 = Pid_9010 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_9010 'TIPOS DE DOCUMENTOS'

        #region Tabla Alccom_9010_1 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'

        /// <summary>
        /// Inserta registros tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010">Identificador registro 'TIPOS DE DOCUMENTOS'</param>
        /// <param name="Codigo">Código TIDO DE MOVIMIENTO</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Inicio">Año inicio validez</param>
        /// <param name="Final">Año final validez</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_9010_1(string CadenaConexion, string Bdd_Comunes, int Pid_9010, string Codigo, string Denominacion, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1 Temporal = new Alccom_9010_1(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_9010 = Pid_9010,
                        tmo_9010_1 = Codigo,
                        des_9010_1 = Denominacion,
                        aiv_9010_1 = Inicio == null ? null : Inicio,
                        afv_9010_1 = Final == null ? null : Final,
                        spt_9010_1 = Codigo.Contains("+") || Codigo.Contains("-") ? (byte)1 : (byte)0,
                        uar_9010_1 = Variables_Globales.IdUsuario,
                        far_9010_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_9010_1 = Variables_Globales.IpPrivada,
                        uua_9010_1 = Variables_Globales.IdUsuario,
                        fua_9010_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_9010_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_9010_1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'</param>
        /// <param name="Denominacion">Denominación</param>
        /// <param name="Inicio">Año inicio validez</param>
        /// <param name="Final">Año final validez</param>
        public static void Actualizar_Alccom_9010_1(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1, string Denominacion, short? Inicio, short? Final)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1_Extras Temporal = new Alccom_9010_1_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_9010_1 = Pid_9010_1,

                        des_9010_1 = Denominacion,
                        aiv_9010_1 = Inicio == null ? null : Inicio,
                        afv_9010_1 = Final == null ? null : Final,
                        dip_9010_1 = Variables_Globales.IpPrivada,
                        uua_9010_1 = Variables_Globales.IdUsuario,
                        fua_9010_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'</param>
        public static void Eliminar_Alccom_9010_1(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1 Temporal = new Alccom_9010_1(Cadena_Conexion, Bdd_Comunes) { ide_9010_1 = Pid_9010_1 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_9010_1 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'

        #region Tabla Alccom_9010_1_1 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS'

        /// <summary>
        /// Inserta registros tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'</param>
        /// <param name="Cuenta">Código cuenta</param>
        /// <param name="Debe_Haber">Debe/Haber</param>
        /// <param name="Tipo_Cuenta">Tipo de cuenta<br/>
        /// 0 - Sin tipología<br/>
        /// 1 - Tercero<br/>
        /// 2 - Endosatario<br/>
        /// 3 - Impuesto(Tercero)<br/>
        /// 4 -  Retención<br/>
        /// 5 - Descuento<br/>
        /// 6 - Cuenta de tesorería<br/>
        /// 7 - Cuenta imputación gastos/ingresos<br/>
        /// 8 - Impuesto<br/>
        /// 9 - Cargos
        /// </param>
        /// <param name="Signo">Signo</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_9010_1_1(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1, string Cuenta, string Debe_Haber, byte Tipo_Cuenta, short Signo)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1_1 Temporal = new Alccom_9010_1_1(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_9010_1 = Pid_9010_1,

                        cta_9010_1_1 = Cuenta,
                        doh_9010_1_1 = Debe_Haber,
                        sig_9010_1_1 = Signo,
                        tcu_9010_1_1 = Tipo_Cuenta,
                        uar_9010_1_1 = Variables_Globales.IdUsuario,
                        far_9010_1_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_9010_1_1 = Variables_Globales.IpPrivada,
                        uua_9010_1_1 = Variables_Globales.IdUsuario,
                        fua_9010_1_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_9010_1_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_9010_1_1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS'</param>
        /// <param name="Cuenta">Código cuenta</param>
        /// <param name="Debe_Haber">Debe/Haber</param>
        /// <param name="Tipo_Cuenta">Tipo de cuenta<br/>
        /// 0 - Sin tipología<br/>
        /// 1 - Tercero<br/>
        /// 2 - Endosatario<br/>
        /// 3 - Impuesto(Tercero)<br/>
        /// 4 -  Retención<br/>
        /// 5 - Descuento<br/>
        /// 6 - Cuenta de tesorería<br/>
        /// 7 - Cuenta imputación gastos/ingresos<br/>
        /// 8 - Impuesto
        /// </param>
        /// <param name="Signo">Signo</param>
        public static void Actualizar_Alccom_9010_1_1(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1_1, string Cuenta, string Debe_Haber, byte Tipo_Cuenta, short Signo)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1_1_Extras Temporal = new Alccom_9010_1_1_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_9010_1_1 = Pid_9010_1_1,

                        cta_9010_1_1 = Cuenta,
                        doh_9010_1_1 = Debe_Haber,
                        sig_9010_1_1 = Signo,
                        tcu_9010_1_1 = Tipo_Cuenta,
                        dip_9010_1_1 = Variables_Globales.IpPrivada,
                        uua_9010_1_1 = Variables_Globales.IdUsuario,
                        fua_9010_1_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS'</param>
        public static void Eliminar_Alccom_9010_1_1(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1_1)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1_1 Temporal = new Alccom_9010_1_1(Cadena_Conexion, Bdd_Comunes) { ide_9010_1_1 = Pid_9010_1_1 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_9010_1_1 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS'

        #region Tabla Alccom_9010_1_2 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'

        /// <summary>
        /// Inserta registros tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'</param>
        /// <param name="Pid_9000">Identificador registro 'FIRMANTES'</param>
        /// <param name="Firmante">Código firmante</param>
        /// <param name="Ubicacion">Ubicación firma<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1, int Pid_9000, short Firmante, byte Ubicacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1_2 Temporal = new Alccom_9010_1_2(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_9010_1 = Pid_9010_1,
                        ide_9000 = Pid_9000,
                        cod_9010_1_2 = Firmante,
                        ord_9010_1_2 = Ubicacion,
                        uar_9010_1_2 = Variables_Globales.IdUsuario,
                        far_9010_1_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_9010_1_2 = Variables_Globales.IpPrivada,
                        uua_9010_1_2 = Variables_Globales.IdUsuario,
                        fua_9010_1_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_9010_1_2 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_9010_1_2;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registro tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1_2">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FRIMAS'</param>
        /// <param name="Ubicacion">Ubicación firma<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// <returns>Identificador registro</returns>
        public static void Actualizar_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1_2, byte Ubicacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1_2_Extras Temporal = new Alccom_9010_1_2_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_9010_1_2 = Pid_9010_1_2,

                        ord_9010_1_2 = Ubicacion,
                        dip_9010_1_2 = Variables_Globales.IpPrivada,
                        uua_9010_1_2 = Variables_Globales.IdUsuario,
                        fua_9010_1_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Ubicacion();
                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registro tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1_2">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FRIMAS'</param>
        /// <param name="Pid_9000">Identificador registro 'FIRMANTES'</param>
        /// <param name="Firmante">Código firmante</param>
        /// <param name="Ubicacion">Ubicación firma<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// <returns>Identificador registro</returns>
        public static void Actualizar_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1_2, int Pid_9000, short Firmante, byte Ubicacion)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1_2_Extras Temporal = new Alccom_9010_1_2_Extras(Cadena_Conexion, Bdd_Comunes)
                    {
                        ide_9010_1_2 = Pid_9010_1_2,

                        ide_9000 = Pid_9000,
                        cod_9010_1_2 = Firmante,
                        ord_9010_1_2 = Ubicacion,
                        dip_9010_1_2 = Variables_Globales.IpPrivada,
                        uua_9010_1_2 = Variables_Globales.IdUsuario,
                        fua_9010_1_2 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Firmas();
                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registro en la tabla 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1_2">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'</param>
        public static void Eliminar_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1_2)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alccom_9010_1_2 Temporal = new Alccom_9010_1_2(Cadena_Conexion, Bdd_Comunes) { ide_9010_1_2 = Pid_9010_1_2 };

                    Temporal.Eliminar();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alccom_9010_1_2 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'

    }

}