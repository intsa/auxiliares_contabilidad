﻿using ClaseIntsa.Controles;
using ClaseIntsa.DataBase;
using Comunes_Contabilidad.Controles;
using Historico_Contabilidad;
using Microsoft.Win32;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Comunes_Contabilidad
{
    public static class Auxiliares_Comunes
    {
        /// <summary>
        /// Obtiene la tabla 'HISTÓRICO. CLAVES ORGÁNICAS. NIVELES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio actual</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Longitud_Actual">Longitud de la clave</param>
        /// <returns>DataTable de niveles</returns>
        public static DataTable Niveles_Alccom_5000(string CadenaConexion, short Ejercicio, string Bdd_Comunes, byte Longitud_Actual = 0)
        {
            DataTable Niveles = new DataTable();

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5000 idecla, niv_5000 nivcla, lnc_5000 loncla, des_5000 descla";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5000";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5000,  0) AND ISNULL (afv_5000, 9999)";
            string Restriccion_Longitud_No_Cero = "lnc_5000 != 0";
            string Restriccion_Longitud = $"lnc_5000 < {Longitud_Actual}";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Longitud_No_Cero}";
            string Cadena_Order = " ORDER BY lnc_5000 DESC";

            if (Longitud_Actual != 0)
                Cadena_Where += $" AND {Restriccion_Longitud}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where + Cadena_Order;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.HasRows)
                        Niveles.Load(Data_Reader);
                    else
                        Niveles = null;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Niveles;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Obtiene la tabla 'CLAVES FUNCIONALES. NIVELES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio actual</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Longitud_Actual">Longitud de la clave</param>
        /// <returns>DataTable de niveles</returns>
        public static DataTable Niveles_Alccom_5001(string CadenaConexion, short Ejercicio, string Bdd_Comunes, byte Longitud_Actual = 0)
        {
            DataTable Niveles = new DataTable();
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5001 idecla, niv_5001 nivcla, lnc_5001 loncla, des_5001 descla";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5001";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5001, 0) AND ISNULL (afv_5001, 9999)";
            string Restriccion_Longitud_No_Cero = "lnc_5001 != 0";
            string Restriccion_Longitud = $"lnc_5001 < {Longitud_Actual}";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Longitud_No_Cero}";
            string Cadena_Order = " ORDER BY lnc_5001 DESC";

            if (Longitud_Actual != 0)
                Cadena_Where += $" AND {Restriccion_Longitud}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where + Cadena_Order;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.HasRows)
                        Niveles.Load(Data_Reader);
                    else
                        Niveles = null;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Niveles;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Obtiene la tabla 'CLAVES ECONÓMICAS. NIVELES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio actual</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Longitud_Actual">Longitud de la clave</param>
        /// <returns>DataTable de niveles</returns>
        public static DataTable Niveles_Alccom_5002(string CadenaConexion, short Ejercicio, string Bdd_Comunes, byte Longitud_Actual = 0)
        {
            DataTable Niveles = new DataTable();
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5002 idecla, niv_5002 nivcla, lnc_5002 loncla, des_5002 descla";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5002";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5002, 0) AND ISNULL (afv_5002, 9999)";
            string Restriccion_Longitud_No_Cero = "lnc_5002 != 0";
            string Restriccion_Longitud = $"lnc_5002 < {Longitud_Actual}";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Longitud_No_Cero}";
            string Cadena_Order = " ORDER BY lnc_5002 DESC";

            if (Longitud_Actual != 0)
                Cadena_Where += $" AND {Restriccion_Longitud}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where + Cadena_Order;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.HasRows)
                        Niveles.Load(Data_Reader);
                    else
                        Niveles = null;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Niveles;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene y verifica la existencia de los niveles anteriores de una clave orgánica
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Longitud_Nivel_Actual">Longitud del nivel de la clave orgánica actual</param>
        /// <param name="Clave">Clave orgánica actual</param>
        /// <returns>Cadena de denominaciones de niveles que no están creados</returns>
        public static string Existe_Nivel_Organico(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Bdd_Comunes, byte Longitud_Nivel_Actual, string Clave)
        {
            string Clave_Actual;
            string Niveles = "";
            byte Longitud;

            DataTable Organicas = Niveles_Alccom_5000(CadenaConexion, Ejercicio, Bdd_Comunes, Longitud_Nivel_Actual);

            if (Organicas != null)
            {
                foreach (DataRow Fila in Organicas.Rows)
                {
                    Longitud = (byte)Fila["loncla"];
                    Clave_Actual = Clave.Left(Longitud);

                    if (Auxiliares_Historico.Identificador_Alchis_1000(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Actual) == 0)
                    {
                        if (Niveles.Trim() == string.Empty)
                            Niveles = Fila["descla"].ToString().Trim();
                        else
                            Niveles = $"{Niveles.Trim()} - {Fila["descla"].ToString().Trim()}";
                    }
                }
            }

            return Niveles.Trim();
        }

        /// <summary>
        /// Obtiene y verifica la existencia de los niveles anteriores de una clave funcional
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Longitud_Nivel_Actual">Longitud del nivel de la clave orgánica actual</param>
        /// <param name="Clave">Clave orgánica actual</param>
        /// <returns>Cadena de denominaciones de niveles que no están creados</returns>
        public static string Existe_Nivel_Funcional(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Bdd_Comunes, byte Longitud_Nivel_Actual, string Clave)
        {
            string Clave_Actual;
            string Niveles = "";
            byte Longitud;

            DataTable Funcionales = Niveles_Alccom_5001(CadenaConexion, Ejercicio, Bdd_Comunes, Longitud_Nivel_Actual);

            if (Funcionales != null)
            {
                foreach (DataRow Fila in Funcionales.Rows)
                {
                    Longitud = (byte)Fila["loncla"];
                    Clave_Actual = Clave.Left(Longitud);

                    if (Auxiliares_Historico.Identificador_Alchis_1001(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Actual) == 0)
                    {
                        if (Niveles.Trim() == string.Empty)
                            Niveles = Fila["descla"].ToString().Trim();
                        else
                            Niveles = $"{Niveles.Trim()} - {Fila["descla"].ToString().Trim()}";
                    }
                }
            }

            return Niveles.Trim();
        }

        /// <summary>
        /// Obtiene y verifica la existencia de los niveles anteriores de una clave económicas
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Longitud_Nivel_Actual">Longitud del nivel de la clave orgánica actual</param>
        /// <param name="Clave">Clave orgánica actual</param>
        /// <returns>Cadena de denominaciones de niveles que no están creados</returns>
        public static string Existe_Nivel_Economico_Gastos(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Bdd_Comunes, byte Longitud_Nivel_Actual, string Clave)
        {
            string Clave_Actual;
            string Niveles = "";
            byte Longitud;

            DataTable Economicos = Niveles_Alccom_5002(CadenaConexion, Ejercicio, Bdd_Comunes, Longitud_Nivel_Actual);

            if (Economicos != null)
            {
                foreach (DataRow Fila in Economicos.Rows)
                {
                    Longitud = (byte)Fila["loncla"];
                    Clave_Actual = Clave.Left(Longitud);

                    if (Auxiliares_Historico.Identificador_Alchis_1002(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Actual) == 0)
                    {
                        if (Niveles.Trim() == string.Empty)
                            Niveles = Fila["descla"].ToString().Trim();
                        else
                            Niveles = $"{Niveles.Trim()} - {Fila["descla"].ToString().Trim()}";
                    }
                }
            }

            return Niveles.Trim();
        }

        /// <summary>
        /// Obtiene y verifica la existencia de los niveles anteriores de una clave económicas
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Longitud_Nivel_Actual">Longitud del nivel de la clave orgánica actual</param>
        /// <param name="Clave">Clave orgánica actual</param>
        /// <returns>Cadena de denominaciones de niveles que no están creados</returns>
        public static string Existe_Nivel_Economico_Ingresos(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Bdd_Comunes, byte Longitud_Nivel_Actual, string Clave)
        {
            string Clave_Actual;
            string Niveles = "";
            byte Longitud;

            DataTable Economicos = Niveles_Alccom_5002(CadenaConexion, Ejercicio, Bdd_Comunes, Longitud_Nivel_Actual);

            if (Economicos != null)
            {
                foreach (DataRow Fila in Economicos.Rows)
                {
                    Longitud = (byte)Fila["loncla"];
                    Clave_Actual = Clave.Left(Longitud);

                    if (Auxiliares_Historico.Identificador_Alchis_1003(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Actual) == 0)
                    {
                        if (Niveles.Trim() == string.Empty)
                            Niveles = (string)Fila["descla"];
                        else
                            Niveles = Niveles.Trim() + " - " + (string)Fila["descla"];
                    }
                }
            }

            return Niveles.Trim();
        }

        /// <summary>
        /// Obtiene la longitud en caracteres/dígitos 'CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion"></param>
        /// <param name="Ejercicio"></param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_5000">Identificador registro 'CLAVES ORGÁNICAS'</param>
        /// <returns>Longitud en caracteres/dígitos 'CLAVES ORGÁNICAS'</returns>
        public static byte Longitud_Clave_Alccom_5000(string CadenaConexion, short Ejercicio, string Bdd_Comunes, short Id_5000)
        {
            byte Longitud_Clave = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT lnc_5000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5000";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5000, 0) AND ISNULL (afv_5000, 9999)";
            string Restriccion_Identificador = $"ide_5000 = {Id_5000}";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Identificador}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Longitud_Clave = (byte)Data_Reader["lnc_5000"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Longitud_Clave;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la longitud en caracteres/dígitos 'CLAVES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion"></param>
        /// <param name="Ejercicio"></param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_5001">Identificador registro 'CLAVES FUNCIONALES'</param>
        /// <returns>Longitud en caracteres/dígitos 'CLAVES FUNCIONALES'</returns>
        public static byte Longitud_Clave_Alccom_5001(string CadenaConexion, short Ejercicio, string Bdd_Comunes, short Id_5001)
        {
            byte Longitud_Clave = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT lnc_5001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5001";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5001, 0) AND ISNULL (afv_5001, 9999)";
            string Restriccion_Identificador = $"ide_5001 = '{Id_5001}'";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Identificador}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Longitud_Clave = (byte)Data_Reader["lnc_5001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Longitud_Clave;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la longitud en caracteres/dígitos 'CLAVES ECONÓMICAS'
        /// </summary>
        /// <param name="CadenaConexion"></param>
        /// <param name="Ejercicio"></param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_5002">Identificador registro 'CLAVES ECONÓMICAS'</param>
        /// <returns>Longitud en caracteres/dígitos 'CLAVES ECONÓMICAS'</returns>
        public static byte Longitud_Clave_Alccom_5002(string CadenaConexion, short Ejercicio, string Bdd_Comunes, short Id_5002)
        {
            byte Longitud_Clave = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT lnc_5002";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5002";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5002, 0) AND ISNULL (afv_5002, 9999)";
            string Restriccion_Identificador = $"ide_5002 = '{Id_5002}'";
            string Cadena_Where = $" WHERE {Restriccion_Validez} and {Restriccion_Identificador}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Longitud_Clave = (byte)Data_Reader["lnc_5002"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Longitud_Clave;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la longitud en caracteres/dígitos 'CLAVES ECONÓMICAS'
        /// </summary>
        /// <param name="CadenaConexion"></param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio de validación</param>
        /// <returns>Longitud en caracteres/dígitos 'CLAVES ECONÓMICAS'</returns>
        public static Tuple<short, byte[]> Longitud_Claves_Alccom_5003(string CadenaConexion, string Bdd_Comunes, short Ejercicio)
        {
            short Id_5003 = 0;
            byte[] Longitud_Clave = { 0, 0, 0 };
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5003, ldo_5003, ldf_5003, lde_5003";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5003";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5003, 0) AND ISNULL (afv_5003, 9999)";
            string Cadena_Where = $" WHERE {Restriccion_Validez}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5003 = (short)Data_Reader["ide_5003"];
                        Longitud_Clave[0] = (byte)Convert.ToByte(Data_Reader["ldo_5003"]);
                        Longitud_Clave[1] = (byte)Convert.ToByte(Data_Reader["ldf_5003"]);
                        Longitud_Clave[2] = (byte)Convert.ToByte(Data_Reader["lde_5003"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte[]>(Id_5003, Longitud_Clave);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador del último nivel de las CLAVES ORGÁNICAS
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <returns>Identificador registro</returns>
        public static short Ultimo_Nivel_Organicas(string CadenaConexion, short Ejercicio, string Bdd_Comunes)
        {
            short Id_5000 = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5000";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5000, 0) AND ISNULL (afv_5000, 9999)";
            string Restriccion_Longitud = "lnc_5000 != 0";
            string Cadena_Where = $"{Restriccion_Validez} AND {Restriccion_Longitud}";
            string Cadena_Order = " ORDER BY lnc_5000 desc";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = $"{Cadena_Select}{Cadena_From} WHERE {Cadena_Where}{Cadena_Order}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_5000 = (short)Data_Reader["ide_5000"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_5000;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador del último nivel de las CLAVES FUNCIONALES
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <returns>Identificador registro</returns>
        public static short Ultimo_Nivel_Funcionales(string CadenaConexion, short Ejercicio, string Bdd_Comunes)
        {
            short Id_5001 = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5001";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5001, 0) AND ISNULL (afv_5001, 9999)";
            string Restriccion_Longitud = "lnc_5001 != 0";
            string Cadena_Where = $"{Restriccion_Validez} AND {Restriccion_Longitud}";
            string Cadena_Order = " ORDER BY lnc_5001 desc";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = $"{Cadena_Select}{Cadena_From} WHERE {Cadena_Where}{Cadena_Order}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_5001 = (short)Data_Reader["ide_5001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_5001;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador del último nivel de las CLAVES ECONÓMICAS
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <returns>Identificador registro</returns>
        public static short Ultimo_Nivel_Economicas(string CadenaConexion, short Ejercicio, string Bdd_Comunes)
        {
            short Id_5002 = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5002";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5002";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5002, 0) AND ISNULL (afv_5002, 9999)";
            string Restriccion_Longitud = "lnc_5002 != 0";
            string Cadena_Where = $"{Restriccion_Validez} AND {Restriccion_Longitud}";
            string Cadena_Order = " ORDER BY lnc_5002 desc";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = $"{Cadena_Select}{Cadena_From} WHERE {Cadena_Where}{Cadena_Order}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_5002 = (short)Data_Reader["ide_5002"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_5002;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación 'NIVELES ORGÁNICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_5000">Identificador nivel</param>
        /// <returns>Denominación 'NIVELES ORGÁNICOS'</returns>
        public static string Denominacion_Nivel_Organico(string CadenaConexion, string Bdd_Comunes, int Id_5000)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT des_5000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5000";
            string Restriccion_Identificador = $"ide_5000 = {Id_5000}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["des_5000"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación 'NIVELES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_5001">Identificador nivel</param>
        /// <returns>Denominación 'NIVELES FUNCIONALES'</returns>
        public static string Denominacion_Nivel_Funcional(string CadenaConexion, string Bdd_Comunes, int Id_5001)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT des_5001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5001";
            string Restriccion_Identificador = $"ide_5001 = {Id_5001}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["des_5001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación 'NIVELES ECONÓMICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_5002">Identificador nivel</param>
        /// <returns>Denominación 'NIVELES ECONÓMICOS'</returns>
        public static string Denominacion_Nivel_Economico(string CadenaConexion, string Bdd_Comunes, int Id_5002)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT des_5002";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5002";
            string Restriccion_Identificador = $"ide_5002 = {Id_5002}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["des_5002"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. PLAN GENERAL CONTABLE. MAYOR'
        /// Item1 : Identificador registro
        /// Item2 : Denominación
        /// Item3 : Nivel
        /// Item4 : Semáforo último nivel
        /// Item5 : Año comienzo validez
        /// Item6 : Año final validez
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Cuenta_Contable">Cuenta contable</param>
        /// <param name="Restriccion_Nivel">Restriccion del nivel</param>
        /// <returns>Datos 'COMUNES. PLAN GENERAL CONTABLE. MAYOR'
        /// Item1 : Identificador registro
        /// Item2 : Denominación
        /// Item3 : Nivel
        /// Item4 : Semáforo último nivel
        /// Item5 : Año comienzo validez
        /// Item6 : Año final validez
        /// </returns>
        public static Tuple<int, string, byte, bool, short, short> Datos_Alccom_1001(string CadenaConexion, string Bdd_Comunes, string Cuenta_Contable, string Restriccion_Nivel)
        {
            int Id_1001 = 0;
            string Denominacion = "";
            byte Nivel = 0;
            bool Ultimo_Nivel = false;
            short Inicio = 0;
            short Final = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1001, des_1001, niv_1001, sun_1001, acv_1001, afv_1001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_1001";
            string Restriccion_Cuenta = $"cta_1001 = '{Cuenta_Contable}'";
            string Cadena_Restricciones = $"{Restriccion_Nivel} AND {Restriccion_Cuenta}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1001 = (int)Data_Reader["ide_1001"];
                        Denominacion = (string)Data_Reader["des_1001"];
                        Nivel = (byte)Data_Reader["niv_1001"];
                        Ultimo_Nivel = (bool)Data_Reader["sun_1001"];
                        Inicio = (short)Data_Reader["acv_1001"];
                        Final = (short)Data_Reader["afv_1001"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, byte, bool, short, short>(Id_1001, Denominacion, Nivel, Ultimo_Nivel, Inicio, Final);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. PLAN GENERAL CONTABLE. MAYOR'
        /// Item1 : Identificador registro
        /// Item2 : Denominación
        /// Item3 : Nivel
        /// Item4 : Semáforo último nivel
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Cuenta_Contable">Cuenta contabke</param>
        /// <param name="Restriccion">Restricción por parametro/vacia</param>
        /// <returns>Datos 'COMUNES. PLAN GENERAL CONTABLE. MAYOR'
        /// Item1 : Identificador registro
        /// Item2 : Denominación
        /// Item3 : Nivel
        /// Item4 : Semáforo último nivel
        /// </returns>
        public static Tuple<int, string, byte, bool, short, short> Datos_Alccom_1001(string CadenaConexion, short Ejercicio, string Bdd_Comunes, string Cuenta_Contable, string Restriccion = "")
        {
            int Id_1001 = 0;
            string Denominacion = "";
            byte Nivel = 0;
            bool Ultimo_Nivel = false;
            short Inicio = 0;
            short Final = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1001, des_1001, niv_1001, sun_1001, acv_1001, afv_1001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_1001";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN acv_1001 AND IIF (afv_1001 = 0, 9999, afv_1001)";
            string Restriccion_Cuenta = $"cta_1001 = '{Cuenta_Contable}'";
            string Cadena_Where = $" WHERE {Restriccion_Cuenta} AND {Restriccion_Validez}";

            if (Restriccion.Trim() != string.Empty)
                Cadena_Where += $" AND {Restriccion}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1001 = (int)Data_Reader["ide_1001"];
                        Denominacion = (string)Data_Reader["des_1001"];
                        Nivel = (byte)Data_Reader["niv_1001"];
                        Ultimo_Nivel = (bool)Data_Reader["sun_1001"];
                        Inicio = (short)Data_Reader["acv_1001"];
                        Final = (short)Data_Reader["afv_1001"];
                    }
                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, byte, bool, short, short>(Id_1001, Denominacion, Nivel, Ultimo_Nivel, Inicio, Final);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. FORMAS DE PAGO Y COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Codigo_Forma">Código forma de pago/cobro</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. FORMAS DE PAGO Y COBRO' [ide_2000]<br/>
        /// Item2 : Tipo [tpc_2000]<br/>
        /// · 1  Pago<br/>
        /// · 2 Cobro<br/>
        /// · 3 Ambas<br/>
        /// Item3 : Denominación [den_2000]<br/>
        /// Item4 : Tipo documento [tdo_2000]<br/>
        /// · 1 Transferencia<br/>
        /// · 2 Cheque<br/>
        /// · 3 Pagaré<br/>
        /// · 4 Otros<br/>
        /// Item5 : Número de días a vencimiento [ndi_2000]
        /// </returns>
        public static Tuple<int, short, byte, string, byte, short> Datos_Alccom_2000(string CadenaConexion, string Bdd_Comunes, short Codigo_Forma)
        {
            int Id_2000 = 0;
            short Codigo = 0;
            byte Tipo = 0;
            string Denominacion = "";
            byte Tipo_Documento = 0;
            short Dias = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2000, cod_2000, tpc_2000, den_2000, tdo_2000, ndi_2000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2000";
            string Restriccion_Codigo = $"cod_2000 = {Codigo_Forma}";
            string Cadena_Where = $" WHERE {Restriccion_Codigo}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2000 = (int)Data_Reader["ide_2000"];
                        Codigo = (short)Data_Reader["cod_2000"];
                        Tipo = (byte)Data_Reader["tpc_2000"];
                        Denominacion = (string)Data_Reader["den_2000"];
                        Tipo_Documento = (byte)Data_Reader["tdo_2000"];
                        Dias = (short)Data_Reader["ndi_2000"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, byte, string, byte, short>(Id_2000, Codigo, Tipo, Denominacion, Tipo_Documento, Dias);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. FORMAS DE PAGO Y COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_2000">Identificador registro 'COMUNES. FORMAS DE PAGO Y COBRO'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. FORMAS DE PAGO Y COBRO' [ide_2000]<br/>
        /// Item2 : Tipo [tpc_2000]<br/>
        /// · 1  Pago<br/>
        /// · 2 Cobro<br/>
        /// · 3 Ambas<br/>
        /// Item3 : Denominación [den_2000]<br/>
        /// Item4 : Tipo documento [tdo_2000]<br/>
        /// · 1 Transferencia<br/>
        /// · 2 Cheque<br/>
        /// · 3 Pagaré<br/>
        /// · 4 Otros<br/>
        /// Item5 : Número de días a vencimiento [ndi_2000]
        /// </returns>
        public static Tuple<int, short, byte, string, byte, short> Datos_Alccom_2000(string CadenaConexion, string Bdd_Comunes, int Pid_2000)
        {
            int Id_2000 = 0;
            short Codigo = 0;
            byte Tipo = 0;
            string Denominacion = "";
            byte Tipo_Documento = 0;
            short Dias = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2000, cod_2000, tpc_2000, den_2000, tdo_2000, ndi_2000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2000";
            string Restriccion_Identificador = $"ide_2000 = {Pid_2000}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2000 = (int)Data_Reader["ide_2000"];
                        Codigo = (short)Data_Reader["cod_2000"];
                        Tipo = (byte)Data_Reader["tpc_2000"];
                        Denominacion = (string)Data_Reader["den_2000"];
                        Tipo_Documento = (byte)Data_Reader["tdo_2000"];
                        Dias = (short)Data_Reader["ndi_2000"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, byte, string, byte, short>(Id_2000, Codigo, Tipo, Denominacion, Tipo_Documento, Dias);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. CANAL DE PAGO Y COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Codigo_Canal">Código canal de pago/cobro</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. CANALES DE PAGO Y COBRO' [ide_2010]<br/>
        /// Item2 : Ejercicio [eje_2010]<br/>
        /// Item3 : Tipo [tca_2010]<br/>
        /// · 1 Pago<br/>
        /// · 2 Cobro<br/>
        /// · 3  Pago y cobro<br/>
        /// Item4 : Denominación [den_2010]<br/>
        /// Item5 : Clase canal [cca_2010]<br/>
        /// · 1 Entidad financiera<br/>
        /// · 2 Otros<br/>
        /// Item6 : I.B.A.N. [iba_2010]<br/>
        /// Item7 :Cuenta contables [cta_2010]
        /// </returns>
        public static Tuple<int, short, byte, string, byte, string, string> Datos_Alccom_2010(string CadenaConexion, string Bdd_Comunes, short Codigo_Canal)
        {
            int Id_2010 = 0;
            short Ejercicio = 0;
            byte Tipo = 0;
            string Denominacion = "";
            byte Clase = 0;
            string Iban = "";
            string Cuenta = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2010, eje_2010, cod_2010, tca_2010, den_2010, cca_2010, iba_2010, cta_2010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2010";
            string Restriccion_Codigo = $"cod_2010 = {Codigo_Canal}";
            string Cadena_Where = $" WHERE {Restriccion_Codigo}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2010 = (int)Data_Reader["ide_2010"];
                        Ejercicio = (short)Data_Reader["eje_2010"];
                        Tipo = (byte)Data_Reader["tca_2010"];
                        Denominacion = (string)Data_Reader["den_2010"];
                        Clase = (byte)Data_Reader["cca_2010"];
                        Iban = (string)Data_Reader["iba_2010"];
                        Cuenta = (string)Data_Reader["cta_2010"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, byte, string, byte, string, string>(Id_2010, Ejercicio, Tipo, Denominacion, Clase, Iban, Cuenta);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. CANAL DE PAGO Y COBRO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_2010">dentificador registro 'COMUNES. CANALES DE PAGO Y COBRO' </param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. CANALES DE PAGO Y COBRO' [ide_2010]<br/>
        /// Item2 : Ejercicio  [eje_2010]<br/>
        /// Irem3 : Código forma de pago
        /// Item4 : Tipo [tca_2010]<br/>
        /// · 1 Pago<br/>
        /// · 2 Cobro<br/>
        /// · 3  Pago y cobro<br/>
        /// Item5 : Denominación [den_2010]<br/>
        /// Item6 : Clase canal [cca_2010]<br/>
        /// · 1 Entidad financiera<br/>
        /// · 2 Otros<br/>
        /// Item7 : Matriz datos auxiliares<br/>
        /// · 0 - I.B.A.N. [iba_2010]<br/>
        ///  · 1 - Cuenta contables [cta_2010]
        /// </returns>
        public static Tuple<int, short, short, byte, string, byte, string[]> Datos_Alccom_2010(string CadenaConexion, string Bdd_Comunes, int Pid_2010)
        {
            int Id_2010 = 0;
            short Ejercicio = 0;
            short Codigo = 0;
            byte Tipo = 0;
            string Denominacion = "";
            byte Clase = 0;
            string[] Datos_Auxiliares = { "", "" };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2010, eje_2010, cod_2010, tca_2010, den_2010, cca_2010, iba_2010, cta_2010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2010";
            string Restriccion_Identificador = $"ide_2010 = {Pid_2010}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2010 = (int)Data_Reader["ide_2010"];
                        Ejercicio = (short)Data_Reader["eje_2010"];
                        Codigo = (short)Data_Reader["cod_2010"];
                        Tipo = (byte)Data_Reader["tca_2010"];
                        Denominacion = (string)Data_Reader["den_2010"];
                        Clase = (byte)Data_Reader["cca_2010"];
                        Datos_Auxiliares[0] = (string)Data_Reader["iba_2010"];
                        Datos_Auxiliares[1] = (string)Data_Reader["cta_2010"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, short, byte, string, byte, string[]>(Id_2010, Ejercicio, Codigo, Tipo, Denominacion, Clase, Datos_Auxiliares);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. TIPOS DE FACTURAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Tipo_Factura">Código tipo de factura</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. TIPOS DE FACTURAS' [ide_2020]<br/>
        /// Item2 : Código tipo de factura [tdf_2020]<br/>
        /// Item3 : Denominación [den_2020]<br/>
        /// Item4 : Fecha para el cáculo de la fecha tope de pago [fca_2020]<br/>
        /// · 0 No aplicable<br/>
        /// · 1 Fecha registro<br/>
        /// · 2 Fecha aprobación
        /// Item5 : Días a vencimiento [dia_2020]<br/>
        /// Item6 : Semáforos<br/>
        /// · 0 Semáforo inclusión en morosidad [smo_2020]<br/>
        /// · 1 Semáforo asignación '347' [sgi_2020]<br/>
        /// · 2 Semáforo factura rectificativa [sre_2020]
        /// </returns>
        public static Tuple<int, short, string, byte, short, bool[]> Datos_Alccom_2020(string CadenaConexion, string Bdd_Comunes, short Tipo_Factura)
        {
            int Id_2020 = 0;
            short Codigo = 0;
            string Denominacion = "";
            byte Fecha_Calculo = 0;
            short Dias = 0;
            bool[] Semaforos = { false, false, false };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2020, tdf_2020, den_2020, fca_2020, dia_2020, smo_2020, sgi_2020, sre_2020";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2020";
            string Restriccion_Codigo = $"tdf_2020 = {Tipo_Factura}";
            string Cadena_Where = $" WHERE {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2020 = (int)Data_Reader["ide_2020"];
                        Codigo = (short)Data_Reader["tdf_2020"];
                        Denominacion = (string)Data_Reader["den_2020"];
                        Fecha_Calculo = (byte)Data_Reader["fca_2020"];
                        Dias = (short)Data_Reader["dia_2020"];

                        Semaforos[0] = Convert.ToBoolean(Data_Reader["smo_2020"]);
                        Semaforos[1] = Convert.ToBoolean(Data_Reader["sgi_2020"]);
                        Semaforos[2] = Convert.ToBoolean(Data_Reader["sre_2020"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, string, byte, short, bool[]>(Id_2020, Codigo, Denominacion, Fecha_Calculo, Dias, Semaforos);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. TIPOS DE FACTURAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_2020">Identificador registro 'COMUNES. TIPOS DE FACTURAS'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. TIPOS DE FACTURAS' [ide_2020]<br/>
        /// Item2 : Código tipo de factura [tdf_2020]<br/>
        /// Item3 : Denominación [den_2020]<br/>
        /// Item4 : Fecha para el cáculo de la fecha tope de pago [fca_2020]<br/>
        /// · 0 No aplicable<br/>
        /// · 1 Fecha registro<br/>
        /// · 2 Fecha aprobación
        /// Item5 : Días a vencimiento [dia_2020]<br/>
        /// Item6 : Semáforos<br/>
        /// · 0 Semáforo inclusión en morosidad [smo_2020]<br/>
        /// · 1 Semáforo asignación '347' [sgi_2020]<br/>
        /// · 2 Semáforo factura rectificativa [sre_2020]
        /// </returns>
        public static Tuple<int, short, string, byte, short, bool[]> Datos_Alccom_2020(string CadenaConexion, string Bdd_Comunes, int Pid_2020)
        {
            int Id_2020 = 0;
            short Codigo = 0;
            string Denominacion = "";
            byte Fecha_Calculo = 0;
            short Dias = 0;
            bool[] Semaforos = { false, false, false };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2020, tdf_2020, den_2020, fca_2020, dia_2020, smo_2020, sgi_2020, sre_2020";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2020";
            string Restriccion_Identificador = $"ide_2020 = {Pid_2020}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2020 = (int)Data_Reader["ide_2020"];
                        Codigo = (short)Data_Reader["tdf_2020"];
                        Denominacion = (string)Data_Reader["den_2020"];
                        Fecha_Calculo = (byte)Data_Reader["fca_2020"];
                        Dias = (short)Data_Reader["dia_2020"];

                        Semaforos[0] = Convert.ToBoolean(Data_Reader["smo_2020"]);
                        Semaforos[1] = Convert.ToBoolean(Data_Reader["sgi_2020"]);
                        Semaforos[2] = Convert.ToBoolean(Data_Reader["sre_2020"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, string, byte, short, bool[]>(Id_2020, Codigo, Denominacion, Fecha_Calculo, Dias, Semaforos);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. TIPOS DE CONTRATOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Tipo_Contrato">Código tipo de contrato</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. TIPOS DE CONTRATOS' [ide_2030]<br/>
        /// Item2 : Código tipo de contrato [tco_2030]<br/>
        /// Item3 : Denominación [den_2030]<br/>
        /// Item4 : Semáforos<br/>
        /// · 0 Semáforo contrato menor [scm_2030]<br/>
        /// · 1 Semáforo de prestación servicios [sps_2030]<br/>
        /// · 2 Semáforo petición capacidad y aptitud profesional [spc_2030]
        /// · 3 Semáforo presentación en facturas [spf_2030]
        /// </returns>
        public static Tuple<int, short, string, bool[]> Datos_Alccom_2030(string CadenaConexion, string Bdd_Comunes, short Tipo_Contrato)
        {
            int Id_2030 = 0;
            short Codigo = 0;
            string Denominacion = "";
            bool[] Semaforos = { false, false, false, false };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2030, tco_2030, den_2030, scm_2030, sps_2030, spc_2030, spf_2030";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2030";
            string Restriccion_Codigo = $"tco_2030 = {Tipo_Contrato}";
            string Cadena_Where = $" WHERE {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2030 = (int)Data_Reader["ide_2030"];
                        Codigo = (short)Data_Reader["tco_2030"];
                        Denominacion = (string)Data_Reader["den_2030"];

                        Semaforos[0] = Convert.ToBoolean(Data_Reader["scm_2030"]);
                        Semaforos[1] = Convert.ToBoolean(Data_Reader["sps_2030"]);
                        Semaforos[2] = Convert.ToBoolean(Data_Reader["spc_2030"]);
                        Semaforos[3] = Convert.ToBoolean(Data_Reader["spf_2030"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, string, bool[]>(Id_2030, Codigo, Denominacion, Semaforos);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. TIPOS DE CONTRATOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_2030">Identificador registro 'COMUNES. TIPOS DE CONTRATOS'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. TIPOS DE CONTRATOS' [ide_2030]<br/>
        /// Item2 : Código tipo de contrato [tco_2030]<br/>
        /// Item3 : Denominación [den_2030]<br/>
        /// Item4 : Semáforos<br/>
        /// · 0 Semáforo contrato menor [scm_2030]<br/>
        /// · 1 Semáforo de prestación servicios [sps_2030]<br/>
        /// · 2 Semáforo petición capacidad y aptitud profesional [spc_2030]
        /// · 3 Semáforo presentación en facturas [spf_2030]
        /// </returns>
        public static Tuple<int, short, string, bool[]> Datos_Alccom_2030(string CadenaConexion, string Bdd_Comunes, int Pid_2030)
        {
            int Id_2030 = 0;
            short Codigo = 0;
            string Denominacion = "";
            bool[] Semaforos = { false, false, false, false };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2030, tco_2030, den_2030, scm_2030, sps_2030, spc_2030, spf_2030";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2030";
            string Restriccion_Identificador = $"ide_2030 = {Pid_2030}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2030 = (int)Data_Reader["ide_2030"];
                        Codigo = (short)Data_Reader["tco_2030"];
                        Denominacion = (string)Data_Reader["den_2030"];

                        Semaforos[0] = Convert.ToBoolean(Data_Reader["scm_2030"]);
                        Semaforos[1] = Convert.ToBoolean(Data_Reader["sps_2030"]);
                        Semaforos[2] = Convert.ToBoolean(Data_Reader["spc_2030"]);
                        Semaforos[3] = Convert.ToBoolean(Data_Reader["spf_2030"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, string, bool[]>(Id_2030, Codigo, Denominacion, Semaforos);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. TIPOS DE SESIONES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Tipo_Sesion">Código tipo de sesión</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. TIPOS DE SESIONES' [ide_2040]<br/>
        /// Item2 : Código tipo de sesión [cod_2040]<br/>
        /// Item3 : Denominación [den_2040]
        /// </returns>
        public static Tuple<int, short, string> Datos_Alccom_2040(string CadenaConexion, string Bdd_Comunes, short Tipo_Sesion)
        {
            int Id_2040 = 0;
            short Codigo = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2040, cod_2040, den_2040";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2040";
            string Restriccion_Codigo = $"cod_2040 = {Tipo_Sesion}";
            string Cadena_Where = $" WHERE {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2040 = (int)Data_Reader["ide_2040"];
                        Codigo = (short)Data_Reader["cod_2040"];
                        Denominacion = (string)Data_Reader["den_2040"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, string>(Id_2040, Codigo, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. TIPOS DE SESIONES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_2040">Identificador registro 'COMUNES. TIPOS DE SESIONES'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. TIPOS DE SESIONES' [ide_2040]<br/>
        /// Item2 : Código tipo de sesión [cod_2040]<br/>
        /// Item3 : Denominación [den_2040]
        /// </returns>
        public static Tuple<int, short, string> Datos_Alccom_2040(string CadenaConexion, string Bdd_Comunes, int Pid_2040)
        {
            int Id_2040 = 0;
            short Codigo = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2040, cod_2040, den_2040";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2040";
            string Restriccion_Identificador = $"ide_2040 = {Pid_2040}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2040 = (int)Data_Reader["ide_2040"];
                        Codigo = (short)Data_Reader["cod_2040"];
                        Denominacion = (string)Data_Reader["den_2040"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, string>(Id_2040, Codigo, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Grupo_Motivos">Código grupos de motivos de disconformidad/reparo</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO' [ide_2050]<br/>
        /// Item2 : Código grupos de motivos de disconformidad/reparo [cod_2050]<br/>
        /// Item3 : Denominación [den_2050]<br/>
        /// Item4 : Matriz de fechas de validez<br/>
        /// · 0 - Fecha inicial [fvi_2050]<br/>
        /// · 1 - Fecha final [fvf_2050]
        /// </returns>
        public static Tuple<int, short, string, DateTime?[]> Datos_Alccom_2050(string CadenaConexion, string Bdd_Comunes, short Grupo_Motivos)
        {
            int Id_2050 = 0;
            short Codigo = 0;
            string Denominacion = "";
            DateTime?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2050, cod_2050, den_2050, fvi_2050, fvf_2050";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2050";
            string Restriccion_Codigo = $"cod_2050 = {Grupo_Motivos}";
            string Cadena_Where = $" WHERE {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2050 = (int)Data_Reader["ide_2050"];
                        Codigo = (short)Data_Reader["cod_2050"];
                        Denominacion = (string)Data_Reader["den_2050"];

                        Validez[0] = Data_Reader["fvi_2050"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(Data_Reader["fvi_2050"]);
                        Validez[1] = Data_Reader["fvf_2050"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(Data_Reader["fvf_2050"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, string, DateTime?[]>(Id_2050, Codigo, Denominacion, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_2050">Identificador registro 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO' [ide_2050]<br/>
        /// Item2 : Código grupos de motivos de disconformidad/reparo [cod_2050]<br/>
        /// Item3 : Denominación [den_2050]<br/>
        /// Item4 : Matriz de fechas de validez<br/>
        /// · 0 - Fecha inicial [fvi_2050]<br/>
        /// · 1 - Fecha final [fvf_2050]
        /// </returns>
        public static Tuple<int, short, string, DateTime?[]> Datos_Alccom_2050(string CadenaConexion, string Bdd_Comunes, int Pid_2050)
        {
            int Id_2050 = 0;
            short Codigo = 0;
            string Denominacion = "";
            DateTime?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2050, cod_2050, den_2050, fvi_2050, fvf_2050";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2050";
            string Restriccion_Identificador = $"ide_2050 = {Pid_2050}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2050 = (int)Data_Reader["ide_2050"];
                        Codigo = (short)Data_Reader["cod_2050"];
                        Denominacion = (string)Data_Reader["den_2050"];

                        Validez[0] = Data_Reader["fvi_2050"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(Data_Reader["fvi_2050"]);
                        Validez[1] = Data_Reader["fvf_2050"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(Data_Reader["fvf_2050"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, string, DateTime?[]>(Id_2050, Codigo, Denominacion, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO. DETALLE'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_2050">Identificador registro 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'</param>
        /// <param name="Codigo_Motivo">Código motivo de disconformidad/reparo</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO.DETALLE' [ide_2050_1]<br/>
        /// Item2 :Código motivo de disconformidad/reparo [tdr_2050_1]<br/>
        /// Item3 : Descripción [den_2050_1]<br/>
        /// Item4 : Semáforo último nivel [sun_2050_1]<br/>
        /// </returns>
        public static Tuple<int, string, string, bool> Datos_Alccom_2050_1(string CadenaConexion, string Bdd_Comunes, int Pid_2050, string Codigo_Motivo)
        {
            int Id_2050_1 = 0;
            string Codigo = "";
            string Denominacion = "";
            bool Ultimo_Nivel = false;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2050_1, tdr_2050_1, den_2050_1, sun_2050_1";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2050_1";
            string Restriccion_Identificador = $"ide_2050 = {Pid_2050}";
            string Restriccion_Codigo = $"tdr_2050_1 = '{Codigo_Motivo}'";
            string Cadena_Where = $" WHERE {Restriccion_Identificador} AND {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2050_1 = (int)Data_Reader["ide_2050_1"];
                        Codigo = (string)Data_Reader["tdr_2050_1"];
                        Denominacion = (string)Data_Reader["den_2050_1"];
                        Ultimo_Nivel = Convert.ToBoolean(Data_Reader["sun_2050_1"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string, bool>(Id_2050_1, Codigo, Denominacion, Ultimo_Nivel);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO. DETALLE'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_2050_1">Identificador registro 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO. DETALLE'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO.DETALLE' [ide_2050_1]<br/>
        /// Item2 :Código motivo de disconformidad/reparo [tdr_2050_1]<br/>
        /// Item3 : Descripción [den_2050_1]<br/>
        /// Item4 : Semáforo último nivel [sun_2050_1]<br/>
        /// </returns>
        public static Tuple<int, string, string, bool> Datos_Alccom_2050_1(string CadenaConexion, string Bdd_Comunes, int Pid_2050_1)
        {
            int Id_2050_1 = 0;
            string Codigo = "";
            string Denominacion = "";
            bool Ultimo_Nivel = false;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2050_1, tdr_2050_1, den_2050_1, sun_2050_1";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2050_1";
            string Restriccion_Identificador = $"ide_2050_1 = {Pid_2050_1}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_2050_1 = (int)Data_Reader["ide_2050_1"];
                        Codigo = (string)Data_Reader["tdr_2050_1"];
                        Denominacion = (string)Data_Reader["den_2050_1"];
                        Ultimo_Nivel = Convert.ToBoolean(Data_Reader["sun_2050_1"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string, bool>(Id_2050_1, Codigo, Denominacion, Ultimo_Nivel);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES ORGÁNICOS' con periodo de validez activo
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Nivel_Actual">Nivel</param>
        /// <param name="Ejercicio">Ejercicio de validación</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES ORGÁNICOS' [ide_5000]<br/>
        /// Item2 : Nivel [niv_5000]<br/>
        /// Item3 : Denominación [des_5000]<br/>
        /// Item4 : Longitud en dígitos de la clave [lnc_5000]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5000]<br/>
        /// · 1 - Año final [afv_5000]
        /// </returns>
        public static Tuple<short, byte, string, byte, short?[]> Datos_Alccom_5000(string CadenaConexion, string Bdd_Comunes, byte Nivel_Actual, short Ejercicio)
        {
            short Id_5000 = 0;
            byte Nivel = 0;
            string Denominacion = "";
            byte Longitud = 0;
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5000, niv_5000, des_5000, lnc_5000, aiv_5000, afv_5000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5000";
            string Restriccion_Nivel = $"niv_5000 = {Nivel_Actual}";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5000, 0001) AND ISNULL (afv_5000, {Ejercicio})";
            string Cadena_Where = $" WHERE {Restriccion_Nivel} AND {Restriccion_Validez}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5000 = (short)Data_Reader["ide_5000"];
                        Nivel = (byte)Data_Reader["niv_5000"];
                        Denominacion = (string)Data_Reader["des_5000"];
                        Longitud = (byte)Data_Reader["lnc_5000"];

                        Validez[0] = Data_Reader["aiv_5000"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5000"]);
                        Validez[1] = Data_Reader["afv_5000"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5000"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte, string, byte, short?[]>(Id_5000, Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES ORGÁNICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Nivel_Actual">Nivel</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES ORGÁNICOS' [ide_5000]<br/>
        /// Item2 : Nivel [niv_5000]<br/>
        /// Item3 : Denominación [des_5000]<br/>
        /// Item4 : Longitud en dígitos de la clave [lnc_5000]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5000]<br/>
        /// · 1 - Año final [afv_5000]
        /// </returns>
        public static Tuple<short, byte, string, byte, short?[]> Datos_Alccom_5000(string CadenaConexion, string Bdd_Comunes, byte Nivel_Actual)
        {
            short Id_5000 = 0;
            byte Nivel = 0;
            string Denominacion = "";
            byte Longitud = 0;
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5000, niv_5000, des_5000, lnc_5000, aiv_5000, afv_5000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5000";
            string Restriccion_Nivel = $"niv_5000 = {Nivel_Actual}";
            string Cadena_Where = $" WHERE {Restriccion_Nivel}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5000 = (short)Data_Reader["ide_5000"];
                        Nivel = (byte)Data_Reader["niv_5000"];
                        Denominacion = (string)Data_Reader["des_5000"];
                        Longitud = (byte)Data_Reader["lnc_5000"];

                        Validez[0] = Data_Reader["aiv_5000"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5000"]);
                        Validez[1] = Data_Reader["afv_5000"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5000"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte, string, byte, short?[]>(Id_5000, Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES ORGÁNICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_5000">Identificador registro 'COMUNES. NIVELES ORGÁNICOS'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES ORGÁNICOS' [ide_5000]<br/>
        /// Item2 : Nivel [niv_5000]<br/>
        /// Item3 : Denominación [des_5000]<br/>
        /// Item4 : Longitud en dígitos de la clave [lnc_5000]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5000]<br/>
        /// · 1 - Año final [afv_5000]
        /// </returns>
        public static Tuple<short, byte, string, byte, short?[]> Datos_Alccom_5000(string CadenaConexion, string Bdd_Comunes, short Pid_5000)
        {
            short Id_5000 = 0;
            byte Nivel = 0;
            string Denominacion = "";
            byte Longitud = 0;
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5000, niv_5000, des_5000, lnc_5000, aiv_5000, afv_5000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5000";
            string Restriccion_Nivel = $"ide_5000 = {Pid_5000}";
            string Cadena_Where = $" WHERE {Restriccion_Nivel}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5000 = (short)Data_Reader["ide_5000"];
                        Nivel = (byte)Data_Reader["niv_5000"];
                        Denominacion = (string)Data_Reader["des_5000"];
                        Longitud = (byte)Data_Reader["lnc_5000"];

                        Validez[0] = Data_Reader["aiv_5000"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5000"]);
                        Validez[1] = Data_Reader["afv_5000"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5000"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte, string, byte, short?[]>(Id_5000, Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES FUNCIONALES' con periodo de validez activo
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Nivel_Actual">Nivel</param>
        /// <param name="Ejercicio">Ejercicio de validación</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES FUNCIONALES' [ide_5001]<br/>
        /// Item2 : Nivel [niv_5001]<br/>
        /// Item3 : Denominación [des_5001]<br/>
        /// Item4 : Longitud en dígitos de la clave [lnc_5001]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5001]<br/>
        /// · 1 - Año final [afv_5001]
        /// </returns>
        public static Tuple<short, byte, string, byte, short?[]> Datos_Alccom_5001(string CadenaConexion, string Bdd_Comunes, byte Nivel_Actual, short Ejercicio)
        {
            short Id_5001 = 0;
            byte Nivel = 0;
            string Denominacion = "";
            byte Longitud = 0;
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5001, niv_5001, des_5001, lnc_5001, aiv_5001, afv_5001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5001";
            string Restriccion_Nivel = $"niv_5001 = {Nivel_Actual}";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5001, 0001) AND ISNULL (afv_5001, {Ejercicio})";
            string Cadena_Where = $" WHERE {Restriccion_Nivel} AND {Restriccion_Validez}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5001 = (short)Data_Reader["ide_5001"];
                        Nivel = (byte)Data_Reader["niv_5001"];
                        Denominacion = (string)Data_Reader["des_5001"];
                        Longitud = (byte)Data_Reader["lnc_5001"];

                        Validez[0] = Data_Reader["aiv_5001"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5001"]);
                        Validez[1] = Data_Reader["afv_5001"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5001"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte, string, byte, short?[]>(Id_5001, Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Nivel_Actual">Nivel</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES FUNCIONALES' [ide_5001]<br/>
        /// Item2 : Nivel [niv_5001]<br/>
        /// Item3 : Denominación [des_5001]<br/>
        /// Item4 : Longitud en dígitos de la clave [lnc_5001]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5001]<br/>
        /// · 1 - Año final [afv_5001]
        /// </returns>
        public static Tuple<short, byte, string, byte, short?[]> Datos_Alccom_5001(string CadenaConexion, string Bdd_Comunes, byte Nivel_Actual)
        {
            short Id_5001 = 0;
            byte Nivel = 0;
            string Denominacion = "";
            byte Longitud = 0;
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5001, niv_5001, des_5001, lnc_5001, aiv_5001, afv_5001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5001";
            string Restriccion_Nivel = $"niv_5001 = {Nivel_Actual}";
            string Cadena_Where = $" WHERE {Restriccion_Nivel}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5001 = (short)Data_Reader["ide_5001"];
                        Nivel = (byte)Data_Reader["niv_5001"];
                        Denominacion = (string)Data_Reader["des_5001"];
                        Longitud = (byte)Data_Reader["lnc_5001"];

                        Validez[0] = Data_Reader["aiv_5001"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5001"]);
                        Validez[1] = Data_Reader["afv_5001"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5001"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte, string, byte, short?[]>(Id_5001, Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_5001">Identificador registro 'COMUNES. NIVELES FUNCIONALES'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES FUNCIONALES' [ide_5001]<br/>
        /// Item2 : Nivel [niv_5001]<br/>
        /// Item3 : Denominación [des_5001]<br/>
        /// Item4 : Longitud en dígitos de la clave [lnc_5001]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5001]<br/>
        /// · 1 - Año final [afv_5001]
        /// </returns>
        public static Tuple<short, byte, string, byte, short?[]> Datos_Alccom_5001(string CadenaConexion, string Bdd_Comunes, short Pid_5001)
        {
            short Id_5001 = 0;
            byte Nivel = 0;
            string Denominacion = "";
            byte Longitud = 0;
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5001, niv_5001, des_5001, lnc_5001, aiv_5001, afv_5001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5001";
            string Restriccion_Nivel = $"ide_5001 = {Pid_5001}";
            string Cadena_Where = $" WHERE {Restriccion_Nivel}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5001 = (short)Data_Reader["ide_5001"];
                        Nivel = (byte)Data_Reader["niv_5001"];
                        Denominacion = (string)Data_Reader["des_5001"];
                        Longitud = (byte)Data_Reader["lnc_5001"];

                        Validez[0] = Data_Reader["aiv_5001"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5001"]);
                        Validez[1] = Data_Reader["afv_5001"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5001"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte, string, byte, short?[]>(Id_5001, Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES ECONÓMICOS' con periodo de validez activo
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Nivel_Actual">Nivel</param>
        /// <param name="Ejercicio">Ejercicio de validación</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES ECONÓMICOS' [ide_5002]<br/>
        /// Item2 : Nivel [niv_5002]<br/>
        /// Item3 : Denominación [des_5002]<br/>
        /// Item4 : Longitud en dígitos de la clave [lnc_5002]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5002]<br/>
        /// · 1 - Año final [afv_5002]
        /// </returns>
        public static Tuple<short, byte, string, byte, short?[]> Datos_Alccom_5002(string CadenaConexion, string Bdd_Comunes, byte Nivel_Actual, short Ejercicio)
        {
            short Id_5002 = 0;
            byte Nivel = 0;
            string Denominacion = "";
            byte Longitud = 0;
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5002, niv_5002, des_5002, lnc_5002, aiv_5002, afv_5002";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5002";
            string Restriccion_Nivel = $"niv_5002 = {Nivel_Actual}";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5002, 0001) AND ISNULL (afv_5002, {Ejercicio})";
            string Cadena_Where = $" WHERE {Restriccion_Nivel} AND {Restriccion_Validez}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5002 = (short)Data_Reader["ide_5002"];
                        Nivel = (byte)Data_Reader["niv_5002"];
                        Denominacion = (string)Data_Reader["des_5002"];
                        Longitud = (byte)Data_Reader["lnc_5002"];

                        Validez[0] = Data_Reader["aiv_5002"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5002"]);
                        Validez[1] = Data_Reader["afv_5002"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5002"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte, string, byte, short?[]>(Id_5002, Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES ECONÓMICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Nivel_Actual">Nivel</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES ECONÓMICOS' [ide_5002]<br/>
        /// Item2 : Nivel [niv_5002]<br/>
        /// Item3 : Denominación [des_5002]<br/>
        /// Item4 : Longitud en dígitos de la clave [lnc_5002]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5002]<br/>
        /// · 1 - Año final [afv_5002]
        /// </returns>
        public static Tuple<short, byte, string, byte, short?[]> Datos_Alccom_5002(string CadenaConexion, string Bdd_Comunes, byte Nivel_Actual)
        {
            short Id_5002 = 0;
            byte Nivel = 0;
            string Denominacion = "";
            byte Longitud = 0;
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5002, niv_5002, des_5002, lnc_5002, aiv_5002, afv_5002";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5002";
            string Restriccion_Nivel = $"niv_5002 = {Nivel_Actual}";
            string Cadena_Where = $" WHERE {Restriccion_Nivel}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5002 = (short)Data_Reader["ide_5002"];
                        Nivel = (byte)Data_Reader["niv_5002"];
                        Denominacion = (string)Data_Reader["des_5002"];
                        Longitud = (byte)Data_Reader["lnc_5002"];

                        Validez[0] = Data_Reader["aiv_5002"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5002"]);
                        Validez[1] = Data_Reader["afv_5002"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5002"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte, string, byte, short?[]>(Id_5002, Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES ECONÓMICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_5002">Identificador registro 'COMUNES. NIVELES ECONÓMICOS'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES ECONÓMICOS' [ide_5002]<br/>
        /// Item2 : Nivel [niv_5002]<br/>
        /// Item3 : Denominación [des_5002]<br/>
        /// Item4 : Longitud en dígitos de la clave [lnc_5002]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5002]<br/>
        /// · 1 - Año final [afv_5002]
        /// </returns>
        public static Tuple<short, byte, string, byte, short?[]> Datos_Alccom_5002(string CadenaConexion, string Bdd_Comunes, short Pid_5002)
        {
            short Id_5002 = 0;
            byte Nivel = 0;
            string Denominacion = "";
            byte Longitud = 0;
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5002, niv_5002, des_5002, lnc_5002, aiv_5002, afv_5002";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5002";
            string Restriccion_Nivel = $"ide_5002 = {Pid_5002}";
            string Cadena_Where = $" WHERE {Restriccion_Nivel}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5002 = (short)Data_Reader["ide_5002"];
                        Nivel = (byte)Data_Reader["niv_5002"];
                        Denominacion = (string)Data_Reader["des_5002"];
                        Longitud = (byte)Data_Reader["lnc_5002"];

                        Validez[0] = Data_Reader["aiv_5002"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5002"]);
                        Validez[1] = Data_Reader["afv_5002"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5002"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, byte, string, byte, short?[]>(Id_5002, Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES DE VINCULACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_5003">Identificador registro 'COMUNES. NIVELES DE VINCULACIÓN'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES DE VINCULACIÓN' [ide_5003]<br/>
        /// Item2 :  Identificador registro<br/>
        /// · 0 - 'COMUNES. NIVELES ORGÁNICAS' [ide_5000]<br/>
        /// · 1 - 'COMUNES. NIVELES FUNCIONALES' [ide_5001]<br/>
        /// · 2 - 'COMUNES. NIVELES ECONÓMICOS' [ide_5002]<br/>
        /// Item3 : Longitud en dígitos de la clave<br/>
        /// · 0 - 'COMUNES. NIVELES ORGÁNICAS' [ldo_5003]<br/>
        /// · 1 - 'COMUNES. NIVELES FUNCIONALES' [ldF_5003]<br/>
        /// · 2 - 'COMUNES. NIVELES ECONÓMICOS' [ldE_5003]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5003]<br/>
        /// · 1 - Año final [afv_5003]
        /// </returns>
        public static Tuple<short, short[], byte[], short?[]> Datos_Alccom_5003(string CadenaConexion, string Bdd_Comunes, short Pid_5003)
        {
            short Id_5003 = 0;
            short[] Id_Nivel = { 0, 0, 0 };
            byte[] Longitud ={ 0, 0, 0 };
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5003, ide_5000, ide_5001, ide_5002, ldo_5003, ldf_5003, lde_5003, aiv_5003, afv_5003";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5003";
            string Restriccion_Identificador = $"ide_5003 = {Pid_5003}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5003 = (short)Data_Reader["ide_5003"];
                        Id_Nivel[0] = (short)Data_Reader["ide_5000"];
                        Id_Nivel[1] = (short)Data_Reader["ide_5001"];
                        Id_Nivel[2] = (short)Data_Reader["ide_5002"];


                        Longitud[0] = (byte)Data_Reader["ldo_5003"];
                        Longitud[1] = (byte)Data_Reader["ldf_5003"];
                        Longitud[2] = (byte)Data_Reader["lde_5003"];

                        Validez[0] = Data_Reader["aiv_5003"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5003"]);
                        Validez[1] = Data_Reader["afv_5003"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5003"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, short[], byte[], short?[]>(Id_5003, Id_Nivel, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. NIVELES DE VINCULACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_5003">Identificador registro 'COMUNES. NIVELES DE VINCULACIÓN'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. NIVELES DE VINCULACIÓN' [ide_5003]<br/>
        /// Item2 :  Identificador registro<br/>
        /// · 0 - 'COMUNES. NIVELES ORGÁNICAS' [ide_5000]<br/>
        /// · 1 - 'COMUNES. NIVELES FUNCIONALES' [ide_5001]<br/>
        /// · 2 - 'COMUNES. NIVELES ECONÓMICOS' [ide_5002]<br/>
        /// Item2 : Denominación niveles claves presupuestarias<br/>
        /// · 0 - 'COMUNES. NIVELES ORGÁNICAS' [desnor]<br/>
        /// · 1 - 'COMUNES. NIVELES FUNCIONALES' [desnfu]<br/>
        /// · 2 - 'COMUNES. NIVELES ECONÓMICOS' [desnec]<br/>
        /// Item4 : Longitud en dígitos de la clave<br/>
        /// · 0 - 'COMUNES. NIVELES ORGÁNICAS' [ldo_5003]<br/>
        /// · 1 - 'COMUNES. NIVELES FUNCIONALES' [ldF_5003]<br/>
        /// · 2 - 'COMUNES. NIVELES ECONÓMICOS' [ldE_5003]<br/>
        /// Item5 : Matriz de años de validez<br/>
        /// · 0 - Año inicial [aiv_5003]<br/>
        /// · 1 - Año final [afv_5003]
        /// </returns>
        public static Tuple<short, short[], string [], byte[], short?[]> Datos_Alccom_v_5003(string CadenaConexion, string Bdd_Comunes, short Pid_5003)
        {
            short Id_5003 = 0;
            short[] Id_Nivel = { 0, 0, 0 };
            string[] Denominacion = { "", "", "" };
            byte[] Longitud = { 0, 0, 0 };
            short?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5003, ide_5000, ide_5001, ide_5002, desnor, desnfu, desnec, ldo_5003, ldf_5003, lde_5003, aiv_5003, afv_5003";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_v_5003";
            string Restriccion_Identificador = $"ide_5003 = {Pid_5003}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_5003 = (short)Data_Reader["ide_5003"];
                        Id_Nivel[0] = (short)Data_Reader["ide_5000"];
                        Id_Nivel[1] = (short)Data_Reader["ide_5001"];
                        Id_Nivel[2] = (short)Data_Reader["ide_5002"];

                        Denominacion[0] = (string)Data_Reader["desnor"];
                        Denominacion[1] = (string)Data_Reader["desnfu"];
                        Denominacion[2] = (string)Data_Reader["desnec"];

                        Longitud[0] = (byte)Data_Reader["ldo_5003"];
                        Longitud[1] = (byte)Data_Reader["ldf_5003"];
                        Longitud[2] = (byte)Data_Reader["lde_5003"];

                        Validez[0] = Data_Reader["aiv_5003"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_5003"]);
                        Validez[1] = Data_Reader["afv_5003"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_5003"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<short, short[], string[], byte[], short?[]>(Id_5003, Id_Nivel, Denominacion, Longitud, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. FIRMANTES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Codigo">Código del firmante</param>
        /// <param name="Fecha">Fecha validez</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. FIRMANTES' [ide_9000]<br/>
        /// Item2 : Primer apellido [ap1_9000]<br/>
        /// Item3 : Segundo apellido [ap2_9000]<br/>
        /// Item4 : Nombre [nom_9000]<br/>
        /// Item5 : Cargo [car_9000]<br/>
        /// Item6 : Matriz periodo validez<br/>
        /// · 0 - Fecha inicio [fiv_9000]<br/>
        /// · 1 - Fecha final [ffv_9000]
        /// </returns>
        public static Tuple<int, string, string, string, string, DateTime?[]> Datos_Alccom_9000(string CadenaConexion, string Bdd_Comunes, short Codigo, DateTime Fecha)
        {
            int Id_9000 = 0;
            string Apellido_1 = "";
            string Apellido_2 = "";
            string Nombre = "";
            string Cargo = "";
            DateTime?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9000, ap1_9000, ap2_9000, nom_9000, car_9000, fiv_9000, ffv_9000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9000";
            string Restriccion_Codigo = $"cod_9000 = '{Codigo}'";
            string Restriccion_Validez = $"'{Fecha:dd/MM/yyyy}' BETWEEN ISNULL (fiv_9000, '01/01/0001') AND ISNULL (ffv_9000, '{Fecha:dd/MM/yyyy}')";
            string Cadena_Where = $" WHERE {Restriccion_Codigo} AND {Restriccion_Validez}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9000 = (int)Data_Reader["ide_9000"];
                        Apellido_1 = (string)Data_Reader["ap1_9000"];
                        Apellido_2 = (string)Data_Reader["ap2_9000"];
                        Nombre = (string)Data_Reader["nom_9000"];
                        Cargo = (string)Data_Reader["car_9000"];

                        for (byte i = 0; i < Validez.Length; i++)
                            Validez[i] = Data_Reader[5 + i] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(Data_Reader[5 + i]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string, string, string, DateTime?[]>(Id_9000, Apellido_1, Apellido_2, Nombre, Cargo, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. FIRMANTES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Codigo">Código del firmante</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. FIRMANTES' [ide_9000]<br/>
        /// Item2 : Primer apellido [ap1_9000]<br/>
        /// Item3 : Segundo apellido [ap2_9000]<br/>
        /// Item4 : Nombre [nom_9000]<br/>
        /// Item5 : Cargo [car_9000]<br/>
        /// Item6 : Matriz periodo validez<br/>
        /// · 0 - Fecha inicio [fiv_9000]<br/>
        /// · 1 - Fecha final [ffv_9000]
        /// </returns>
        public static Tuple<int, string, string, string, string, DateTime?[]> Datos_Alccom_9000(string CadenaConexion, string Bdd_Comunes, short Codigo)
        {
            int Id_9000 = 0;
            string Apellido_1 = "";
            string Apellido_2 = "";
            string Nombre = "";
            string Cargo = "";
            DateTime?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9000, ap1_9000, ap2_9000, nom_9000, car_9000, fiv_9000, ffv_9000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9000";
            string Restriccion_Codigo = $"cod_9000 = '{Codigo}'";
            string Cadena_Where = $" WHERE {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9000 = (int)Data_Reader["ide_9000"];
                        Apellido_1 = (string)Data_Reader["ap1_9000"];
                        Apellido_2 = (string)Data_Reader["ap2_9000"];
                        Nombre = (string)Data_Reader["nom_9000"];
                        Cargo = (string)Data_Reader["car_9000"];

                        for (byte i = 0; i < Validez.Length; i++)
                            Validez[i] = Data_Reader[5 + i] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(Data_Reader[5 + i]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string, string, string, DateTime?[]>(Id_9000, Apellido_1, Apellido_2, Nombre, Cargo, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'COMUNES. FIRMANTES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_9000">Identificador registro 'COMUNES. FIRMANTES'</param>
        /// <returns>
        /// Item1 : Identificador registro 'COMUNES. FIRMANTES' [ide_9000]<br/>
        /// Item2 : Código firmante [cod_9000]<br/>
        /// Item3 : Primer apellido [ap1_9000]<br/>
        /// Item4 : Segundo apellido [ap2_9000]<br/>
        /// Item5 : Nombre [nom_9000]<br/>
        /// Item6 : Cargo [car_9000]<br/>
        /// Item7 : Matriz periodo validez<br/>
        /// · 0 - Fecha inicio [aiv_9000]<br/>
        /// · 1 - Fecha final [afv_9000]
        /// </returns>
        public static Tuple<int, short, string, string, string, string, DateTime?[]> Datos_Alccom_9000(string CadenaConexion, string Bdd_Comunes, int Pid_9000)
        {
            int Id_9000 = 0;
            short Codigo = 0;
            string Apellido_1 = "";
            string Apellido_2 = "";
            string Nombre = "";
            string Cargo = "";
            DateTime?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9000, cod_9000, ap1_9000, ap2_9000, nom_9000, car_9000, fiv_9000, ffv_9000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9000";
            string Restriccion_Identificador = $"ide_9000 = '{Pid_9000}'";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9000 = (int)Data_Reader["ide_9000"];
                        Codigo = (short)Data_Reader["cod_9000"];
                        Apellido_1 = (string)Data_Reader["ap1_9000"];
                        Apellido_2 = (string)Data_Reader["ap2_9000"];
                        Nombre = (string)Data_Reader["nom_9000"];
                        Cargo = (string)Data_Reader["car_9000"];

                        for (byte i = 0; i < Validez.Length; i++)
                            Validez[i] = Data_Reader[6 + i] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(Data_Reader[6 + i]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, string, string, string, string, DateTime?[]>(Id_9000, Codigo, Apellido_1, Apellido_2, Nombre, Cargo, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene la denominación 'AGRUPACIONES NO PRESUPUESTARIAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_5010">Identificador registro 'AGRUPACIONES NO PRESUPUESTARIAS'</param>
        /// <returns>Denominación 'AGRUPACIONES NO PRESUPUESTARIAS'</returns>
        public static string Denominacion_Agrupacion_No_Presupuestaria(string CadenaConexion, short Ejercicio, string Bdd_Comunes, short Id_5010)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT des_5010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5010";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5010, 0) AND ISNULL (afv_5010, {Ejercicio})";
            string Restriccion_Codigo = $"ide_5010 = {Id_5010}";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Codigo}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["des_5010"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la serie documental 'TIPOS DE DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_9010">Identificador tipo documento</param>
        /// <returns>Serie documental SEGUEX 'TIPOS DE DOCUMENTOS'</returns>
        public static string Serie_Documental_Documento(string CadenaConexion, string Bdd_Comunes, int Id_9010)
        {
            string Serie_Seguex = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT sdo_9010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010";
            string Restriccion_Identificador = $"ide_9010 = {Id_9010}";
            string Cadena_Where = Restriccion_Identificador;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + " WHERE " + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Serie_Seguex = (string)Data_Reader["sdo_9010"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Serie_Seguex;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la serie documental 'TIPOS DE DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Tipo_Documento">Tipo documento</param>
        /// <returns>Serie documental SEGUEX 'TIPOS DE DOCUMENTOS'</returns>
        public static string Serie_Documental_Documento(string CadenaConexion, string Bdd_Comunes, string Tipo_Documento)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT sdo_9010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010";
            string Restriccion_Identificador = $"tdo_9010 = '{Tipo_Documento.Trim()}'";
            string Cadena_Where = Restriccion_Identificador;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + " WHERE " + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["sdo_9010"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación 'TIPOS DE DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010">Identificador registro 'TIPOS DE DOCUMENTOS'</param>
        /// <returns>
        /// Item1 : Identificador registro 'TIPOS DE DOCUMENTOS' [ide_9010]<br/>
        /// Item2 : Código tipo de documento [tdo_9010]<br/>
        /// Item3 : Denominación [des_9010]<br/>
        /// Item4 : Serie documental [sdo_9010]<br/>
        /// Item5 : Semáforo presentación [smp_9010]
        /// </returns>
        public static Tuple<int, string, string, string, byte> Datos_Alccom_9010(string CadenaConexion, string Bdd_Comunes, int Pid_9010)
        {
            int Id_9010 = 0;
            string Codigo = "";
            string Denominacion = "";
            string Serie = "";
            byte Semaforo = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010, tdo_9010, des_9010, sdo_9010, smp_9010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010";
            string Restriccion_Identificador = $"ide_9010 = {Pid_9010}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010 = (int)Data_Reader["ide_9010"];
                        Codigo = (string)Data_Reader["tdo_9010"];
                        Denominacion = (string)Data_Reader["des_9010"];
                        Serie = (string)Data_Reader["sdo_9010"];
                        Semaforo = (byte)Data_Reader["smp_9010"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string, string, byte>(Id_9010, Codigo, Denominacion, Serie, Semaforo);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Tipo_Documento">Tipo de documento</param>
        /// <returns>
        /// Item1 : Identificador registro 'TIPOS DE DOCUMENTOS' [ide_9010]<br/>
        /// Item2 : Denominación [des_9010]<br/>
        /// Item3 : Serie documental [sdo_9010]<br/>
        /// Item4 : Semáforo presentación [smp_9010]
        /// </returns>
        public static Tuple<int, string, string, byte> Datos_Alccom_9010(string CadenaConexion, string Bdd_Comunes, string Tipo_Documento)
        {
            int Id_9010 = 0;
            string Denominacion = "";
            string Serie = "";
            byte Semaforo = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010, des_9010, sdo_9010, smp_9010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010";
            string Restriccion_Tipo_Documento = $"tdo_9010 = '{Tipo_Documento}'";
            string Cadena_Where = Restriccion_Tipo_Documento;

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();

                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + " WHERE " + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010 = (int)Data_Reader["ide_9010"];
                        Denominacion = (string)Data_Reader["des_9010"];
                        Serie = (string)Data_Reader["sdo_9010"];
                        Semaforo = (byte)Data_Reader["smp_9010"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string, byte>(Id_9010, Denominacion, Serie, Semaforo);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE MOVIMIENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010">Identificador registro 'TIPOS DE DOCUMENTOS'</param>
        /// <param name="Tipo_Movimiento">Tipo de movimiento</param>
        /// <returns>
        /// Item1: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS' [ide_9010_1]<br/>
        /// Item2 : Denominación [des_9010_1]<br/>
        /// Item3 : Año inicio [aiv_9010_1]<br/><br/>
        /// Item4 : Fecha final [afv_9010_1]<br/>
        /// Item5 : Semáforo presentación en cuadros [des_9010_1]
        /// </returns>
        public static Tuple<int, string, short?, short?, byte> Datos_Alccom_9010_1(string CadenaConexion, string Bdd_Comunes, int Pid_9010, string Tipo_Movimiento)
        {
            int Id_9010_1 = 0;
            string Denominacion = "";
            short? Inicio = null;
            short? Final = null;
            byte Semaforo = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010_1, des_9010_1, aiv_9010_1, afv_9010_1, spt_9010_1";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1";
            string Restriccion_Tipo = $"tmo_9010_1 = '{Tipo_Movimiento}'";
            string Restriccion_Identificador = $"ide_9010 = {Pid_9010}";
            string Cadena_Where = $" WHERE {Restriccion_Tipo} AND {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010_1 = (int)Data_Reader["ide_9010_1"];
                        Denominacion = (string)Data_Reader["des_9010_1"];
                        Inicio = Data_Reader["aiv_9010_1"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_9010_1"]);
                        Final = Data_Reader["afv_9010_1"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_9010_1"]);
                        Semaforo = (byte)Data_Reader["spt_9010_1"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, short?, short?, byte>(Id_9010_1, Denominacion, Inicio, Final, Semaforo);
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'</param>
        /// <returns>
        /// Item1: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS' [ide_9010_1]<br/>
        /// Item2: Identificador registro 'TIPOS DE DOCUMENTOS' [ide_9010]<br/>
        /// Item3: Código 'TIPOS DE MOVIMIENTOS' [tmo_9010_1]<br/>
        /// Item4 : Denominación [des_9010_1]<br/>
        /// Item5 : Año inicio [aiv_9010_1]<br/><br/>
        /// Item6 : Fecha final [afv_9010_1]<br/>
        /// Item7 : Semáforo presentación en cuadros [des_9010_1] 
        /// </returns>
        public static Tuple<int, int, string, string, short?, short?, byte> Datos_Alccom_9010_1(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1)
        {
            int Id_9010_1 = 0;
            int Id_9010 = 0;
            string Tipo_Movimiento = "";
            string Denominacion = "";
            short? Inicio = null;
            short? Final = null;
            byte Semaforo = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010_1, ide_9010,  tmo_9010_1,  des_9010_1, aiv_9010_1, afv_9010_1, spt_9010_1";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1";
            string Restriccion_Identificador = $"ide_9010_1 = {Pid_9010_1}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010_1 = (int)Data_Reader["ide_9010_1"];
                        Id_9010 = (int)Data_Reader["ide_9010"];
                        Tipo_Movimiento = (string)Data_Reader["tmo_9010_1"];
                        Denominacion = (string)Data_Reader["des_9010_1"];
                        Inicio = Data_Reader["aiv_9010_1"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["aiv_9010_1"]);
                        Final = Data_Reader["afv_9010_1"] == DBNull.Value ? null : (short?)Convert.ToInt16(Data_Reader["afv_9010_1"]);
                        Semaforo = (byte)Data_Reader["spt_9010_1"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int, string, string, short?, short?, byte>(Id_9010_1, Id_9010, Tipo_Movimiento, Denominacion, Inicio, Final, Semaforo);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS'</param>
        /// <returns>
        /// Item1: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. APUNTES CONTABLES ASOCIADOS' [ide_9010_1_1]<br/>
        /// Item2: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS' [ide_9010_1]<br/>
        /// Item3: Cuenta contable  [cta_9010_1_1]<br/>
        /// Item4 : Debe o haber [doh_9010_1_1]<br/>
        /// Item5 : Tipo de cuenta [tcu_9010_1_1]<br/><br/>
        /// Item6 : Signo [sig_9010_1_1]<br/>
        /// 1 - Positivo<br/>
        /// -1 - Negativo
        /// </returns>
        public static Tuple<int, int, string, string, byte, short> Datos_Alccom_9010_1_1(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1_1)
        {
            int Id_9010_1_1 = 0;
            int Id_9010_1 = 0;
            string Cuenta = "";
            string Debe_Haber = "";
            byte Tipo_Cuenta = 0;
            short Signo = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010_1_1, ide_9010_1, cta_9010_1_1, doh_9010_1_1, tcu_9010_1_1, sig_9010_1_1";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1_1";
            string Restriccion_Identificador = $"ide_9010_1_1 = {Pid_9010_1_1}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010_1_1 = (int)Data_Reader["ide_9010_1_1"];
                        Id_9010_1 = (int)Data_Reader["ide_9010_1"];
                        Cuenta = (string)Data_Reader["cta_9010_1_1"];
                        Debe_Haber = (string)Data_Reader["doh_9010_1_1"];
                        Tipo_Cuenta = (byte)Data_Reader["tcu_9010_1_1"];
                        Signo = Convert.ToInt16(Data_Reader["sig_9010_1_1"]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int, string, string, byte, short>(Id_9010_1_1, Id_9010_1, Cuenta, Debe_Haber, Tipo_Cuenta, Signo);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1_2">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'</param>
        /// <returns>
        /// Item1: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS' [ide_9010_1_2]<br/>
        /// Item2: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS' [ide_9010_1]<br/>
        /// Item3: Identificador registro 'FIRMANTES' [ide_9000]<br/>
        /// Item4: Código firmante  [cod_9010_1_2]<br/>
        /// Item5 : Posicion firma [ord_9010_1_2]<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// </returns>
        public static Tuple<int, int, int, short, byte> Datos_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1_2)
        {
            int Id_9010_1_2 = 0;
            int Id_9010_1 = 0;
            int Id_9000 = 0;
            short Codigo = 0;
            byte Posicion = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010_1_2, ide_9010_1, ide_9000, cod_9010_1_2, ord_9010_1_2";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1_2";
            string Restriccion_Identificador = $"ide_9010_1_2 = {Pid_9010_1_2}";
            string Cadena_Where = $" WHERE {Restriccion_Identificador}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010_1_2 = (int)Data_Reader["ide_9010_1_2"];
                        Id_9010_1 = (int)Data_Reader["ide_9010_1"];
                        Id_9000 = (int)Data_Reader["ide_9000"];
                        Codigo = (short)Data_Reader["cod_9010_1_2"];
                        Posicion = (byte)Data_Reader["ord_9010_1_2"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int, int, short, byte>(Id_9010_1_2, Id_9010_1, Id_9000, Codigo, Posicion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'</param>
        /// <param name="Ubicacion">Ubicación  de la firma<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// </param>
        /// <returns>
        /// Item1: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS' [ide_9010_1_2]<br/>
        /// Item2: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS' [ide_9010_1]<br/>
        /// Item3: Identificador registro 'FIRMANTES' [ide_9000]<br/>
        /// Item4: Código firmante  [cod_9010_1_2]<br/>
        /// Item5 : Posicion firma [ord_9010_1_2]<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// </returns>
        public static Tuple<int, int, int, short, byte> Datos_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1, byte Ubicacion)
        {
            int Id_9010_1_2 = 0;
            int Id_9010_1 = 0;
            int Id_9000 = 0;
            short Codigo = 0;
            byte Posicion = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010_1_2, ide_9010_1, ide_9000, cod_9010_1_2, ord_9010_1_2";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1_2";
            string Restriccion_Movimiento = $"ide_9010_1 = {Pid_9010_1}";
            string Restriccion_Posicion = $"ord_9010_1_2 = {Ubicacion}";
            string Cadena_Where = $" WHERE {Restriccion_Movimiento} AND {Restriccion_Posicion}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010_1_2 = (int)Data_Reader["ide_9010_1_2"];
                        Id_9010_1 = (int)Data_Reader["ide_9010_1"];
                        Id_9000 = (int)Data_Reader["ide_9000"];
                        Codigo = (short)Data_Reader["cod_9010_1_2"];
                        Posicion = (byte)Data_Reader["ord_9010_1_2"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int, int, short, byte>(Id_9010_1_2, Id_9010_1, Id_9000, Codigo, Posicion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Pid_9010_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS'</param>
        /// <param name="Firmante">Código firmante</param>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// </param>
        /// <returns>
        /// Item1: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS' [ide_9010_1_2]<br/>
        /// Item2: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS' [ide_9010_1]<br/>
        /// Item3: Identificador registro 'FIRMANTES' [ide_9000]<br/>
        /// Item4: Código firmante  [cod_9010_1_2]<br/>
        /// Item5 : Posicion firma [ord_9010_1_2]<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// </returns>
        public static Tuple<int, int, int, short, byte> Datos_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1, short Firmante)
        {
            int Id_9010_1_2 = 0;
            int Id_9010_1 = 0;
            int Id_9000 = 0;
            short Codigo = 0;
            byte Posicion = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010_1_2, ide_9010_1, ide_9000, cod_9010_1_2, ord_9010_1_2";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1_2";
            string Restriccion_Movimiento = $"ide_9010_1 = {Pid_9010_1}";
            string Restriccion_Firmante = $"cod_9000 = {Firmante}";
            string Cadena_Where = $" WHERE {Restriccion_Movimiento} AND {Restriccion_Firmante}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010_1_2 = (int)Data_Reader["ide_9010_1_2"];
                        Id_9010_1 = (int)Data_Reader["ide_9010_1"];
                        Id_9000 = (int)Data_Reader["ide_9000"];
                        Codigo = (short)Data_Reader["cod_9010_1_2"];
                        Posicion = (byte)Data_Reader["ord_9010_1_2"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int, int, short, byte>(Id_9010_1_2, Id_9010_1, Id_9000, Codigo, Posicion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Firmante">Código firmante</param>
        /// <param name="Ubicacion">Ubicación  de la firma<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// </param>
        /// <returns>
        /// Item1: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS' [ide_9010_1_2]<br/>
        /// Item2: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS' [ide_9010_1]<br/>
        /// Item3: Identificador registro 'FIRMANTES' [ide_9000]<br/>
        /// Item4: Código firmante  [cod_9010_1_2]<br/>
        /// Item5 : Posicion firma [ord_9010_1_2]<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// </returns>
        public static Tuple<int, int, int, short, byte> Datos_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, short Firmante, byte Ubicacion)
        {
            int Id_9010_1_2 = 0;
            int Id_9010_1 = 0;
            int Id_9000 = 0;
            short Codigo = 0;
            byte Posicion = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010_1_2, ide_9010_1, ide_9000, cod_9010_1_2, ord_9010_1_2";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1_2";
            string Restriccion_Firmante = $"cod_9000 = {Firmante}";
            string Restriccion_Posicion = $"ord_9010_1_2 = {Ubicacion}";
            string Cadena_Where = $" WHERE {Restriccion_Firmante} AND {Restriccion_Posicion}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010_1_2 = (int)Data_Reader["ide_9010_1_2"];
                        Id_9010_1 = (int)Data_Reader["ide_9010_1"];
                        Id_9000 = (int)Data_Reader["ide_9000"];
                        Codigo = (short)Data_Reader["cod_9010_1_2"];
                        Posicion = (byte)Data_Reader["ord_9010_1_2"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, int, int, short, byte>(Id_9010_1_2, Id_9010_1, Id_9000, Codigo, Posicion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene datos 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Firmante">Código firmante</param>
        /// <param name="Ubicacion">Ubicación  de la firma<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// </param>
        /// <returns>
        /// Item1: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS' [ide_9010_1_2]<br/>
        /// Item2: Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS' [ide_9010_1]<br/>
        /// Item3: Código firmante  [cod_9000]<br/>
        /// Item4 : Posicion firma [ord_9010_1_2]<br/>
        /// 1 - Izquierda<br/>
        /// 2 - Centro<br/>
        /// 3 - Derecha
        /// </returns>
        public static Tuple<int, short, byte, string, string, DateTime?[]> Datos_v_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, int Pid_9010_1, int Pid_9000)
        {
            int Id_9010_1_2 = 0;
            short Codigo = 0;
            byte Posicion = 0;
            string Nombre = "";
            string Cargo = "";
            DateTime?[] Validez = { null, null };

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010_1_2, cod_9000, ord_9010_1_2, Funciones_Generales.dbo.Normaliza_Nombre_Apellidos(nom_9000, ap1_9000 + ' ' + ap2_9000) nomape, car_9000, fiv_9000, ffv_9000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_v_9010_1_2";
            string Restriccion_Tipo = $"ide_9010_1 = {Pid_9010_1}";
            string Restriccion_Firmante = $"ide_9000= {Pid_9000}";
            string Cadena_Where = $" WHERE {Restriccion_Tipo} AND {Restriccion_Firmante}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_9010_1_2 = (int)Data_Reader["ide_9010_1_2"];
                        Codigo = (short)Data_Reader["cod_9000"];
                        Posicion = (byte)Data_Reader["ord_9010_1_2"];
                        Nombre = (string)Data_Reader["nomape"];
                        Cargo = (string)Data_Reader["car_9000"];

                        for (byte i = 0; i < Validez.Length; i++)
                            Validez[i] = Data_Reader[5 + i] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(Data_Reader[5 + i]);
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, short, byte, string, string, DateTime?[]>(Id_9010_1_2, Codigo, Posicion, Nombre, Cargo, Validez);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Genera un DATATABLE con las cuentas del asiento automático 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. ASIENTOS PREDEFINIDOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Id_9010_1_1">Identificador registro 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. ASIENTOS PREDEFINIDOS'</param>
        /// <returns>Tabla con las cuentas del asiento automático</returns>
        public static DataTable Generar_Asiento_Automatico(string CadenaConexion, string Bdd_Comunes, int Id_9010_1_1)
        {
            string Cadena_Sql;
            string Cadena_Select = "SELECT cta_9010_1_1 codcta, doh_9010_1_1 debhab, sig_9010_1_1 sigcta";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1_1";
            string Restriccion_Identificador = $"ide_9010_1 =  {Id_9010_1_1}";
            string Cadena_Restricciones = Restriccion_Identificador;
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    DataTable Data_Table = new DataTable();
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();
                    Data_Table.Load(Data_Reader);
                    Data_Reader.Close();

                    return Data_Table;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'HISTÓRICO. CUENTAS CONTABLES. MAYOR'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Cuenta_Contable">Código nivel</param>
        /// <returns>Identificador registro 'HISTÓRICO. CUENTAS CONTABLES. MAYOR'</returns>
        public static int Identificador_Alccom_1001(string CadenaConexion, string Bdd_Comunes, string Cuenta_Contable)
        {
            int Id_1001 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_1001";
            string Restriccion_Cuenta = $"cta_1001 = '{Cuenta_Contable}'";
            string Cadena_Where = $" WHERE {Restriccion_Cuenta}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1001 = (int)Data_Reader["ide_1001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1001;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'HISTÓRICO. CUENTAS CONTABLES. MAYOR'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Cuenta_Contable">Código nivel</param>
        /// <param name="Restriccion_Busqueda">Restricción de parametrizada</param>
        /// <returns>Identificador registro 'HISTÓRICO. CUENTAS CONTABLES. MAYOR'</returns>
        public static int Identificador_Alccom_1001(string CadenaConexion, string Bdd_Comunes, string Cuenta_Contable, string Restriccion_Busqueda)
        {
            int Id_1001 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1001";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_1001";
            string Restriccion_Cuenta = $"cta_1001 = '{Cuenta_Contable}'";
            string Cadena_Where = $" WHERE {Restriccion_Cuenta} AND {Restriccion_Busqueda}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1001 = (int)Data_Reader["ide_1001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1001;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'NIVELES ORGÁNICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Nivel">Código nivel</param>
        /// <returns>Identificador registro 'NIVELES ORGÁNICOS'</returns>
        public static short Identificador_Alccom_5000(string CadenaConexion, short Ejercicio, string Bdd_Comunes, byte Nivel)
        {
            short Id_5000 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5000";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5000";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5000, 0) AND ISNULL (afv_5000, 9999)";
            string Restriccion_Nivel = $"niv_5000 = {Nivel}";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Nivel}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_5000 = (Int16)Data_Reader["ide_5000"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_5000;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'RAZONES DISCONFORMIDAD/REPARO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Fecha_Actual">Fecha validación</param>
        /// <returns>Identificador registro  'RAZONES DISCONFORMIDAD/REPARO'</returns>
        public static int Identificador_Alccom_2050(string CadenaConexion, string Bdd_Comunes, DateTime Fecha_Actual)
        {
            int Id_2050 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2050";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2050";
            string Restriccion_Validez = $"'{Fecha_Actual:dd/MM/yyyy}' BETWEEN ISNULL (fvi_2050, '01/01/1800') AND ISNULL (fvf_2050, '31/12/2500')";
            string Cadena_Where = $" WHERE {Restriccion_Validez}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_2050 = (int)Data_Reader["ide_2050"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_2050;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'NIVELES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Nivel">Código nivel</param>
        /// <returns>Identificador registro 'NIVELES FUNCIONALES'</returns>
        public static short Identificador_Alccom_5001(string CadenaConexion, short Ejercicio, string Bdd_Comunes, byte Nivel)
        {
            short Id_5001 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5001";
            string Cadena_From = " FROM " + Bdd_Comunes + ".dbo.alccom_5001";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5001, 0) AND ISNULL (afv_5001, 9999)";
            string Restriccion_Nivel = $"niv_5001 = {Nivel}";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Nivel}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_5001 = (short)Data_Reader["ide_5001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_5001;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'NIVELES ECONÓMICOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Nivel">Código nivel</param>
        /// <returns>Identificador registro 'NIVELES ECONÓMICOS'</returns>
        public static short Identificador_Alccom_5002(string CadenaConexion, short Ejercicio, string Bdd_Comunes, byte Nivel)
        {
            short Id_5002 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5002";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5002";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5002, 0) AND ISNULL (afv_5002, 9999)";
            string Restriccion_Nivel = $"niv_5002 = {Nivel}";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Nivel}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_5002 = (short)Data_Reader["ide_5002"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_5002;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'AGRUPACIONES NO PRESUPUESTARIAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Agrupacion">Código agrupación</param>
        /// <returns>Identificador registro 'AGRUPACIONES NO PRESUPUESTARIAS'</returns>
        public static short Identificador_Alccom_5010(string CadenaConexion, short Ejercicio, string Bdd_Comunes, string Agrupacion)
        {
            short Id_5010 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5010";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5010, 0) AND ISNULL (afv_5010, {Ejercicio})";
            string Restriccion_Codigo = $"agr_5010 = '{Agrupacion}'";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Codigo}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_5010 = (short)Data_Reader["ide_5010"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_5010;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'AGRUPACIONES NO PRESUPUESTARIAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param> 
        /// <param name="Pid_5010">Identificador registro 'AGRUPACIONES NO PRESUPUESTARIAS'</param>
        /// <returns>Identificador registro 'AGRUPACIONES NO PRESUPUESTARIAS'</returns>
        public static short Identificador_Alccom_5010(string CadenaConexion, short Ejercicio, string Bdd_Comunes, short Pid_5010)
        {
            short Id_5010 = 0;

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_5010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5010";
            string Restriccion_Validez = $"{Ejercicio} BETWEEN ISNULL (aiv_5010, 0) AND ISNULL (afv_5010, {Ejercicio})";
            string Restriccion_Identificador = $"ide_5010 = {Pid_5010}";
            string Cadena_Where = $" WHERE {Restriccion_Validez} AND {Restriccion_Identificador}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_5010 = (short)Data_Reader["ide_5010"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_5010;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador del registro 'TIPOS DE DOCUMENTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param> 
        /// <param name="Tipo_Documento">Tipo documento</param>
        /// <param name="Campo_Busqueda">Campo de búsqueda por defecto</param>
        /// <returns>Identificador del registro 'TIPOS DE DOCUMENTOS'</returns>
        public static int Identificador_Alccom_9010(string CadenaConexion, string Bdd_Comunes, string Tipo_Documento, string Campo_Busqueda = "tdo_9010")
        {
            int Id_9010 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010";
            string Restriccion_Tipo = $"{Campo_Busqueda} = '{Tipo_Documento}'";
            string Cadena_Where = " WHERE " + Restriccion_Tipo;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_9010 = (int)Data_Reader["ide_9010"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_9010;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador del tipo de documento
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param> 
        /// <param name="Id_9010">Identificador tipo de documento</param>
        /// <param name="Tipo_Movimiento">Cadena de conexión</param>
        /// <returns>Identificador del registro</returns>
        public static int Identificador_Alccom_9010_1(string CadenaConexion, string Bdd_Comunes, int Id_9010, string Tipo_Movimiento)
        {
            int Id_9010_1 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_9010_1";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1";
            string Restriccion_Tipo = $"ide_9010  = {Id_9010}";
            string Restriccion_Movimiento = $"tmo_9010_1  = '{Tipo_Movimiento}'";
            string Cadena_Where = $" WHERE {Restriccion_Tipo} AND {Restriccion_Movimiento}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_9010_1 = (int)Data_Reader["ide_9010_1"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_9010_1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el número ocurrencias que existen en las tablas
        /// · Balance de situación
        /// · Cuenta del resultado económico-patrimonial
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param> 
        /// <param name="Cuenta">Cuenta contable</param>
        /// <returns>Número ocurrecias</returns>
        public static int Ocurrencias_Alccom_1001(string CadenaConexion, string Bdd_Comunes, string Cuenta)
        {
            int Ocurrencias = 0;

            string Cadena_Sql;
            string Cadena_Select;
            string[] Cadena_From = new string[6];
            string Cadena_Where;
            string Cadena_Group;

            Cadena_Select = "SELECT COUNT (alccom_1001.ide_1001) totreg";
            Cadena_From[0] = $" FROM {Bdd_Comunes}.dbo.alccom_1001 INNER JOIN {Bdd_Comunes}.dbo.alccom_1002_1_1 ON alccom_1001.ide_1001 = alccom_1002_1_1.ide_1001";
            Cadena_From[1] = $" FROM {Bdd_Comunes}.dbo.alccom_1001 INNER JOIN {Bdd_Comunes}.dbo.alccom_1002_1_2 ON alccom_1001.ide_1001 = alccom_1002_1_2.ide_1001";
            Cadena_From[2] = $" FROM {Bdd_Comunes}.dbo.alccom_1001 INNER JOIN {Bdd_Comunes}.dbo.alccom_1002_1_3 ON alccom_1001.ide_1001 = alccom_1002_1_3.ide_1001";
            Cadena_From[3] = $" FROM {Bdd_Comunes}.dbo.alccom_1001 INNER JOIN {Bdd_Comunes}.dbo.alccom_1003_1_1 ON alccom_1001.ide_1001 = alccom_1003_1_1.ide_1001";
            Cadena_From[4] = $" FROM {Bdd_Comunes}.dbo.alccom_1001 INNER JOIN {Bdd_Comunes}.dbo.alccom_1003_1_2 ON alccom_1001.ide_1001 = alccom_1003_1_2.ide_1001";
            Cadena_From[5] = $" FROM {Bdd_Comunes}.dbo.alccom_1001 INNER JOIN {Bdd_Comunes}.dbo.alccom_1003_1_3 ON alccom_1001.ide_1001 = alccom_1003_1_3.ide_1001";

            Cadena_Where = $" WHERE cta_1001 = '{Cuenta}'";
            Cadena_Group = " GROUP BY cta_1001";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    for (byte i = 0; i < Cadena_From.Length; i++)
                    {
                        Cadena_Sql = Cadena_Select + Cadena_From[i] + Cadena_Where + Cadena_Group;

                        Comando_Sql.CommandText = Cadena_Sql;
                        Data_Reader = Comando_Sql.ExecuteReader();

                        if (Data_Reader.Read())
                            Ocurrencias += (int)Data_Reader["totreg"];

                        Data_Reader.Close();

                        if (Ocurrencias != 0)
                            break;
                    }

                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Ocurrencias;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene número de ocurrencias del código en 'TIPOS DE DOCUMENTOS. TIPOS DE MOVIMIENTOS. FIRMAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Codigo">Código firmante</param>
        /// <returns>Número de ocurrencias</returns>
        public static int Ocurrencias_Alccom_9010_1_2(string CadenaConexion, string Bdd_Comunes, short Codigo)
        {
            int Ocurrencias;
            string Cadena_Sql;
            string Cadena_Select = "SELECT COUNT (*) numreg";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_9010_1_2";
            string Restriccion_Codigo = $"cod_9010_1_2 = {Codigo}";
            string Cadena_Where = $" WHERE {Restriccion_Codigo}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Ocurrencias = Data_Reader.Read() ? (int)Data_Reader["numreg"] : 0;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Ocurrencias;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica la existencia de niveles por encima del actual
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param> 
        /// <param name="Pid_2050">Identificador registro 'GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO'</param>
        /// <param name="Codigo_Motivo">Cuenta contable</param>
        /// <returns>Existencia de niveles por encima del actual</returns>
        public static bool Niveles_Alccom_2050_1(string CadenaConexion, string Bdd_Comunes, int Pid_2050, string Codigo_Motivo)
        {
            bool Existe;

            string Restriccion_Identificador = $"ide_2050 = {Pid_2050}";
            string Restriccion_Posteriores = $"CHARINDEX ('{Codigo_Motivo.Trim()}', tdr_2050_1) = 1";
            string Restriccion_Codigo = $"tdr_2050_1 != '{Codigo_Motivo.Trim()}'";

            string Cadena_Sql;
            string Cadena_Select = "SELECT tdr_2050_1";
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_2050_1";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From} WHERE {Restriccion_Identificador} AND {Restriccion_Posteriores} AND {Restriccion_Codigo}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Asigna valores Accsibilidad y ToolTip
        /// </summary>
        /// <param name="Acceso"></param>
        /// <param name="ToolTip_Seguex"></param>
        public static void Asignar_ToolTip_Seguex(Panel_Seguex Seguex, byte[] Acceso, string[] ToolTip_Seguex)
        {
            string Seguex_ToolTip;
            string Nombre_Control;
            Control[] Cuadro_Texto;
            txtTextoGeneral Texto;

            for (byte i = 0; i < Seguex.Controls.Count; i++)
            {
                Nombre_Control = "Seguex" + Convert.ToString(i + 1);
                Cuadro_Texto = Seguex.Controls.Find(Nombre_Control, false);

                if (Cuadro_Texto.Length > 0)
                {
                    Seguex_ToolTip = ToolTip_Seguex[i];
                    Texto = (txtTextoGeneral)Cuadro_Texto[0];

                    //         ttpFormulario.SetToolTip(Texto, Seguex_ToolTip);
                    Texto.Enabled = Acceso[i] != 0;
                }
            }
        }

        /// <summary>
        /// Verifica  el siguiente número de la secuencia
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Secuencia">Nombre de la secuencia</param>
        /// <param name="Id_Funcion">Identificador de funcion a utilizar<br/>
        /// 1 - Código forma de pago/cobro<br/>
        /// </param>
        /// <returns>Numero de secuencia válido</returns>
        public static int Verificar_Secuencia(string CadenaConexion, string Bdd_Comunes, string Secuencia, byte Id_Funcion)
        {
            bool Secuencia_Valida = false;
            int Numero_Secuencia = 0;
            int Id_Secuencia = 0;

            while (!Secuencia_Valida)
            {
                Numero_Secuencia = Secuencias.Siguiente_Secuencia(CadenaConexion, Bdd_Comunes, Secuencia);

                if (Numero_Secuencia != 0)
                {
                    switch (Id_Funcion)
                    {
                        case 1:             // NÚMERO DE DOCUMENTO CONTABLE
                            Id_Secuencia = Datos_Alccom_2000(CadenaConexion, Bdd_Comunes, Convert.ToInt16(Numero_Secuencia)).Item1;

                            break;
                    }

                    if (Id_Secuencia == 0)
                        Secuencia_Valida = true;

                }
            }

            return Numero_Secuencia;
        }

        /// <summary>
        /// Verifica  el siguiente número de la secuencia (short)
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Secuencia">Nombre de la secuencia</param>
        /// <param name="Id_Funcion">Identificador de funcion a utilizar<br/>
        /// 1 - Código formas de pago/cobro [cod_2000]<br/>
        /// 2 - Código canales de pago/cobro [cod_2010]<br/>
        /// 3 - Código tipos de facturas [tdf_2020]<br/>
        /// 4 - Código tipos de facturas [tco_2030]<br/>
        /// 5 - Código tipos de sesiones [cod_2040]
        /// 6 - Código grupos disconformidad/reparo [cod_2050]
        /// </param>
        /// <returns>Numero de secuencia válido</returns>
        public static short Verificar_Secuencia_Short(string CadenaConexion, string Bdd_Comunes, string Secuencia, byte Id_Funcion)
        {
            bool Secuencia_Valida = false;
            short Numero_Secuencia = 0;
            int Id_Registro = 0;

            while (!Secuencia_Valida)
            {
                Numero_Secuencia = Secuencias.Siguiente_Secuencia_Short(CadenaConexion, Bdd_Comunes, Secuencia);

                if (Numero_Secuencia != 0)
                {
                    switch (Id_Funcion)
                    {
                        case 1:             // CÓDIGO FORMAS DE PAGO Y COBRO
                            Id_Registro = Datos_Alccom_2000(CadenaConexion, Bdd_Comunes, Numero_Secuencia).Item1;

                            break;

                        case 2:             // CÓDIGO CANALES DE PAGO Y COBRO
                            Id_Registro = Datos_Alccom_2010(CadenaConexion, Bdd_Comunes, Numero_Secuencia).Item1;

                            break;

                        case 3:             // CÓDIGO TIPOS DE FACTURAS
                            Id_Registro = Datos_Alccom_2020(CadenaConexion, Bdd_Comunes, Numero_Secuencia).Item1;

                            break;

                        case 4:             // CÓDIGO TIPOS DE CONTRATOS
                            Id_Registro = Datos_Alccom_2030(CadenaConexion, Bdd_Comunes, Numero_Secuencia).Item1;

                            break;

                        case 5:             // CÓDIGO TIPOS DE SESIONES
                            Id_Registro = Datos_Alccom_2040(CadenaConexion, Bdd_Comunes, Numero_Secuencia).Item1;

                            break;

                        case 6:             // CÓDIGO GRUPOS DE MOTIVOS DE DISCONFORMIDAD/REPARO
                            Id_Registro = Datos_Alccom_2050(CadenaConexion, Bdd_Comunes, Numero_Secuencia).Item1;

                            break;
                    }

                    if (Id_Registro == 0)
                        Secuencia_Valida = true;

                }
            }

            return Numero_Secuencia;
        }

        /// <summary>
        /// Verifica si existe el identificador de registro 'COMUNES. NIVELES DE VINCULACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD'</param>
        /// <param name="Pid_Nivel">Identificador registro 'COMUNES. NIVELES PRESUPUESTARIOS'</param>
        /// <param name="Tipo">Tipo de selección<br/>
        /// · 0 - Niveles orgánicos<br/>
        /// · 1 - Niveles funcionales<br/>
        /// · 2 - Niveles económicos
        ///'</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Ocurrencias_Alccom_5003(string CadenaConexion, string Bdd_Comunes, short Pid_Nivel, byte Tipo)
        {
            bool Existe;
            string Cadena_Sql;
            string[] Cadena_Select = { "SELECT TOP (1) ide_5000", "SELECT TOP (1) ide_5001", "SELECT TOP (1) ide_5002" };
            string Cadena_From = $" FROM {Bdd_Comunes}.dbo.alccom_5003";
            string[] Restriccion_Identificador = { $"ide_5000 = {Pid_Nivel}", $"ide_5001 = {Pid_Nivel}", $"ide_5002 = {Pid_Nivel}" };
            string Cadena_Where = $" WHERE {Restriccion_Identificador[Tipo]}";

            Cadena_Sql = $"{Cadena_Select[Tipo]}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

    }

}