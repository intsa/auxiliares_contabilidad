﻿namespace Comunes_Contabilidad.Controles
{
    partial class Panel_Seguex
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Label_Seguex = new ClaseIntsa.Controles.lblLabelGeneral(this.components);
            this.Seguex1 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Seguex2 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Seguex3 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Seguex4 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            this.Seguex5 = new ClaseIntsa.Controles.txtTextoGeneral(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Label_Seguex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex5)).BeginInit();
            this.SuspendLayout();
            // 
            // Label_Seguex
            // 
            this.Label_Seguex.AutoSize = true;
            this.Label_Seguex.BackColor = System.Drawing.Color.Transparent;
            this.Label_Seguex.CausesValidation = false;
            this.Label_Seguex.Depth = 0;
            this.Label_Seguex.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.Label_Seguex.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(100)))), ((int)(((byte)(255)))));
            this.Label_Seguex.FormatoCorporativo = false;
            this.Label_Seguex.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label_Seguex.Location = new System.Drawing.Point(0, 0);
            this.Label_Seguex.Margin = new System.Windows.Forms.Padding(0);
            this.Label_Seguex.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Label_Seguex.Name = "Label_Seguex";
            this.Label_Seguex.Size = new System.Drawing.Size(171, 15);
            this.Label_Seguex.TabIndex = 5;
            this.Label_Seguex.Text = "Referencia expediente SEGUEX";
            this.Label_Seguex.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Seguex1
            // 
            this.Seguex1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Seguex1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Seguex1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Seguex1.BusquedaF8 = false;
            this.Seguex1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Seguex1.Depth = 0;
            this.Seguex1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Seguex1.ForeColor = System.Drawing.Color.Green;
            this.Seguex1.GridNumero = ((byte)(0));
            this.Seguex1.Location = new System.Drawing.Point(0, 25);
            this.Seguex1.MaxLength = 10;
            this.Seguex1.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Seguex1.Multiline = false;
            this.Seguex1.Name = "Seguex1";
            this.Seguex1.PasswordChar = '\0';
            this.Seguex1.ReadOnly = false;
            this.Seguex1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Seguex1.SelectedText = "";
            this.Seguex1.SelectionLength = 0;
            this.Seguex1.SelectionStart = 0;
            this.Seguex1.Size = new System.Drawing.Size(40, 23);
            this.Seguex1.TabIndex = 0;
            this.Seguex1.TabStop = false;
            this.Seguex1.Tag = "";
            this.Seguex1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Seguex1.UseSystemPasswordChar = false;
            this.Seguex1.Enter += new System.EventHandler(this.Seguex1_Enter);
            this.Seguex1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Seguex1_KeyPress);
            this.Seguex1.Validating += new System.ComponentModel.CancelEventHandler(this.Seguex1_Validating);
            // 
            // Seguex2
            // 
            this.Seguex2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Seguex2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Seguex2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Seguex2.BusquedaF8 = false;
            this.Seguex2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Seguex2.Depth = 0;
            this.Seguex2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Seguex2.ForeColor = System.Drawing.Color.Green;
            this.Seguex2.GridNumero = ((byte)(0));
            this.Seguex2.Location = new System.Drawing.Point(45, 25);
            this.Seguex2.MaxLength = 10;
            this.Seguex2.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Seguex2.Multiline = false;
            this.Seguex2.Name = "Seguex2";
            this.Seguex2.PasswordChar = '\0';
            this.Seguex2.ReadOnly = false;
            this.Seguex2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Seguex2.SelectedText = "";
            this.Seguex2.SelectionLength = 0;
            this.Seguex2.SelectionStart = 0;
            this.Seguex2.Size = new System.Drawing.Size(130, 23);
            this.Seguex2.TabIndex = 1;
            this.Seguex2.TabStop = false;
            this.Seguex2.Tag = "";
            this.Seguex2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Seguex2.UseSystemPasswordChar = false;
            this.Seguex2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Seguex2_KeyPress);
            this.Seguex2.Validating += new System.ComponentModel.CancelEventHandler(this.Seguex2_Validating);
            // 
            // Seguex3
            // 
            this.Seguex3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Seguex3.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Seguex3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Seguex3.BusquedaF8 = false;
            this.Seguex3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Seguex3.Depth = 0;
            this.Seguex3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Seguex3.ForeColor = System.Drawing.Color.Green;
            this.Seguex3.GridNumero = ((byte)(0));
            this.Seguex3.Location = new System.Drawing.Point(180, 25);
            this.Seguex3.MaxLength = 10;
            this.Seguex3.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Seguex3.Multiline = false;
            this.Seguex3.Name = "Seguex3";
            this.Seguex3.PasswordChar = '\0';
            this.Seguex3.ReadOnly = false;
            this.Seguex3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Seguex3.SelectedText = "";
            this.Seguex3.SelectionLength = 0;
            this.Seguex3.SelectionStart = 0;
            this.Seguex3.Size = new System.Drawing.Size(80, 23);
            this.Seguex3.TabIndex = 2;
            this.Seguex3.TabStop = false;
            this.Seguex3.Tag = "";
            this.Seguex3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Seguex3.UseSystemPasswordChar = false;
            this.Seguex3.Validating += new System.ComponentModel.CancelEventHandler(this.Seguex3_Validating);
            // 
            // Seguex4
            // 
            this.Seguex4.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Seguex4.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Seguex4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Seguex4.BusquedaF8 = false;
            this.Seguex4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Seguex4.Depth = 0;
            this.Seguex4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Seguex4.ForeColor = System.Drawing.Color.Green;
            this.Seguex4.GridNumero = ((byte)(0));
            this.Seguex4.Location = new System.Drawing.Point(265, 25);
            this.Seguex4.MaxLength = 10;
            this.Seguex4.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Seguex4.Multiline = false;
            this.Seguex4.Name = "Seguex4";
            this.Seguex4.PasswordChar = '\0';
            this.Seguex4.ReadOnly = false;
            this.Seguex4.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Seguex4.SelectedText = "";
            this.Seguex4.SelectionLength = 0;
            this.Seguex4.SelectionStart = 0;
            this.Seguex4.Size = new System.Drawing.Size(80, 23);
            this.Seguex4.TabIndex = 3;
            this.Seguex4.TabStop = false;
            this.Seguex4.Tag = "";
            this.Seguex4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Seguex4.UseSystemPasswordChar = false;
            this.Seguex4.Validating += new System.ComponentModel.CancelEventHandler(this.Seguex4_Validating);
            // 
            // Seguex5
            // 
            this.Seguex5.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.Seguex5.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.Seguex5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Seguex5.BusquedaF8 = false;
            this.Seguex5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Seguex5.Depth = 0;
            this.Seguex5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Seguex5.ForeColor = System.Drawing.Color.Green;
            this.Seguex5.GridNumero = ((byte)(0));
            this.Seguex5.Location = new System.Drawing.Point(350, 25);
            this.Seguex5.MaxLength = 10;
            this.Seguex5.MouseState = ClaseIntsa.Controles_Material.MouseState.HOVER;
            this.Seguex5.Multiline = false;
            this.Seguex5.Name = "Seguex5";
            this.Seguex5.PasswordChar = '\0';
            this.Seguex5.ReadOnly = false;
            this.Seguex5.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Seguex5.SelectedText = "";
            this.Seguex5.SelectionLength = 0;
            this.Seguex5.SelectionStart = 0;
            this.Seguex5.Size = new System.Drawing.Size(80, 23);
            this.Seguex5.TabIndex = 4;
            this.Seguex5.TabStop = false;
            this.Seguex5.Tag = "";
            this.Seguex5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Seguex5.UseSystemPasswordChar = false;
            this.Seguex5.Validating += new System.ComponentModel.CancelEventHandler(this.Seguex5_Validating);
            // 
            // Panel_Seguex
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.Seguex5);
            this.Controls.Add(this.Seguex4);
            this.Controls.Add(this.Seguex3);
            this.Controls.Add(this.Seguex2);
            this.Controls.Add(this.Seguex1);
            this.Controls.Add(this.Label_Seguex);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimumSize = new System.Drawing.Size(430, 50);
            this.Name = "Panel_Seguex";
            this.Size = new System.Drawing.Size(430, 50);
            ((System.ComponentModel.ISupportInitialize)(this.Label_Seguex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Seguex5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ClaseIntsa.Controles.lblLabelGeneral Label_Seguex;
        private ClaseIntsa.Controles.txtTextoGeneral Seguex1;
        private ClaseIntsa.Controles.txtTextoGeneral Seguex2;
        private ClaseIntsa.Controles.txtTextoGeneral Seguex3;
        private ClaseIntsa.Controles.txtTextoGeneral Seguex4;
        private ClaseIntsa.Controles.txtTextoGeneral Seguex5;
    }
}
