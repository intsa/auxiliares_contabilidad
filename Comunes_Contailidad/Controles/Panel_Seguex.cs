﻿using ClaseIntsa.Controles;
using ClaseIntsa.Formularios;
using ClaseIntsa.Funciones;
using ClaseIntsa.Propiedades;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Comunes_Contabilidad.Controles
{
    [ToolboxItem(true)]
    public partial class Panel_Seguex : UserControl
    {
        #region Propiedades privadas 

        ///// <summary>
        ///// Semáforo de validación total del control, activa los 'CancelEventArgs' de los controles secundarios
        ///// </summary>
        //private bool Validacion_Panel { get; set; } = false;

        #endregion

        #region Propiedades protegidas y públicas

        /// <summary>
        /// Datos auxiliares del control
        /// </summary>
        [Description("Datos auxiliares del control)")]
        public string Datos_Auxiliares { get; set; } = "";

        /// <summary>
        /// Ejercicio de trabajo
        /// </summary>
        [Description("Ejercicio de trabajo (Asignarlo para cada formulario)")]
        public short Ejercicio { get; set; } = 0;

        /// <summary>
        /// Control que recibe el foco cuando la validación de controles global no es satisfactoria
        /// </summary>
        public Control Control_Foco { get; set; } = null;

        /// <summary>
        /// Control que recibe el foco cuando la validación de controles global no es satisfactoria
        /// </summary>
        [Description("Resultado de una validación correcta (Referencia SEGUEX normalizada AAAA-DDDDDDDDDD-NNNNNNNNNN-SSSSSSSSSS-VVVVVVVVVV\n · A - Año\n · D - Serie documental\n · N Número de expediente\n · S - Subexpediente\n · V - Volumen")]
        public string Referencia_Normalizada { get; set; } = "";

        #endregion

        #region Propiedades seleccionables 

        /// <summary>
        /// Accesibilidad y asiganción automática de valores
        /// · 0 No accesible
        /// · 1 Obligatoria
        /// - 2 Opcional
        /// · 3 Asignable si no tiene valor
        /// </summary>
        public byte[] Accesibilidad = new byte[5];

        #endregion  Propiedades seleccionables

        /// <summary>
        /// 
        /// </summary>
        public Panel_Seguex()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="container"></param>
        public Panel_Seguex(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        /// <summary>
        /// Crear controles en el panel
        /// </summary>
        protected override void OnCreateControl()
        {
            ttpMensajeObjeto Control = new ttpMensajeObjeto();
            base.OnCreateControl();

            Control.SetToolTip(Seguex1, "Año expediente");
            Control.SetToolTip(Seguex2, "Serie documental");
            Control.SetToolTip(Seguex3, "Número de expediente");
            Control.SetToolTip(Seguex4, "Número de subexpediente");
            Control.SetToolTip(Seguex5, "Número de volumen");
        }

        /// <summary>
        /// Activa/desactiva campos dependiendo de la accesibilidad
        /// </summary>
        public void Asignar_Accesos()
        {
            string Nombre_Control;
            Control[] Cuadro_Texto;
            txtTextoGeneral Texto;

            for (byte i = 0; i < Controls.Count; i++)
            {
                Nombre_Control = "Seguex" + Convert.ToString(i + 1);
                Cuadro_Texto = Controls.Find(Nombre_Control, false);

                if (Cuadro_Texto.Length > 0)
                {
                    Texto = (txtTextoGeneral)Cuadro_Texto[0];
                    Texto.Enabled = Accesibilidad[i] != 0;
                }
            }
        }

        /// <summary>
        /// Genera la referencia normalizada
        /// </summary>
        /// <returns></returns>
        public string Generar_Normalizado()
        {
            string Referencia;
            string[] Seguex;

            Seguex = Asignar_Valores();
            Referencia = Funciones_Genericas._Normalizar_Cadenas(Seguex, '-');

            return Referencia;
        }

        /// <summary>
        /// Limpia los controles del panel
        /// </summary>
        public void Limpiar_Controles(string Ejercicio = "")
        {
            Control[] Cuadro_Texto;
            txtTextoGeneral Texto;

            for (byte i = 0; i < Controls.Count; i++)
            {
                Cuadro_Texto = Controls.Find("Seguex" + Convert.ToString(i + 1), false);

                if (Cuadro_Texto.Length > 0)
                {
                    Texto = (txtTextoGeneral)Cuadro_Texto[0];
                    Texto.Text = "";
                }
            }

            Seguex1.Text = Ejercicio;
        }

        /// <summary>
        /// Asigna un valor a un campo asignable
        /// </summary>
        /// <param name="Nombre_Campo"></param>
        /// <param name="Valor_Campo"></param>
        /// <returns></returns>
        public string Asignar_Valor(string Nombre_Campo, string Valor_Campo)
        {
            string Referencia_Completa;
            Control[] Cuadro_Texto;
            txtTextoGeneral Texto;

            Cuadro_Texto = Controls.Find(Nombre_Campo, false);

            if (Cuadro_Texto.Length > 0)
            {
                Texto = (txtTextoGeneral)Cuadro_Texto[0];

                if (Texto.Text.Trim() == string.Empty)
                    Texto.Text = Valor_Campo;
            }

            Referencia_Completa = Generar_Normalizado();

            return Referencia_Completa;
        }

        /// <summary>
        /// Activa/desactiva controles del contenedor
        /// </summary>
        /// <param name="Semaforo_Activacion"></param>
        public void Activar_Controles(bool Semaforo_Activacion)
        {
            Control[] Cuadro_Texto;
            txtTextoGeneral Texto;

            for (byte i = 0; i < Controls.Count; i++)
            {
                Cuadro_Texto = Controls.Find("Seguex" + Convert.ToString(i + 1), false);

                if (Cuadro_Texto.Length > 0)
                {
                    Texto = (txtTextoGeneral)Cuadro_Texto[0];
                    Texto.Enabled = Accesibilidad[i] != 0 & Semaforo_Activacion;
                }
            }
        }

        /// <summary>
        /// Limpia los controles del panel
        /// </summary>
        public void Presentar_Expediente_Seguex(string Referencia_Seguex)
        {
            string[] Valores = Referencia_Seguex.Split('-');
            Control[] Cuadro_Texto;
            txtTextoGeneral Texto;

            for (byte i = 0; i < Valores.Length; i++)
            {
                Cuadro_Texto = Controls.Find("Seguex" + Convert.ToString(i + 1), false);

                if (Cuadro_Texto.Length > 0)
                {
                    Texto = (txtTextoGeneral)Cuadro_Texto[0];
                    Texto.Text = Valores[i].Trim();
                }
            }
        }

        /// <summary>
        /// Asigna los campos a una matriz para normalizar la referencia
        /// </summary>
        /// <returns></returns>
        private string[] Asignar_Valores()
        {
            string[] Referencia_Seguex = new string[5];
            Control[] Cuadro_Texto;
            txtTextoGeneral Texto;

            for (byte i = 0; i < Controls.Count; i++)
            {
                Cuadro_Texto = Controls.Find("Seguex" + Convert.ToString(i + 1), false);

                if (Cuadro_Texto.Length > 0)
                {
                    Texto = (txtTextoGeneral)Cuadro_Texto[0];
                    Referencia_Seguex[i] = Texto.Text.Trim();
                }
            }

            return Referencia_Seguex;
        }

        /// <summary>
        /// Valida todos los controles activos del control
        /// </summary>
        /// <returns>
        /// Item1 - Si es o no correcta la validación
        /// Item2 - Control incorrecto
        /// Item3 - En caso de que sea correcto referencia del expediente normalizada en caso contrario string.Empty
        /// </returns>
        public Tuple<bool, Control, string> Validar_Control()
        {
            bool Correcto = ValidateChildren(ValidationConstraints.Enabled);

            Referencia_Normalizada = Correcto ? Generar_Normalizado() : "";

            return Tuple.Create<bool, Control, string>(Correcto, Control_Foco, Referencia_Normalizada);
        }

        /// <summary>
        /// Rellenar el año en el caso que este vacio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex1_Enter(object sender, EventArgs e)
        {
            if (Seguex1.Text == string.Empty)
                Seguex1.Text = Ejercicio.ToString();
        }

        /// <summary>
        /// Comprobación de que solo sean números
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar)
                &&
                (e.KeyChar != (char)Keys.Back))
                e.Handled = true;
        }

        /// <summary>
        /// Validación de los dígitos de control del Año
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex1_Validating(object sender, CancelEventArgs e)
        {
            frmFormularioSimple Formulario_Actual = (frmFormularioSimple)FindForm();

            if (Seguex1.Enabled)
            {
                if (Seguex1.Text.Trim() == string.Empty)
                    Seguex1.Text = Ejercicio.ToString();

                if (!(Convert.ToInt16(Seguex1.Text) >= 2010 && Convert.ToInt16(Seguex1.Text) <= Ejercicio))
                {
                    Formulario_Actual.PresentaMensaje($"AÑO. RANGO ERRÓNEO. RANGO 2010 - {Ejercicio}", 2, Color.Red);
                    Control_Foco = Seguex1;
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Comprobación de que solo sean números y letras mayúsculas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar) < 48 && e.KeyChar != 8 && e.KeyChar != 32 || (e.KeyChar) > 57 && (e.KeyChar) < 65 || (e.KeyChar) > 90)
                e.Handled = true;
        }

        /// <summary>
        /// Validación de la serie documental
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex2_Validating(object sender, CancelEventArgs e)
        {
            frmFormularioSimple Formulario_Actual = (frmFormularioSimple)FindForm();

            if (Seguex2.Text.Trim() == string.Empty)
            {
                Formulario_Actual.PresentaMensaje("SERIE DOCUMENTAL. OBLIGATORIA", 3, Color.Red);
                Control_Foco = Seguex2;
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Comprobación de que solo sean números
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar)
                &&
                (e.KeyChar != (char)Keys.Back))
                e.Handled = true;
        }

        /// <summary>
        /// Validación del número de expediente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex3_Validating(object sender, CancelEventArgs e)
        {
            frmFormularioSimple Formulario_Actual = (frmFormularioSimple)FindForm();

            if (Seguex3.Enabled)
            {
                if (Seguex3.Text.Trim() == string.Empty)
                {
                    if (Accesibilidad[2] == 1)
                    {
                        Formulario_Actual.PresentaMensaje("NÚMERO DE EXPEDIENTE. OBLIGATORIO", 3, Color.Red);
                        Control_Foco = Seguex3;
                        e.Cancel = true;
                    }
                }
            }
        }

        /// <summary>
        /// Comprobación de que solo sean números
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar)
                &&
                (e.KeyChar != (char)Keys.Back))
                e.Handled = true;
        }

        /// <summary>
        /// Validación del subexpediente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex4_Validating(object sender, CancelEventArgs e)
        {
            frmFormularioSimple Formulario_Actual = (frmFormularioSimple)FindForm();

            if (Seguex4.Enabled)
            {
                if (Seguex4.Text.Trim() == string.Empty)
                {
                    if (Accesibilidad[3] == 1)
                    {
                        Formulario_Actual.PresentaMensaje("SUBEXPEDIENTE. OBLIGATORIO", 3, Color.Red);
                        Control_Foco = Seguex4;
                        e.Cancel = true;
                    }
                }
            }
        }

        /// <summary>
        /// Comprobación de que solo sean números
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar)
                &&
                (e.KeyChar != (char)Keys.Back))
                e.Handled = true;
        }

        /// <summary>
        /// Validación del número de volumen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Seguex5_Validating(object sender, CancelEventArgs e)
        {
            frmFormularioSimple Formulario_Actual = (frmFormularioSimple)FindForm();

            if (Seguex5.Enabled)
            {
                if (Seguex5.Text.Trim() == string.Empty)
                {
                    if (Accesibilidad[4] == 1)
                    {
                        Formulario_Actual.PresentaMensaje("NÚMERO DE VOLUMEN. OBLIGATORIO", 3, Color.Red);
                        Control_Foco = Seguex5;
                        e.Cancel = true;
                    }
                }
            }
        }

    }
}