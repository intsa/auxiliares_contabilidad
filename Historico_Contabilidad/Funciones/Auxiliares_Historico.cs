﻿using System;
using System.Data;
using System.Data.SqlClient;
using Comunes_Contabilidad;

namespace Historico_Contabilidad
{
    public static class Auxiliares_Historico
    {
        /// <summary>
        /// Verifica la estructura de niveles clave orgánica
        /// Creando los niveles que no existan
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio donde se van a verificar las claves</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Denominacion">Denominación de la clave</param>
        public static void Estructura_Organica(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Bdd_Comunes, string Clave_Organica, string Denominacion)
        {
            int Id_1000;                // IDENTIFICADOR REGISTRO

            if (Clave_Organica.Trim() != string.Empty)
            {
                Id_1000 = Identificador_Alchis_1000(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica);

                if (Id_1000 == 0)
                {
                    Generar_Estructura_Niveles_Alchis_1000(CadenaConexion, Ejercicio, Bdd_Historico, Bdd_Comunes, Clave_Organica, Denominacion);
                    Id_1000 = Identificador_Alchis_1000(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica);
                }

                Desencadenantes_Historico.Actualizar_Alchis_1000(CadenaConexion, Bdd_Historico, Id_1000, Denominacion, true);
            }
        }

        /// <summary>
        /// Genera la estructura de niveles tabla 'CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercico actual</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica último nivel</param>
        public static void Generar_Estructura_Niveles_Alchis_1000(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Bdd_Comunes, string Clave_Organica, string Denominacion)
        {
            DataTable Niveles = Auxiliares_Comunes.Niveles_Alccom_5000(CadenaConexion, Ejercicio, Bdd_Comunes);
            string Clave_Auxiliar;
            string Denominacion_Auxiliar;
            short Id_5000;
            byte Primer_Registro = 1;
            byte Longitud;

            foreach (DataRow Fila in Niveles.Rows)
            {
                Id_5000 = Convert.ToInt16(Fila["idecla"]);
                Longitud = Convert.ToByte(Fila["loncla"]);
                Clave_Auxiliar = Clave_Organica.Left(Longitud);

                if (Clave_Auxiliar.Trim() == string.Empty)
                {
                    if (Identificador_Alchis_1000(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Auxiliar) == 0)
                    {
                        Denominacion_Auxiliar = Primer_Registro == 1 ? Denominacion : "* * * MODIFICAR LA DENOMINACIÓN DE ESTA CLAVE ORGÁNICA";
                        Desencadenantes_Historico.Insertar_Alchis_1000(CadenaConexion, Bdd_Historico, Ejercicio, Id_5000, Clave_Auxiliar, Denominacion, Primer_Registro == 1);
                    }
                    else
                        break;

                    Primer_Registro = 0;
                }
            }
        }

        /// <summary>
        /// Genera la estructura de niveles tabla 'CLAVES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercico</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Clave_Funcional">Clave funcional último nivel</param>
        public static void Generar_Estructura_Niveles_Alchis_1001(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Bdd_Comunes, string Clave_Funcional, string Denominacion)
        {
            DataTable Niveles = Auxiliares_Comunes.Niveles_Alccom_5001(CadenaConexion, Ejercicio, Bdd_Comunes);
            string Clave_Auxiliar;
            string Denominacion_Auxiliar;
            short Id_5001;
            byte Primer_Registro = 1;
            byte Longitud;

            foreach (DataRow Fila in Niveles.Rows)
            {
                Id_5001 = Convert.ToInt16(Fila["idecla"]);
                Longitud = Convert.ToByte(Fila["loncla"]);
                Clave_Auxiliar = Clave_Funcional.Left(Longitud);

                if (Clave_Auxiliar.Trim() != string.Empty)
                {
                    if (Identificador_Alchis_1001(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Auxiliar) == 0)
                    {
                        Denominacion_Auxiliar = Primer_Registro == 1 ? Denominacion : "* * * MODIFICAR LA DENOMINACIÓN DE ESTA CLAVE FUNCIONAL";

                        Desencadenantes_Historico.Insertar_Alchis_1001(CadenaConexion, Bdd_Historico, Ejercicio, Id_5001, Clave_Auxiliar, Denominacion, Primer_Registro == 1);
                    }
                }
                else
                    break;

                Primer_Registro = 0;
            }
        }

        /// <summary>
        /// Genera la estructura de niveles tabla 'CLAVES ECONÓMICAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercico</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica último nivel</param>
        public static void Generar_Estructura_Niveles_Alchis_1002(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Bdd_Comunes, string Clave_Economica, string Denominacion)
        {
            DataTable Niveles = Auxiliares_Comunes.Niveles_Alccom_5002(CadenaConexion, Ejercicio, Bdd_Comunes);
            string Clave_Auxiliar;
            string Denominacion_Auxiliar;
            short Id_5002;
            byte Primer_Registro = 1;
            byte Longitud;

            foreach (DataRow Fila in Niveles.Rows)
            {
                Id_5002 = Convert.ToInt16(Fila["idecla"]);
                Longitud = Convert.ToByte(Fila["loncla"]);
                Clave_Auxiliar = Clave_Economica.Left(Longitud);

                if (Clave_Auxiliar.Trim() != string.Empty)
                {
                    if (Identificador_Alchis_1002(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Auxiliar) == 0)
                    {
                        Denominacion_Auxiliar = Primer_Registro == 1 ? Denominacion : "* * * MODIFICAR LA DENOMINACIÓN DE ESTA CLAVE ECONÓMICA";
                        Desencadenantes_Historico.Insertar_Alchis_1002(CadenaConexion, Bdd_Historico, Ejercicio, Id_5002, Clave_Auxiliar, Denominacion_Auxiliar, Primer_Registro == 1);
                    }
                }
                else
                    break;

                Primer_Registro = 0;
            }
        }

        /// <summary>
        /// Genera la estructura de niveles tabla 'CLAVES ECONÓMICAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercico</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Bdd_Comunes">Nombre BDD 'COMUNES. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica último nivel</param>
        public static void Generar_Estructura_Niveles_Alchis_1003(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Bdd_Comunes, string Clave_Economica, string Denominacion)
        {
            DataTable Niveles = Auxiliares_Comunes.Niveles_Alccom_5002(CadenaConexion, Ejercicio, Bdd_Comunes);
            string Clave_Auxiliar;
            string Denominacion_Auxiliar;
            short Id_5002;
            byte Primer_Registro = 1;
            byte Longitud;

            foreach (DataRow Fila in Niveles.Rows)
            {
                Id_5002 = Convert.ToInt16(Fila["idecla"]);
                Longitud = Convert.ToByte(Fila["loncla"]);
                Clave_Auxiliar = Clave_Economica.Left(Longitud);

                if (Clave_Auxiliar.Trim() != string.Empty)
                {
                    if (Identificador_Alchis_1003(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Auxiliar) == 0)
                    {
                        Denominacion_Auxiliar = Primer_Registro == 1 ? Denominacion : "* * * MODIFICAR LA DENOMINACIÓN DE ESTA CLAVE ECONÓMICA";
                        Desencadenantes_Historico.Insertar_Alchis_1003(CadenaConexion, Bdd_Historico, Ejercicio, Id_5002, Clave_Auxiliar, Denominacion_Auxiliar, Primer_Registro == 1);
                    }
                }
                else
                    break;

                Primer_Registro = 0;
            }
        }

        /// <summary>
        /// Verifica existencia/crea 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejecicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        public static void Verificar_Historico_Alchis_1004(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica, string Denominacion)
        {
            if (Identificador_Alchis_1004(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica, Clave_Funcional, Clave_Economica) == 0)
                Desencadenantes_Historico.Insertar_Alchis_1004(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica, Clave_Funcional, Clave_Economica, Denominacion);
        }

        /// <summary>
        /// Obtiene el identificador de la cuenta contable. Histórico
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Cuenta_Contable">Cuenta contable</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_0001(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Cuenta_Contable)
        {
            int Id_0001 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_0001";
            string Restriccion_Ejercicio = $"eje_0001 = {Ejercicio}";
            string Restriccion_Cuenta = $"cta_0001 = '{Cuenta_Contable}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Cuenta}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_0001 = (int)Data_Reader["ide_0001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_0001;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador de la cuenta contable. Histórico
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_Registro">Identificador registro</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_0001(string CadenaConexion, string Bdd_Historico, int Id_Registro)
        {
            int Id_0001 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_0001";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_0001";
            string Restriccion_Identificador = $"ide_0001 = {Id_Registro}";
            string Cadena_Restricciones = Restriccion_Identificador;
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_0001 = (int)Data_Reader["ide_0001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_0001;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES ORGÁNICAS'
        /// Item1 : Identificador registro
        /// Item2 : Denominación clave orgánica
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1000(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Restriccion_Nivel = "")
        {
            int Id_1000 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1000, den_1000";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1000";
            string Restriccion_Ejercicio = $"eje_1000 = {Ejercicio}";
            string Restriccion_Clave = $"cor_1000 = '{Clave_Organica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            if (Restriccion_Nivel != string.Empty)
                Cadena_Where += $" AND {Restriccion_Nivel}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1000 = (int)Data_Reader["ide_1000"];
                        Denominacion = (string)Data_Reader["den_1000"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1000, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES ORGÁNICAS'
        /// Item1 : Identificador registro
        /// Item2 : Denominación clave orgánica
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1000(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, bool Ultimo_Nivel)
        {
            int Id_1000 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1000, den_1000";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1000";
            string Restriccion_Ejercicio = $"eje_1000 = {Ejercicio}";
            string Restriccion_Clave = $"cor_1000 = '{Clave_Organica}'";
            string Restriccion_Nivel = Ultimo_Nivel ? "sun_1000 = 1" : "sun_1000 = 0";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave} AND {Restriccion_Nivel}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1000 = (int)Data_Reader["ide_1000"];
                        Denominacion = (string)Data_Reader["den_1000"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1000, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador, clave y denominación 'HISTÓRICO. CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1000">Identificador registro 'HISTÓRICO. CLAVES ORGÁNICAS'</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES ORGÁNICAS'
        /// Item1 : Identificador registro
        /// Item2 : Clave orgánica
        /// Item3 : Denominación clave orgánica
        /// </returns>
        public static Tuple<int, string, string> Datos_Alchis_1000(string CadenaConexion, string Bdd_Historico, int Id_1000)
        {
            int Id_Clave = 0;
            string Clave = "";
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1000, cor_1000, den_1000";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1000";
            string Restriccion_Identificador = $"ide_1000 = {Id_1000}";
            string Cadena_Restricciones = $"{Restriccion_Identificador}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_Clave = (int)Data_Reader["ide_1000"];
                        Clave = (string)Data_Reader["cor_1000"];
                        Denominacion = (string)Data_Reader["den_1000"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string>(Id_Clave, Clave, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador de la clave orgánica. Histórico
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1000(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica)
        {
            int Id_Registro = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1000";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1000";
            string Restriccion_Ejercicio = $"eje_1000 = {Ejercicio}";
            string Restriccion_Clave = $"cor_1000 = '{Clave_Organica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_Registro = (int)Data_Reader["ide_1000"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_Registro;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador de la clave orgánica. Histórico
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1000(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, bool Ultimo_Nivel)
        {
            int Id_Registro = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1000";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1000";
            string Restriccion_Ejercicio = $"eje_1000 = {Ejercicio}";
            string Restriccion_Clave = $"cor_1000 = '{Clave_Organica}'";
            string Restriccion_Nivel = Ultimo_Nivel ? "sun_1000 = 1" : "sun_1000 = 0";
            string Cadena_Restricciones;
            string Cadena_Where;

            Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave} AND {Restriccion_Nivel}";
            Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_Registro = (int)Data_Reader["ide_1000"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_Registro;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. CLAVES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES FUNCIONALES'
        /// Item1 : Identificador registro
        /// Item2 : Denominación clave funcional
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1001(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Funcional, string Restriccion_Nivel = "")
        {
            int Id_1001 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1001, den_1001";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1001";
            string Restriccion_Ejercicio = $"eje_1001 = {Ejercicio}";
            string Restriccion_Clave = $"cfu_1001 = '{Clave_Funcional}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            if (Restriccion_Nivel != string.Empty)
                Cadena_Where += $" AND {Restriccion_Nivel}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1001 = (int)Data_Reader["ide_1001"];
                        Denominacion = (string)Data_Reader["den_1001"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1001, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. CLAVES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES FUNCIONALES'
        /// Item1 : Identificador registro
        /// Item2 : Denominación clave funcional
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1001(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Funcional, bool Ultimo_Nivel)
        {
            int Id_1001 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1001, den_1001";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1001";
            string Restriccion_Ejercicio = $"eje_1001 = {Ejercicio}";
            string Restriccion_Clave = $"cfu_1001 = '{Clave_Funcional}'";
            string Restriccion_Nivel = Ultimo_Nivel ? "sun_1001 = 1" : "sun_1001 = 0";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave} AND {Restriccion_Nivel}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1001 = (int)Data_Reader["ide_1001"];
                        Denominacion = (string)Data_Reader["den_1001"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1001, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador, clave y denominación 'HISTÓRICO. CLAVES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1001">Identificador registro 'HISTÓRICO. CLAVES FUNCIONALES'</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES FUNCIONALES'
        /// Item1 : Identificador registro
        /// Item2 : Clave funcional
        /// Item3 : Denominación clave funcional
        /// </returns>
        public static Tuple<int, string, string> Datos_Alchis_1001(string CadenaConexion, string Bdd_Historico, int Id_1001)
        {
            int Id_Clave = 0;
            string Clave = "";
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1001, cfu_1001, den_1001";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1001";
            string Restriccion_Identificador = $"ide_1001 = {Id_1001}";
            string Cadena_Restricciones = $"{Restriccion_Identificador}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_Clave = (int)Data_Reader["ide_1001"];
                        Clave = (string)Data_Reader["cfu_1001"];
                        Denominacion = (string)Data_Reader["den_1001"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string>(Id_Clave, Clave, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador de la clave funcional. Histórico
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1001(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Funcional)
        {
            int Id_Registro = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1001";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1001";
            string Restriccion_Ejercicio = $"eje_1001 = {Ejercicio}";
            string Restriccion_Clave = $"cfu_1001 = '{Clave_Funcional}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = " WHERE " + Cadena_Restricciones;

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_Registro = (int)Data_Reader["ide_1001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_Registro;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador de la clave funcional. Histórico
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1001(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Funcional, bool Ultimo_Nivel)
        {
            int Id_Registro = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1001";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1001";
            string Restriccion_Ejercicio = $"eje_1001 = {Ejercicio}";
            string Restriccion_Clave = $"cfu_1001 = '{Clave_Funcional}'";
            string Restriccion_Nivel;
            string Cadena_Restricciones;
            string Cadena_Where;

            Restriccion_Nivel = Ultimo_Nivel ? "sun_1001 = 1" : "sun_1001 = 0";

            Cadena_Restricciones = Restriccion_Ejercicio + " AND " + Restriccion_Clave + " AND " + Restriccion_Nivel;
            Cadena_Where = " WHERE " + Cadena_Restricciones;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_Registro = (int)Data_Reader["ide_1001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_Registro;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica. Gastos</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'
        /// Item1 : Identificador registro
        /// Item2 : Denominación clave económica
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1002(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica, string Restriccion_Nivel = "")
        {
            int Id_1002 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1002, den_1002";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1002";
            string Restriccion_Ejercicio = $"eje_1002 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1002 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            if (Restriccion_Nivel != string.Empty)
                Cadena_Where += $" AND {Restriccion_Nivel}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1002 = (int)Data_Reader["ide_1002"];
                        Denominacion = (string)Data_Reader["den_1002"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1002, Denominacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica. Gastos</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'
        /// Item1 : Identificador registro
        /// Item2 : Denominación clave económica
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1002(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica, bool Ultimo_Nivel)
        {
            int Id_1002 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1002, den_1002";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1002";
            string Restriccion_Ejercicio = $"eje_1002 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1002 = '{Clave_Economica}'";
            string Restriccion_Nivel = Ultimo_Nivel ? "sun_1002 = 1" : "sun_1002 = 0";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave} AND {Restriccion_Nivel}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1002 = (int)Data_Reader["ide_1002"];
                        Denominacion = (string)Data_Reader["den_1002"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1002, Denominacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador, clave y denominación 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1002">Identificador registro 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'
        /// Item1 : Identificador registro
        /// Item2 : Clave económica
        /// Item3 : Denominación clave económica
        /// </returns>
        public static Tuple<int, string, string> Datos_Alchis_1002(string CadenaConexion, string Bdd_Historico, int Id_1002)
        {
            int Id_Clave = 0;
            string Clave = "";
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1002, cec_1002, den_1002";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1002";
            string Restriccion_Identificador = $"ide_1002 = {Id_1002}";
            string Cadena_Restricciones = $"{Restriccion_Identificador}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_Clave = (int)Data_Reader["ide_1002"];
                        Clave = (string)Data_Reader["cec_1002"];
                        Denominacion = (string)Data_Reader["den_1002"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string>(Id_Clave, Clave, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador de la clave económica. Gastos. Histórico
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1002(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica)
        {
            int Id_Registro = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1002";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1002";
            string Restriccion_Ejercicio = $"eje_1002 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1002 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = " WHERE " + Cadena_Restricciones;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_Registro = (int)Data_Reader["ide_1002"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_Registro;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador de la clave económica. Gastos. Histórico
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1002(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica, bool Ultimo_Nivel)
        {
            int Id_Registro = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1002";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1002";
            string Restriccion_Ejercicio = $"eje_1002 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1002 = '{Clave_Economica}'";
            string Restriccion_Nivel;
            string Cadena_Restricciones;
            string Cadena_Where;

            Restriccion_Nivel = Ultimo_Nivel ? "sun_1002 = 1" : "sun_1002 = 0";

            Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave} AND {Restriccion_Nivel}";
            Cadena_Where = " WHERE " + Cadena_Restricciones;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_Registro = (int)Data_Reader["ide_1002"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_Registro;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador de la cuenta contable asociada. Clave económica. Gastos
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_Clave">Identificador clave económica. Gastos</param>
        /// <param name="Cuenta_Contable">Cuenta contable</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1002_1(string CadenaConexion, string Bdd_Historico, int Id_Clave, string Cuenta_Contable)
        {
            int Id_1002_1 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1002_1";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1002_1";
            string Restriccion_Ejercicio = $"ide_1002 = {Id_Clave}";
            string Restriccion_Clave = $"cta_1002_1 = '{Cuenta_Contable}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1002_1 = (int)Data_Reader["ide_1002_1"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1002_1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica. Ingresos</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'
        /// Item1 : Identificador registro
        /// Item2 : Denominación clave económica
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1003(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica, string Restriccion_Nivel = "")
        {
            int Id_1003 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1003, den_1003";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1003";
            string Restriccion_Ejercicio = $"eje_1003 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1003 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            if (Restriccion_Nivel != string.Empty)
                Cadena_Where += $" AND {Restriccion_Nivel}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1003 = (int)Data_Reader["ide_1003"];
                        Denominacion = (string)Data_Reader["den_1003"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1003, Denominacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica. Ingresos</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'
        /// Item1 : Identificador registro
        /// Item2 : Denominación clave económica
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1003(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica, bool Ultimo_Nivel)
        {
            int Id_1003 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1003, den_1003";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1003";
            string Restriccion_Ejercicio = $"eje_1003 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1003 = '{Clave_Economica}'";
            string Restriccion_Nivel = Ultimo_Nivel ? "sun_1003 = 1" : "sun_1003 = 0";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave} AND {Restriccion_Nivel}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1003 = (int)Data_Reader["ide_1003"];
                        Denominacion = (string)Data_Reader["den_1003"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1003, Denominacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador, clave y denominación 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1003">Identificador registro 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'</param>
        /// <returns> Datos 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'
        /// Item1 : Identificador registro
        /// Item2 : Clave económica
        /// Item3 : Denominación clave económica
        /// </returns>
        public static Tuple<int, string, string> Datos_Alchis_1003(string CadenaConexion, string Bdd_Historico, int Id_1003)
        {
            int Id_Clave = 0;
            string Clave = "";
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1003, cec_1003, den_1003";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1003";
            string Restriccion_Identificador = $"ide_1003 = {Id_1003}";
            string Cadena_Restricciones = $"{Restriccion_Identificador}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_Clave = (int)Data_Reader["ide_1003"];
                        Clave = (string)Data_Reader["cec_1003"];
                        Denominacion = (string)Data_Reader["den_1003"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string, string>(Id_Clave, Clave, Denominacion);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador de la clave económica. Ingresos
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1003(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica)
        {
            int Id_1003 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1003";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1003";
            string Restriccion_Ejercicio = $"eje_1003 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1003 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1003 = (int)Data_Reader["ide_1003"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1003;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador de la clave económica. Ingresos
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1003(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica, bool Ultimo_Nivel)
        {
            int Id_1003 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1003";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1003";
            string Restriccion_Ejercicio = $"eje_1003 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1003 = '{Clave_Economica}'";
            string Restriccion_Nivel;
            string Cadena_Restricciones;
            string Cadena_Where;

            Restriccion_Nivel = Ultimo_Nivel ? "sun_1003 = 1" : "sun_1003 = 0";

            Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave} AND {Restriccion_Nivel}";
            Cadena_Where = " WHERE " + Cadena_Restricciones;

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1003 = (int)Data_Reader["ide_1003"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1003;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'HISTÓRICO. CLAVE ECONÓMICA. INGRESOS .CUENTAS CONTABLES ASOCIADAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1003">Identificador registro  'HISTÓRICO. CLAVE ECONÓMICA. INGRESOS'</param>
        /// <param name="Cuenta_Contable">Cuenta contable</param>
        /// <returns>Identificador registro 'HISTÓRICO. CLAVE ECONÓMICA. INGRESOS .CUENTAS CONTABLES ASOCIADAS'</returns>
        public static int Identificador_Alchis_1003_1(string CadenaConexion, string Bdd_Historico, int Id_1003, string Cuenta_Contable)
        {
            int Id_1003_1 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1003_1";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1003_1";
            string Restriccion_Clave = $"ide_1003 = {Id_1003}";
            string Restriccion_Cuenta = $"cta_1003_1 = '{Cuenta_Contable}'";
            string Cadena_Restricciones = $"{Restriccion_Clave} AND {Restriccion_Cuenta}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1003_1 = (int)Data_Reader["ide_1003_1"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1003_1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns> Datos  'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS'
        /// Item1 : Identificador registro
        /// Item2 : Denominación aplicación presupuestaria
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1004(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica)
        {
            int Id_1004 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1004, den_1004";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1004";
            string Restriccion_Ejercicio = $"eje_1004 = {Ejercicio}";
            string Restriccion_Organica = $"cor_1004 = '{Clave_Organica}'";
            string Restriccion_Funcional = $"cfu_1004 = '{Clave_Funcional}'";
            string Restriccion_Economica = $"cec_1004 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Funcional} AND {Restriccion_Economica}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1004 = (int)Data_Reader["ide_1004"];
                        Denominacion = (string)Data_Reader["den_1004"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1004, Denominacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1004(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica)
        {
            int Id_Registro = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1004";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1004";
            string Restriccion_Ejercicio = $"eje_1004 = {Ejercicio}";
            string Restriccion_Organica = $"cor_1004 = '{Clave_Organica}'";
            string Restriccion_Funcional = $"cfu_1004 = '{Clave_Funcional}'";
            string Restriccion_Economica = $"cec_1004 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Funcional} AND {Restriccion_Economica}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_Registro = (int)Data_Reader["ide_1004"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_Registro;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS. CUENTAS CONTABLES ASOCIADAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Id_1004">Identificador registro 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS'</param>
        /// <param name="Cuenta_Contable">Cuenta contable</param>
        /// <returns>Identificador registro 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS. CUENTAS CONTABLES ASOCIADAS'</returns>
        public static int Identificador_Alchis_1004_1(string CadenaConexion, string Bdd_Historico, int Id_1004, string Cuenta_Contable)
        {
            int ide_1004_1 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1004_1";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1004_1";
            string Restriccion_Identificador = "ide_1004 = " + Id_1004;
            string Restriccion_Clave = $"cta_1004_1 = '{Cuenta_Contable}'";
            string Cadena_Restricciones = $"{Restriccion_Identificador} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        ide_1004_1 = (int)Data_Reader["ide_1004_1"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return ide_1004_1;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS. VINCULACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns> Datos  'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS. VINCULACIÓN'
        /// Item1 : Identificador registro
        /// Item2 : Denominación aplicación presupuestaria
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1005(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica)
        {
            int Id_1005 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1005, den_1005";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1005";
            string Restriccion_Ejercicio = $"eje_1004 = {Ejercicio}";
            string Restriccion_Organica = $"cor_1005 = '{Clave_Organica}'";
            string Restriccion_Funcional = $"cfu_1005 = '{Clave_Funcional}'";
            string Restriccion_Economica = $"cec_1005 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Funcional} AND {Restriccion_Economica}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1005 = (int)Data_Reader["ide_1005"];
                        Denominacion = (string)Data_Reader["den_1005"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1005, Denominacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador registro 'APLICACIONES PRESUPUESTARIAS. INGRESOS"
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1006(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Economica)
        {
            int Id_1006 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1006";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1006";
            string Restriccion_Ejercicio = $"eje_1006 = {Ejercicio}";
            string Restriccion_Organica = $"cor_1006 = '{Clave_Organica}'";
            string Restriccion_Economica = $"cec_1006 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Economica}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1006 = (int)Data_Reader["ide_1006"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1006;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador y denominación 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. INGRESOS
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns> Datos  'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. INGRESOS'
        /// Item1 : Identificador registro
        /// Item2 : Denominación aplicación presupuestaria
        /// </returns>
        public static Tuple<int, string> Datos_Alchis_1006(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Economica)
        {
            int Id_1006 = 0;
            string Denominacion = "";

            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1006, den_1006";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1006";
            string Restriccion_Ejercicio = $"eje_1006 = {Ejercicio}";
            string Restriccion_Organica = $"cor_1006 = '{Clave_Organica}'";
            string Restriccion_Economica = $"cec_1006 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Economica}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                    {
                        Id_1006 = (int)Data_Reader["ide_1006"];
                        Denominacion = (string)Data_Reader["den_1006"];
                    }

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Tuple.Create<int, string>(Id_1006, Denominacion);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el identificador de la cuenta contable asociada. Aplicación presupuestaria. Ingresos. Histórico
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1006">Identificador Aplicación presupuestaria. Ingresos. Histórico</param>
        /// <param name="Cuenta_Contable">Cuenta contable</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_1006_1(string CadenaConexion, string Bdd_Historico, int Id_1006, string Cuenta_Contable)
        {
            int Id_1006_1 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_1006_1";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1006_1";
            string Restriccion_Clave = $"ide_1006 = {Id_1006}";
            string Restriccion_Cuenta = $"cta_1006_1 = '{Cuenta_Contable}'";
            string Cadena_Restricciones = $"{Restriccion_Clave} AND {Restriccion_Cuenta}";
            string Cadena_Where = " WHERE " + Cadena_Restricciones;

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_1006_1 = (int)Data_Reader["ide_1006_1"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_1006_1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador registro ' 'REMANENTES DE CRÉDITO. APLICACIONES PRESUPUESTARIAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_2004(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica)
        {
            int Id_2004 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2004";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_2004";
            string Restriccion_Ejercicio = $"eje_2004 = {Ejercicio}";
            string Restriccion_Organica = $"cor_2004 = '{Clave_Organica}'";
            string Restriccion_Funcional = $"cfu_2004 = '{Clave_Funcional}'";
            string Restriccion_Economica = $"cec_2004 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Funcional} AND {Restriccion_Economica}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_2004 = (int)Data_Reader["ide_2004"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_2004;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el identificador del registro 'REMANENTES DE CRÉDITO. VICULACIÓN'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns>Identificador registro</returns>
        public static int Identificador_Alchis_2005(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica)
        {
            int Id_2005 = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT ide_2005";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_2005";
            string Restriccion_Ejercicio = $"eje_2005 = {Ejercicio}";
            string Restriccion_Organica = $"cor_2005 = '{Clave_Organica}'";
            string Restriccion_Funcional = $"cfu_2005 = '{Clave_Funcional}'";
            string Restriccion_Economica = $"cec_2005 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Funcional} AND {Restriccion_Economica}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Id_2005 = (int)Data_Reader["ide_2005"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Id_2005;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación de la clave orgánica
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <returns>Denominación de la clave</returns>
        public static string Denominacion_Clave_Organica(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT den_1000";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1000";
            string Restriccion_Ejercicio = $"eje_1000 = {Ejercicio}";
            string Restriccion_Clave = $"cor_1000 = '{Clave_Organica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["den_1000"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación de la clave funcional
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <returns></returns>
        public static string Denominacion_Clave_Funcional(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Funcional)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT den_1001";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1001";
            string Restriccion_Ejercicio = $"eje_1001 = {Ejercicio}";
            string Restriccion_Clave = $"cfu_1001 = '{Clave_Funcional}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["den_1001"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación de la clave económica. Gastos
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns></returns>
        public static string Denominacion_Clave_Economica_Gastos(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT den_1002";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1002";
            string Restriccion_Ejercicio = $"eje_1002 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1002 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["den_1002"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación de la aplicación presupuestaria. Gastos
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns></returns>
        public static string Denominacion_Aplicacion_Gastos(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT den_1004";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1004";
            string Restriccion_Ejercicio = $"eje_1004 = {Ejercicio}";
            string Restriccion_Organica = $"cor_1004 = '{Clave_Organica}'";
            string Restriccion_Funcional = $"cfu_1004 = '{Clave_Funcional}'";
            string Restriccion_Economica = $"cec_1004 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Funcional} AND {Restriccion_Economica}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["den_1004"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación de la aplicación presupuestaria. Gastos
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_Aplicacion">Identificador registro 'APLICACIONES PRESUPUESTARIAS. GASTOS'</param>
        /// <returns></returns>
        public static string Denominacion_Aplicacion_Gastos(string CadenaConexion, string Bdd_Historico, int Id_Aplicacion)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT den_1004";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1004";
            string Restriccion_Aplicacion = $"ide_1004 = {Id_Aplicacion}";
            string Cadena_Restricciones = Restriccion_Aplicacion;
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["den_1004"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación de la aplicación presupuestaria. Ingresos
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns></returns>
        public static string Denominacion_Aplicacion_Ingresos(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Economica)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT den_1006";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_10046";
            string Restriccion_Ejercicio = $"eje_1006 = {Ejercicio}";
            string Restriccion_Organica = $"cor_1006 = '{Clave_Organica}'";
            string Restriccion_Economica = $"cec_1006 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Economica}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["ide_1006"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación de la aplicación presupuestaria. Ingresos
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_Aplicacion">Identificador registro 'APLICACIONES PRESUPUESTARIAS. INGRESOS'</param>
        /// <returns></returns>
        public static string Denominacion_Aplicacion_Ingresos(string CadenaConexion, string Bdd_Historico, int Id_Aplicacion)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT den_1006";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1006";
            string Restriccion_Aplicacion = $"ide_1006 = {Id_Aplicacion}";
            string Cadena_Restricciones = Restriccion_Aplicacion;
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["den_1006"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene la denominación de la clave económica. Ingresos
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns></returns>
        public static string Denominacion_Clave_Economica_Ingresos(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica)
        {
            string Denominacion = "";
            string Cadena_Sql;
            string Cadena_Select = "SELECT den_1003";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1003";
            string Restriccion_Ejercicio = $"eje_1003 = {Ejercicio}";
            string Restriccion_Clave = $"cec_1003 = '{Clave_Economica}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Clave}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Denominacion = (string)Data_Reader["den_1003"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Denominacion;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Verifica la existencia/crea 'HISTÓRICO. REMANENTES DE CRÉDITO. VINCULACIONES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        /// <returns>Identificador registro 'HISTÓRICO. REMANENTES DE CRÉDITO. VINCULACIONES'</returns>
        public static int Verificar_Alchis_2005(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica, string Denominacion)
        {
            int Id_2005 = Identificador_Alchis_2005(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica, Clave_Funcional, Clave_Economica);
            string Denominacion_Organica;
            string Denominacion_Funcional;
            string Denominacion_Economica;
            string Nueva_Denominacion = Denominacion.Trim();

            if (Id_2005 == 0)
            {
                if (Denominacion.Trim() == string.Empty)
                {
                    Denominacion_Organica = Denominacion_Clave_Organica(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica);
                    Denominacion_Funcional = Denominacion_Clave_Funcional(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Funcional);
                    Denominacion_Economica = Denominacion_Clave_Economica_Gastos(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Economica);
                    Nueva_Denominacion = (Denominacion_Organica.Trim() + " " + Denominacion_Funcional.Trim() + " " + Denominacion_Economica.Trim()).Trim();
                }

                Id_2005 = Desencadenantes_Historico.Insertar_Alchis_2005(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica, Clave_Funcional, Clave_Economica, Nueva_Denominacion);
            }

            return Id_2005;
        }

        /// <summary>
        /// Obtiene el número de niveles superiores existentes para una clave dada 'CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <returns>Número de niveles</returns>
        public static int Numero_Niveles_Organicos(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica)
        {
            int Numero_Niveles = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT count (*) numreg";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1000";
            string Restriccion_Ejercicio = $"eje_1000 = {Ejercicio}";
            string Restriccion_Organica = $"cor_1000 LIKE '{Clave_Organica.Trim()}%'";
            string Restriccion_Iguales = $"cor_1000 != '{Clave_Organica.Trim()}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Organica} AND {Restriccion_Iguales}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Numero_Niveles = (int)Data_Reader["numreg"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Numero_Niveles;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene el número de niveles superiores existentes para una clave dada 'CLAVES FUNCINALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <returns>Número de niveles</returns>
        public static int Numero_Niveles_Funcionales(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Funcional)
        {
            int Numero_Niveles = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT count (*) numreg";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1001";
            string Restriccion_Ejercicio = $"eje_1001 = {Ejercicio}";
            string Restriccion_Funcional = $"cfu_1001 LIKE '{Clave_Funcional.Trim()}%'";
            string Restriccion_Iguales = $"cfu_1001 != '{Clave_Funcional.Trim()}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Funcional} AND {Restriccion_Iguales}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Numero_Niveles = (int)Data_Reader["numreg"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Numero_Niveles;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el número de niveles superiores existentes para una clave dada 'CLAVES ECONÓMICAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns>Número de niveles</returns>
        public static int Numero_Niveles_Economicos_Gastos(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica)
        {
            int Numero_Niveles = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT count (*) numreg";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1002";
            string Restriccion_Ejercicio = $"eje_1002 = {Ejercicio}";
            string Restriccion_Economica = $"cec_1002 LIKE '{Clave_Economica.Trim()}%'";
            string Restriccion_Iguales = $"cec_1002 != '{Clave_Economica.Trim()}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Economica} AND {Restriccion_Iguales}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Numero_Niveles = (int)Data_Reader["numreg"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Numero_Niveles;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Obtiene el número de niveles superiores existentes para una clave dada 'CLAVES ECONÓMICAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Ejercicio">Ejercicio</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <returns>Número de niveles</returns>
        public static int Numero_Niveles_Economicos_Ingresos(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Economica)
        {
            int Numero_Niveles = 0;
            string Cadena_Sql;
            string Cadena_Select = "SELECT count (*) numreg";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1003";
            string Restriccion_Ejercicio = $"eje_1003 = {Ejercicio}";
            string Restriccion_Economica = $"cec_1003 LIKE '{Clave_Economica.Trim()}%'";
            string Restriccion_Iguales = $"cec_1003 != '{Clave_Economica.Trim()}'";
            string Cadena_Restricciones = $"{Restriccion_Ejercicio} AND {Restriccion_Economica} AND {Restriccion_Iguales}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand();
                    SqlDataReader Data_Reader;
                    Comando_Sql.Connection = Cadena_Conexion;

                    Cadena_Sql = Cadena_Select + Cadena_From + Cadena_Where;

                    Comando_Sql.CommandText = Cadena_Sql;
                    Data_Reader = Comando_Sql.ExecuteReader();

                    if (Data_Reader.Read())
                        Numero_Niveles = (int)Data_Reader["numreg"];

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Numero_Niveles;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Verifica si existe el identificador de registro 'HISTÓRICO. CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Pid_5000">Identificador registro 'COMUNES. NIVELES ORGÁNICOS'</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Ocurrencias_Alchis_1000(string CadenaConexion, string Bdd_Historico, short Pid_5000)
        {
            bool Existe;
            string Cadena_Sql;
            string Cadena_Select = "SELECT TOP (1) ide_5000";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1000";
            string Restriccion_Identificador = $"ide_5000 = {Pid_5000}";
            string Cadena_Restricciones = $"{Restriccion_Identificador}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica si existe el identificador de registro 'HISTÓRICO. CLAVES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Pid_5001">Identificador registro 'COMUNES. NIVELES FUNCIONALES'</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Ocurrencias_Alchis_1001(string CadenaConexion, string Bdd_Historico, short Pid_5001)
        {
            bool Existe;
            string Cadena_Sql;
            string Cadena_Select = "SELECT TOP (1) ide_5001";
            string Cadena_From = $" FROM {Bdd_Historico}.dbo.alchis_1001";
            string Restriccion_Identificador = $"ide_5001 = {Pid_5001}";
            string Cadena_Restricciones = $"{Restriccion_Identificador}";
            string Cadena_Where = $" WHERE {Cadena_Restricciones}";

            Cadena_Sql = $"{Cadena_Select}{Cadena_From}{Cadena_Where}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Verifica si existe el identificador de registro 'HISTÓRICO. CLAVES ECONÓMICAS GASTOS E INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Pid_5002">Identificador registro 'COMUNES. NIVELES ECONÓMICOS'</param>
        /// <returns>Semáforo existencia</returns>
        public static bool Ocurrencias_Alchis_1002_1003(string CadenaConexion, string Bdd_Historico, short Pid_5002)
        {
            bool Existe;
            string Cadena_Sql;
            string[] Sql_Cadena = new string[2];
            string[] Cadena_Select = { "SELECT TOP (1) ide_5002", "SELECT TOP (1) ide_5002" };
            string[] Cadena_From = { $" FROM {Bdd_Historico}.dbo.alchis_1002", $" FROM {Bdd_Historico}.dbo.alchis_1003" };
            string[] Restriccion_Identificador = { $"ide_5002 = {Pid_5002}", $"ide_5002 = {Pid_5002}" };
            string[] Cadena_Where = { $" WHERE {Restriccion_Identificador[0]}", $" WHERE {Restriccion_Identificador[1]}" };

            for (byte i = 0; i < Sql_Cadena.Length; i++)
                Sql_Cadena[i] = $"{Cadena_Select[i]}{Cadena_From[i]}{Cadena_Where[i]}";

            Cadena_Sql = Sql_Cadena[0];

            for (byte i = 1; i < Sql_Cadena.Length; i++)
                Cadena_Sql += $" UNION ALL {Sql_Cadena[i]}";

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    SqlCommand Comando_Sql = new SqlCommand
                    {
                        Connection = Cadena_Conexion,
                        CommandText = Cadena_Sql
                    };

                    SqlDataReader Data_Reader = Comando_Sql.ExecuteReader();

                    Existe = Data_Reader.HasRows;

                    Data_Reader.Close();
                    Comando_Sql = null;
                    Cadena_Conexion.Close();

                    return Existe;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

    }
}