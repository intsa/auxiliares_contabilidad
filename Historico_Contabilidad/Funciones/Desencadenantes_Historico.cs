﻿using ClaseIntsa.Propiedades;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Historico_Contabilidad
{
    public static class Desencadenantes_Historico
    {
        #region Tabla Alchis_0001 'CUENTAS CONTABLES. HISTÓRICO'

        /// <summary>
        /// Inserta registros tabla 'CUENTAS CONTABLES. HISTÓRICO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio clave</param>
        /// <param name="Cuenta">Cuenta contable</param>
        /// <param name="Denominacion">Denominación cuenta contable</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alchis_0001(string CadenaConexion, string Bdd_Historico, short Ejercicio, string Cuenta, string Denominacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    Alchis_0001 Temporal = new Alchis_0001(Cadena_Conexion, Bdd_Historico)
                    {
                        eje_0001 = Ejercicio,
                        cta_0001 = Cuenta,
                        des_0001 = Denominacion,
                        uar_0001 = Variables_Globales.IdUsuario,
                        far_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_0001 = Variables_Globales.IpPrivada,
                        uua_0001 = Variables_Globales.IdUsuario,
                        fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_0001 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_0001;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualizar registros tabla 'CUENTAS CONTABLES. HISTÓRICO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_0001">Identificador registro 'CUENTAS CONTABLES. HISTÓRICO'</param>
        /// <param name="Denominacion">Denominación cuenta contable</param>
        public static void Actualizar_Alchis_0001(string CadenaConexion, string Bdd_Historico, int Id_0001, string Denominacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    Alchis_0001_Extras Temporal = new Alchis_0001_Extras(Cadena_Conexion, Bdd_Historico)
                    {
                        ide_0001 = Id_0001,
                        des_0001 = Denominacion,
                        dip_0001 = Variables_Globales.IpPrivada,
                        uua_0001 = Variables_Globales.IdUsuario,
                        fua_0001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina registro en la tabla  'CUENTAS CONTABLES. HISTÓRICO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_0001">Identificador registro 'CUENTAS CONTABLES. HISTÓRICO'</param>
        public static void Eliminar_Alchis_0001(string CadenaConexion, string Bdd_Historico, int Id_0001)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    Alchis_0001 Temporal = new Alchis_0001(Cadena_Conexion, Bdd_Historico) { ide_0001 = Id_0001 };

                    Temporal.Eliminar();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Alchis_0001 'CUENTAS CONTABLES. HISTÓRICO'

        #region Tabla Alchis_1000 'CLAVES ORGÁNICAS'

        /// <summary>
        /// Inserta registros tabla 'CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio clave</param>
        /// <param name="Id_5000">Identificador registro 'CLAVES ORGÁNICAS. NIVELES'</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Denominacion">Denominación clave orgánica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alchis_1000(string CadenaConexion, string Bdd_Historico, short Ejercicio, short Id_5000, string Clave_Organica, string Denominacion, bool Ultimo_Nivel)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1000 Temporal = new Alchis_1000(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();

                    Temporal.eje_1000 = Ejercicio;
                    Temporal.ide_5000 = Id_5000;
                    Temporal.cor_1000 = Clave_Organica;
                    Temporal.den_1000 = Denominacion;
                    Temporal.sun_1000 = Ultimo_Nivel ? (byte)1 : (byte)0;
                    Temporal.uar_1000 = Variables_Globales.IdUsuario;
                    Temporal.far_1000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_1000 = Variables_Globales.IpPrivada;
                    Temporal.uua_1000 = Variables_Globales.IdUsuario;
                    Temporal.fua_1000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_1000 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_1000;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Inserta registros tabla 'CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1000">Identificador registro 'HISTÓRICO. CLAVES ORGÁNICAS'</param>
        /// <param name="Denominacion">Denominación clave orgánica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        public static void Actualizar_Alchis_1000(string CadenaConexion, string Bdd_Historico, int Id_1000, string Denominacion, bool Ultimo_Nivel)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1000_Extras Temporal = new Alchis_1000_Extras(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1000 = Id_1000;

                    Temporal.den_1000 = Denominacion;
                    Temporal.sun_1000 = Ultimo_Nivel ? (byte)1 : (byte)0;
                    Temporal.dip_1000 = Variables_Globales.IpPrivada;
                    Temporal.uua_1000 = Variables_Globales.IdUsuario;
                    Temporal.fua_1000 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina registros tabla 'CLAVES ORGÁNICAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1000">Identificador registro 'HISTÓRICO. CLAVES ORGÁNICAS'</param>
        public static void Eliminar_Alchis_1000(string CadenaConexion, string Bdd_Historico, int Id_1000)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    Alchis_1000 Temporal = new Alchis_1000(Cadena_Conexion, Bdd_Historico) { ide_1000 = Id_1000 };

                    Temporal.Eliminar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion  Tabla Alchis_1000 'CLAVES ORGÁNICAS'

        #region Tabla Alchis_1001 'CLAVES FUNCIONALES'

        /// <summary>
        /// Inserta registros tabla 'CLAVES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio clave</param>
        /// <param name="Id_5001">Identificador registro 'CLAVES FUNCIONALES. NIVELES'</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Denominacion">Denominación clave funcional</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alchis_1001(string CadenaConexion, string Bdd_Historico, short Ejercicio, short Id_5001, string Clave_Funcional, string Denominacion, bool Ultimo_Nivel)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1001 Temporal = new Alchis_1001(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();

                    Temporal.eje_1001 = Ejercicio;
                    Temporal.ide_5001 = Id_5001;
                    Temporal.cfu_1001 = Clave_Funcional;
                    Temporal.den_1001 = Denominacion;
                    Temporal.sun_1001 = Ultimo_Nivel ? (byte)1 : (byte)0;
                    Temporal.uar_1001 = Variables_Globales.IdUsuario;
                    Temporal.far_1001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_1001 = Variables_Globales.IpPrivada;
                    Temporal.uua_1001 = Variables_Globales.IdUsuario;
                    Temporal.fua_1001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_1001 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_1001;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Inserta registros tabla 'CLAVES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1001">Identificador registro 'HISTÓRICO. CLAVES FUNCIONALES'</param>
        /// <param name="Denominacion">Denominación clave funcional</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static void Actualizar_Alchis_1001(string CadenaConexion, string Bdd_Historico, int Id_1001, string Denominacion, bool Ultimo_Nivel)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1001_Extras Temporal = new Alchis_1001_Extras(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1001 = Id_1001;

                    Temporal.den_1001 = Denominacion;
                    Temporal.sun_1001 = Ultimo_Nivel ? (byte)1 : (byte)0;
                    Temporal.dip_1001 = Variables_Globales.IpPrivada;
                    Temporal.uua_1001 = Variables_Globales.IdUsuario;
                    Temporal.fua_1001 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina registros tabla 'CLAVES FUNCIONALES'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1001">Identificador registro 'HISTÓRICO. CLAVES FUNCIONALES'</param>
        public static void Eliminar_Alchis_1001(string CadenaConexion, string Bdd_Historico, int Id_1001)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1001 Temporal = new Alchis_1001(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1001 = Id_1001;

                    Temporal.Eliminar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion  Tabla Alchis_1001 'CLAVES FUNCIONALES'

        #region Tabla Alchis_1002 'CLAVES ECONÓMICAS. GASTOS'

        /// <summary>
        /// Inserta registros tabla 'CLAVES ECONÓMICAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio clave</param>
        /// <param name="Id_5002">Identificador registro 'CLAVES ECONÓMICAS. NIVELES'</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación clave económica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alchis_1002(string CadenaConexion, string Bdd_Historico, short Ejercicio, short Id_5002, string Clave_Economica, string Denominacion, bool Ultimo_Nivel)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1002 Temporal = new Alchis_1002(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();

                    Temporal.eje_1002 = Ejercicio;
                    Temporal.ide_5002 = Id_5002;
                    Temporal.cec_1002 = Clave_Economica;
                    Temporal.den_1002 = Denominacion;
                    Temporal.sun_1002 = Ultimo_Nivel ? (byte)1 : (byte)0;
                    Temporal.uar_1002 = Variables_Globales.IdUsuario;
                    Temporal.far_1002 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_1002 = Variables_Globales.IpPrivada;
                    Temporal.uua_1002 = Variables_Globales.IdUsuario;
                    Temporal.fua_1002 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_1002 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_1002;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'CLAVES ECONÓMICAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1002">Identificador registro 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'</param>
        /// <param name="Denominacion">Denominación clave económica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static void Actualizar_Alchis_1002(string CadenaConexion, string Bdd_Historico, int Id_1002, string Denominacion, bool Ultimo_Nivel)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1002_Extras Temporal = new Alchis_1002_Extras(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1002 = Id_1002;

                    Temporal.den_1002 = Denominacion;
                    Temporal.sun_1002 = Ultimo_Nivel ? (byte)1 : (byte)0;
                    Temporal.dip_1002 = Variables_Globales.IpPrivada;
                    Temporal.uua_1002 = Variables_Globales.IdUsuario;
                    Temporal.fua_1002 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina registros tabla 'CLAVES ECONÓMICAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1002">Identificador registro 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'</param>
        public static void Eliminar_Alchis_1002(string CadenaConexion, string Bdd_Historico, int Id_1002)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1002 Temporal = new Alchis_1002(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1002 = Id_1002;

                    Temporal.Eliminar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion  Tabla Alchis_1002 'CLAVES ECONÓMICAS. GASTOS'

        #region Tabla Alchis_1002_1  'CLAVES ECONÓMICAS. GASTOS. CUENTAS ASOCIADAS'

        /// <summary>
        /// Inserta registros tabla 'CLAVES ECONOMICAS. CUENTAS CONTABLES ASOCIADAS. HISTÓRICO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1002_1">Identificador clave</param>
        /// <param name="Cuenta_Contable">Cuenta contable asociada</param>
        public static void Insertar_Alchis_1002_1(string CadenaConexion, string Bdd_Historico, int Id_1002_1, string Cuenta_Contable)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alchis_1002_1 Temporal = new Alchis_1002_1(Cadena_Conexion, Bdd_Historico)
                    {
                        ide_1002 = Id_1002_1,
                        cta_1002_1 = Cuenta_Contable,
                        uar_1002_1 = Variables_Globales.IdUsuario,
                        far_1002_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_1002_1 = Variables_Globales.IpPrivada,
                        uua_1002_1 = Variables_Globales.IdUsuario,
                        fua_1002_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_1002_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();
                }

                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registros tabla 'CLAVES ECONÓMICAS. GASTOS. CUENTAS ASOCIADAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1002">Identificador registro 'HISTÓRICO. CLAVES ECONÓMICAS. GASTOS'</param>
        public static void Eliminar_Alchis_1002_1(string CadenaConexion, string Bdd_Historico, int Id_1002)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    Alchis_1002_1_Extras Temporal = new Alchis_1002_1_Extras(Cadena_Conexion, Bdd_Historico) { ide_1002 = Id_1002 };

                    Temporal.Eliminar_Extras();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion  Tabla Alchis_1002_1  'CLAVES ECONÓMICAS. GASTOS. CUENTAS ASOCIADAS'

        #region Tabla Alchis_1003 'CLAVES ECONÓMICAS. INGRESOS'

        /// <summary>
        /// Inserta registros tabla 'CLAVES ECONÓMICAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio clave</param>
        /// <param name="Id_5002">Identificador registro 'CLAVES ECONÓMICAS. NIVELES'</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación clave económica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alchis_1003(string CadenaConexion, string Bdd_Historico, short Ejercicio, short Id_5002, string Clave_Economica, string Denominacion, bool Ultimo_Nivel)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alchis_1003 Temporal = new Alchis_1003(Cadena_Conexion, Bdd_Historico)
                    {
                        eje_1003 = Ejercicio,
                        ide_5002 = Id_5002,
                        cec_1003 = Clave_Economica,
                        den_1003 = Denominacion,
                        sun_1003 = Ultimo_Nivel ? (byte)1 : (byte)0,
                        uar_1003 = Variables_Globales.IdUsuario,
                        far_1003 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_1003 = Variables_Globales.IpPrivada,
                        uua_1003 = Variables_Globales.IdUsuario,
                        fua_1003 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_1003 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_1003;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'CLAVES ECONÓMICAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1003">Identificador registro 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'</param>
        /// <param name="Denominacion">Denominación clave económica</param>
        /// <param name="Ultimo_Nivel">Semáforo último nivel</param>
        /// <returns>Identificador registro</returns>
        public static void Actualizar_Alchis_1003(string CadenaConexion, string Bdd_Historico, int Id_1003, string Denominacion, bool Ultimo_Nivel)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alchis_1003_Extras Temporal = new Alchis_1003_Extras(Cadena_Conexion, Bdd_Historico)
                    {
                        ide_1003 = Id_1003,

                        den_1003 = Denominacion,
                        sun_1003 = Ultimo_Nivel ? (byte)1 : (byte)0,
                        dip_1003 = Variables_Globales.IpPrivada,
                        uua_1003 = Variables_Globales.IdUsuario,
                        fua_1003 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Elimina registros tabla 'CLAVES ECONÓMICAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1003">Identificador registro 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'</param>
        public static void Eliminar_Alchis_1003(string CadenaConexion, string Bdd_Historico, int Id_1003)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1003 Temporal = new Alchis_1003(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1003 = Id_1003;

                    Temporal.Eliminar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion  Tabla Alchis_1003 'CLAVES ECONÓMICAS. INGRESOS'

        #region Tabla Alchis_1003_1  'CLAVES ECONÓMICAS. INGRESOS. CUENTAS ASOCIADAS'

        /// <summary>
        /// Inserta registros tabla 'CLAVES ECONOMICAS. INGRESOS. CUENTAS CONTABLES ASOCIADAS. HISTÓRICO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1003_1">Identificador clave</param>
        /// <param name="Cuenta_Contable">Cuenta contable asociada</param>
        public static void Insertar_Alchis_1003_1(string CadenaConexion, string Bdd_Historico, int Id_1003_1, string Cuenta_Contable)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1003_1 Temporal = new Alchis_1003_1(Cadena_Conexion, Bdd_Historico);
                    Cadena_Conexion.Open();

                    Temporal.ide_1003 = Id_1003_1;
                    Temporal.cta_1003_1 = Cuenta_Contable;
                    Temporal.uar_1003_1 = Variables_Globales.IdUsuario;
                    Temporal.far_1003_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_1003_1 = Variables_Globales.IpPrivada;
                    Temporal.uua_1003_1 = Variables_Globales.IdUsuario;
                    Temporal.fua_1003_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_1003_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Elimina registros tabla 'CLAVES ECONÓMICAS. GASTOS. CUENTAS ASOCIADAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1003">Identificador registro 'HISTÓRICO. CLAVES ECONÓMICAS. INGRESOS'</param>
        public static void Eliminar_Alchis_1003_1(string CadenaConexion, string Bdd_Historico, int Id_1003)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1003_1_Extras Temporal = new Alchis_1003_1_Extras(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1003 = Id_1003;

                    Temporal.Eliminar_Extras();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion  Tabla Alchis_1002_1  'CLAVES ECONÓMICAS. GASTOS. CUENTAS ASOCIADAS'

        #region Tabla Alchis_1004 'APLICACIONES PRESUPUESTARIAS. GASTOS'

        /// <summary>
        /// Inserta registros tabla 'APLICACIONES PRESUPUESTARIAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio de trabajo</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación de la aplicación presupuestaria</param>
        public static void Insertar_Alchis_1004(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica, string Denominacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1004 Temporal = new Alchis_1004(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();

                    Temporal.eje_1004 = Ejercicio;
                    Temporal.ide_1000 = Auxiliares_Historico.Identificador_Alchis_1000(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica);
                    Temporal.cor_1004 = Clave_Organica;
                    Temporal.dco_1004 = Auxiliares_Historico.Denominacion_Clave_Organica(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica);
                    Temporal.ide_1001 = Auxiliares_Historico.Identificador_Alchis_1001(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Funcional);
                    Temporal.cfu_1004 = Clave_Funcional;
                    Temporal.dcf_1004 = Auxiliares_Historico.Denominacion_Clave_Funcional(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Funcional);
                    Temporal.ide_1002 = Auxiliares_Historico.Identificador_Alchis_1002(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Economica);
                    Temporal.cec_1004 = Clave_Economica;
                    Temporal.dce_1004 = Auxiliares_Historico.Denominacion_Clave_Economica_Gastos(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Economica);
                    Temporal.den_1004 = Denominacion;
                    Temporal.uar_1004 = Variables_Globales.IdUsuario;
                    Temporal.far_1004 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_1004 = Variables_Globales.IpPrivada;
                    Temporal.uua_1004 = Variables_Globales.IdUsuario;
                    Temporal.fua_1004 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_1004 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Inserta registros tabla 'APLICACIONES PRESUPUESTARIAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio clave</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD'</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Denominacion_Organica">Denominación clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Denominacion_Funcional">Denominación clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion_Economica">Denominación clave económica</param>
        /// <param name="Denominacion_Aplicacion">Denominación aplicación presupuestaria</param>
        /// <returns>Identificador registro 'APLICACIONES PRESUPUESTARIAS. GASTOS'</returns>
        public static int Insertar_Alchis_1004(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica,
            string Denominacion_Organica, string Clave_Funcional, string Denominacion_Funcional, string Clave_Economica,
            string Denominacion_Economica, string Denominacion_Aplicacion)
        {
            int Id_1000 = Auxiliares_Historico.Identificador_Alchis_1000(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica);
            int Id_1001 = Auxiliares_Historico.Identificador_Alchis_1001(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Funcional);
            int Id_1002 = Auxiliares_Historico.Identificador_Alchis_1002(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Economica);

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    Alchis_1004 Temporal = new Alchis_1004(Cadena_Conexion, Bdd_Historico)
                    {
                        eje_1004 = Ejercicio,
                        ide_1000 = Id_1000,
                        cor_1004 = Clave_Organica.Trim(),
                        dco_1004 = Denominacion_Organica.Trim(),
                        ide_1001 = Id_1001,
                        cfu_1004 = Clave_Funcional.Trim(),
                        dcf_1004 = Denominacion_Funcional.Trim(),
                        ide_1002 = Id_1002,
                        cec_1004 = Clave_Economica.Trim(),
                        dce_1004 = Denominacion_Economica.Trim(),
                        den_1004 = Denominacion_Aplicacion.Trim(),
                        uar_1004 = Variables_Globales.IdUsuario,
                        far_1004 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_1004 = Variables_Globales.IpPrivada,
                        uua_1004 = Variables_Globales.IdUsuario,
                        fua_1004 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_1004 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_1004;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'APLICACIONES PRESUPUESTARIAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1004">Identificador registro 'HISTÓRICO. 'APLICACIONES PRESUPUESTARIAS. GASTOS'</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        /// <returns>Identificador registro</returns>
        public static void Actualizar_Alchis_1004(string CadenaConexion, string Bdd_Historico, int Id_1004, string Denominacion_Organica, string Denominacion_Funcional, string Denominacion_Economica, string Denominacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1004_Extras Temporal = new Alchis_1004_Extras(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1004 = Id_1004;

                    Temporal.dco_1004 = Denominacion_Organica;
                    Temporal.dcf_1004 = Denominacion_Funcional;
                    Temporal.dce_1004 = Denominacion_Economica;
                    Temporal.den_1004 = Denominacion;
                    Temporal.dip_1004 = Variables_Globales.IpPrivada;
                    Temporal.uua_1004 = Variables_Globales.IdUsuario;
                    Temporal.fua_1004 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza un registro en la tabla 'APLICACIONES PRESUPUESTARIA. GASTOS. HISTÓRICO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1004">Identificador registro 'APLICACIONES PRESUPUESTARIA. GASTOS. HISTÓRICO'</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        public static void Actualizar_Alchis_1004(string CadenaConexion, string Bdd_Historico, int Id_1004, string Denominacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1004_Extras Temporal = new Alchis_1004_Extras(Cadena_Conexion, Bdd_Historico);
                    Cadena_Conexion.Open();
                    Temporal.ide_1004 = Id_1004;
                    Temporal.den_1004 = Denominacion;
                    Temporal.dip_1004 = Variables_Globales.IpPrivada;
                    Temporal.uua_1004 = Variables_Globales.IdUsuario;
                    Temporal.fua_1004 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Denominacion();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina registros tabla 'APLICACIONES PRESUPUESTARIAS. GASTOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1004">Identificador registro 'HISTÓRICO. 'APLICACIONES PRESUPUESTARIAS. GASTOS'</param>
        public static void Eliminar_Alchis_1004(string CadenaConexion, string Bdd_Historico, int Id_1004)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1004 Temporal = new Alchis_1004(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1004 = Id_1004;

                    Temporal.Eliminar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Alchis_1004 'APLICACIONES PRESUPUESTARIAS. GASTOS'

        #region Tabla Alchis_1004_1  'APLICACIONES PRESUPUESTARIAS. GASTOS. CUENTAS ASOCIADAS'

        /// <summary>
        /// Inserta registros tabla 'APLICACIONES PRESUPUESTARIAS. GASTOS. CUENTAS CONTABLES ASOCIADAS. HISTÓRICO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1004">Identificador clave</param>
        /// <param name="Cuenta_Contable">Cuenta contable asociada</param>
        public static void Insertar_Alchis_1004_1(string CadenaConexion, string Bdd_Historico, int Id_1004, string Cuenta_Contable)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1004_1 Temporal = new Alchis_1004_1(Cadena_Conexion, Bdd_Historico);
                    Cadena_Conexion.Open();

                    Temporal.ide_1004 = Id_1004;
                    Temporal.cta_1004_1 = Cuenta_Contable;
                    Temporal.uar_1004_1 = Variables_Globales.IdUsuario;
                    Temporal.far_1004_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_1004_1 = Variables_Globales.IpPrivada;
                    Temporal.uua_1004_1 = Variables_Globales.IdUsuario;
                    Temporal.fua_1004_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_1004_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Elimina registros tabla 'APLICACIONES PRESUPUESTARIAS. GASTOS. CUENTAS ASOCIADAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1004">Identificador registro 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. GASTOS'</param>
        public static void Eliminar_Alchis_1004_1(string CadenaConexion, string Bdd_Historico, int Id_1004)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1004_1_Extras Temporal = new Alchis_1004_1_Extras(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1004 = Id_1004;

                    Temporal.Eliminar_Extras();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Alchis_1004_1  'APLICACIONES PRESUPUESTARIAS. GASTOS. CUENTAS ASOCIADAS'

        #region Tabla Alchis_1006 'APLICACIONES PRESUPUESTARIAS. INGRESOS'

        /// <summary>
        /// Inserta registros tabla 'APLICACIONES PRESUPUESTARIAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Ejercicio">Ejercicio clave</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Denominacion_Organica">Denominación clave orgánica</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion_Economica">Denominación clave económica</param>
        /// <param name="Denominacion_Aplicacion">Denominación aplicación presupuestaria</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alchis_1006(string CadenaConexion, string Bdd_Historico, short Ejercicio, string Clave_Organica, string Denominacion_Organica,
                                                                              string Clave_Economica, string Denominacion_Economica, string Denominacion_Aplicacion)
        {
            int Id_1000 = Auxiliares_Historico.Identificador_Alchis_1000(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica);
            int Id_1003 = Auxiliares_Historico.Identificador_Alchis_1003(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Economica);

            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alchis_1006 Temporal = new Alchis_1006(Cadena_Conexion, Bdd_Historico)
                    {
                        eje_1006 = Ejercicio,
                        ide_1000 = Id_1000,
                        cor_1006 = Clave_Organica.Trim(),
                        dco_1006 = Denominacion_Organica.Trim(),
                        ide_1003 = Id_1003,
                        cec_1006 = Clave_Economica.Trim(),
                        dce_1006 = Denominacion_Economica.Trim(),
                        den_1006 = Denominacion_Aplicacion.Trim(),
                        uar_1006 = Variables_Globales.IdUsuario,
                        far_1006 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_1006 = Variables_Globales.IpPrivada,
                        uua_1006 = Variables_Globales.IdUsuario,
                        fua_1006 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_1006 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_1006;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza registros tabla 'APLICACIONES PRESUPUESTARIAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1006">Identificador registro 'HISTÓRICO. 'APLICACIONES PRESUPUESTARIAS. INGRESOS'</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        /// <returns>Identificador registro</returns>
        public static void Actualizar_Alchis_1006(string CadenaConexion, string Bdd_Historico, int Id_1006, string Denominacion_Organica, string Denominacion_Economica, string Denominacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1006_Extras Temporal = new Alchis_1006_Extras(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1006 = Id_1006;

                    Temporal.dco_1006 = Denominacion_Organica;
                    Temporal.dce_1006 = Denominacion_Economica;
                    Temporal.den_1006 = Denominacion;
                    Temporal.dip_1006 = Variables_Globales.IpPrivada;
                    Temporal.uua_1006 = Variables_Globales.IdUsuario;
                    Temporal.fua_1006 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Extras();
                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza un registro en la tabla de acumulados de 'APLICACIONES PRESUPUESTARIA. INGRESOS. HISTÓRICO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1006">Identificador registro 'APLICACIONES PRESUPUESTARIA. INGRESOS. HISTÓRICO'</param>
        /// <param name="Denominacion">Denominación aplicación presupuestaria</param>
        public static void Actualizar_Alchis_1006(string CadenaConexion, string Bdd_Historico, int Id_1006, string Denominacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1006_Extras Temporal = new Alchis_1006_Extras(Cadena_Conexion, Bdd_Historico);
                    Cadena_Conexion.Open();

                    Temporal.ide_1006 = Id_1006;
                    Temporal.den_1006 = Denominacion;
                    Temporal.dip_1006 = Variables_Globales.IpPrivada;
                    Temporal.uua_1006 = Variables_Globales.IdUsuario;
                    Temporal.fua_1006 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Actualizar_Denominacion();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Elimina registros tabla'APLICACIONES PRESUPUESTARIAS. INGRESOS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1006">Identificador registro 'HISTÓRICO. 'APLICACIONES PRESUPUESTARIAS. INGRESOS'</param>
        public static void Eliminar_Alchis_1006(string CadenaConexion, string Bdd_Historico, int Id_1006)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1006 Temporal = new Alchis_1006(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1006 = Id_1006;

                    Temporal.Eliminar();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Alchis_1004 'APLICACIONES PRESUPUESTARIAS. INGRESOS'

        #region Tabla Alchis_1006_1  'APLICACIONES PRESUPUESTARIAS. INGRESOS. CUENTAS ASOCIADAS'

        /// <summary>
        /// Inserta registros tabla 'APLICACIONES PRESUPUESTARIAS. INGRESOS. CUENTAS CONTABLES ASOCIADAS. HISTÓRICO'
        /// </summary>
        /// <param name="CadenaConexion">Cadena conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1006">Identificador clave</param>
        /// <param name="Cuenta_Contable">Cuenta contable asociada</param>
        public static void Insertar_Alchis_1006_1(string CadenaConexion, string Bdd_Historico, int Id_1006, string Cuenta_Contable)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Cadena_Conexion.Open();
                    Alchis_1006_1 Temporal = new Alchis_1006_1(Cadena_Conexion, Bdd_Historico)
                    {
                        ide_1006 = Id_1006,
                        cta_1006_1 = Cuenta_Contable,
                        uar_1006_1 = Variables_Globales.IdUsuario,
                        far_1006_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"),
                        dip_1006_1 = Variables_Globales.IpPrivada,
                        uua_1006_1 = Variables_Globales.IdUsuario,
                        fua_1006_1 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Insertar();

                    if (Temporal.ide_1006_1 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Elimina registros tabla 'APLICACIONES PRESUPUESTARIAS. INGRESOS. CUENTAS ASOCIADAS'
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_1006">Identificador registro 'HISTÓRICO. APLICACIONES PRESUPUESTARIAS. INGRESOS'</param>
        public static void Eliminar_Alchis_1006_1(string CadenaConexion, string Bdd_Historico, int Id_1006)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_1006_1_Extras Temporal = new Alchis_1006_1_Extras(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_1006 = Id_1006;

                    Temporal.Eliminar_Extras();

                    Cadena_Conexion.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Alchis_1004_1  'APLICACIONES PRESUPUESTARIAS. GASTOS. CUENTAS ASOCIADAS'

        #region Tabla Alchis_2004 'HISTÓRICO. REMANENTES DE CRÉDITO'

        /// <summary>
        /// Inserta un registro en la tabla 'REMANENTES DE CRÉDITO. HISTÓRICO. VINCULACIONES' 
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio actual</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param>
        /// <param name="Denominacion">Denominación aplicacion presupuestaria</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alchis_2004(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica, string Denominacion,
                                                                              int Id_Vinculacion, decimal Remanente_Incorporable, decimal Remanente_No_Incorporable, decimal Remanente_Ejecutado)
        {
            int Id_Aplicacion = Auxiliares_Historico.Identificador_Alchis_1004(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica, Clave_Funcional, Clave_Economica);

            if (Denominacion.Trim() == string.Empty)
                Denominacion = Auxiliares_Historico.Denominacion_Aplicacion_Gastos(CadenaConexion, Bdd_Historico, Id_Aplicacion);

            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_2004 Temporal = new Alchis_2004(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.ide_2005 = Id_Vinculacion;
                    Temporal.ide_1004 = Id_Aplicacion;
                    Temporal.eje_2004 = Ejercicio;
                    Temporal.cor_2004 = Clave_Organica.Trim();
                    Temporal.cfu_2004 = Clave_Funcional.Trim();
                    Temporal.cec_2004 = Clave_Economica.Trim();
                    Temporal.den_2004 = Denominacion.Trim();
                    Temporal.rci_2004 = Remanente_Incorporable;
                    Temporal.rni_2004 = Remanente_No_Incorporable;
                    Temporal.rej_2004 = Remanente_Ejecutado;
                    Temporal.uar_2004 = Variables_Globales.IdUsuario;
                    Temporal.far_2004 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_2004 = Variables_Globales.IpPrivada;
                    Temporal.uua_2004 = Variables_Globales.IdUsuario;
                    Temporal.fua_2004 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_2004 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_2004;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Actualiza un registro en la tabla 'REMANENTES DE CRÉDITO. HISTÓRICO' 
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Id_2004">Identificador registro 'REMANENTES DE CRÉDITO. HISTÓRICO' </param>
        /// <param name="Importe">Importe a actualizar</param>
        /// <param name="Anulado">Semáforo actualiza/desactualiza
        /// -1 Desactualiza
        /// 1 Actualiza
        /// </param>
        /// <returns>Identificador registro</returns>
        public static void Actualizar_Alchis_2004(string CadenaConexion, string Bdd_Historico, int Id_2004, decimal Importe, sbyte Anulado = 1)
        {
            using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
            {
                try
                {
                    Cadena_Conexion.Open();
                    Alchis_2004_Extras Temporal = new Alchis_2004_Extras(Cadena_Conexion, Bdd_Historico)
                    {
                        ide_2004 = Id_2004,
                        rej_2004 = Importe * Anulado,
                        dip_2004 = Variables_Globales.IpPrivada,
                        uua_2004 = Variables_Globales.IdUsuario,
                        fua_2004 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
                    };

                    Temporal.Actualizar();
                    Cadena_Conexion.Close();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion Tabla Alchis_2004 'HISTÓRICO. REMANENTES DE CRÉDITO'

        #region Tabla Alchis_2005 'HISTÓRICO. REMANENTES DE CRÉDITO. VINCULACIONES'

        /// <summary>
        /// Inserta un registro en la tabla 'REMANENTES DE CRÉDITO. HISTÓRICO. VINCULACIONES' 
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio actual</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica vinculada</param>
        /// <param name="Clave_Funcional">Clave funcional vinculada</param>
        /// <param name="Clave_Economica">Clave económica vinculada</param>
        /// <param name="Denominacion">Denominación aplicacion presupuestaria viculada</param>
        /// <returns>Identificador registro</returns>
        public static int Insertar_Alchis_2005(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica, string Denominacion)
        {
            try
            {
                using (SqlConnection Cadena_Conexion = new SqlConnection(CadenaConexion))
                {
                    Alchis_2005 Temporal = new Alchis_2005(Cadena_Conexion, Bdd_Historico);

                    Cadena_Conexion.Open();
                    Temporal.eje_2005 = Ejercicio;
                    Temporal.cor_2005 = Clave_Organica.Trim();
                    Temporal.cfu_2005 = Clave_Funcional.Trim();
                    Temporal.cec_2005 = Clave_Economica.Trim();
                    Temporal.den_2005 = Denominacion.Trim();
                    Temporal.uar_2005 = Variables_Globales.IdUsuario;
                    Temporal.far_2005 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    Temporal.dip_2005 = Variables_Globales.IpPrivada;
                    Temporal.uua_2005 = Variables_Globales.IdUsuario;
                    Temporal.fua_2005 = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    Temporal.Insertar();

                    if (Temporal.ide_2005 == 0)
                    {
                        Temporal.Eliminar();
                        Temporal.Insertar();
                    }

                    Cadena_Conexion.Close();

                    return Temporal.ide_2005;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Tabla Alchis_2005 'HISTÓRICO. REMANENTES DE CRÉDITO. VINCULACIONES'

        /// <summary>
        /// Verifica y actualiza los importe incorporados  'REMANENTES DE CRÉDITO. HISTÓRICO. VINCULACIONES' 
        /// </summary>
        /// <param name="CadenaConexion">Cadena de conexión</param>
        /// <param name="Ejercicio">Ejercicio actual</param>
        /// <param name="Bdd_Historico">Nombre BDD 'HISTÓRICO. CONTABILIDAD</param>
        /// <param name="Clave_Organica">Clave orgánica</param>
        /// <param name="Clave_Funcional">Clave funcional</param>
        /// <param name="Clave_Economica">Clave económica</param> 
        /// <param name="Importe">Importe</param> 
        public static void Verificar_Alchis_2004(string CadenaConexion, short Ejercicio, string Bdd_Historico, string Clave_Organica, string Clave_Funcional, string Clave_Economica, decimal Importe, sbyte Anulado = 1)
        {
            int Id_2004 = Auxiliares_Historico.Identificador_Alchis_2004(CadenaConexion, Ejercicio, Bdd_Historico, Clave_Organica, Clave_Funcional, Clave_Economica);

            if (Id_2004 != 0)
                // ACTUALIZAR IMPORTE INCORPORADO TABLA 'HISTÓRICO. REMANENETES DE CRÉDITO. APLICACIONES PRESUPUESTARIAS'
                Desencadenantes_Historico.Actualizar_Alchis_2004(CadenaConexion, Bdd_Historico, Id_2004, Importe, Anulado);
        }


    }
}