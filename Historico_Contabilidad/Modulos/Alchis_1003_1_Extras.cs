using System;
using System.Data.SqlClient;

public class Alchis_1003_1_Extras
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1003_1;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_1003;                                          // 2   CLAVE_UNICA 
    private string _cta_1003_1;                                     // 3   CLAVE_UNICA 
    private int _uar_1003_1;                                        // 4    
    private string _far_1003_1;                                     // 5    
    private string _dip_1003_1;                                     // 6    
    private int _uua_1003_1;                                        // 7    
    private string _fua_1003_1;                                     // 8    

    #endregion

    #region Propiedades

    public int ide_1003_1
    {
        get { return _ide_1003_1; }
        set { _ide_1003_1 = value; }
    }
    public int ide_1003
    {
        get { return _ide_1003; }
        set { _ide_1003 = value; }
    }
    public string cta_1003_1
    {
        get { return _cta_1003_1; }
        set { _cta_1003_1 = value; }
    }
    public int uar_1003_1
    {
        get { return _uar_1003_1; }
        set { _uar_1003_1 = value; }
    }
    public string far_1003_1
    {
        get { return _far_1003_1; }
        set { _far_1003_1 = value; }
    }
    public string dip_1003_1
    {
        get { return _dip_1003_1; }
        set { _dip_1003_1 = value; }
    }
    public int uua_1003_1
    {
        get { return _uua_1003_1; }
        set { _uua_1003_1 = value; }
    }
    public string fua_1003_1
    {
        get { return _fua_1003_1; }
        set { _fua_1003_1 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1003_1_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1003_1 = 0;
        _ide_1003 = 0;
        _cta_1003_1 = "";
        _uar_1003_1 = 0;
        _far_1003_1 = "";
        _dip_1003_1 = "";
        _uua_1003_1 = 0;
        _fua_1003_1 = "";
    }

    #endregion

    #region Métodos públicos

    /// <summary>
    /// Elimina todos los registros relacionados 
    /// </summary>
    public void Eliminar_Extras()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_1003_1 WHERE ide_1003 = " + _ide_1003;

        Command.ExecuteNonQuery();
    }

    #endregion

}