using System;
using System.Data.SqlClient;

public class Alchis_1002_Extras
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1002;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_1002;                                        // 2   CLAVE_UNICA 
    private Int16 _ide_5002;                                        // 3    
    private string _cec_1002;                                       // 4   CLAVE_UNICA 
    private string _den_1002;                                       // 5    
    private byte _sun_1002;                                         // 6    
    private int _uar_1002;                                          // 7    
    private string _far_1002;                                       // 8    
    private string _dip_1002;                                       // 9    
    private int _uua_1002;                                          // 10    
    private string _fua_1002;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_1002
    {
        get { return _ide_1002; }
        set { _ide_1002 = value; }
    }
    public Int16 eje_1002
    {
        get { return _eje_1002; }
        set { _eje_1002 = value; }
    }
    public Int16 ide_5002
    {
        get { return _ide_5002; }
        set { _ide_5002 = value; }
    }
    public string cec_1002
    {
        get { return _cec_1002; }
        set { _cec_1002 = value; }
    }
    public string den_1002
    {
        get { return _den_1002; }
        set { _den_1002 = value; }
    }
    public byte sun_1002
    {
        get { return _sun_1002; }
        set { _sun_1002 = value; }
    }
    public int uar_1002
    {
        get { return _uar_1002; }
        set { _uar_1002 = value; }
    }
    public string far_1002
    {
        get { return _far_1002; }
        set { _far_1002 = value; }
    }
    public string dip_1002
    {
        get { return _dip_1002; }
        set { _dip_1002 = value; }
    }
    public int uua_1002
    {
        get { return _uua_1002; }
        set { _uua_1002 = value; }
    }
    public string fua_1002
    {
        get { return _fua_1002; }
        set { _fua_1002 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1002_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1002 = 0;
        _eje_1002 = 0;
        _ide_5002 = 0;
        _cec_1002 = "";
        _den_1002 = "";
        _sun_1002 = 0;
        _uar_1002 = 0;
        _far_1002 = "";
        _dip_1002 = "";
        _uua_1002 = 0;
        _fua_1002 = "";
    }

    #endregion

    #region Metodos Públicos

    /// <summary>
    /// Actualiza las columnas 
    /// - Denominación
    /// - Semáforo último nivel
    /// </summary>
    public void Actualizar_Extras()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_1002 SET");
        strSQL += String.Concat(" den_1002 = @den_1002");
        strSQL += String.Concat(", sun_1002 = @sun_1002");
        strSQL += String.Concat(", dip_1002 = @dip_1002");
        strSQL += String.Concat(", uua_1002 = @uua_1002");
        strSQL += String.Concat(", fua_1002 = @fua_1002");
        strSQL += String.Concat(" WHERE ide_1002 = ", _ide_1002);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_1002", _den_1002);
        Command.Parameters.AddWithValue("@sun_1002", _sun_1002);
        Command.Parameters.AddWithValue("@dip_1002", _dip_1002);
        Command.Parameters.AddWithValue("@uua_1002", _uua_1002);
        Command.Parameters.AddWithValue("@fua_1002", _fua_1002);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    #endregion

}