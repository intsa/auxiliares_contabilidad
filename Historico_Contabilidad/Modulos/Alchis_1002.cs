using System;
using System.Data.SqlClient;

public class Alchis_1002
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1002;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_1002;                                        // 2   CLAVE_UNICA 
    private Int16 _ide_5002;                                        // 3    
    private string _cec_1002;                                       // 4   CLAVE_UNICA 
    private string _den_1002;                                       // 5    
    private byte _sun_1002;                                         // 6    
    private int _uar_1002;                                          // 7    
    private string _far_1002;                                       // 8    
    private string _dip_1002;                                       // 9    
    private int _uua_1002;                                          // 10    
    private string _fua_1002;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_1002
    {
        get { return _ide_1002; }
        set { _ide_1002 = value; }
    }
    public Int16 eje_1002
    {
        get { return _eje_1002; }
        set { _eje_1002 = value; }
    }
    public Int16 ide_5002
    {
        get { return _ide_5002; }
        set { _ide_5002 = value; }
    }
    public string cec_1002
    {
        get { return _cec_1002; }
        set { _cec_1002 = value; }
    }
    public string den_1002
    {
        get { return _den_1002; }
        set { _den_1002 = value; }
    }
    public byte sun_1002
    {
        get { return _sun_1002; }
        set { _sun_1002 = value; }
    }
    public int uar_1002
    {
        get { return _uar_1002; }
        set { _uar_1002 = value; }
    }
    public string far_1002
    {
        get { return _far_1002; }
        set { _far_1002 = value; }
    }
    public string dip_1002
    {
        get { return _dip_1002; }
        set { _dip_1002 = value; }
    }
    public int uua_1002
    {
        get { return _uua_1002; }
        set { _uua_1002 = value; }
    }
    public string fua_1002
    {
        get { return _fua_1002; }
        set { _fua_1002 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1002(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1002 = 0;
        _eje_1002 = 0;
        _ide_5002 = 0;
        _cec_1002 = "";
        _den_1002 = "";
        _sun_1002 = 0;
        _uar_1002 = 0;
        _far_1002 = "";
        _dip_1002 = "";
        _uua_1002 = 0;
        _fua_1002 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.alchis_1002 (" +
        "  eje_1002" + ", ide_5002" + ", cec_1002" + ", den_1002" + ", sun_1002" + ", uar_1002" + ", far_1002" + ", dip_1002" + ", uua_1002" + ", fua_1002" +
        ") OUTPUT INSERTED.ide_1002 VALUES (" +
        "  @eje_1002" + ", @ide_5002" + ", @cec_1002" + ", @den_1002" + ", @sun_1002" + ", @uar_1002" + ", @far_1002" + ", @dip_1002" + ", @uua_1002" + ", @fua_1002" +
         ")";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1002", _eje_1002);
        Command.Parameters.AddWithValue("@ide_5002", _ide_5002);
        Command.Parameters.AddWithValue("@cec_1002", _cec_1002);
        Command.Parameters.AddWithValue("@den_1002", _den_1002);
        Command.Parameters.AddWithValue("@sun_1002", _sun_1002);
        Command.Parameters.AddWithValue("@uar_1002", _uar_1002);
        Command.Parameters.AddWithValue("@far_1002", _far_1002);
        Command.Parameters.AddWithValue("@dip_1002", _dip_1002);
        Command.Parameters.AddWithValue("@uua_1002", _uua_1002);
        Command.Parameters.AddWithValue("@fua_1002", _fua_1002);
        Command.Connection = _sqlCon;

        _ide_1002 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1002 WHERE ide_1002 = " + _ide_1002;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1002 = (int)dr["ide_1002"];

            if (AsignarPropiedades)
            {
                eje_1002 = (Int16)dr["eje_1002"];
                ide_5002 = (Int16)dr["ide_5002"];
                cec_1002 = (string)dr["cec_1002"];
                den_1002 = (string)dr["den_1002"];
                sun_1002 = Convert.ToByte(dr["sun_1002"]);
                uar_1002 = (int)dr["uar_1002"];
                far_1002 = dr["far_1002"].ToString();
                dip_1002 = (string)dr["dip_1002"];
                uua_1002 = (int)dr["uua_1002"];
                fua_1002 = dr["fua_1002"].ToString();
            }
        }
        else
        {
            ide_1002 = 0;
        }

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_1002 WHERE ide_1002 = " + _ide_1002;

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_1002 SET");
        strSQL += String.Concat(" eje_1002 = @eje_1002");
        strSQL += String.Concat(", ide_5002 = @ide_5002");
        strSQL += String.Concat(", cec_1002 = @cec_1002");
        strSQL += String.Concat(", den_1002 = @den_1002");
        strSQL += String.Concat(", sun_1002 = @sun_1002");
        strSQL += String.Concat(", dip_1002 = @dip_1002");
        strSQL += String.Concat(", uua_1002 = @uua_1002");
        strSQL += String.Concat(", fua_1002 = @fua_1002");
        strSQL += String.Concat(" WHERE ide_1002 = ", _ide_1002);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1002", _eje_1002);
        Command.Parameters.AddWithValue("@ide_5002", _ide_5002);
        Command.Parameters.AddWithValue("@cec_1002", _cec_1002);
        Command.Parameters.AddWithValue("@den_1002", _den_1002);
        Command.Parameters.AddWithValue("@sun_1002", _sun_1002);
        Command.Parameters.AddWithValue("@dip_1002", _dip_1002);
        Command.Parameters.AddWithValue("@uua_1002", _uua_1002);
        Command.Parameters.AddWithValue("@fua_1002", _fua_1002);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: eje_1002 cec_1002
    public void Cargar_eje_1002_cec_1002(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1002 WHERE eje_1002 = " + _eje_1002 + " AND cec_1002 = '" + _cec_1002 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1002 = (int)dr["ide_1002"];

            if (AsignarPropiedades)
            {
                eje_1002 = (Int16)dr["eje_1002"];
                ide_5002 = (Int16)dr["ide_5002"];
                cec_1002 = (string)dr["cec_1002"];
                den_1002 = (string)dr["den_1002"];
                sun_1002 = Convert.ToByte(dr["sun_1002"]);
                uar_1002 = (int)dr["uar_1002"];
                far_1002 = dr["far_1002"].ToString();
                dip_1002 = (string)dr["dip_1002"];
                uua_1002 = (int)dr["uua_1002"];
                fua_1002 = dr["fua_1002"].ToString();
            }
        }
        else
        {
            ide_1002 = 0;
        }

        dr.Close();
    }

    #endregion

}