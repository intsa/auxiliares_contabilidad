using System;
using System.Data.SqlClient;

public class Alchis_1006_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1006;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_1006;                                        // 2   CLAVE_UNICA 
    private int _ide_1000;                                          // 3    
    private string _cor_1006;                                       // 4   CLAVE_UNICA 
    private string _dco_1006;                                       // 5    
    private int _ide_1003;                                          // 6    
    private string _cec_1006;                                       // 7   CLAVE_UNICA 
    private string _dce_1006;                                       // 8    
    private string _den_1006;                                       // 9    
    private int _uar_1006;                                          // 11    
    private string _far_1006;                                       // 12    
    private string _dip_1006;                                       // 13    
    private int _uua_1006;                                          // 14    
    private string _fua_1006;                                       // 15    

    #endregion

    #region Propiedades

    public int ide_1006
    {
        get { return _ide_1006; }
        set { _ide_1006 = value; }
    }
    public Int16 eje_1006
    {
        get { return _eje_1006; }
        set { _eje_1006 = value; }
    }
    public int ide_1000
    {
        get { return _ide_1000; }
        set { _ide_1000 = value; }
    }
    public string cor_1006
    {
        get { return _cor_1006; }
        set { _cor_1006 = value; }
    }
    public string dco_1006
    {
        get { return _dco_1006; }
        set { _dco_1006 = value; }
    }
    public int ide_1003
    {
        get { return _ide_1003; }
        set { _ide_1003 = value; }
    }
    public string cec_1006
    {
        get { return _cec_1006; }
        set { _cec_1006 = value; }
    }
    public string dce_1006
    {
        get { return _dce_1006; }
        set { _dce_1006 = value; }
    }
    public string den_1006
    {
        get { return _den_1006; }
        set { _den_1006 = value; }
    }
    public int uar_1006
    {
        get { return _uar_1006; }
        set { _uar_1006 = value; }
    }
    public string far_1006
    {
        get { return _far_1006; }
        set { _far_1006 = value; }
    }
    public string dip_1006
    {
        get { return _dip_1006; }
        set { _dip_1006 = value; }
    }
    public int uua_1006
    {
        get { return _uua_1006; }
        set { _uua_1006 = value; }
    }
    public string fua_1006
    {
        get { return _fua_1006; }
        set { _fua_1006 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1006_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1006 = 0;
        _eje_1006 = 0;
        _ide_1000 = 0;
        _cor_1006 = "";
        _dco_1006 = "";
        _ide_1003 = 0;
        _cec_1006 = "";
        _dce_1006 = "";
        _den_1006 = "";
        _uar_1006 = 0;
        _far_1006 = "";
        _dip_1006 = "";
        _uua_1006 = 0;
        _fua_1006 = "";
    }

    #endregion

    #region Métodos públicos

    /// <summary>
    /// Actualiza las columnas 
    /// - Denominación clave orgánica
    /// - Denominación clave económica
    /// - Denominación aplicación presupuestaria
    /// </summary>
    public void Actualizar_Extras()
    {
        string Cadena_Sql = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        Cadena_Sql = string.Concat(Cadena_Sql, "UPDATE " + _BaseDatos + ".dbo.alchis_1006 SET");
        Cadena_Sql += string.Concat(" dco_1006 = @dco_1006");
        Cadena_Sql += string.Concat(", dce_1006 = @dce_1006");
        Cadena_Sql += string.Concat(", den_1006 = @den_1006");
        Cadena_Sql += string.Concat(", dip_1006 = @dip_1006");
        Cadena_Sql += string.Concat(", uua_1006 = @uua_1006");
        Cadena_Sql += string.Concat(", fua_1006 = @fua_1006");
        Cadena_Sql += string.Concat(" WHERE ide_1006 = ", _ide_1006);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@dco_1006", _dco_1006);
        Command.Parameters.AddWithValue("@dce_1006", _dce_1006);
        Command.Parameters.AddWithValue("@den_1006", _den_1006);
        Command.Parameters.AddWithValue("@dip_1006", _dip_1006);
        Command.Parameters.AddWithValue("@uua_1006", _uua_1006);
        Command.Parameters.AddWithValue("@fua_1006", _fua_1006);

        Command.CommandText = Cadena_Sql;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza las columnas 
    /// - Denominación aplicación presupuestaria
    /// </summary>
    public void Actualizar_Denominacion()
    {
        string Cadena_Sql = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        Cadena_Sql = string.Concat(Cadena_Sql, "UPDATE " + _BaseDatos + ".dbo.alchis_1006 SET");
        Cadena_Sql += string.Concat(" den_1006 = @den_1006");
        Cadena_Sql += string.Concat(", dip_1006 = @dip_1006");
        Cadena_Sql += string.Concat(", uua_1006 = @uua_1006");
        Cadena_Sql += string.Concat(", fua_1006 = @fua_1006");
        Cadena_Sql += string.Concat(" WHERE ide_1006 = ", _ide_1006);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_1006", _den_1006);
        Command.Parameters.AddWithValue("@dip_1006", _dip_1006);
        Command.Parameters.AddWithValue("@uua_1006", _uua_1006);
        Command.Parameters.AddWithValue("@fua_1006", _fua_1006);

        Command.CommandText = Cadena_Sql;
        Command.ExecuteNonQuery();
    }

    #endregion

}