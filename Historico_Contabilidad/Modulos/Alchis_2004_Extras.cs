using System;
using System.Data.SqlClient;

public class Alchis_2004_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_2004;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_1004;                                          // 2    
    private int _ide_2005;                                          // 3    
    private Int16 _eje_2004;                                        // 4   CLAVE_UNICA 
    private string _cor_2004;                                       // 5   CLAVE_UNICA 
    private string _cfu_2004;                                       // 6   CLAVE_UNICA 
    private string _cec_2004;                                       // 7   CLAVE_UNICA 
    private string _den_2004;                                       // 8    
    private decimal _rci_2004;                                      // 9    
    private decimal _rni_2004;                                      // 10    
    private decimal? _tri_2004;                                     // 11    CALCULADO
    private decimal _rej_2004;                                      // 12    
    private int _uar_2004;                                          // 13    
    private string _far_2004;                                       // 14    
    private string _dip_2004;                                       // 15    
    private int _uua_2004;                                          // 16    
    private string _fua_2004;                                       // 17    

    #endregion

    #region Propiedades

    public int ide_2004
    {
        get { return _ide_2004; }
        set { _ide_2004 = value; }
    }
    public int ide_1004
    {
        get { return _ide_1004; }
        set { _ide_1004 = value; }
    }
    public int ide_2005
    {
        get { return _ide_2005; }
        set { _ide_2005 = value; }
    }
    public Int16 eje_2004
    {
        get { return _eje_2004; }
        set { _eje_2004 = value; }
    }
    public string cor_2004
    {
        get { return _cor_2004; }
        set { _cor_2004 = value; }
    }
    public string cfu_2004
    {
        get { return _cfu_2004; }
        set { _cfu_2004 = value; }
    }
    public string cec_2004
    {
        get { return _cec_2004; }
        set { _cec_2004 = value; }
    }
    public string den_2004
    {
        get { return _den_2004; }
        set { _den_2004 = value; }
    }
    public decimal rci_2004
    {
        get { return _rci_2004; }
        set { _rci_2004 = value; }
    }
    public decimal rni_2004
    {
        get { return _rni_2004; }
        set { _rni_2004 = value; }
    }
    public decimal? tri_2004
    {
        get { return _tri_2004; }
        set { _tri_2004 = value; }
    }
    public decimal rej_2004
    {
        get { return _rej_2004; }
        set { _rej_2004 = value; }
    }
    public int uar_2004
    {
        get { return _uar_2004; }
        set { _uar_2004 = value; }
    }
    public string far_2004
    {
        get { return _far_2004; }
        set { _far_2004 = value; }
    }
    public string dip_2004
    {
        get { return _dip_2004; }
        set { _dip_2004 = value; }
    }
    public int uua_2004
    {
        get { return _uua_2004; }
        set { _uua_2004 = value; }
    }
    public string fua_2004
    {
        get { return _fua_2004; }
        set { _fua_2004 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_2004_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? "Adm_Loc_Con_His_000999";

        _ide_2004 = 0;
        _ide_1004 = 0;
        _ide_2005 = 0;
        _eje_2004 = 0;
        _cor_2004 = "";
        _cfu_2004 = "";
        _cec_2004 = "";
        _den_2004 = "";
        _rci_2004 = 0;
        _rni_2004 = 0;
        _tri_2004 = null;
        _rej_2004 = 0;
        _uar_2004 = 0;
        _far_2004 = "";
        _dip_2004 = "";
        _uua_2004 = 0;
        _fua_2004 = "";
    }

    #endregion

    #region Metodos Públicos

    /// <summary>
    /// Acualiza el importe incorporado del remanente
    /// </summary>
    public void Actualizar()
    {
        string Cadena_Sql = "";
        string Cadena_Update = $"UPDATE {_BaseDatos}.dbo.alchis_2004";
        string Cadena_Valores = "";
        string Cadena_Where = "";

        SqlCommand Command = new SqlCommand();
        Command.Connection = _sqlCon;

        Cadena_Valores = " SET rej_2004 = rej_2004 + " + Convert.ToString(_rej_2004).Replace(",", ".") + ",";
        Cadena_Valores = Cadena_Valores + " dip_2004 = '" + _dip_2004 + "',";
        Cadena_Valores = Cadena_Valores + " uua_2004 = " + _uua_2004 + ",";
        Cadena_Valores = Cadena_Valores + " fua_2004 = '" + _fua_2004 + "'";
        Cadena_Where = " WHERE ide_2004 = " + _ide_2004;

        Cadena_Sql = Cadena_Update + Cadena_Valores + Cadena_Where;

        Command.CommandText = Cadena_Sql;

        Command.ExecuteNonQuery();
        Command = null;
    }

    #endregion

}