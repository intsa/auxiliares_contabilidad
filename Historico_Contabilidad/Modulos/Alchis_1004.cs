using System;
using System.Data.SqlClient;

public class Alchis_1004
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1004;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _eje_1004;                                        // 2   CLAVE_UNICA 
    private int _ide_1000;                                          // 3    
    private string _cor_1004;                                       // 4   CLAVE_UNICA 
    private string _dco_1004;                                       // 5    
    private int _ide_1001;                                          // 6    
    private string _cfu_1004;                                       // 7   CLAVE_UNICA 
    private string _dcf_1004;                                       // 8    
    private int _ide_1002;                                          // 9    
    private string _cec_1004;                                       // 10   CLAVE_UNICA 
    private string _dce_1004;                                       // 11    
    private string _den_1004;                                       // 12    
    private int _uar_1004;                                          // 35    
    private string _far_1004;                                       // 36    
    private string _dip_1004;                                       // 37    
    private int _uua_1004;                                          // 38    
    private string _fua_1004;                                       // 39    

    #endregion

    #region Propiedades

    public int ide_1004
    {
        get { return _ide_1004; }
        set { _ide_1004 = value; }
    }
    public short eje_1004
    {
        get { return _eje_1004; }
        set { _eje_1004 = value; }
    }
    public int ide_1000
    {
        get { return _ide_1000; }
        set { _ide_1000 = value; }
    }
    public string cor_1004
    {
        get { return _cor_1004; }
        set { _cor_1004 = value; }
    }
    public string dco_1004
    {
        get { return _dco_1004; }
        set { _dco_1004 = value; }
    }
    public int ide_1001
    {
        get { return _ide_1001; }
        set { _ide_1001 = value; }
    }
    public string cfu_1004
    {
        get { return _cfu_1004; }
        set { _cfu_1004 = value; }
    }
    public string dcf_1004
    {
        get { return _dcf_1004; }
        set { _dcf_1004 = value; }
    }
    public int ide_1002
    {
        get { return _ide_1002; }
        set { _ide_1002 = value; }
    }
    public string cec_1004
    {
        get { return _cec_1004; }
        set { _cec_1004 = value; }
    }
    public string dce_1004
    {
        get { return _dce_1004; }
        set { _dce_1004 = value; }
    }
    public string den_1004
    {
        get { return _den_1004; }
        set { _den_1004 = value; }
    }
    public int uar_1004
    {
        get { return _uar_1004; }
        set { _uar_1004 = value; }
    }
    public string far_1004
    {
        get { return _far_1004; }
        set { _far_1004 = value; }
    }
    public string dip_1004
    {
        get { return _dip_1004; }
        set { _dip_1004 = value; }
    }
    public int uua_1004
    {
        get { return _uua_1004; }
        set { _uua_1004 = value; }
    }
    public string fua_1004
    {
        get { return _fua_1004; }
        set { _fua_1004 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1004(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1004 = 0;
        _eje_1004 = 0;
        _ide_1000 = 0;
        _cor_1004 = "";
        _dco_1004 = "";
        _ide_1001 = 0;
        _cfu_1004 = "";
        _dcf_1004 = "";
        _ide_1002 = 0;
        _cec_1004 = "";
        _dce_1004 = "";
        _den_1004 = "";
        _uar_1004 = 0;
        _far_1004 = "";
        _dip_1004 = "";
        _uua_1004 = 0;
        _fua_1004 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.alchis_1004 (" +
        "  eje_1004" + ", ide_1000" + ", cor_1004" + ", dco_1004" + ", ide_1001" + ", cfu_1004" + ", dcf_1004" + ", ide_1002" + ", cec_1004" + ", dce_1004" +
        ", den_1004" + ", uar_1004" + ", far_1004" + ", dip_1004" + ", uua_1004" + ", fua_1004" +
        ") OUTPUT INSERTED.ide_1004 VALUES (" +
        "  @eje_1004" + ", @ide_1000" + ", @cor_1004" + ", @dco_1004" + ", @ide_1001" + ", @cfu_1004" + ", @dcf_1004" + ", @ide_1002" + ", @cec_1004" + ", @dce_1004" +
        ", @den_1004" + ", @uar_1004" + ", @far_1004" + ", @dip_1004" + ", @uua_1004" + ", @fua_1004" +
         ")";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1004", _eje_1004);
        Command.Parameters.AddWithValue("@ide_1000", _ide_1000);
        Command.Parameters.AddWithValue("@cor_1004", _cor_1004);
        Command.Parameters.AddWithValue("@dco_1004", _dco_1004);
        Command.Parameters.AddWithValue("@ide_1001", _ide_1001);
        Command.Parameters.AddWithValue("@cfu_1004", _cfu_1004);
        Command.Parameters.AddWithValue("@dcf_1004", _dcf_1004);
        Command.Parameters.AddWithValue("@ide_1002", _ide_1002);
        Command.Parameters.AddWithValue("@cec_1004", _cec_1004);
        Command.Parameters.AddWithValue("@dce_1004", _dce_1004);
        Command.Parameters.AddWithValue("@den_1004", _den_1004);
        Command.Parameters.AddWithValue("@uar_1004", _uar_1004);
        Command.Parameters.AddWithValue("@far_1004", _far_1004);
        Command.Parameters.AddWithValue("@dip_1004", _dip_1004);
        Command.Parameters.AddWithValue("@uua_1004", _uua_1004);
        Command.Parameters.AddWithValue("@fua_1004", _fua_1004);
        Command.Connection = _sqlCon;

        _ide_1004 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1004 WHERE ide_1004 = " + _ide_1004;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1004 = (int)dr["ide_1004"];

            if (AsignarPropiedades)
            {
                eje_1004 = (Int16)dr["eje_1004"];
                ide_1000 = (int)dr["ide_1000"];
                cor_1004 = (string)dr["cor_1004"];
                dco_1004 = (string)dr["dco_1004"];
                ide_1001 = (int)dr["ide_1001"];
                cfu_1004 = (string)dr["cfu_1004"];
                dcf_1004 = (string)dr["dcf_1004"];
                ide_1002 = (int)dr["ide_1002"];
                cec_1004 = (string)dr["cec_1004"];
                dce_1004 = (string)dr["dce_1004"];
                den_1004 = (string)dr["den_1004"];
                uar_1004 = (int)dr["uar_1004"];
                far_1004 = dr["far_1004"].ToString();
                dip_1004 = (string)dr["dip_1004"];
                uua_1004 = (int)dr["uua_1004"];
                fua_1004 = dr["fua_1004"].ToString();
            }
        }
        else
        {
            ide_1004 = 0;
        }

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_1004 WHERE ide_1004 = " + _ide_1004;

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_1004 SET");
        strSQL += String.Concat(" eje_1004 = @eje_1004");
        strSQL += String.Concat(", ide_1000 = @ide_1000");
        strSQL += String.Concat(", cor_1004 = @cor_1004");
        strSQL += String.Concat(", dco_1004 = @dco_1004");
        strSQL += String.Concat(", ide_1001 = @ide_1001");
        strSQL += String.Concat(", cfu_1004 = @cfu_1004");
        strSQL += String.Concat(", dcf_1004 = @dcf_1004");
        strSQL += String.Concat(", ide_1002 = @ide_1002");
        strSQL += String.Concat(", cec_1004 = @cec_1004");
        strSQL += String.Concat(", dce_1004 = @dce_1004");
        strSQL += String.Concat(", den_1004 = @den_1004");
        strSQL += String.Concat(", dip_1004 = @dip_1004");
        strSQL += String.Concat(", uua_1004 = @uua_1004");
        strSQL += String.Concat(", fua_1004 = @fua_1004");
        strSQL += String.Concat(" WHERE ide_1004 = ", _ide_1004);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1004", _eje_1004);
        Command.Parameters.AddWithValue("@ide_1000", _ide_1000);
        Command.Parameters.AddWithValue("@cor_1004", _cor_1004);
        Command.Parameters.AddWithValue("@dco_1004", _dco_1004);
        Command.Parameters.AddWithValue("@ide_1001", _ide_1001);
        Command.Parameters.AddWithValue("@cfu_1004", _cfu_1004);
        Command.Parameters.AddWithValue("@dcf_1004", _dcf_1004);
        Command.Parameters.AddWithValue("@ide_1002", _ide_1002);
        Command.Parameters.AddWithValue("@cec_1004", _cec_1004);
        Command.Parameters.AddWithValue("@dce_1004", _dce_1004);
        Command.Parameters.AddWithValue("@den_1004", _den_1004);
        Command.Parameters.AddWithValue("@dip_1004", _dip_1004);
        Command.Parameters.AddWithValue("@uua_1004", _uua_1004);
        Command.Parameters.AddWithValue("@fua_1004", _fua_1004);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: eje_1004 cor_1004 cfu_1004 cec_1004
    public void Cargar_eje_1004_cor_1004_cfu_1004_cec_1004(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1004 WHERE eje_1004 = " + _eje_1004 + " AND cor_1004 = '" + _cor_1004 + "' AND cfu_1004 = '" + _cfu_1004 + "' AND cec_1004 = '" + _cec_1004 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1004 = (int)dr["ide_1004"];

            if (AsignarPropiedades)
            {
                eje_1004 = (Int16)dr["eje_1004"];
                ide_1000 = (int)dr["ide_1000"];
                cor_1004 = (string)dr["cor_1004"];
                dco_1004 = (string)dr["dco_1004"];
                ide_1001 = (int)dr["ide_1001"];
                cfu_1004 = (string)dr["cfu_1004"];
                dcf_1004 = (string)dr["dcf_1004"];
                ide_1002 = (int)dr["ide_1002"];
                cec_1004 = (string)dr["cec_1004"];
                dce_1004 = (string)dr["dce_1004"];
                den_1004 = (string)dr["den_1004"];
                uar_1004 = (int)dr["uar_1004"];
                far_1004 = dr["far_1004"].ToString();
                dip_1004 = (string)dr["dip_1004"];
                uua_1004 = (int)dr["uua_1004"];
                fua_1004 = dr["fua_1004"].ToString();
            }
        }
        else
        {
            ide_1004 = 0;
        }

        dr.Close();
    }

    #endregion

}