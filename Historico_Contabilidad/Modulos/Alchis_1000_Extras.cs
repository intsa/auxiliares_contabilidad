using System;
using System.Data.SqlClient;

public class Alchis_1000_Extras
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1000;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_1000;                                        // 2   CLAVE_UNICA 
    private Int16 _ide_5000;                                        // 3    
    private string _cor_1000;                                       // 4   CLAVE_UNICA 
    private string _den_1000;                                       // 5    
    private byte _sun_1000;                                         // 6    
    private int _uar_1000;                                          // 7    
    private string _far_1000;                                       // 8    
    private string _dip_1000;                                       // 9    
    private int _uua_1000;                                          // 10    
    private string _fua_1000;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_1000
    {
        get { return _ide_1000; }
        set { _ide_1000 = value; }
    }
    public Int16 eje_1000
    {
        get { return _eje_1000; }
        set { _eje_1000 = value; }
    }
    public Int16 ide_5000
    {
        get { return _ide_5000; }
        set { _ide_5000 = value; }
    }
    public string cor_1000
    {
        get { return _cor_1000; }
        set { _cor_1000 = value; }
    }
    public string den_1000
    {
        get { return _den_1000; }
        set { _den_1000 = value; }
    }
    public byte sun_1000
    {
        get { return _sun_1000; }
        set { _sun_1000 = value; }
    }
    public int uar_1000
    {
        get { return _uar_1000; }
        set { _uar_1000 = value; }
    }
    public string far_1000
    {
        get { return _far_1000; }
        set { _far_1000 = value; }
    }
    public string dip_1000
    {
        get { return _dip_1000; }
        set { _dip_1000 = value; }
    }
    public int uua_1000
    {
        get { return _uua_1000; }
        set { _uua_1000 = value; }
    }
    public string fua_1000
    {
        get { return _fua_1000; }
        set { _fua_1000 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1000_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1000 = 0;
        _eje_1000 = 0;
        _ide_5000 = 0;
        _cor_1000 = "";
        _den_1000 = "";
        _sun_1000 = 0;
        _uar_1000 = 0;
        _far_1000 = "";
        _dip_1000 = "";
        _uua_1000 = 0;
        _fua_1000 = "";
    }

    #endregion

    #region Metodos Públicos

    /// <summary>
    /// Actualiza las columnas 
    /// - Denominación
    /// - Semáforo último nivel
    /// </summary>
    public void Actualizar_Extras()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_1000 SET");
        strSQL += String.Concat(" den_1000 = @den_1000");
        strSQL += String.Concat(", sun_1000 = @sun_1000");
        strSQL += String.Concat(", dip_1000 = @dip_1000");
        strSQL += String.Concat(", uua_1000 = @uua_1000");
        strSQL += String.Concat(", fua_1000 = @fua_1000");
        strSQL += String.Concat(" WHERE ide_1000 = ", _ide_1000);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_1000", _den_1000);
        Command.Parameters.AddWithValue("@sun_1000", _sun_1000);
        Command.Parameters.AddWithValue("@dip_1000", _dip_1000);
        Command.Parameters.AddWithValue("@uua_1000", _uua_1000);
        Command.Parameters.AddWithValue("@fua_1000", _fua_1000);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    #endregion

}