using System;
using System.Data.SqlClient;

public class Alchis_1006
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1006;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_1006;                                        // 2   CLAVE_UNICA 
    private int _ide_1000;                                          // 3    
    private string _cor_1006;                                       // 4   CLAVE_UNICA 
    private string _dco_1006;                                       // 5    
    private int _ide_1003;                                          // 6    
    private string _cec_1006;                                       // 7   CLAVE_UNICA 
    private string _dce_1006;                                       // 8    
    private string _den_1006;                                       // 9    
    private int _uar_1006;                                          // 11    
    private string _far_1006;                                       // 12    
    private string _dip_1006;                                       // 13    
    private int _uua_1006;                                          // 14    
    private string _fua_1006;                                       // 15    

    #endregion

    #region Propiedades

    public int ide_1006
    {
        get { return _ide_1006; }
        set { _ide_1006 = value; }
    }
    public Int16 eje_1006
    {
        get { return _eje_1006; }
        set { _eje_1006 = value; }
    }
    public int ide_1000
    {
        get { return _ide_1000; }
        set { _ide_1000 = value; }
    }
    public string cor_1006
    {
        get { return _cor_1006; }
        set { _cor_1006 = value; }
    }
    public string dco_1006
    {
        get { return _dco_1006; }
        set { _dco_1006 = value; }
    }
    public int ide_1003
    {
        get { return _ide_1003; }
        set { _ide_1003 = value; }
    }
    public string cec_1006
    {
        get { return _cec_1006; }
        set { _cec_1006 = value; }
    }
    public string dce_1006
    {
        get { return _dce_1006; }
        set { _dce_1006 = value; }
    }
    public string den_1006
    {
        get { return _den_1006; }
        set { _den_1006 = value; }
    }
    public int uar_1006
    {
        get { return _uar_1006; }
        set { _uar_1006 = value; }
    }
    public string far_1006
    {
        get { return _far_1006; }
        set { _far_1006 = value; }
    }
    public string dip_1006
    {
        get { return _dip_1006; }
        set { _dip_1006 = value; }
    }
    public int uua_1006
    {
        get { return _uua_1006; }
        set { _uua_1006 = value; }
    }
    public string fua_1006
    {
        get { return _fua_1006; }
        set { _fua_1006 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1006(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1006 = 0;
        _eje_1006 = 0;
        _ide_1000 = 0;
        _cor_1006 = "";
        _dco_1006 = "";
        _ide_1003 = 0;
        _cec_1006 = "";
        _dce_1006 = "";
        _den_1006 = "";
        _uar_1006 = 0;
        _far_1006 = "";
        _dip_1006 = "";
        _uua_1006 = 0;
        _fua_1006 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.alchis_1006 (" +
        "  eje_1006" + ", ide_1000" + ", cor_1006" + ", dco_1006" + ", ide_1003" + ", cec_1006" + ", dce_1006" + ", den_1006" + ", uar_1006" + ", far_1006" +
        ", dip_1006" + ", uua_1006" + ", fua_1006" +
        ") OUTPUT INSERTED.ide_1006 VALUES (" +
        "  @eje_1006" + ", @ide_1000" + ", @cor_1006" + ", @dco_1006" + ", @ide_1003" + ", @cec_1006" + ", @dce_1006" + ", @den_1006" + ", @uar_1006" + ", @far_1006" +
        ", @dip_1006" + ", @uua_1006" + ", @fua_1006" +
         ")";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1006", _eje_1006);
        Command.Parameters.AddWithValue("@ide_1000", _ide_1000);
        Command.Parameters.AddWithValue("@cor_1006", _cor_1006);
        Command.Parameters.AddWithValue("@dco_1006", _dco_1006);
        Command.Parameters.AddWithValue("@ide_1003", _ide_1003);
        Command.Parameters.AddWithValue("@cec_1006", _cec_1006);
        Command.Parameters.AddWithValue("@dce_1006", _dce_1006);
        Command.Parameters.AddWithValue("@den_1006", _den_1006);
        Command.Parameters.AddWithValue("@uar_1006", _uar_1006);
        Command.Parameters.AddWithValue("@far_1006", _far_1006);
        Command.Parameters.AddWithValue("@dip_1006", _dip_1006);
        Command.Parameters.AddWithValue("@uua_1006", _uua_1006);
        Command.Parameters.AddWithValue("@fua_1006", _fua_1006);
        Command.Connection = _sqlCon;

        _ide_1006 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1006 WHERE ide_1006 = " + _ide_1006;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1006 = (int)dr["ide_1006"];

            if (AsignarPropiedades)
            {
                eje_1006 = (Int16)dr["eje_1006"];
                ide_1000 = (int)dr["ide_1000"];
                cor_1006 = (string)dr["cor_1006"];
                dco_1006 = (string)dr["dco_1006"];
                ide_1003 = (int)dr["ide_1003"];
                cec_1006 = (string)dr["cec_1006"];
                dce_1006 = (string)dr["dce_1006"];
                den_1006 = (string)dr["den_1006"];
                uar_1006 = (int)dr["uar_1006"];
                far_1006 = dr["far_1006"].ToString();
                dip_1006 = (string)dr["dip_1006"];
                uua_1006 = (int)dr["uua_1006"];
                fua_1006 = dr["fua_1006"].ToString();
            }
        }
        else
        {
            ide_1006 = 0;
        }

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_1006 WHERE ide_1006 = " + _ide_1006;

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_1006 SET");
        strSQL += String.Concat(" eje_1006 = @eje_1006");
        strSQL += String.Concat(", ide_1000 = @ide_1000");
        strSQL += String.Concat(", cor_1006 = @cor_1006");
        strSQL += String.Concat(", dco_1006 = @dco_1006");
        strSQL += String.Concat(", ide_1003 = @ide_1003");
        strSQL += String.Concat(", cec_1006 = @cec_1006");
        strSQL += String.Concat(", dce_1006 = @dce_1006");
        strSQL += String.Concat(", den_1006 = @den_1006");
        strSQL += String.Concat(", dip_1006 = @dip_1006");
        strSQL += String.Concat(", uua_1006 = @uua_1006");
        strSQL += String.Concat(", fua_1006 = @fua_1006");
        strSQL += String.Concat(" WHERE ide_1006 = ", _ide_1006);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1006", _eje_1006);
        Command.Parameters.AddWithValue("@ide_1000", _ide_1000);
        Command.Parameters.AddWithValue("@cor_1006", _cor_1006);
        Command.Parameters.AddWithValue("@dco_1006", _dco_1006);
        Command.Parameters.AddWithValue("@ide_1003", _ide_1003);
        Command.Parameters.AddWithValue("@cec_1006", _cec_1006);
        Command.Parameters.AddWithValue("@dce_1006", _dce_1006);
        Command.Parameters.AddWithValue("@den_1006", _den_1006);
        Command.Parameters.AddWithValue("@dip_1006", _dip_1006);
        Command.Parameters.AddWithValue("@uua_1006", _uua_1006);
        Command.Parameters.AddWithValue("@fua_1006", _fua_1006);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: eje_1006 cor_1006 cec_1006
    public void Cargar_eje_1006_cor_1006_cec_1006(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1006 WHERE eje_1006 = " + _eje_1006 + " AND cor_1006 = '" + _cor_1006 + "' AND cec_1006 = '" + _cec_1006 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1006 = (int)dr["ide_1006"];

            if (AsignarPropiedades)
            {
                eje_1006 = (Int16)dr["eje_1006"];
                ide_1000 = (int)dr["ide_1000"];
                cor_1006 = (string)dr["cor_1006"];
                dco_1006 = (string)dr["dco_1006"];
                ide_1003 = (int)dr["ide_1003"];
                cec_1006 = (string)dr["cec_1006"];
                dce_1006 = (string)dr["dce_1006"];
                den_1006 = (string)dr["den_1006"];
                uar_1006 = (int)dr["uar_1006"];
                far_1006 = dr["far_1006"].ToString();
                dip_1006 = (string)dr["dip_1006"];
                uua_1006 = (int)dr["uua_1006"];
                fua_1006 = dr["fua_1006"].ToString();
            }
        }
        else
        {
            ide_1006 = 0;
        }

        dr.Close();
    }

    #endregion

}