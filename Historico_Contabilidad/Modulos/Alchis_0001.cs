using System;
using System.Data.SqlClient;

public class Alchis_0001
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_0001;                                        // 2   CLAVE_UNICA 
    private string _cta_0001;                                       // 3   CLAVE_UNICA 
    private string _des_0001;                                       // 4    
    private int _uar_0001;                                          // 5    
    private string _far_0001;                                       // 6    
    private string _dip_0001;                                       // 7    
    private int _uua_0001;                                          // 8    
    private string _fua_0001;                                       // 9    

    #endregion

    #region Propiedades

    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public Int16 eje_0001
    {
        get { return _eje_0001; }
        set { _eje_0001 = value; }
    }
    public string cta_0001
    {
        get { return _cta_0001; }
        set { _cta_0001 = value; }
    }
    public string des_0001
    {
        get { return _des_0001; }
        set { _des_0001 = value; }
    }
    public int uar_0001
    {
        get { return _uar_0001; }
        set { _uar_0001 = value; }
    }
    public string far_0001
    {
        get { return _far_0001; }
        set { _far_0001 = value; }
    }
    public string dip_0001
    {
        get { return _dip_0001; }
        set { _dip_0001 = value; }
    }
    public int uua_0001
    {
        get { return _uua_0001; }
        set { _uua_0001 = value; }
    }
    public string fua_0001
    {
        get { return _fua_0001; }
        set { _fua_0001 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_0001(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001 = 0;
        _eje_0001 = 0;
        _cta_0001 = "";
        _des_0001 = "";
        _uar_0001 = 0;
        _far_0001 = "";
        _dip_0001 = "";
        _uua_0001 = 0;
        _fua_0001 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand
        {
            CommandText = "INSERT INTO " + _BaseDatos + ".dbo.alchis_0001 (" +
        "  eje_0001" + ", cta_0001" + ", des_0001" + ", uar_0001" + ", far_0001" + ", dip_0001" + ", uua_0001" + ", fua_0001" +
        ") OUTPUT INSERTED.ide_0001 VALUES (" +
        "  @eje_0001" + ", @cta_0001" + ", @des_0001" + ", @uar_0001" + ", @far_0001" + ", @dip_0001" + ", @uua_0001" + ", @fua_0001" +
         ")"
        };

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_0001", _eje_0001);
        Command.Parameters.AddWithValue("@cta_0001", _cta_0001);
        Command.Parameters.AddWithValue("@des_0001", _des_0001);
        Command.Parameters.AddWithValue("@uar_0001", _uar_0001);
        Command.Parameters.AddWithValue("@far_0001", _far_0001);
        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.Connection = _sqlCon;
        _ide_0001 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_0001 WHERE ide_0001 = " + _ide_0001;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001 = (int)dr["ide_0001"];

            if (AsignarPropiedades)
            {
                eje_0001 = (Int16)dr["eje_0001"];
                cta_0001 = (string)dr["cta_0001"];
                des_0001 = (string)dr["des_0001"];
                uar_0001 = (int)dr["uar_0001"];
                far_0001 = dr["far_0001"].ToString();
                dip_0001 = (string)dr["dip_0001"];
                uua_0001 = (int)dr["uua_0001"];
                fua_0001 = dr["fua_0001"].ToString();
            }
        }
        else
            ide_0001 = 0;

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand
        {
            Connection = _sqlCon,
            CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_0001 WHERE ide_0001 = " + _ide_0001
        };

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        string strSQL = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        strSQL = string.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_0001 SET");
        strSQL += string.Concat(" eje_0001 = @eje_0001");
        strSQL += string.Concat(", cta_0001 = @cta_0001");
        strSQL += string.Concat(", des_0001 = @des_0001");
        strSQL += string.Concat(", dip_0001 = @dip_0001");
        strSQL += string.Concat(", uua_0001 = @uua_0001");
        strSQL += string.Concat(", fua_0001 = @fua_0001");
        strSQL += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_0001", _eje_0001);
        Command.Parameters.AddWithValue("@cta_0001", _cta_0001);
        Command.Parameters.AddWithValue("@des_0001", _des_0001);
        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.CommandText = strSQL;
        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: eje_0001 cta_0001
    public void Cargar_eje_0001_cta_0001(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_0001 WHERE eje_0001 = " + _eje_0001 + " AND cta_0001 = '" + _cta_0001 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_0001 = (int)dr["ide_0001"];

            if (AsignarPropiedades)
            {
                eje_0001 = (Int16)dr["eje_0001"];
                cta_0001 = (string)dr["cta_0001"];
                des_0001 = (string)dr["des_0001"];
                uar_0001 = (int)dr["uar_0001"];
                far_0001 = dr["far_0001"].ToString();
                dip_0001 = (string)dr["dip_0001"];
                uua_0001 = (int)dr["uua_0001"];
                fua_0001 = dr["fua_0001"].ToString();
            }
        }
        else
            ide_0001 = 0;

        dr.Close();
    }

    #endregion

}