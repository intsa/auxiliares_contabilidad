using System;
using System.Data.SqlClient;

public class Alchis_1001
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1001;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_1001;                                        // 2   CLAVE_UNICA 
    private Int16 _ide_5001;                                        // 3    
    private string _cfu_1001;                                       // 4   CLAVE_UNICA 
    private string _den_1001;                                       // 5    
    private byte _sun_1001;                                         // 6    
    private int _uar_1001;                                          // 7    
    private string _far_1001;                                       // 8    
    private string _dip_1001;                                       // 9    
    private int _uua_1001;                                          // 10    
    private string _fua_1001;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_1001
    {
        get { return _ide_1001; }
        set { _ide_1001 = value; }
    }
    public Int16 eje_1001
    {
        get { return _eje_1001; }
        set { _eje_1001 = value; }
    }
    public Int16 ide_5001
    {
        get { return _ide_5001; }
        set { _ide_5001 = value; }
    }
    public string cfu_1001
    {
        get { return _cfu_1001; }
        set { _cfu_1001 = value; }
    }
    public string den_1001
    {
        get { return _den_1001; }
        set { _den_1001 = value; }
    }
    public byte sun_1001
    {
        get { return _sun_1001; }
        set { _sun_1001 = value; }
    }
    public int uar_1001
    {
        get { return _uar_1001; }
        set { _uar_1001 = value; }
    }
    public string far_1001
    {
        get { return _far_1001; }
        set { _far_1001 = value; }
    }
    public string dip_1001
    {
        get { return _dip_1001; }
        set { _dip_1001 = value; }
    }
    public int uua_1001
    {
        get { return _uua_1001; }
        set { _uua_1001 = value; }
    }
    public string fua_1001
    {
        get { return _fua_1001; }
        set { _fua_1001 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1001(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1001 = 0;
        _eje_1001 = 0;
        _ide_5001 = 0;
        _cfu_1001 = "";
        _den_1001 = "";
        _sun_1001 = 0;
        _uar_1001 = 0;
        _far_1001 = "";
        _dip_1001 = "";
        _uua_1001 = 0;
        _fua_1001 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.alchis_1001 (" +
        "  eje_1001" + ", ide_5001" + ", cfu_1001" + ", den_1001" + ", sun_1001" + ", uar_1001" + ", far_1001" + ", dip_1001" + ", uua_1001" + ", fua_1001" +
        ") OUTPUT INSERTED.ide_1001 VALUES (" +
        "  @eje_1001" + ", @ide_5001" + ", @cfu_1001" + ", @den_1001" + ", @sun_1001" + ", @uar_1001" + ", @far_1001" + ", @dip_1001" + ", @uua_1001" + ", @fua_1001" +
         ")";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1001", _eje_1001);
        Command.Parameters.AddWithValue("@ide_5001", _ide_5001);
        Command.Parameters.AddWithValue("@cfu_1001", _cfu_1001);
        Command.Parameters.AddWithValue("@den_1001", _den_1001);
        Command.Parameters.AddWithValue("@sun_1001", _sun_1001);
        Command.Parameters.AddWithValue("@uar_1001", _uar_1001);
        Command.Parameters.AddWithValue("@far_1001", _far_1001);
        Command.Parameters.AddWithValue("@dip_1001", _dip_1001);
        Command.Parameters.AddWithValue("@uua_1001", _uua_1001);
        Command.Parameters.AddWithValue("@fua_1001", _fua_1001);
        Command.Connection = _sqlCon;

        _ide_1001 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1001 WHERE ide_1001 = " + _ide_1001;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1001 = (int)dr["ide_1001"];

            if (AsignarPropiedades)
            {
                eje_1001 = (Int16)dr["eje_1001"];
                ide_5001 = (Int16)dr["ide_5001"];
                cfu_1001 = (string)dr["cfu_1001"];
                den_1001 = (string)dr["den_1001"];
                sun_1001 = Convert.ToByte(dr["sun_1001"]);
                uar_1001 = (int)dr["uar_1001"];
                far_1001 = dr["far_1001"].ToString();
                dip_1001 = (string)dr["dip_1001"];
                uua_1001 = (int)dr["uua_1001"];
                fua_1001 = dr["fua_1001"].ToString();
            }
        }
        else
        {
            ide_1001 = 0;
        }

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_1001 WHERE ide_1001 = " + _ide_1001;

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_1001 SET");
        strSQL += String.Concat(" eje_1001 = @eje_1001");
        strSQL += String.Concat(", ide_5001 = @ide_5001");
        strSQL += String.Concat(", cfu_1001 = @cfu_1001");
        strSQL += String.Concat(", den_1001 = @den_1001");
        strSQL += String.Concat(", sun_1001 = @sun_1001");
        strSQL += String.Concat(", dip_1001 = @dip_1001");
        strSQL += String.Concat(", uua_1001 = @uua_1001");
        strSQL += String.Concat(", fua_1001 = @fua_1001");
        strSQL += String.Concat(" WHERE ide_1001 = ", _ide_1001);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1001", _eje_1001);
        Command.Parameters.AddWithValue("@ide_5001", _ide_5001);
        Command.Parameters.AddWithValue("@cfu_1001", _cfu_1001);
        Command.Parameters.AddWithValue("@den_1001", _den_1001);
        Command.Parameters.AddWithValue("@sun_1001", _sun_1001);
        Command.Parameters.AddWithValue("@dip_1001", _dip_1001);
        Command.Parameters.AddWithValue("@uua_1001", _uua_1001);
        Command.Parameters.AddWithValue("@fua_1001", _fua_1001);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: eje_1001 cfu_1001
    public void Cargar_eje_1001_cfu_1001(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1001 WHERE eje_1001 = " + _eje_1001 + " AND cfu_1001 = '" + _cfu_1001 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1001 = (int)dr["ide_1001"];

            if (AsignarPropiedades)
            {
                eje_1001 = (Int16)dr["eje_1001"];
                ide_5001 = (Int16)dr["ide_5001"];
                cfu_1001 = (string)dr["cfu_1001"];
                den_1001 = (string)dr["den_1001"];
                sun_1001 = Convert.ToByte(dr["sun_1001"]);
                uar_1001 = (int)dr["uar_1001"];
                far_1001 = dr["far_1001"].ToString();
                dip_1001 = (string)dr["dip_1001"];
                uua_1001 = (int)dr["uua_1001"];
                fua_1001 = dr["fua_1001"].ToString();
            }
        }
        else
        {
            ide_1001 = 0;
        }

        dr.Close();
    }

    #endregion

}