using System;
using System.Data.SqlClient;

public class Alchis_1002_1_Extras
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1002_1;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_1002;                                          // 2   CLAVE_UNICA 
    private string _cta_1002_1;                                     // 3   CLAVE_UNICA 
    private int _uar_1002_1;                                        // 4    
    private string _far_1002_1;                                     // 5    
    private string _dip_1002_1;                                     // 6    
    private int _uua_1002_1;                                        // 7    
    private string _fua_1002_1;                                     // 8    

    #endregion

    #region Propiedades

    public int ide_1002_1
    {
        get { return _ide_1002_1; }
        set { _ide_1002_1 = value; }
    }
    public int ide_1002
    {
        get { return _ide_1002; }
        set { _ide_1002 = value; }
    }
    public string cta_1002_1
    {
        get { return _cta_1002_1; }
        set { _cta_1002_1 = value; }
    }
    public int uar_1002_1
    {
        get { return _uar_1002_1; }
        set { _uar_1002_1 = value; }
    }
    public string far_1002_1
    {
        get { return _far_1002_1; }
        set { _far_1002_1 = value; }
    }
    public string dip_1002_1
    {
        get { return _dip_1002_1; }
        set { _dip_1002_1 = value; }
    }
    public int uua_1002_1
    {
        get { return _uua_1002_1; }
        set { _uua_1002_1 = value; }
    }
    public string fua_1002_1
    {
        get { return _fua_1002_1; }
        set { _fua_1002_1 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1002_1_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1002_1 = 0;
        _ide_1002 = 0;
        _cta_1002_1 = "";
        _uar_1002_1 = 0;
        _far_1002_1 = "";
        _dip_1002_1 = "";
        _uua_1002_1 = 0;
        _fua_1002_1 = "";
    }

    #endregion

    #region Métodos públicos

    /// <summary>
    /// Elimina todos los registros relacionados 
    /// </summary>
    public void Eliminar_Extras()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_1002_1 WHERE ide_1002 = " + _ide_1002;

        Command.ExecuteNonQuery();
    }

    #endregion

}