using System;
using System.Data.SqlClient;

public class Alchis_2005
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_2005;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_2005;                                        // 2   CLAVE_UNICA 
    private string _cor_2005;                                       // 3   CLAVE_UNICA 
    private string _cfu_2005;                                       // 4   CLAVE_UNICA 
    private string _cec_2005;                                       // 5   CLAVE_UNICA 
    private string _den_2005;                                       // 6    
    private int _uar_2005;                                          // 11    
    private string _far_2005;                                       // 12    
    private string _dip_2005;                                       // 13    
    private int _uua_2005;                                          // 14    
    private string _fua_2005;                                       // 15    

    #endregion

    #region Propiedades

    public int ide_2005
    {
        get { return _ide_2005; }
        set { _ide_2005 = value; }
    }
    public Int16 eje_2005
    {
        get { return _eje_2005; }
        set { _eje_2005 = value; }
    }
    public string cor_2005
    {
        get { return _cor_2005; }
        set { _cor_2005 = value; }
    }
    public string cfu_2005
    {
        get { return _cfu_2005; }
        set { _cfu_2005 = value; }
    }
    public string cec_2005
    {
        get { return _cec_2005; }
        set { _cec_2005 = value; }
    }
    public string den_2005
    {
        get { return _den_2005; }
        set { _den_2005 = value; }
    }
    public int uar_2005
    {
        get { return _uar_2005; }
        set { _uar_2005 = value; }
    }
    public string far_2005
    {
        get { return _far_2005; }
        set { _far_2005 = value; }
    }
    public string dip_2005
    {
        get { return _dip_2005; }
        set { _dip_2005 = value; }
    }
    public int uua_2005
    {
        get { return _uua_2005; }
        set { _uua_2005 = value; }
    }
    public string fua_2005
    {
        get { return _fua_2005; }
        set { _fua_2005 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_2005(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? "Adm_Loc_Con_His_000999";

        _ide_2005 = 0;
        _eje_2005 = 0;
        _cor_2005 = "";
        _cfu_2005 = "";
        _cec_2005 = "";
        _den_2005 = "";
        _uar_2005 = 0;
        _far_2005 = "";
        _dip_2005 = "";
        _uua_2005 = 0;
        _fua_2005 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.alchis_2005 (" +
        "  eje_2005" + ", cor_2005" + ", cfu_2005" + ", cec_2005" + ", den_2005" + ", uar_2005" + ", far_2005" + ", dip_2005" + ", uua_2005" + ", fua_2005" +
        ") OUTPUT INSERTED.ide_2005 VALUES (" +
        "  @eje_2005" + ", @cor_2005" + ", @cfu_2005" + ", @cec_2005" + ", @den_2005" + ", @uar_2005" + ", @far_2005" + ", @dip_2005" + ", @uua_2005" + ", @fua_2005" +
         ")";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_2005", _eje_2005);
        Command.Parameters.AddWithValue("@cor_2005", _cor_2005);
        Command.Parameters.AddWithValue("@cfu_2005", _cfu_2005);
        Command.Parameters.AddWithValue("@cec_2005", _cec_2005);
        Command.Parameters.AddWithValue("@den_2005", _den_2005);
        Command.Parameters.AddWithValue("@uar_2005", _uar_2005);
        Command.Parameters.AddWithValue("@far_2005", _far_2005);
        Command.Parameters.AddWithValue("@dip_2005", _dip_2005);
        Command.Parameters.AddWithValue("@uua_2005", _uua_2005);
        Command.Parameters.AddWithValue("@fua_2005", _fua_2005);
        Command.Connection = _sqlCon;

        _ide_2005 = (int)Command.ExecuteScalar();
        Command = null;
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;

        Command.Connection = _sqlCon;
        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_2005 WHERE ide_2005 = " + _ide_2005;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2005 = (int)dr["ide_2005"];

            if (AsignarPropiedades)
            {
                eje_2005 = (Int16)dr["eje_2005"];
                cor_2005 = (string)dr["cor_2005"];
                cfu_2005 = (string)dr["cfu_2005"];
                cec_2005 = (string)dr["cec_2005"];
                den_2005 = (string)dr["den_2005"];
                uar_2005 = (int)dr["uar_2005"];
                far_2005 = dr["far_2005"].ToString();
                dip_2005 = (string)dr["dip_2005"];
                uua_2005 = (int)dr["uua_2005"];
                fua_2005 = dr["fua_2005"].ToString();
            }
        }
        else
        {
            ide_2005 = 0;
        }

        dr.Close();
        Command = null;
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_2005 WHERE ide_2005 = " + _ide_2005;

        Command.ExecuteNonQuery();
        Command = null;
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_2005 Set ");
        strSQL += String.Concat("  eje_2005 = @eje_2005");
        strSQL += String.Concat(", cor_2005 = @cor_2005");
        strSQL += String.Concat(", cfu_2005 = @cfu_2005");
        strSQL += String.Concat(", cec_2005 = @cec_2005");
        strSQL += String.Concat(", den_2005 = @den_2005");
        strSQL += String.Concat(", dip_2005 = @dip_2005");
        strSQL += String.Concat(", uua_2005 = @uua_2005");
        strSQL += String.Concat(", fua_2005 = @fua_2005");
        strSQL += String.Concat(" WHERE ide_2005 = ", _ide_2005);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_2005", _eje_2005);
        Command.Parameters.AddWithValue("@cor_2005", _cor_2005);
        Command.Parameters.AddWithValue("@cfu_2005", _cfu_2005);
        Command.Parameters.AddWithValue("@cec_2005", _cec_2005);
        Command.Parameters.AddWithValue("@den_2005", _den_2005);
        Command.Parameters.AddWithValue("@dip_2005", _dip_2005);
        Command.Parameters.AddWithValue("@uua_2005", _uua_2005);
        Command.Parameters.AddWithValue("@fua_2005", _fua_2005);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
        Command = null;
    }

    //METODO CARGAR CLAVE UNICA: eje_2005 cor_2005 cfu_2005 cec_2005
    public void Cargar_eje_2005_cor_2005_cfu_2005_cec_2005(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;

        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_2005 WHERE eje_2005 = " + _eje_2005 + " AND cor_2005 = '" + _cor_2005 + "' AND cfu_2005 = '" + _cfu_2005 + "' AND cec_2005 = '" + _cec_2005 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2005 = (int)dr["ide_2005"];

            if (AsignarPropiedades)
            {
                eje_2005 = (Int16)dr["eje_2005"];
                cor_2005 = (string)dr["cor_2005"];
                cfu_2005 = (string)dr["cfu_2005"];
                cec_2005 = (string)dr["cec_2005"];
                den_2005 = (string)dr["den_2005"];
                uar_2005 = (int)dr["uar_2005"];
                far_2005 = dr["far_2005"].ToString();
                dip_2005 = (string)dr["dip_2005"];
                uua_2005 = (int)dr["uua_2005"];
                fua_2005 = dr["fua_2005"].ToString();
            }
        }
        else
        {
            ide_2005 = 0;
        }

        dr.Close();
        Command = null;
    }

    #endregion

}