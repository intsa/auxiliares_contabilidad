using System;
using System.Data.SqlClient;

public class Alchis_1004_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1004;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _eje_1004;                                        // 2   CLAVE_UNICA 
    private int _ide_1000;                                          // 3    
    private string _cor_1004;                                       // 4   CLAVE_UNICA 
    private string _dco_1004;                                       // 5    
    private int _ide_1001;                                          // 6    
    private string _cfu_1004;                                       // 7   CLAVE_UNICA 
    private string _dcf_1004;                                       // 8    
    private int _ide_1002;                                          // 9    
    private string _cec_1004;                                       // 10   CLAVE_UNICA 
    private string _dce_1004;                                       // 11    
    private string _den_1004;                                       // 12    
    private int _uar_1004;                                          // 35    
    private string _far_1004;                                       // 36    
    private string _dip_1004;                                       // 37    
    private int _uua_1004;                                          // 38    
    private string _fua_1004;                                       // 39    

    #endregion

    #region Propiedades

    public int ide_1004
    {
        get { return _ide_1004; }
        set { _ide_1004 = value; }
    }
    public short eje_1004
    {
        get { return _eje_1004; }
        set { _eje_1004 = value; }
    }
    public int ide_1000
    {
        get { return _ide_1000; }
        set { _ide_1000 = value; }
    }
    public string cor_1004
    {
        get { return _cor_1004; }
        set { _cor_1004 = value; }
    }
    public string dco_1004
    {
        get { return _dco_1004; }
        set { _dco_1004 = value; }
    }
    public int ide_1001
    {
        get { return _ide_1001; }
        set { _ide_1001 = value; }
    }
    public string cfu_1004
    {
        get { return _cfu_1004; }
        set { _cfu_1004 = value; }
    }
    public string dcf_1004
    {
        get { return _dcf_1004; }
        set { _dcf_1004 = value; }
    }
    public int ide_1002
    {
        get { return _ide_1002; }
        set { _ide_1002 = value; }
    }
    public string cec_1004
    {
        get { return _cec_1004; }
        set { _cec_1004 = value; }
    }
    public string dce_1004
    {
        get { return _dce_1004; }
        set { _dce_1004 = value; }
    }
    public string den_1004
    {
        get { return _den_1004; }
        set { _den_1004 = value; }
    }
    public int uar_1004
    {
        get { return _uar_1004; }
        set { _uar_1004 = value; }
    }
    public string far_1004
    {
        get { return _far_1004; }
        set { _far_1004 = value; }
    }
    public string dip_1004
    {
        get { return _dip_1004; }
        set { _dip_1004 = value; }
    }
    public int uua_1004
    {
        get { return _uua_1004; }
        set { _uua_1004 = value; }
    }
    public string fua_1004
    {
        get { return _fua_1004; }
        set { _fua_1004 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1004_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1004 = 0;
        _eje_1004 = 0;
        _ide_1000 = 0;
        _cor_1004 = "";
        _dco_1004 = "";
        _ide_1001 = 0;
        _cfu_1004 = "";
        _dcf_1004 = "";
        _ide_1002 = 0;
        _cec_1004 = "";
        _dce_1004 = "";
        _den_1004 = "";
        _uar_1004 = 0;
        _far_1004 = "";
        _dip_1004 = "";
        _uua_1004 = 0;
        _fua_1004 = "";
    }

    #endregion

    #region Métodos públicos

    /// <summary>
    /// Actualiza las columnas 
    /// - Denominación clave orgánica
    /// - Denominación clave funcional
    /// - Denominación clave económica
    /// - Denominación aplicación presupuestaria
    /// </summary>
    public void Actualizar_Extras()
    {
        string Cadena_Sql = "";
        SqlCommand Command = new SqlCommand{Connection = _sqlCon};

        Cadena_Sql = string.Concat(Cadena_Sql, "UPDATE " + _BaseDatos + ".dbo.alchis_1004 SET");
        Cadena_Sql += string.Concat(" dco_1004 = @dco_1004");
        Cadena_Sql += string.Concat(", dcf_1004 = @dcf_1004");
        Cadena_Sql += string.Concat(", dce_1004 = @dce_1004");
        Cadena_Sql += string.Concat(", den_1004 = @den_1004");
        Cadena_Sql += string.Concat(", dip_1004 = @dip_1004");
        Cadena_Sql += string.Concat(", uua_1004 = @uua_1004");
        Cadena_Sql += string.Concat(", fua_1004 = @fua_1004");
        Cadena_Sql += string.Concat(" WHERE ide_1004 = ", _ide_1004);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@dco_1004", _dco_1004);
        Command.Parameters.AddWithValue("@dcf_1004", _dcf_1004);
        Command.Parameters.AddWithValue("@dce_1004", _dce_1004);
        Command.Parameters.AddWithValue("@den_1004", _den_1004);
        Command.Parameters.AddWithValue("@dip_1004", _dip_1004);
        Command.Parameters.AddWithValue("@uua_1004", _uua_1004);
        Command.Parameters.AddWithValue("@fua_1004", _fua_1004);

        Command.CommandText = Cadena_Sql;
        Command.ExecuteNonQuery();
    }

    /// <summary>
    /// Actualiza las columnas 
    /// - Denominación aplicación presupuestaria
    /// </summary>
    public void Actualizar_Denominacion()
    {
        string Cadena_Sql = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        Cadena_Sql = string.Concat(Cadena_Sql, "UPDATE " + _BaseDatos + ".dbo.alchis_1004 SET");
        Cadena_Sql += string.Concat(" den_1004 = @den_1004");
        Cadena_Sql += string.Concat(", dip_1004 = @dip_1004");
        Cadena_Sql += string.Concat(", uua_1004 = @uua_1004");
        Cadena_Sql += string.Concat(", fua_1004 = @fua_1004");
        Cadena_Sql += string.Concat(" WHERE ide_1004 = ", _ide_1004);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_1004", _den_1004);
        Command.Parameters.AddWithValue("@dip_1004", _dip_1004);
        Command.Parameters.AddWithValue("@uua_1004", _uua_1004);
        Command.Parameters.AddWithValue("@fua_1004", _fua_1004);

        Command.CommandText = Cadena_Sql;
        Command.ExecuteNonQuery();
    }
    #endregion

}