using System;
using System.Data.SqlClient;

public class Alchis_0001_Extras
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_0001;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private short _eje_0001;                                        // 2   CLAVE_UNICA 
    private string _cta_0001;                                       // 3   CLAVE_UNICA 
    private string _des_0001;                                       // 4    
    private int _uar_0001;                                          // 5    
    private string _far_0001;                                       // 6    
    private string _dip_0001;                                       // 7    
    private int _uua_0001;                                          // 8    
    private string _fua_0001;                                       // 9    

    #endregion

    #region Propiedades

    public int ide_0001
    {
        get { return _ide_0001; }
        set { _ide_0001 = value; }
    }
    public short eje_0001
    {
        get { return _eje_0001; }
        set { _eje_0001 = value; }
    }
    public string cta_0001
    {
        get { return _cta_0001; }
        set { _cta_0001 = value; }
    }
    public string des_0001
    {
        get { return _des_0001; }
        set { _des_0001 = value; }
    }
    public int uar_0001
    {
        get { return _uar_0001; }
        set { _uar_0001 = value; }
    }
    public string far_0001
    {
        get { return _far_0001; }
        set { _far_0001 = value; }
    }
    public string dip_0001
    {
        get { return _dip_0001; }
        set { _dip_0001 = value; }
    }
    public int uua_0001
    {
        get { return _uua_0001; }
        set { _uua_0001 = value; }
    }
    public string fua_0001
    {
        get { return _fua_0001; }
        set { _fua_0001 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_0001_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_0001 = 0;
        _eje_0001 = 0;
        _cta_0001 = "";
        _des_0001 = "";
        _uar_0001 = 0;
        _far_0001 = "";
        _dip_0001 = "";
        _uua_0001 = 0;
        _fua_0001 = "";
    }

    #endregion

    #region Metodos Públicos

    /// <summary>
    /// Actualiza registro columna
    /// - Denominación aplicación presupuestaria
    /// </summary>
    public void Actualizar_Extras()
    {
        string Cadena_Sql = "";
        SqlCommand Command = new SqlCommand { Connection = _sqlCon };

        Cadena_Sql = string.Concat(Cadena_Sql, $"UPDATE {_BaseDatos}.dbo.alchis_0001 SET");
        Cadena_Sql += string.Concat(" des_0001 = @des_0001");
        Cadena_Sql += string.Concat(", dip_0001 = @dip_0001");
        Cadena_Sql += string.Concat(", uua_0001 = @uua_0001");
        Cadena_Sql += string.Concat(", fua_0001 = @fua_0001");
        Cadena_Sql += string.Concat(" WHERE ide_0001 = ", _ide_0001);

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@des_0001", _des_0001);
        Command.Parameters.AddWithValue("@dip_0001", _dip_0001);
        Command.Parameters.AddWithValue("@uua_0001", _uua_0001);
        Command.Parameters.AddWithValue("@fua_0001", _fua_0001);

        Command.CommandText = Cadena_Sql;
        Command.ExecuteNonQuery();
    }

    #endregion

}