using System;
using System.Data.SqlClient;

public class Alchis_2004
{
    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_2004;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_1004;                                          // 2    
    private int _ide_2005;                                          // 3    
    private Int16 _eje_2004;                                        // 4   CLAVE_UNICA 
    private string _cor_2004;                                       // 5   CLAVE_UNICA 
    private string _cfu_2004;                                       // 6   CLAVE_UNICA 
    private string _cec_2004;                                       // 7   CLAVE_UNICA 
    private string _den_2004;                                       // 8    
    private decimal _rci_2004;                                      // 9    
    private decimal _rni_2004;                                      // 10    
    private decimal? _tri_2004;                                     // 11    CALCULADO
    private decimal _rej_2004;                                      // 12    
    private int _uar_2004;                                          // 13    
    private string _far_2004;                                       // 14    
    private string _dip_2004;                                       // 15    
    private int _uua_2004;                                          // 16    
    private string _fua_2004;                                       // 17    

    #endregion

    #region Propiedades

    public int ide_2004
    {
        get { return _ide_2004; }
        set { _ide_2004 = value; }
    }
    public int ide_1004
    {
        get { return _ide_1004; }
        set { _ide_1004 = value; }
    }
    public int ide_2005
    {
        get { return _ide_2005; }
        set { _ide_2005 = value; }
    }
    public Int16 eje_2004
    {
        get { return _eje_2004; }
        set { _eje_2004 = value; }
    }
    public string cor_2004
    {
        get { return _cor_2004; }
        set { _cor_2004 = value; }
    }
    public string cfu_2004
    {
        get { return _cfu_2004; }
        set { _cfu_2004 = value; }
    }
    public string cec_2004
    {
        get { return _cec_2004; }
        set { _cec_2004 = value; }
    }
    public string den_2004
    {
        get { return _den_2004; }
        set { _den_2004 = value; }
    }
    public decimal rci_2004
    {
        get { return _rci_2004; }
        set { _rci_2004 = value; }
    }
    public decimal rni_2004
    {
        get { return _rni_2004; }
        set { _rni_2004 = value; }
    }
    public decimal? tri_2004
    {
        get { return _tri_2004; }
        set { _tri_2004 = value; }
    }
    public decimal rej_2004
    {
        get { return _rej_2004; }
        set { _rej_2004 = value; }
    }
    public int uar_2004
    {
        get { return _uar_2004; }
        set { _uar_2004 = value; }
    }
    public string far_2004
    {
        get { return _far_2004; }
        set { _far_2004 = value; }
    }
    public string dip_2004
    {
        get { return _dip_2004; }
        set { _dip_2004 = value; }
    }
    public int uua_2004
    {
        get { return _uua_2004; }
        set { _uua_2004 = value; }
    }
    public string fua_2004
    {
        get { return _fua_2004; }
        set { _fua_2004 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_2004(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? "Adm_Loc_Con_His_000999";

        _ide_2004 = 0;
        _ide_1004 = 0;
        _ide_2005 = 0;
        _eje_2004 = 0;
        _cor_2004 = "";
        _cfu_2004 = "";
        _cec_2004 = "";
        _den_2004 = "";
        _rci_2004 = 0;
        _rni_2004 = 0;
        _tri_2004 = null;
        _rej_2004 = 0;
        _uar_2004 = 0;
        _far_2004 = "";
        _dip_2004 = "";
        _uua_2004 = 0;
        _fua_2004 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.alchis_2004 (" +
        "  ide_1004" + ", ide_2005" + ", eje_2004" + ", cor_2004" + ", cfu_2004" + ", cec_2004" + ", den_2004" + ", rci_2004" + ", rni_2004" + ", rej_2004" +
        ", uar_2004" + ", far_2004" + ", dip_2004" + ", uua_2004" + ", fua_2004" +
        ") OUTPUT INSERTED.ide_2004 VALUES (" +
        "  @ide_1004" + ", @ide_2005" + ", @eje_2004" + ", @cor_2004" + ", @cfu_2004" + ", @cec_2004" + ", @den_2004" + ", @rci_2004" + ", @rni_2004" + ", @rej_2004" +
        ", @uar_2004" + ", @far_2004" + ", @dip_2004" + ", @uua_2004" + ", @fua_2004" +
         ")";

        // ASIGNACIÓN DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_1004", _ide_1004);
        Command.Parameters.AddWithValue("@ide_2005", _ide_2005);
        Command.Parameters.AddWithValue("@eje_2004", _eje_2004);
        Command.Parameters.AddWithValue("@cor_2004", _cor_2004);
        Command.Parameters.AddWithValue("@cfu_2004", _cfu_2004);
        Command.Parameters.AddWithValue("@cec_2004", _cec_2004);
        Command.Parameters.AddWithValue("@den_2004", _den_2004);
        Command.Parameters.AddWithValue("@rci_2004", _rci_2004);
        Command.Parameters.AddWithValue("@rni_2004", _rni_2004);
        Command.Parameters.AddWithValue("@rej_2004", _rej_2004);
        Command.Parameters.AddWithValue("@uar_2004", _uar_2004);
        Command.Parameters.AddWithValue("@far_2004", _far_2004);
        Command.Parameters.AddWithValue("@dip_2004", _dip_2004);
        Command.Parameters.AddWithValue("@uua_2004", _uua_2004);
        Command.Parameters.AddWithValue("@fua_2004", _fua_2004);
        Command.Connection = _sqlCon;

        _ide_2004 = (int)Command.ExecuteScalar();
        Command = null;
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "Select * FROM " + _BaseDatos + ".dbo.alchis_2004 WHERE ide_2004 = " + _ide_2004;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2004 = (int)dr["ide_2004"];

            if (AsignarPropiedades)
            {
                ide_1004 = (int)dr["ide_1004"];
                ide_2005 = (int)dr["ide_2005"];
                eje_2004 = (Int16)dr["eje_2004"];
                cor_2004 = (string)dr["cor_2004"];
                cfu_2004 = (string)dr["cfu_2004"];
                cec_2004 = (string)dr["cec_2004"];
                den_2004 = (string)dr["den_2004"];
                rci_2004 = (decimal)dr["rci_2004"];
                rni_2004 = (decimal)dr["rni_2004"];
                tri_2004 = DBNull.Value.Equals(dr["tri_2004"]) ? (decimal?)null : (decimal?)dr["tri_2004"];
                rej_2004 = (decimal)dr["rej_2004"];
                uar_2004 = (int)dr["uar_2004"];
                far_2004 = dr["far_2004"].ToString();
                dip_2004 = (string)dr["dip_2004"];
                uua_2004 = (int)dr["uua_2004"];
                fua_2004 = dr["fua_2004"].ToString();
            }
        }
        else
        {
            ide_2004 = 0;
        }

        dr.Close();
        Command = null;
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_2004 WHERE ide_2004 = " + _ide_2004;

        Command.ExecuteNonQuery();
        Command = null;
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_2004 Set ");
        strSQL += String.Concat("  ide_1004 = @ide_1004");
        strSQL += String.Concat(", ide_2005 = @ide_2005");
        strSQL += String.Concat(", eje_2004 = @eje_2004");
        strSQL += String.Concat(", cor_2004 = @cor_2004");
        strSQL += String.Concat(", cfu_2004 = @cfu_2004");
        strSQL += String.Concat(", cec_2004 = @cec_2004");
        strSQL += String.Concat(", den_2004 = @den_2004");
        strSQL += String.Concat(", rci_2004 = @rci_2004");
        strSQL += String.Concat(", rni_2004 = @rni_2004");
        strSQL += String.Concat(", rej_2004 = @rej_2004");
        strSQL += String.Concat(", dip_2004 = @dip_2004");
        strSQL += String.Concat(", uua_2004 = @uua_2004");
        strSQL += String.Concat(", fua_2004 = @fua_2004");
        strSQL += String.Concat(" WHERE ide_2004 = ", _ide_2004);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_1004", _ide_1004);
        Command.Parameters.AddWithValue("@ide_2005", _ide_2005);
        Command.Parameters.AddWithValue("@eje_2004", _eje_2004);
        Command.Parameters.AddWithValue("@cor_2004", _cor_2004);
        Command.Parameters.AddWithValue("@cfu_2004", _cfu_2004);
        Command.Parameters.AddWithValue("@cec_2004", _cec_2004);
        Command.Parameters.AddWithValue("@den_2004", _den_2004);
        Command.Parameters.AddWithValue("@rci_2004", _rci_2004);
        Command.Parameters.AddWithValue("@rni_2004", _rni_2004);
        Command.Parameters.AddWithValue("@rej_2004", _rej_2004);
        Command.Parameters.AddWithValue("@dip_2004", _dip_2004);
        Command.Parameters.AddWithValue("@uua_2004", _uua_2004);
        Command.Parameters.AddWithValue("@fua_2004", _fua_2004);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
        Command = null;
    }

    //METODO CARGAR CLAVE UNICA: eje_2004 cor_2004 cfu_2004 cec_2004
    public void Cargar_eje_2004_cor_2004_cfu_2004_cec_2004(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;

        Command.Connection = _sqlCon;
        Command.CommandText = "Select * FROM " + _BaseDatos + ".dbo.alchis_2004 WHERE eje_2004 = " + _eje_2004 + " AND cor_2004 = '" + _cor_2004 + "' AND cfu_2004 = '" + _cfu_2004 + "' AND cec_2004 = '" + _cec_2004 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_2004 = (int)dr["ide_2004"];

            if (AsignarPropiedades)
            {
                ide_1004 = (int)dr["ide_1004"];
                ide_2005 = (int)dr["ide_2005"];
                eje_2004 = (Int16)dr["eje_2004"];
                cor_2004 = (string)dr["cor_2004"];
                cfu_2004 = (string)dr["cfu_2004"];
                cec_2004 = (string)dr["cec_2004"];
                den_2004 = (string)dr["den_2004"];
                rci_2004 = (decimal)dr["rci_2004"];
                rni_2004 = (decimal)dr["rni_2004"];
                tri_2004 = DBNull.Value.Equals(dr["tri_2004"]) ? (decimal?)null : (decimal?)dr["tri_2004"];
                rej_2004 = (decimal)dr["rej_2004"];
                uar_2004 = (int)dr["uar_2004"];
                far_2004 = dr["far_2004"].ToString();
                dip_2004 = (string)dr["dip_2004"];
                uua_2004 = (int)dr["uua_2004"];
                fua_2004 = dr["fua_2004"].ToString();
            }
        }
        else
        {
            ide_2004 = 0;
        }

        dr.Close();
        Command = null;
    }

    #endregion

}