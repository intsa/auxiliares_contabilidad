using System;
using System.Data.SqlClient;

public class Alchis_1003_Extras
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1003;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_1003;                                        // 2   CLAVE_UNICA 
    private Int16 _ide_5002;                                        // 3    
    private string _cec_1003;                                       // 4   CLAVE_UNICA 
    private string _den_1003;                                       // 5    
    private byte _sun_1003;                                         // 6    
    private int _uar_1003;                                          // 7    
    private string _far_1003;                                       // 8    
    private string _dip_1003;                                       // 9    
    private int _uua_1003;                                          // 10    
    private string _fua_1003;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_1003
    {
        get { return _ide_1003; }
        set { _ide_1003 = value; }
    }
    public Int16 eje_1003
    {
        get { return _eje_1003; }
        set { _eje_1003 = value; }
    }
    public Int16 ide_5002
    {
        get { return _ide_5002; }
        set { _ide_5002 = value; }
    }
    public string cec_1003
    {
        get { return _cec_1003; }
        set { _cec_1003 = value; }
    }
    public string den_1003
    {
        get { return _den_1003; }
        set { _den_1003 = value; }
    }
    public byte sun_1003
    {
        get { return _sun_1003; }
        set { _sun_1003 = value; }
    }
    public int uar_1003
    {
        get { return _uar_1003; }
        set { _uar_1003 = value; }
    }
    public string far_1003
    {
        get { return _far_1003; }
        set { _far_1003 = value; }
    }
    public string dip_1003
    {
        get { return _dip_1003; }
        set { _dip_1003 = value; }
    }
    public int uua_1003
    {
        get { return _uua_1003; }
        set { _uua_1003 = value; }
    }
    public string fua_1003
    {
        get { return _fua_1003; }
        set { _fua_1003 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1003_Extras(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1003 = 0;
        _eje_1003 = 0;
        _ide_5002 = 0;
        _cec_1003 = "";
        _den_1003 = "";
        _sun_1003 = 0;
        _uar_1003 = 0;
        _far_1003 = "";
        _dip_1003 = "";
        _uua_1003 = 0;
        _fua_1003 = "";
    }

    #endregion

    #region Metodos Públicos

    /// <summary>
    /// Actualiza las columnas 
    /// - Denominación
    /// - Semáforo último nivel
    /// </summary>
    public void Actualizar_Extras()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_1003 SET");
        strSQL += String.Concat(" den_1003 = @den_1003");
        strSQL += String.Concat(", sun_1003 = @sun_1003");
        strSQL += String.Concat(", dip_1003 = @dip_1003");
        strSQL += String.Concat(", uua_1003 = @uua_1003");
        strSQL += String.Concat(", fua_1003 = @fua_1003");
        strSQL += String.Concat(" WHERE ide_1003 = ", _ide_1003);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@den_1003", _den_1003);
        Command.Parameters.AddWithValue("@sun_1003", _sun_1003);
        Command.Parameters.AddWithValue("@dip_1003", _dip_1003);
        Command.Parameters.AddWithValue("@uua_1003", _uua_1003);
        Command.Parameters.AddWithValue("@fua_1003", _fua_1003);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    #endregion

}