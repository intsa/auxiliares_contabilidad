using System;
using System.Data.SqlClient;

public class Alchis_1004_1
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1004_1;                                        // 1 CLAVE_PRIMARIA IDENTIDAD  
    private int _ide_1004;                                          // 2   CLAVE_UNICA 
    private string _cta_1004_1;                                     // 3   CLAVE_UNICA 
    private int _uar_1004_1;                                        // 4    
    private string _far_1004_1;                                     // 5    
    private string _dip_1004_1;                                     // 6    
    private int _uua_1004_1;                                        // 7    
    private string _fua_1004_1;                                     // 8    

    #endregion

    #region Propiedades

    public int ide_1004_1
    {
        get { return _ide_1004_1; }
        set { _ide_1004_1 = value; }
    }
    public int ide_1004
    {
        get { return _ide_1004; }
        set { _ide_1004 = value; }
    }
    public string cta_1004_1
    {
        get { return _cta_1004_1; }
        set { _cta_1004_1 = value; }
    }
    public int uar_1004_1
    {
        get { return _uar_1004_1; }
        set { _uar_1004_1 = value; }
    }
    public string far_1004_1
    {
        get { return _far_1004_1; }
        set { _far_1004_1 = value; }
    }
    public string dip_1004_1
    {
        get { return _dip_1004_1; }
        set { _dip_1004_1 = value; }
    }
    public int uua_1004_1
    {
        get { return _uua_1004_1; }
        set { _uua_1004_1 = value; }
    }
    public string fua_1004_1
    {
        get { return _fua_1004_1; }
        set { _fua_1004_1 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1004_1(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1004_1 = 0;
        _ide_1004 = 0;
        _cta_1004_1 = "";
        _uar_1004_1 = 0;
        _far_1004_1 = "";
        _dip_1004_1 = "";
        _uua_1004_1 = 0;
        _fua_1004_1 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.alchis_1004_1 (" +
        "  ide_1004" + ", cta_1004_1" + ", uar_1004_1" + ", far_1004_1" + ", dip_1004_1" + ", uua_1004_1" + ", fua_1004_1" +
        ") OUTPUT INSERTED.ide_1004_1 VALUES (" +
        "  @ide_1004" + ", @cta_1004_1" + ", @uar_1004_1" + ", @far_1004_1" + ", @dip_1004_1" + ", @uua_1004_1" + ", @fua_1004_1" +
         ")";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_1004", _ide_1004);
        Command.Parameters.AddWithValue("@cta_1004_1", _cta_1004_1);
        Command.Parameters.AddWithValue("@uar_1004_1", _uar_1004_1);
        Command.Parameters.AddWithValue("@far_1004_1", _far_1004_1);
        Command.Parameters.AddWithValue("@dip_1004_1", _dip_1004_1);
        Command.Parameters.AddWithValue("@uua_1004_1", _uua_1004_1);
        Command.Parameters.AddWithValue("@fua_1004_1", _fua_1004_1);
        Command.Connection = _sqlCon;

        _ide_1004_1 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1004_1 WHERE ide_1004_1 = " + _ide_1004_1;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1004_1 = (int)dr["ide_1004_1"];

            if (AsignarPropiedades)
            {
                ide_1004 = (int)dr["ide_1004"];
                cta_1004_1 = (string)dr["cta_1004_1"];
                uar_1004_1 = (int)dr["uar_1004_1"];
                far_1004_1 = dr["far_1004_1"].ToString();
                dip_1004_1 = (string)dr["dip_1004_1"];
                uua_1004_1 = (int)dr["uua_1004_1"];
                fua_1004_1 = dr["fua_1004_1"].ToString();
            }
        }
        else
        {
            ide_1004_1 = 0;
        }

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_1004_1 WHERE ide_1004_1 = " + _ide_1004_1;

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_1004_1 SET");
        strSQL += String.Concat(" ide_1004 = @ide_1004");
        strSQL += String.Concat(", cta_1004_1 = @cta_1004_1");
        strSQL += String.Concat(", dip_1004_1 = @dip_1004_1");
        strSQL += String.Concat(", uua_1004_1 = @uua_1004_1");
        strSQL += String.Concat(", fua_1004_1 = @fua_1004_1");
        strSQL += String.Concat(" WHERE ide_1004_1 = ", _ide_1004_1);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@ide_1004", _ide_1004);
        Command.Parameters.AddWithValue("@cta_1004_1", _cta_1004_1);
        Command.Parameters.AddWithValue("@dip_1004_1", _dip_1004_1);
        Command.Parameters.AddWithValue("@uua_1004_1", _uua_1004_1);
        Command.Parameters.AddWithValue("@fua_1004_1", _fua_1004_1);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: ide_1004 cta_1004_1
    public void Cargar_ide_1004_cta_1004_1(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1004_1 WHERE ide_1004 = " + _ide_1004 + " AND cta_1004_1 = '" + _cta_1004_1 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1004_1 = (int)dr["ide_1004_1"];

            if (AsignarPropiedades)
            {
                ide_1004 = (int)dr["ide_1004"];
                cta_1004_1 = (string)dr["cta_1004_1"];
                uar_1004_1 = (int)dr["uar_1004_1"];
                far_1004_1 = dr["far_1004_1"].ToString();
                dip_1004_1 = (string)dr["dip_1004_1"];
                uua_1004_1 = (int)dr["uua_1004_1"];
                fua_1004_1 = dr["fua_1004_1"].ToString();
            }
        }
        else
        {
            ide_1004_1 = 0;
        }

        dr.Close();
    }

    #endregion

}