using System;
using System.Data.SqlClient;

public class Alchis_1000
{

    #region Atributos Conexión

    private SqlConnection _sqlCon;
    private string _BaseDatos;

    #endregion

    #region Atributos

    private int _ide_1000;                                          // 1 CLAVE_PRIMARIA IDENTIDAD  
    private Int16 _eje_1000;                                        // 2   CLAVE_UNICA 
    private Int16 _ide_5000;                                        // 3    
    private string _cor_1000;                                       // 4   CLAVE_UNICA 
    private string _den_1000;                                       // 5    
    private byte _sun_1000;                                         // 6    
    private int _uar_1000;                                          // 7    
    private string _far_1000;                                       // 8    
    private string _dip_1000;                                       // 9    
    private int _uua_1000;                                          // 10    
    private string _fua_1000;                                       // 11    

    #endregion

    #region Propiedades

    public int ide_1000
    {
        get { return _ide_1000; }
        set { _ide_1000 = value; }
    }
    public Int16 eje_1000
    {
        get { return _eje_1000; }
        set { _eje_1000 = value; }
    }
    public Int16 ide_5000
    {
        get { return _ide_5000; }
        set { _ide_5000 = value; }
    }
    public string cor_1000
    {
        get { return _cor_1000; }
        set { _cor_1000 = value; }
    }
    public string den_1000
    {
        get { return _den_1000; }
        set { _den_1000 = value; }
    }
    public byte sun_1000
    {
        get { return _sun_1000; }
        set { _sun_1000 = value; }
    }
    public int uar_1000
    {
        get { return _uar_1000; }
        set { _uar_1000 = value; }
    }
    public string far_1000
    {
        get { return _far_1000; }
        set { _far_1000 = value; }
    }
    public string dip_1000
    {
        get { return _dip_1000; }
        set { _dip_1000 = value; }
    }
    public int uua_1000
    {
        get { return _uua_1000; }
        set { _uua_1000 = value; }
    }
    public string fua_1000
    {
        get { return _fua_1000; }
        set { _fua_1000 = value; }
    }

    #endregion

    #region Constructores

    public Alchis_1000(SqlConnection sqlCon, string BaseDatos = null)
    {
        _sqlCon = sqlCon;
        _BaseDatos = BaseDatos ?? sqlCon.Database;

        _ide_1000 = 0;
        _eje_1000 = 0;
        _ide_5000 = 0;
        _cor_1000 = "";
        _den_1000 = "";
        _sun_1000 = 0;
        _uar_1000 = 0;
        _far_1000 = "";
        _dip_1000 = "";
        _uua_1000 = 0;
        _fua_1000 = "";
    }

    #endregion

    #region Metodos Públicos

    //METODO INSERTAR
    public void Insertar()
    {
        SqlCommand Command = new SqlCommand();

        Command.CommandText = "INSERT INTO " + _BaseDatos + ".dbo.alchis_1000 (" +
        "  eje_1000" + ", ide_5000" + ", cor_1000" + ", den_1000" + ", sun_1000" + ", uar_1000" + ", far_1000" + ", dip_1000" + ", uua_1000" + ", fua_1000" +
        ") OUTPUT INSERTED.ide_1000 VALUES (" +
        "  @eje_1000" + ", @ide_5000" + ", @cor_1000" + ", @den_1000" + ", @sun_1000" + ", @uar_1000" + ", @far_1000" + ", @dip_1000" + ", @uua_1000" + ", @fua_1000" +
         ")";

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1000", _eje_1000);
        Command.Parameters.AddWithValue("@ide_5000", _ide_5000);
        Command.Parameters.AddWithValue("@cor_1000", _cor_1000);
        Command.Parameters.AddWithValue("@den_1000", _den_1000);
        Command.Parameters.AddWithValue("@sun_1000", _sun_1000);
        Command.Parameters.AddWithValue("@uar_1000", _uar_1000);
        Command.Parameters.AddWithValue("@far_1000", _far_1000);
        Command.Parameters.AddWithValue("@dip_1000", _dip_1000);
        Command.Parameters.AddWithValue("@uua_1000", _uua_1000);
        Command.Parameters.AddWithValue("@fua_1000", _fua_1000);
        Command.Connection = _sqlCon;

        _ide_1000 = (int)Command.ExecuteScalar();
    }

    //METODO CARGAR
    public void Cargar(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1000 WHERE ide_1000 = " + _ide_1000;
        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1000 = (int)dr["ide_1000"];

            if (AsignarPropiedades)
            {
                eje_1000 = (Int16)dr["eje_1000"];
                ide_5000 = (Int16)dr["ide_5000"];
                cor_1000 = (string)dr["cor_1000"];
                den_1000 = (string)dr["den_1000"];
                sun_1000 = Convert.ToByte(dr["sun_1000"]);
                uar_1000 = (int)dr["uar_1000"];
                far_1000 = dr["far_1000"].ToString();
                dip_1000 = (string)dr["dip_1000"];
                uua_1000 = (int)dr["uua_1000"];
                fua_1000 = dr["fua_1000"].ToString();
            }
        }
        else
        {
            ide_1000 = 0;
        }

        dr.Close();
    }

    //METODO ELIMINAR
    public void Eliminar()
    {
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;
        Command.CommandText = "DELETE FROM " + _BaseDatos + ".dbo.alchis_1000 WHERE ide_1000 = " + _ide_1000;

        Command.ExecuteNonQuery();
    }

    //METODO QUE ACTUALIZAR
    public void Actualizar()
    {
        String strSQL = "";
        SqlCommand Command = new SqlCommand();

        Command.Connection = _sqlCon;

        strSQL = String.Concat(strSQL, "UPDATE " + _BaseDatos + ".dbo.alchis_1000 SET");
        strSQL += String.Concat(" eje_1000 = @eje_1000");
        strSQL += String.Concat(", ide_5000 = @ide_5000");
        strSQL += String.Concat(", cor_1000 = @cor_1000");
        strSQL += String.Concat(", den_1000 = @den_1000");
        strSQL += String.Concat(", sun_1000 = @sun_1000");
        strSQL += String.Concat(", dip_1000 = @dip_1000");
        strSQL += String.Concat(", uua_1000 = @uua_1000");
        strSQL += String.Concat(", fua_1000 = @fua_1000");
        strSQL += String.Concat(" WHERE ide_1000 = ", _ide_1000);

        // ASIGNACION DE PARÁMETROS
        Command.Parameters.AddWithValue("@eje_1000", _eje_1000);
        Command.Parameters.AddWithValue("@ide_5000", _ide_5000);
        Command.Parameters.AddWithValue("@cor_1000", _cor_1000);
        Command.Parameters.AddWithValue("@den_1000", _den_1000);
        Command.Parameters.AddWithValue("@sun_1000", _sun_1000);
        Command.Parameters.AddWithValue("@dip_1000", _dip_1000);
        Command.Parameters.AddWithValue("@uua_1000", _uua_1000);
        Command.Parameters.AddWithValue("@fua_1000", _fua_1000);
        Command.CommandText = strSQL;

        Command.ExecuteNonQuery();
    }

    //METODO CARGAR CLAVE UNICA: eje_1000 cor_1000
    public void Cargar_eje_1000_cor_1000(bool AsignarPropiedades = true)
    {
        SqlCommand Command = new SqlCommand();
        SqlDataReader dr;
        Command.Connection = _sqlCon;

        Command.CommandText = "SELECT * FROM " + _BaseDatos + ".dbo.alchis_1000 WHERE eje_1000 = " + _eje_1000 + " AND cor_1000 = '" + _cor_1000 + "'";

        dr = Command.ExecuteReader();

        if (dr.Read())
        {
            ide_1000 = (int)dr["ide_1000"];

            if (AsignarPropiedades)
            {
                eje_1000 = (Int16)dr["eje_1000"];
                ide_5000 = (Int16)dr["ide_5000"];
                cor_1000 = (string)dr["cor_1000"];
                den_1000 = (string)dr["den_1000"];
                sun_1000 = Convert.ToByte(dr["sun_1000"]);
                uar_1000 = (int)dr["uar_1000"];
                far_1000 = dr["far_1000"].ToString();
                dip_1000 = (string)dr["dip_1000"];
                uua_1000 = (int)dr["uua_1000"];
                fua_1000 = dr["fua_1000"].ToString();
            }
        }
        else
        {
            ide_1000 = 0;
        }

        dr.Close();
    }

    #endregion

}